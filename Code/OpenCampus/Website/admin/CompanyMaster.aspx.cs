﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using BusinessLayer;
using System.IO;

public partial class admin_CompanyMaster : Utility // System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!Page.IsPostBack)
        {
            if (Request.QueryString["result"] != null as string)
            {
                string _result = Request.QueryString["result"].ToString();
                if (_result == "success")
                {
                    ShowMessage("Record Saved Successfully", this.Page);
                }
                else if (_result == "error")
                {
                    ShowMessage("Record Not Saved, Try Again", this.Page);
                }
            }
            if (Request.QueryString["Id"] != null as string)
            {
                hfPkey.Value = DecryptString(Request.QueryString["Id"].ToString());
                LoadComapnyDetail(int.Parse(hfPkey.Value));
            }
        }
    }

    private void LoadComapnyDetail(int _companyId)
    {
        JobPostCompanyBL obj = new JobPostCompanyBL();
        List<DataLayer.JobPostCompany> C = obj.LoadJobPostCompanyByPrimaryKey(_companyId);
        TxtCompanyName.Text = C[0].CompanyName;
        TxtAddress.Text = C[0].CompanyAddress;
        hfImagePath.Value = C[0].CompanyLogo;
        ImgLogo.Visible = true;
        ImgLogo.ImageUrl = C[0].CompanyLogo;

        TxtWebsiteUrl.Text = C[0].CompanyWebsite;
    }

    protected void BtnSave_Click(object sender, EventArgs e)
    {
        JobPostCompanyBL obj = new JobPostCompanyBL();
        SaveImages();
        int? _result = null;
        if (hfPkey.Value == "")
        {
          _result = obj.InsertJobPostCompany(TxtCompanyName.Text, TxtAddress.Text, hfImagePath.Value, TxtWebsiteUrl.Text, DateTime.Now, "", "", "", "", "", "", "", "", "", "", "", "");
        }
        else
        {
            _result = obj.UpdateJobPostCompany(int.Parse(hfPkey.Value), TxtCompanyName.Text, TxtAddress.Text, hfImagePath.Value, TxtWebsiteUrl.Text, DateTime.Now, "", "", "", "", "", "", "", "", "", "", "", "");
        }
        if (_result > 0)
        {
            Response.Redirect("CompanyMaster.aspx?result=success", false);
        }
        else
        {
            Response.Redirect("CompanyMaster.aspx?result=error", false);
        }

    }

    private void SaveImages()
    {
        if (FULogo.HasFile)
        {
            string FileName = Path.GetFileName(FULogo.PostedFile.FileName);

            FileName = CheckFileNameExist("ReadWrite/CompanyLogo", FileName);
            FULogo.SaveAs(Server.MapPath("~//ReadWrite//CompanyLogo//" + FileName));

            if (hfImagePath.Value != "")
            {
                DeleteFileFromFolder(hfImagePath.Value);
            }
            hfImagePath.Value = "~/ReadWrite/CompanyLogo/" + FileName;
        }
    }
}