﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data.Objects;
using DataLayer;

namespace BusinessLayer
{
    public class AdminLoginBL
    {
        public int InsertAdminLogin(string FullName, string Email, string Hash, string SaltKey, int UserTypeId, DateTime CreatedOn)
        {
            ObjectParameter AdminLoginId = new ObjectParameter("AdminLoginId", typeof(Int32));
            DBEntities Obj = new DBEntities();

            Obj.InsertAdminLogin(AdminLoginId, FullName, Email, Hash, SaltKey, UserTypeId, CreatedOn);
            return int.Parse(AdminLoginId.Value.ToString());
        }

        public int UpdateAdminLogin(int AdminLoginId, string FullName, string Email, string Hash, string SaltKey, int UserTypeId, DateTime CreatedOn)
        {
            DBEntities Obj = new DBEntities();
            Obj.UpdateAdminLogin(AdminLoginId, FullName, Email, Hash, SaltKey, UserTypeId, CreatedOn);
            return AdminLoginId;
        }
        public List<AdminLogin> LoadAdminLoginByPrimaryKey(int AdminLoginId)
        {
            DBEntities Obj = new DBEntities();
            return Obj.LoadAdminLoginByPrimaryKey(AdminLoginId).ToList();
        }

        public List<AdminLogin> LoadAllAdminLogin()
        {
            DBEntities Obj = new DBEntities();
            return Obj.LoadAllAdminLogin().ToList();
        }

        public List<AdminLogin> LoadAdminLoginByEmail(string Email)
        {
            DBEntities Obj = new DBEntities();
            return Obj.LoadAdminLoginByEmail(Email).ToList();
        }
    }
}
