﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.master" AutoEventWireup="true" CodeFile="employee-login.aspx.cs" Inherits="EmployeeLogin" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
<style type="text/css">
.page-banner
{
        margin-top: -60px;
}
hr 
{
    border:1px;
    border-style:dashed;
}
</style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="cph" Runat="Server">


<div class="main-content container-boxed max offset">

					<div class="page-banner">
                    <div class="col-md-8">
                        <img alt="" src="images/logo/opencampus_employer.png" /></div>
                        <div class="col-md-4">
                       <h6>Open-Campus Helpline</h6> 
<p style="font-size:12px; color:Black;">Email: info@opencampus.in</p>
                        </div>
                       
                    </div>
                 
                 <hr />
					<div class="row">
						<div class="noo1-main col-md-7 post-area standard-blog">
							<article class="hentry">
								<header class="content-header">
									<h2 class="content-title">
										<a>Make your Campus Visible to 30000+ Recruiters When They Hire</a>
									</h2>
																	</header>
									
							
								<div>
									<ul class="list-1">
			<li> Visibility and Branding through links, logos and micro-sites.</li>
			<li> Exclusive Campus Drives from India’s Top Companies.</li>
			<li>Online Placement Brochure with Student and Campus Profiles.</li>
			<li> Fresher Jobs and Internships that Match Student Qualification</li>
			<li>Suite of Online Placement Prep. Tests</li>
		</ul>
									<p>Not a Member Yet? <a href="employer-registration.aspx" class="btn btn-primary">Register Now</a></p>
								</div>
								
							</article>  
     
						</div>
						<div class="noo-sidebar col-md-4">
							<div class="noo-sidebar-wrap">
								<div class="widget widget_search">
									<h4>Recruiter Login</h4>
									<form class="form-horizontal">
										<div class="form-group row">
											<label class="col-sm-3 control-label">Username</label>
											<div class="col-sm-9">
												<input id="empEmail" runat="server" type="text" class="log form-control" required placeholder="Username">
											</div>
										</div>
										<div class="form-group row">
											<label class="col-sm-3 control-label">Password</label>
											<div class="col-sm-9">
												<input id="empPassword" runat="server" type="password" class="pwd form-control" required value="" placeholder="Password">
											</div>
										</div>
										
										<div class="form-group text-center">
                                            <asp:Button ID="BtnEmpLogin" runat="server" Text="Login" 
                                                CssClass="btn btn-primary" onclick="BtnEmpLogin_Click" />
											<div class="login-form-links">
												<span><a target="_blank" href="campus/Login.aspx?action=forgotpassword"><i class="fa fa-question-circle"></i> Forgot Password?</a></span>
												
											</div>
										</div>
									</form>
								</div>
								
								
								
							</div>
						</div>
					</div> 
				</div>
</asp:Content>

