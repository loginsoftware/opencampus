﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.master" AutoEventWireup="true" CodeFile="Login-with-google.aspx.cs" Inherits="Login_with_linkedin" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="cph" Runat="Server">
   <asp:Button ID="btnLogin" Text="Login with Google" runat="server" OnClick="Authorize" />
<asp:Panel ID="pnlProfile" runat="server" Visible="false">
<hr />
<table>
    <tr>
        <td rowspan="6" valign="top">
            <asp:Image ID="ProfileImage" runat="server" Width="50" Height="50" />
        </td>
    </tr>
    <tr>
        <td>
            ID:
            <asp:Label ID="lblId" runat="server" Text=""></asp:Label>
        </td>
    </tr>
    <tr>
        <td>
            Name:
            <asp:Label ID="lblName" runat="server" Text=""></asp:Label>
        </td>
    </tr>
    <tr>
        <td>
            Email:
            <asp:Label ID="lblEmail" runat="server" Text=""></asp:Label>
        </td>
    </tr>
    <tr>
        <td>
            Gender:
            <asp:Label ID="lblGender" runat="server" Text=""></asp:Label>
        </td>
    </tr>
    <tr>
        <td>
            Type:
            <asp:Label ID="lblType" runat="server" Text=""></asp:Label>
        </td>
    </tr>
        <tr>
        <td>
            <asp:Button ID="Button1" Text="Clear" runat="server" OnClick = "Clear" />
        </td>
    </tr>
</table>
</asp:Panel>
</asp:Content>

