﻿<%@ Page Title="" Language="C#" MasterPageFile="~/admin/Admin.master" AutoEventWireup="true" CodeFile="EmployerDetail.aspx.cs" Inherits="admin_EmployerDetail" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="cph" Runat="Server">
      <div class="page-header">
							<h1>
								Dashboard
								<small>
									<i class="ace-icon fa fa-angle-double-right"></i>
									Recruiter's
								</small>
							</h1>
						</div><!-- /.page-header -->

    <div class="row">
        <div class="col-sm-12">
            <div class="widget-box">
											<div class="widget-header">
												<h4 class="widget-title">Filter Recruiter's</h4>
											</div>

											<div class="widget-body">
												<div class="widget-main">
													<div class="form-inline">
                                                       <div class="col-sm-3">
                                                        <label>Recruiter Name</label>
                                                           <div>
                                                             <asp:TextBox ID="TxtRecruiterName" runat="server" Width="100%"></asp:TextBox>
                                                               </div>
                                                       </div>
                                                         <div class="col-sm-3">
                                                        <label>From Date</label>
                                                             <div>
                                                             <asp:TextBox ID="TxtFromDate" runat="server" Width="100%"></asp:TextBox>

                                                                 </div>
                                                       </div>
                                                         <div class="col-sm-3">
                                                        <label>To Date</label>
                                                           <div> <asp:TextBox ID="TxtToDate" runat="server" Width="100%"></asp:TextBox></div>
                                                            
                                                       </div>
                                                      
                                                         <div class="col-sm-3">
                                                             <br />
                     <asp:LinkButton ID="BtnSearch" class="btn btn-info btn-sm" runat="server" OnClick="BtnSearch_Click"><i class="ace-icon fa fa-search bigger-110"></i> Search</asp:LinkButton>
                                                       </div>												
														 <div class="clearfix"></div>
													</div>
												</div>
											</div>
										</div>
        </div>
    </div>
    
						<div class="row">
							<div class="col-xs-12">
								<!-- PAGE CONTENT BEGINS -->

                                <div class="row">
									<div class="col-sm-12">
										<div class="widget-box">
											<div class="widget-header">
												<h4 class="widget-title lighter smaller">
													<i class="ace-icon fa fa-users blue"></i>
													Recruiter's Detail
												</h4>
											</div>
									<div style="overflow-x:scroll;">
       <asp:GridView ID="GVEmployer" runat="server" AutoGenerateColumns="false" CssClass="table  table-bordered table-hover">
           <Columns>
                <asp:TemplateField HeaderText="Sr No">
                   <ItemTemplate>
                      <%# Container.DataItemIndex + 1 %>
                   </ItemTemplate>
               </asp:TemplateField>
               <asp:BoundField DataField="CompanyName" HeaderText="Company" ItemStyle-Wrap="false" />
               <asp:BoundField DataField="ContactPersone" HeaderText="ContactPersone" ItemStyle-Wrap="false" />
               <asp:BoundField DataField="UserNameEmail" HeaderText="Email" />
               <asp:BoundField DataField="MobileNo" HeaderText="Emp Mobile" />
               <asp:TemplateField HeaderText="Date" ItemStyle-Wrap="false">
                   <ItemTemplate>
                       <asp:Label ID="lblRegisteredDate" ToolTip='<%# Eval("CreatedOn") %>' runat="server" Text='<%# Eval("CreatedOn", "{0:dd-MMM-yyyy}") %>'></asp:Label>
                   </ItemTemplate>
               </asp:TemplateField>
               <asp:BoundField DataField="Name" HeaderText="City" />
                <asp:TemplateField HeaderText="Address">
                   <ItemTemplate>
                       <%# Eval("Address1") %> - <%# Eval("Address2") %>
                   </ItemTemplate>
               </asp:TemplateField>
                 <asp:TemplateField HeaderText="Photo">
                   <ItemTemplate>
                       <asp:Label ID="lblImagePath" ToolTip='<%# Eval("ImagePath") %>' runat="server" Text='<%# Eval("ImagePath") == "" ? "No" : "Yes" %>' CssClass='<%# Eval("ImagePath").ToString() == "" ? "label label-danger" : "label label-success" %>'></asp:Label>
                   </ItemTemplate>
               </asp:TemplateField>
             
           </Columns>

       </asp:GridView>
						</div>

                                            </div>
									</div><!-- /.col -->

								</div><!-- /.row -->

                                	<!-- PAGE CONTENT ENDS -->
							</div><!-- /.col -->
						</div><!-- /.row -->
</asp:Content>

