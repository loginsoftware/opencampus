$(document).ready(function(){
	$("[data-toggle=popover]").popover({
		html: true, 
		content: function() {
			  return $('#popover-content').html();
			}
	});
	$("a[data-activity='0']").click(function() {		
		var data_activity = $(this).attr('data-activity-title');
		var anchorEle = this;
		if(parseInt($(this).attr('data-job')) > 0) {
			var Source = 'list';
			var JobId = $(this).attr('data-job');	
			$(this).html('<button type="button" class="btn pull-left">Please Wait...</button>');
		}else {
			var Source = 'detail';
			var JobId = $('input#JobId').val();	
			$(this).html('Please Wait...');
		}
		
		$.ajax({
			type: "POST",
			url: "process/job-process.php",
			data: "JobId=" + JobId+"&activityType="+data_activity
		}).done(function(data) {
			var status = data.status;
			if(status == 0) {
				$(anchorEle).html('<button class="btn pull-left border-r" type="button">'+data_activity+'</button>');	
				$('.alert').remove();
				$('body').before('<div class="alert alert-warning alert-dismissable alert-dismissible">'+data.msg+'</div>');				
				$(".alert-dismissable").fadeTo(2000, 500).slideUp(500, function(){
					$(".alert-dismissable").alert('close');
				});
			}else
			{
				if(Source == 'detail') {
					$(anchorEle).remove();
					$('a[data-activity-title="'+data_activity+'"]').remove();
				}else {
					if(data_activity == 'Apply') {
						$(anchorEle).parent().closest('div.share-group').addClass('more').removeClass('apply');	
						$(anchorEle).html('<button type="button" class="btn pull-left">Applied</button>');
					}else {
						$(anchorEle).remove();
					}
				}
				$('.alert').remove();
				$('body').before('<div class="alert alert-info alert-dismissable alert-dismissible">'+data.msg+'</div>');				
				$(".alert-dismissable").fadeTo(2000, 500).slideUp(500, function(){
					$(".alert-dismissable").alert('close');
				});
			}
		}).fail(function() {
			alert('Error: Please try again!');
		});
	});
});	