﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using BusinessLayer;

public partial class admin_CompanyMasterDetail : Utility // System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!Page.IsPostBack)
        {
            LoadCandidateDetail();
        }
    }

    private void LoadCandidateDetail()
    {
        try
        {
            JobPostCompanyBL obj = new JobPostCompanyBL();
            GVCompany.DataSource = obj.LoadJobPostCompanyByComapnyName(TxtCompanyName.Text);
            GVCompany.DataBind();
        }
        catch { }
    }
    protected void BtnSearch_Click(object sender, EventArgs e)
    {
        LoadCandidateDetail();
    }

    protected void LnkBtnEdit_Click(object sender, EventArgs e)
    {
        LinkButton LnkBtn = (LinkButton)sender;
        GridViewRow gvRow = (GridViewRow)LnkBtn.NamingContainer;
        string Pkey = GVCompany.DataKeys[gvRow.RowIndex].Values[0].ToString();
        Response.Redirect("CompanyMaster.aspx?Id=" + EncryptString(Pkey));
    }
}