﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data.Objects;
using DataLayer;

namespace BusinessLayer
{
    public class CandidateEducationBL
    {
        public int InsertCandidateEducation(int CandidateId, string EducationName, string EducationType, string Specialization, string UniversityInstitute, string PassingYear, string Flag)
        {
            ObjectParameter CandidateEducationId = new ObjectParameter("CandidateEducationId", typeof(Int32));
            DBEntities Obj = new DBEntities();

            Obj.InsertCandidateEducation(CandidateEducationId, CandidateId, EducationName, EducationType, Specialization, UniversityInstitute, PassingYear, Flag);
            return int.Parse(CandidateEducationId.Value.ToString());
        }

        public int UpdateCandidateEducation(int CandidateEducationId, int CandidateId, string EducationName, string EducationType, string Specialization, string UniversityInstitute, string PassingYear, string Flag)
        {
            DBEntities Obj = new DBEntities();
            Obj.UpdateCandidateEducation(CandidateEducationId, CandidateId, EducationName, EducationType, Specialization, UniversityInstitute, PassingYear, Flag);
            return CandidateEducationId;
        }

        public int DeleteCandidateEducation(int CandidateEducationId)
        {
            DBEntities Obj = new DBEntities();
            Obj.DeleteCandidateEducation(CandidateEducationId);
            return CandidateEducationId;
        }

        public List<CandidateEducation> LoadCandidateEducationByPrimaryKey(int CandidateEducationId)
        {
            DBEntities Obj = new DBEntities();
            return Obj.LoadCandidateEducationByPrimaryKey(CandidateEducationId).ToList();
        }

        public List<CandidateEducation> LoadAllCandidateEducation()
        {
            DBEntities Obj = new DBEntities();
            return Obj.LoadAllCandidateEducation().ToList();
        }

        public List<CandidateEducation> LoadCandidateEducationByCandidateId(int CandidateId)
        {
            DBEntities Obj = new DBEntities();
            return Obj.LoadCandidateEducationByCandidateId(CandidateId).ToList();
        }
    }
}
