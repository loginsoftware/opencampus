﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data.Objects;
using DataLayer;

namespace BusinessLayer
{
    public class SectorBL
    {
        public List<Sector> LoadSectorBySectorType(string SectorType)
        {
            DBEntities Obj = new DBEntities();
            return Obj.LoadSectorBySectorType(SectorType).ToList();
        }
    }
}
