




; (function ($) {
	'use strict';
	var DH = {
		init: function(){
			
			//User Login and register account.
			this.userInit();
		},
		userInit: function(){
			
			$(document).on('click','[data-rel=registerModal]',function(e){
				e.stopPropagation();
				e.preventDefault();
				if($('#userloginModal').length){
					$('#userloginModal').modal('hide');
				}
				if($('#userlostpasswordModal').length){
					$('#userlostpasswordModal').modal('hide');
				}
				if($('#userregisterModal').length){
					$('#userregisterModal').modal('show');
				}
			});
			$(document).on('click','[data-rel=loginModal]',function(e){
				e.stopPropagation();
				e.preventDefault();
				if($('#userregisterModal').length){
					$('#userregisterModal').modal('hide');
				}
				if($('#userlostpasswordModal').length){
					$('#userlostpasswordModal').modal('hide');
				}
				if($('#userloginModal').length){
					$('#userloginModal').modal('show');
				}
			});
		},
	};
	$(document).ready(function(){
		DH.init();
	});

	$(document).ready(function () {
	    $('#txtemailregistration').change(function () {
	        if (!isEmail($("#txtemailregistration").val())) {
	            alert("Invalid Email address !");
	            $("#txtemailregistration").val("").focus();
	            return;
	        }	       
	    })
	    $('#txtpasswordregistration').keyup(function () {
	        $('#passwordstrengthresult').html(checkStrength($('#txtpasswordregistration').val()))
	    })

	    $('#txtconfirmpasswordregistration').change(function () {
	        if ($("#txtpasswordregistration").val() != $("#txtconfirmpasswordregistration").val()) {
	            alert("Confirm password not matched !");
	            $("#txtconfirmpasswordregistration").val("").focus();
	            return;
	        }
	    })

	    function isEmail(email) {
	        var regex = /^([a-zA-Z0-9_.+-])+\@(([a-zA-Z0-9-])+\.)+([a-zA-Z0-9]{2,4})+$/;
	        return regex.test(email);
	    }

	    function checkStrength(password) {
	        var strength = 0
	        if (password.length < 6) {
	            $('#passwordstrengthresult').removeClass()
	            $('#passwordstrengthresult').addClass('short')
	            return 'Too short'
	        }
	        if (password.length > 7) strength += 1
	        // If password contains both lower and uppercase characters, increase strength value.
	        if (password.match(/([a-z].*[A-Z])|([A-Z].*[a-z])/)) strength += 1
	        // If it has numbers and characters, increase strength value.
	        if (password.match(/([a-zA-Z])/) && password.match(/([0-9])/)) strength += 1
	        // If it has one special character, increase strength value.
	        if (password.match(/([!,%,&,@,#,$,^,*,?,_,~])/)) strength += 1
	        // If it has two special characters, increase strength value.
	        if (password.match(/(.*[!,%,&,@,#,$,^,*,?,_,~].*[!,%,&,@,#,$,^,*,?,_,~])/)) strength += 1
	        // Calculated strength value, we can return messages
	        // If value is less than 2
	        if (strength < 2) {
	            $('#passwordstrengthresult').removeClass()
	            $('#passwordstrengthresult').addClass('weak')
	            return 'Weak'
	        } else if (strength == 2) {
	            $('#passwordstrengthresult').removeClass()
	            $('#passwordstrengthresult').addClass('good')
	            return 'Good'
	        } else {
	            $('#passwordstrengthresult').removeClass()
	            $('#passwordstrengthresult').addClass('strong')
	            return 'Strong'
	        }
	    }


	});

})(jQuery);


