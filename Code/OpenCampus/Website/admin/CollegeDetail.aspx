﻿<%@ Page Title="" Language="C#" MasterPageFile="~/admin/Admin.master" AutoEventWireup="true" CodeFile="CollegeDetail.aspx.cs" Inherits="admin_CollegeDetail" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="cph" Runat="Server">
      <div class="page-header">
							<h1>
								Dashboard
								<small>
									<i class="ace-icon fa fa-angle-double-right"></i>
								Colleges
								</small>
							</h1>
						</div><!-- /.page-header -->

    <div class="row">
        <div class="col-sm-12">
            <div class="widget-box">
											<div class="widget-header">
												<h4 class="widget-title">Filter Colleges</h4>
											</div>

											<div class="widget-body">
												<div class="widget-main">
													<div class="form-inline">
                                                       <div class="col-sm-3">
                                                        <label>College Name</label>
                                                           <div>
                                                             <asp:TextBox ID="TxtCollegeName" runat="server" Width="100%"></asp:TextBox>
                                                               </div>
                                                       </div>
                                                         <div class="col-sm-3">
                                                        <label>From Date</label>
                                                             <div>
                                                             <asp:TextBox ID="TxtFromDate" runat="server" Width="100%"></asp:TextBox>

                                                                 </div>
                                                       </div>
                                                         <div class="col-sm-3">
                                                        <label>To Date</label>
                                                           <div> <asp:TextBox ID="TxtToDate" runat="server" Width="100%"></asp:TextBox></div>
                                                            
                                                       </div>
                                                      
                                                         <div class="col-sm-3">
                                                             <br />
                     <asp:LinkButton ID="BtnSearch" class="btn btn-info btn-sm" runat="server" OnClick="BtnSearch_Click"><i class="ace-icon fa fa-search bigger-110"></i> Search</asp:LinkButton>
                                                       </div>												
														 <div class="clearfix"></div>
													</div>
												</div>
											</div>
										</div>
        </div>
    </div>
    
						<div class="row">
							<div class="col-xs-12">
								<!-- PAGE CONTENT BEGINS -->

                                <div class="row">
									<div class="col-sm-12">
										<div class="widget-box">
											<div class="widget-header">
												<h4 class="widget-title lighter smaller">
													<i class="ace-icon fa fa-users blue"></i>
													Colleges Detail
												</h4>
											</div>
									<div style="overflow-x:scroll;">
       <asp:GridView ID="GVCollege" runat="server" AutoGenerateColumns="false" CssClass="table  table-bordered table-hover">
           <Columns>
                <asp:TemplateField HeaderText="Sr No">
                   <ItemTemplate>
                      <%# Container.DataItemIndex + 1 %>
                   </ItemTemplate>
               </asp:TemplateField>
              <asp:BoundField DataField="CollegeName" HeaderText="College" ItemStyle-Wrap="false" />
               <asp:BoundField DataField="CityName" HeaderText="City" ItemStyle-Wrap="false" />
               <asp:BoundField DataField="UserNameEmail" HeaderText="Email" />
               <asp:BoundField DataField="PhoneNumber" HeaderText="Phone" />
               <asp:BoundField DataField="PlacementCoordinator" HeaderText="PlacementCoordinator" />
               <asp:BoundField DataField="PlacementPhoneNo" HeaderText="PlacementPhoneNo" />
               <asp:TemplateField HeaderText="Date" ItemStyle-Wrap="false">
                   <ItemTemplate>
                       <asp:Label ID="lblRegisteredDate" ToolTip='<%# Eval("CreatedOn") %>' runat="server" Text='<%# Eval("CreatedOn", "{0:dd-MMM-yyyy}") %>'></asp:Label>
                   </ItemTemplate>
               </asp:TemplateField>
               <asp:BoundField DataField="UniversityName" HeaderText="University" />
               <asp:BoundField DataField="CollegeWebsite" HeaderText="Website" />
                 <asp:TemplateField HeaderText="Logo">
                   <ItemTemplate>
                       <asp:Label ID="lblUploadLogoImagePath" ToolTip='<%# Eval("UploadLogoImagePath") %>' runat="server" Text='<%# Eval("UploadLogoImagePath") == "" ? "No" : "Yes" %>' CssClass='<%# Eval("UploadLogoImagePath").ToString() == "" ? "label label-danger" : "label label-success" %>'></asp:Label>
                   </ItemTemplate>
               </asp:TemplateField>
              <asp:TemplateField HeaderText="Cover">
                   <ItemTemplate>
                       <asp:Label ID="lblUploadPictureImagePath" ToolTip='<%# Eval("UploadPictureImagePath") %>' runat="server" Text='<%# Eval("UploadPictureImagePath") == "" ? "No" : "Yes" %>' CssClass='<%# Eval("UploadPictureImagePath").ToString() == "" ? "label label-danger" : "label label-success" %>'></asp:Label>
                   </ItemTemplate>
               </asp:TemplateField>
           </Columns>

       </asp:GridView>
						</div>

                                            </div>
									</div><!-- /.col -->

								</div><!-- /.row -->

                                	<!-- PAGE CONTENT ENDS -->
							</div><!-- /.col -->
						</div><!-- /.row -->
</asp:Content>

