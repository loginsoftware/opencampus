﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data.Objects;
using DataLayer;

namespace BusinessLayer
{
    public class JobApplicationBL
    {
        public int InsertJobApplication(int JobId, int CandidateId, DateTime CreatedOn, string Status)
        {
            ObjectParameter JobApplicationId = new ObjectParameter("JobApplicationId", typeof(Int32));
            DBEntities Obj = new DBEntities();

            Obj.InsertJobApplication(JobApplicationId, JobId, CandidateId, CreatedOn, Status);
            return int.Parse(JobApplicationId.Value.ToString());
        }

        public int UpdateJobApplication(int JobApplicationId, int JobId, int CandidateId, DateTime CreatedOn, string Status)
        {
            DBEntities Obj = new DBEntities();
            Obj.UpdateJobApplication(JobApplicationId, JobId, CandidateId, CreatedOn, Status);
            return JobApplicationId;
        }

        public int DeleteJobApplication(int JobApplicationId)
        {
            DBEntities Obj = new DBEntities();
            Obj.DeleteJobApplication(JobApplicationId);
            return JobApplicationId;
        }

        public List<JobApplication> LoadJobApplicationByPrimaryKey(int JobApplicationId)
        {
            DBEntities Obj = new DBEntities();
            return Obj.LoadJobApplicationByPrimaryKey(JobApplicationId).ToList();
        }

        public List<JobApplication> LoadAllJobApplication()
        {
            DBEntities Obj = new DBEntities();
            return Obj.LoadAllJobApplication().ToList();
        }
    }
}
