﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using ASPSnippets.LinkedInAPI;
using System.Data;

public partial class Login_with_linkedin : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!Page.IsPostBack)
        {
            LinkedInConnect.APIKey = "813bqmuosrdl1s";
            LinkedInConnect.APISecret = "oP3BHerp9UNLNgml";
            LinkedInConnect.RedirectUrl = Request.Url.AbsoluteUri.Split('?')[0];
            if (LinkedInConnect.IsAuthorized)
            {
                pnlDetails.Visible = true;
                DataSet ds = LinkedInConnect.Fetch();
                imgPicture.ImageUrl = ds.Tables["person"].Rows[0]["picture-url"].ToString();
                lblName.Text = ds.Tables["person"].Rows[0]["first-name"].ToString();
                lblName.Text += " " + ds.Tables["person"].Rows[0]["last-name"].ToString();
                lblEmailAddress.Text = ds.Tables["person"].Rows[0]["email-address"].ToString();
                lblHeadline.Text = ds.Tables["person"].Rows[0]["headline"].ToString();
                lblIndustry.Text = ds.Tables["person"].Rows[0]["industry"].ToString();
                lblLinkedInId.Text = ds.Tables["person"].Rows[0]["id"].ToString();
                lblLocation.Text = ds.Tables["location"].Rows[0]["name"].ToString();
                imgPicture.ImageUrl = ds.Tables["person"].Rows[0]["picture-url"].ToString();
            }
        }
    }
    protected void Authorize(object sender, EventArgs e)
    {
        LinkedInConnect.Authorize();
    }
}