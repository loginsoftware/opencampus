﻿<%@ Page Title="About Us | Opencampus" Language="C#" MasterPageFile="~/Site.master" AutoEventWireup="true" CodeFile="aboutus.aspx.cs" Inherits="About_Us" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
 <title>Open Campus India | Job opportunities in India | jobs for freshers</title>

   <meta name="robots" content="INDEX,FOLLOW" />

   <meta  property="og:description" name="description" content="Apply for the Job vacancies in Government sector, IT, Software, Technical, Banking & Finance, BPO Jobs. Post your resume to apply for job vacancies across top companies in India - Stay connected with opencampus.in."/>

   <meta name="keywords" content="part time jobs, job search, jobs in bangalore, jobs in mumbai, job vacancies, fresher jobs, mechanical engineering jobs, jobs in pune, civil engineering jobs"/>


   <meta name="application-name" content="opencampus.in - Your job search starts and ends with us."/>

   <meta name="copyright" content="2017 opencampus.in" />

   <meta name="content-language" content="EN" />

   <meta name="author" content="www.opencampus.in" />

   <meta name="distribution" content="GLOBAL" />


   <meta name="robots" content="ALL" />

   <meta name="pragma" content="no-cache" />

   <meta name="revisit-after" content="1 day" />

   <meta name="classification" content="Freshers jobs, Software Jobs, IT Jobs, Technical Jobs, Govt Jobs" />

   <meta property="og:image" content="https://opencampus.in/images/logo-opencampus.png"/>

   <link rel="image_src" href="https://opencampus.in/images/logo-opencampus.png" />

   <link href="https://plus.google.com/+opencampus1" rel="publisher" />


	<meta property="og:type" content="website" />

	<meta property="og:url" content="https://opencampus.in" />  

	<meta property="og:title" content="Open Campus" /> 

	<meta property="og:description" content="Open Campus, is India's leading online career and recruitment resource with its cutting edge technology provides relevant profiles to employers"/> 


	<meta property="og:image" content="https://opencampus.in/images/logo-opencampus.png" /> 

	<meta property="og:site_name" content="opencampus.in"/>

<meta name="twitter:card" content="summary">
<meta name="twitter:url" content="https://www.opencampus.in">
<meta name="twitter:site" content="@opencampusdot">
<meta name="twitter:title" content="Open Campus India | Job opportunities in India | jobs for freshers">
<meta name="twitter:description" content="Apply for the Job vacancies in Government sector, IT, Software, Technical, Banking & Finance, BPO Jobs. Post your resume to apply for job vacancies across top companies in India - Stay connected with opencampus.in.">
<meta name="twitter:image" content="https://pbs.twimg.com/profile_images/872485092932833280/umLVkRsm_400x400.jpg">

<link rel="canonical" href="https://www.opencampus.in/aboutus.aspx" />

    <style>
	
	.heading
	{    font-size: 18px;
	}
	.small-heading{
	font-size: 15px;
    font-weight: 100;
	}
	.para{
	font-size: 15px;
	}
	</style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="cph" Runat="Server">
    <div class="noo-page-heading">
				<div class="container-boxed max text-center parallax-content">
					<div class="member-heading-avatar">
						
					</div>
					<div class="page-heading-info ">
						<h1 class="page-title">About Us</h1>
					</div>
					<div class="page-sub-heading-info">
						We’re <span style="color:
					#80bb03;font-size:19px;">Helping People </span>Find Great Jobs,
and <span style="color:
					#80bb03;font-size:19px;">Helping Employers</span> Build Great Companies
					</div>
				</div> 
				<div class="parallax heading" data-parallax="1" data-parallax_no_mobile="1" data-velocity="0.1"></div>
			</div>
			<div class="member-heading">
				<div class="container-boxed max">
					<div class="member-heading-nav">
						<ul>
							<li><a href="advertise-with-us.aspx"><i class="fa fa-file-text-o"></i> Advertise With Us</a></li>
					<li class="active"><a href="aboutus.aspx"><i class="fa fa-newspaper-o"></i> About Us</a></li>
							<%--<li><a href="employer-manage-job.html"><i class="fa fa-file-text-o"></i> Hire Us</a></li>--%>
							<li><a href="contactus.aspx"><i class="fa fa-users"></i> Contact Us</a></li>
							<li><a href="privacy-policy.aspx"><i class="fa fa-sign-out"></i> Privacy Policy</a></li>
							<li><a href="terms-condition.aspx"><i class="fa fa-users"></i> Terms Condition</a></li>
							<li><a href="security-center.aspx"><i class="fa fa-sign-out"></i> Security Center</a></li>
						</ul>
					</div>
				</div>
			</div>
			<div class="container-wrap">
				<div class="main-content container-boxed max">
					<div class="row">
						<div class="noo-main col-md-12">
							<div class="member-manage" style="margin-bottom: 250px;">
								<h3>About Us</h3>
							
							<p class="para">Open Campus, is India's leading online career and recruitment resource with its cutting edge technology provides relevant profiles to employers and relevant jobs to jobseekers across industry verticals, experience levels and geographies. </p>
<p class="para">More than 10,000 people have registered on the Open Campus Worldwide network. Open Campus provides the widest and most sophisticated job seeking, career management, recruitment and talent management capabilities globally. </p>
<p class="para">Open Campus in India started its operations in 2013. Headquartered in Vidisha (Madhya Pradesh). Till date jobs posted in Open Campus are based only in India but we are soon going to widen our horizons to cover all international jobs. </p>

                                <p class="para">Over 500 companies have approached Open Campus and placed job openings. Open Campus has 50k engaging users till date and the figures are still ROLLING!! ;)</p>

                                <h3>Join Us - </h3>
                     <div class="col-md-3"><a href="https://www.facebook.com/opencampus.in">
    <img src="images/join_f.png" /></a> </div>       
                     <div class="col-md-3"><a href="https://plus.google.com/+opencampus1">
    <img src="images/join_g.png" /></a></div>       
                     <div class="col-md-3"><a href="https://twitter.com/opencampusdot">
    <img src="images/join_t.png" /></a></div>       
                     <div class="col-md-3"><a href="https://www.linkedin.com/company/opencampus.in">
    <img src="images/join_in.png" /></a></div>       

							</div>
						</div>  
					</div> 
				</div> 
			</div>
</asp:Content>

