﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class campus_College : System.Web.UI.MasterPage
{
    Utility Ut = new Utility();
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!Page.IsPostBack)
        {
            if (Ut.GetCookie(this.Page, "CollegeId") != null as string)
            {
                int _collegeId = int.Parse(Ut.GetCookie(this.Page, "CollegeId"));
                lnkLogout.Visible = true;
                lnkLogin.Visible = false;

                lnkCollegePreview.Attributes.Add("onclick", "window.location.href='college.aspx?CollegeId=" +Ut.EncryptString(_collegeId.ToString()) + "'");
            }
           
        }
    }
}
