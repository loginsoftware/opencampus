﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data.Objects;
using DataLayer;

namespace BusinessLayer
{
    public class JobQualificationBL
    {
        public int InsertJobQualification(int JobQualificationId, string JobQualification)
        {
           // ObjectParameter CandidateId = new ObjectParameter("CandidateId", typeof(Int32));
            DBEntities Obj = new DBEntities();

            Obj.InsertJobQualification(JobQualificationId, JobQualification);
            return JobQualificationId;
        }

        public int UpdateJobQualification(int JobQualificationId, string JobQualification)
        {
            DBEntities Obj = new DBEntities();
            Obj.UpdateJobQualification(JobQualificationId, JobQualification);
            return JobQualificationId;
        }

        public int DeleteJobQualification(int JobQualificationId)
        {
            DBEntities Obj = new DBEntities();
            Obj.DeleteJobQualification(JobQualificationId);
            return JobQualificationId;
        }

        public List<JobQualification> LoadJobQualificationByPrimaryKey(int JobQualificationId)
        {
            DBEntities Obj = new DBEntities();
            return Obj.LoadJobQualificationByPrimaryKey(JobQualificationId).ToList();
        }

        public List<JobQualification> LoadAllJobQualification()
        {
            DBEntities Obj = new DBEntities();
            return Obj.LoadAllJobQualification().ToList();
        }
    }
}
