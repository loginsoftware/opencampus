﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.master" AutoEventWireup="true" CodeFile="Login-with-linkedin.aspx.cs" Inherits="Login_with_linkedin" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="cph" Runat="Server">
     <asp:Button ID="Button1" Text="Login with LinkedIn" runat="server" OnClick="Authorize" />
<asp:Panel ID="pnlDetails" runat="server" Visible="false">
    <hr />
    <asp:Image ID="imgPicture" runat="server" /><br />
    Name:
    <asp:Label ID="lblName" runat="server" /><br />
    LinkedInId:
    <asp:Label ID="lblLinkedInId" runat="server" /><br />
    Location:
    <asp:Label ID="lblLocation" runat="server" /><br />
    EmailAddress:
    <asp:Label ID="lblEmailAddress" runat="server" /><br />
    Industry:
    <asp:Label ID="lblIndustry" runat="server" /><br />
    Headline:
    <asp:Label ID="lblHeadline" runat="server" />
</asp:Panel>
</asp:Content>

