﻿<%@ Page Title="Job Posting | Open Campus" EnableEventValidation="false" Language="C#" MasterPageFile="~/Employer.master" AutoEventWireup="true" CodeFile="Job_Posting.aspx.cs" Inherits="Job_Posting" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="asp" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
    <link href="css/job-post.css" rel="stylesheet" type="text/css" />
   
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="cph" runat="Server">
    <div class="container-wrap">
        <div class="main-content container-fullwidth">
            <div class="row">
                <div class="noo-main col-md-12 upper">
                    <div class="jform">

                        <div class="jform-body">
                            <div class="container-boxed max">
                                <div class="form-horizontal">
                                    <div class="jstep-content">
                                        <div class="jpanel jpanel-resume-form">

                                            <div class="jpanel-body2">
                                                <div class="jpanel jpanel-resume-preview">
                                                    <div class="jpanel-body1">
<div class="resume-preview">
<div class=" resume-form-detail">
<article class="resume">
<div class="resume-candidate-profile">
																			
																		
																		
																		
<div class="col-md-10">
	<div class="resume-desc">
		<div class="resume-general row div-height">
																							
																								
			<div class=" col-sm-11 shadow-box">
																								
																								
				<h5 class="title-general1 back-b">
					<span>Job Details <span class="jobs">- Specify details of the position/job you are going to post</span>  
</span>
				</h5>	
																									
																								
<div class="col-sm-12">
            <div class="form-group">
																		
            <label for="title" class="col-sm-3 label-style">Select Company </label>
            <div class="col-sm-8">
            <asp:DropDownList ID="DdnJobPostCompany" Width="200px" Height="22px" runat="server" AppendDataBoundItems="true" CssClass="form-control chosen-select" ClientIDMode="Static">
            <asp:ListItem>--Select--</asp:ListItem>
            </asp:DropDownList> <span style="color:red;">Optional (Select If you post other company Job)</span>
            </div>
            </div>	

<div class="form-group">
																		
<label for="title" class="col-sm-3 label-style">Job Display Title</label>
<div class="col-sm-8">
<input type="text" value="" class="form-control1" runat="server" id="txtJobTitleDesignation" placeholder="Eg: Type Senior Manager, not Sr. Mgr." autofocus>  
</div>
<a href="#" data-toggle="tooltip" title="To get best quality responses mention Designation or Department of the Position."><i class="fa fa-question-circle" aria-hidden="true"></i></a>
</div>
<div class="form-group">
																		
            <label for="title" class="col-sm-3 label-style">Designation</label>
            <div class="col-sm-8">
            <asp:DropDownList ID="DdnDesignation" Width="200px" Height="22px" runat="server" AppendDataBoundItems="true" CssClass="form-control chosen-select" ClientIDMode="Static">
            <asp:ListItem>--Select Designation--</asp:ListItem>
            </asp:DropDownList>
            </div>
            </div>
<div class="form-group">
<label for="language" class="col-sm-3 label-style">Job Description</label>

<div class="col-sm-8">

<textarea rows="2" value="" class="form-control" runat="server" id="txtJobDescription" name="title"  placeholder="Describe your job here..." style="height:100px;"></textarea>
</div>
</div>
<div class="form-group">
<label for="highest_degree" class="col-sm-3 label-style">Keywords/Skills</label>
<div class="col-sm-8">
<%--<asp:ListBox ID="LBKeyword" Width="100%" ClientIDMode="Static" runat="server" SelectionMode="Multiple" data-placeholder="Enter Designation, Skills required, Qualification etc.">
        
</asp:ListBox>--%>
<asp:ListBox ID="DdnSkills" ClientIDMode="Static" SelectionMode="Multiple" CssClass="chosen-select" runat="server" Width="100%" >
</asp:ListBox>
<span class="help-block1">Max 250 characters</span> 

</div>
</div>
<div class="form-group">
<label for="experience_year" class="col-sm-3 label-style">Work Experience</label>
<div class="col-sm-3">
<input id="txtMinExperience" runat="server" min="1" max="10" step="0.5" value="1" /> to 
<input id="txtMaxExperience" runat="server" min="1" max="10" step="0.5" value="2.5" /> 
																		   
</div>
																			
</div>

<div class="form-group">
<label for="experience_year" class="col-sm-3 label-style">Annual CTC</label>
<div class="col-sm-6">
<input id="txtMinSalary"  runat="server"  placeholder="Min Salary" value="100000"> to
<input id="txtMaxSalary" runat="server" placeholder="Max Salary" value="300000">
                                                                         
</div>
																		   		
</div> 
<div class="form-group">
																		
<label for="title" class="col-sm-3 label-style">Other Salary Details </label>
<div class="col-sm-8">
<input runat="server" id="txtOtherSalaryDetail" type="text" class="form-control1" required>
</div>
</div>
<div class="form-group">
																		
<label for="title" class="col-sm-3 label-style">Number of Vacancies </label>
<div class="col-sm-3">
<input runat="server" id="txtNoOfVacancy" type="text" class="form-control1" required>
</div>
</div>
<div class="form-group">
																		
<label for="title" class="col-sm-3 label-style">Location(s) of Job </label>
<div class="col-sm-8">
					
<asp:ListBox ID="lstbxJobLocation" runat="server" SelectionMode="Multiple" AppendDataBoundItems="true">
<asp:ListItem>--Enter Job Locations--</asp:ListItem>
</asp:ListBox>
			
</div>
</div>
<div class="form-group">
<label for="title" class="col-sm-3 label-style">Industry </label>
<div class="col-sm-8">
<asp:DropDownList ID="DdnIndustry" ClientIDMode="Static" runat="server" AppendDataBoundItems="true">
<asp:ListItem>--Job Industry Type--</asp:ListItem>
</asp:DropDownList>
</div>
																			
</div>	 
												 
<div class="form-group">
																		
<label for="title" class="col-sm-3 label-style">Functional Area </label>
<div class="col-sm-8">
<asp:DropDownList ID="DdnFunctionalArea" ClientIDMode="Static" runat="server" AppendDataBoundItems="true">
<asp:ListItem>--Job Functional Area--</asp:ListItem>
</asp:DropDownList>
</div>
</div>	
												
							
</div>
			</div>
																								
																								
																								
																							
<div class=" col-sm-11 shadow-box">
																								
																								
<h5 class="title-general1 back-b">
<span>Manage Responses  <span class="jobs"> - Select how you want to receive applications for this job posting</span>   <a href="#" data-toggle="tooltip" title="Hooray!"><i class="fa fa-question-circle" aria-hidden="true"></i>
</a> </span>
</h5>	
																									
																								
<div class="col-sm-12">
<div class="form-group">
																		
<label for="title" class="col-sm-3 label-style">Receive Responses on </label>
<div class="col-sm-8 borders">
																			
<div class="btn-group">
<asp:RadioButtonList ID="rbResponseType" ClientIDMode="Static" runat="server" onchange="checkResponseType();" RepeatDirection="Horizontal">
<asp:ListItem Value="On Email" Selected="True">On Email</asp:ListItem>
<asp:ListItem Value="Walk-In">Walk-In</asp:ListItem>
</asp:RadioButtonList>
  
</div>

<div id="PnlWalkin" runat="server" clientidmode="Static" class="col-sm-12" style="display:none">

<label for="title" class="col-sm-3 label-style">Date From </label>
<div class="col-sm-4"> 
<asp:TextBox ID="txtFromDateWalkin" runat="server" class="form-controls"></asp:TextBox>
<asp:CalendarExtender ID="CalendarExtender1" runat="server" Format="dd-MMM-yyyy" TargetControlID="txtFromDateWalkin" PopupButtonID="txtFromDateWalkin"></asp:CalendarExtender>

</div>
<label for="title" class="col-sm-1 label-style">to </label>
<div class="col-sm-4">
<asp:TextBox ID="txtToDateWalkin" runat="server" class="form-controls"></asp:TextBox>
<asp:CalendarExtender ID="CalendarExtender2" runat="server" Format="dd-MMM-yyyy" TargetControlID="txtToDateWalkin" PopupButtonID="txtToDateWalkin"></asp:CalendarExtender>
</div>	
    											                           
</div>
<div id="PnlEmail" runat="server" clientidmode="Static" class="col-sm-12" style="display:block">
<div class="form-group">
																		
<label for="title" class="col-sm-3 label-style">Email address </label>
<div class="col-sm-8">
<asp:TextBox ID="txtJobResponseEmail" runat="server"></asp:TextBox>
</div>
</div>	
</div>																		
		
																			
</div>
</div>	
<div class="form-group">
																		
<label for="title" class="col-sm-3 label-style">Reference Code </label>
<div class="col-sm-8">
<asp:TextBox ID="txtReferenceCode" runat="server" class="form-controls"></asp:TextBox>  
</div>
</div>	 	
<div class="form-group">
																		
<label for="title" class="col-sm-3 label-style">Website Link to Job (Optional) </label>
<div class="col-sm-8">
<asp:TextBox ID="txtLinkToJob" runat="server" class="form-controls"></asp:TextBox>  
</div>
</div>
   
																			
			

</div>
																						
</div>
																							
																							
<div class="col-sm-11 shadow-box" style="display:none;">
																								
																								
<h5 class="title-general1 back-b">
<span>Featured Jobs  <span class="jobs"> - Browse the Image and Select Is Featured Option to Show Job Image on Website HomePage</span>  </span>
</h5>	
																									
																								
<div class="col-sm-12">
<div class="form-group">
																		
<label for="title" class="col-sm-3 label-style">Browse Image</label>
<div class="col-sm-8">
<asp:FileUpload ID="FUImage" runat="server"></asp:FileUpload>  
</div>
</div>
<div class="form-group">
																		
<label for="title" class="col-sm-3 label-style"></label>
<div class="col-sm-8">
<asp:CheckBox ID="chkIsFeatured" runat="server" Text="IsFeatured"></asp:CheckBox>
</div>
</div>	
						                                                    	

																			
			

</div>
																						
</div>
																													
																		
<div class=" col-sm-11 shadow-box">
																								
																								
																									
																									
																								
<div class="col-sm-12">
<div class="form-group">
																		
																			
																
																			
</div>	
<div class="form-actions1 resume-preview-actions text-center clearfix">
																
<a id="LnkSaveAndPublish" class="btn btn-opencampus">Save & Publish</a>
<a id="LnkSaveAndPublishLater" class="btn btn-opencampus">Save & Publish Later</a>

</div> 	

																			
			

</div>
																						
		</div>
																							

																							
																								
																		
																							
																						
																							
	</div>
</div>
</div>
																		
																			
</article>


</div>
</div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <asp:HiddenField ID="hfImagePath" runat="server" />
        <input id="hfJobResponseType" type="hidden" value="On Email" />
        <script type="text/javascript">

            var a = document.body;
            a.className += ' back-color';

            function checkResponseType() {
                var list = document.getElementById("rbResponseType"); //Client ID of the radiolist
                var inputs = list.getElementsByTagName("input");
                var selected;
                for (var i = 0; i < inputs.length; i++) {
                    if (inputs[i].checked) {
                        selected = inputs[i];
                        break;
                    }
                }
                if (selected) {
                    var _result = selected.value;
                    document.getElementById("hfJobResponseType").value = _result;
                    if (_result == "On Email") {
                        document.getElementById('PnlEmail').style.display = 'block';
                        document.getElementById('PnlWalkin').style.display = 'none';
                    }
                    else if (_result == "Walk-In") {
                        document.getElementById('PnlEmail').style.display = 'none';
                        document.getElementById('PnlWalkin').style.display = 'block';
                    }
                }
            }


        </script>
    </div>
</asp:Content>

