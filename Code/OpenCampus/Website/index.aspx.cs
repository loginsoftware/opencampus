﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Net;
using BusinessLayer;

public partial class index : Utility // System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!Page.IsPostBack)
        {
            LoadFeaturedRecentJob();
            LoadSideBarLinks();
        }
    }

    private void LoadFeaturedRecentJob()
    {
        JobPostingBL obj = new JobPostingBL();
        List<DataLayer.VJobListHomePage> J = obj.LoadJobPostingJobForHomePage();
        /////--- bing repeater
        List<DataLayer.JobPosting> jobRecent = obj.LoadJobPostingRecentJobs(10);
        rptrRecentJobs.DataSource = jobRecent;
        rptrRecentJobs.DataBind();


        for (int i = 0; i < J.Count; i++)
        {
            string _logo = "";
            string _companyName = J[i].CompanyName;
            if (J[i].JobPostCompanyId != null)
            {
                if (J[i].CompanyLogo != "") _logo = J[i].CompanyLogo.Replace("~/","");               
                _companyName = J[i].JobPostCompanyName;
            }

            LtrRecentJobList.Text += "<article class='noo_job hentry'><div class='loop-item-wrap'><div class='item-featured'>"
			+"<a href='#'> <img width='222' height='131' src='"+ _logo + "' alt='"+ J[i].JobTitleDesignation+"'/></a></div>"
			+"														<div class='loop-item-content'>"
			+"															<h3 class='loop-item-title'>"
            + "																<a href='#'>" + J[i].JobTitleDesignation + "</a>"
			+"															</h3>"
			+"															<p class='content-meta'>"
			+"																<span class='job-company'>"
            + "																	<a href='#'>" + _companyName + "</a>"
			+"																</span>"
			+"																<span class='job-type contract'>"
			+"																	<a href='#'>"
			+"				<i class='fa fa-bookmark'></i>"+ J[i].WorkExperience.ToString().Replace("TO", " to ") + " years" +"	</a>"
			+"																</span>"
			+"																<span class='job-location'>"
			+"																	<i class='fa fa-map-marker'></i>"
			+"																	<a href='#'><em>"+J[i].JobLocation+"</em></a>"
			+"																</span>"
			+"																<span>"
			+"			<time class='entry-date' datetime='"+ J[i].CreatedOn + "'><i class='fa fa-calendar'></i>"
            +" "+ J[i].CreatedOn.ToString("dd-MMM-yyyy") + " - " + J[i].JobExpiryDate.ToString("dd-MMM-yyyy") + "</time>"
		+"																	</span>"
		+"																</p>"
		+"															</div>"
		+"															<div class='show-view-more'>"
        + "																<a class='btn btn-opencampus' href='jobs/" + MyUrl(J[i].JobTitleDesignation).Trim('-') + "-" + J[i].JobId + "'>View more </a>"
		+"															</div>"
		+"														</div>"
		+"													</article>";
        }
    }

    private void LoadSideBarLinks()
    {
        FilterMasterBL obj = new FilterMasterBL();
        List<DataLayer.FilterMasterSkillCityDesignationForHomePage_Result> F = obj.LoadFilterMasterSkillCityDesignationForHomePage();

        List<DataLayer.FilterMasterSkillCityDesignationForHomePage_Result> lst_skill = F.Where(f => f.FilterType == "Skills").ToList();

        List<DataLayer.FilterMasterSkillCityDesignationForHomePage_Result> lst_city = F.Where(f => f.FilterType == "City").ToList();

        List<DataLayer.FilterMasterSkillCityDesignationForHomePage_Result> lst_desination = F.Where(f => f.FilterType == "Designation").ToList();

        if (lst_skill.Count > 0)
        {
            for(int i =0; i<lst_skill.Count; i++)
            {
                string _skill = lst_skill[i].FilterName.Trim().Replace("/", "").Replace(".", "dot").Replace(" ", "-").ToLower() + "-jobs";
                ltrJobsBySkills.Text += "<li><a href='/" + MyUrl(_skill) + "' title='" + lst_skill[i].FilterName + " Jobs'>" + lst_skill[i].FilterName + "</a></li>";
            }
        }

        if (lst_city.Count > 0)
        {
            for (int i = 1; i < lst_city.Count; i++)
            {
                string _skill = lst_city[i].FilterName.Trim().Replace("/", "").Replace(".", "dot").Replace(" ", "-").ToLower() + "";
                ltrJobByCity.Text += "<li><a href='/jobs-in-" + MyUrl(_skill) + "' title='Jobs In " + lst_city[i].FilterName + "'>Jobs in " + lst_city[i].FilterName + "</a></li>";
            }
        }
        if (lst_desination.Count > 0)
        {
            for (int i = 0; i < lst_desination.Count; i++)
            {
                string _skill = lst_desination[i].FilterName.Trim().Replace("/", "").Replace(".", "dot").Replace(" ", "-").ToLower() + "";
                ltrJobByDesignation.Text += "<li><a href='/" + MyUrl(_skill) + "-jobs' title='" + lst_desination[i].FilterName + " Jobs'>" + lst_desination[i].FilterName + " Jobs</a></li>";
            }
        }
    }
}