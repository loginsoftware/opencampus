﻿<%@ Application Language="C#" %>
<%@ Import Namespace="System.Web.Routing" %>

<script runat="server">

    void Application_Start(object sender, EventArgs e) 
    {
        // Code that runs on application startup
        RegisterRoutes(RouteTable.Routes);
    }

    static void RegisterRoutes(RouteCollection routes)
    {
        routes.MapPageRoute("JobDetail", "jobs/{JobTitle}-{JobId}", "~/campus/job-in-detail.aspx");
        routes.MapPageRoute("JobsByDesignation2", "jobs-by-designation", "~/jobs-by-designation.aspx");
        routes.MapPageRoute("JobsBySkills", "top-jobs-by-skill", "~/jobs-by-skill.aspx");
        routes.MapPageRoute("JobsByCategory", "jobs-by-category", "~/jobs-by-category.aspx");
        routes.MapPageRoute("JobsByLocation", "jobs-by-location", "~/jobs-by-location.aspx");
        routes.MapPageRoute("JobsByDesignation1", "jobs-by-designation-{filtr}", "~/jobs-by-designation.aspx");
        routes.MapPageRoute("JobsByLocation1", "jobs-by-location-{filtr}", "~/jobs-by-location.aspx");
        routes.MapPageRoute("JobsBySkills1", "top-jobs-by-skill-{filtr}", "~/jobs-by-skill.aspx");
        routes.MapPageRoute("JobDetail1", "campus/{JobTitle}-{JobId}.html", "~/campus/job-in-detail.aspx");        
        routes.MapPageRoute("SearchJobListing3", "{param1}-{param2}-{param3}-{param4}", "~/SearchJobListing.aspx");       
        routes.MapPageRoute("SearchJobListing2", "{param1}-{param2}-{param3}", "~/SearchJobListing.aspx");
        routes.MapPageRoute("SearchJobListing", "{param1}-{param2}", "~/SearchJobListing.aspx");
        
              
    }
    
    void Application_End(object sender, EventArgs e) 
    {
        //  Code that runs on application shutdown

    }
        
    void Application_Error(object sender, EventArgs e) 
    { 
        // Code that runs when an unhandled error occurs

    }

    void Session_Start(object sender, EventArgs e) 
    {
        // Code that runs when a new session is started

    }

    void Session_End(object sender, EventArgs e) 
    {
        // Code that runs when a session ends. 
        // Note: The Session_End event is raised only when the sessionstate mode
        // is set to InProc in the Web.config file. If session mode is set to StateServer 
        // or SQLServer, the event is not raised.

    }
       
</script>
