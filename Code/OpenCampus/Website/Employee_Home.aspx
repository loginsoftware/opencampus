﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Employer.master" AutoEventWireup="true"
    CodeFile="Employee_Home.aspx.cs" Inherits="Employee_Home" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="asp" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
    <style type="text/css">
        .container-boxed.offset
        {
            padding-top: 0px;
        }
        .container
        {
            width: 290px;
            margin-left: -15px;
        }
        .progress
        {
            overflow: hidden;
            height: 20px;
            background-color: #ccc;
            border-radius: -1px;
            -webkit-box-shadow: inset 0 1px 2px rgba(0,0,0,.1);
            box-shadow: inset 0 1px 2px rgba(0,0,0,.1);
            margin-bottom: 20px;
        }
        .progress-bar
        {
            width: 0;
            height: 100%;
            color: #fff;
            text-align: center;
            background-color: #ee303c;
        }
        .progress-striped .progress-bar
        {
            background-image: -webkit-linear-gradient(45deg,rgba(255,255,255,.15) 25%,transparent 25%,transparent 50%,rgba(255,255,255,.15) 50%,rgba(255,255,255,.15) 75%,transparent 75%,transparent);
            background-image: linear-gradient(45deg,rgba(255,255,255,.15) 25%,transparent 25%,transparent 50%,rgba(255,255,255,.15) 50%,rgba(255,255,255,.15) 75%,transparent 75%,transparent);
            background-size: 40px 40px;
        }
        .progress.active .progress-bar
        {
            -webkit-animation: progress-bar-stripes 2s linear infinite;
            animation: progress-bar-stripes 2s linear infinite;
            -moz-animation: progress-bar-stripes 2s linear infinite;
        }
    </style>
    <style>
        .noo-main
        {
            border: 1px solid;
            border-color: #e5e6e9 #dfe0e4 #d0d1d5;
            border-radius: 3px;
        }
        .noo1-main
        {
            padding: 0px 0px 0;
            border-right: 1px solid;
            border-color: #e5e6e9 #dfe0e4 #d0d1d5;
            border-radius: 3px;
        }
        .control-label
        {
            display: inline-block;
            margin-bottom: 10px;
            line-height: 30px;
        }
        
        
        
        .form-actions1
        {
            margin-left: 7%;
        }
        element.style
        {
        }
        .jpanel-body1
        {
            background: #fff;
            padding: 0px;
        }
        body
        {
            -webkit-font-smoothing: antialiased;
            -moz-osx-font-smoothing: grayscale;
        }
        
        .navbar-nav ul.sub-menu li > a:hover, .navbar-nav ul.sub-menu li > a:focus, .navbar-nav ul.sub-menu li:hover > a, .navbar-nav ul.sub-menu li.sfHover > a, .navbar-nav ul.sub-menu li.current-menu-item > a, .posts-loop.grid .event-info a:hover, .posts-loop.grid.staffs .loop-item-title a, .job-action a.bookmark-job i:hover, .job-action a.bookmark-job.bookmarked, .resume .title-general1 span, .member-manage .table tbody tr:hover a:not(.btn-primary):hover, .noo-pricing-table .noo-pricing-column .pricing-content .pricing-info ul li i, .woocommerce ul.products li.product figure .product-wrap .shop-loop-actions a:hover, .woocommerce ul.products li.product figcaption .product_title a:hover, h2.comments-title span, .comment-reply-link, .comment-author a:hover, .wigetized .widget a:hover, .wigetized .widget ul li a:hover, .wigetized .widget ol li a:hover, .wigetized .widget.widget_recent_entries li a:hover, .btn-link:hover, .btn-link:focus, .noo-social a:hover, .login-form-links > span a, .login-form-links > span .fa, .form-control-flat > .radio i, .form-control-flat > .checkbox i
        {
            color: #1e1e1e;
        }
        
        h1 a:hover, h2 a:hover, h3 a:hover, h4 a:hover, h5 a:hover, h6 a:hover, .h1 a:hover, .h2 a:hover, .h3 a:hover, .h4 a:hover, .h5 a:hover, .h6 a:hover, a:hover, a:focus, .text-primary1, a.text-primary:hover, .noo-menu li > a:hover, .noo-menu li > a:active, .noo-menu li.current-menu-item > a, body.page-menu-transparent .navbar:not(.navbar-fixed-top) .navbar-nav > li > a:hover, .navbar-nav li > a:hover, .navbar-nav li > a:focus, .navbar-nav li:hover > a, .navbar-nav li.sfHover > a, .navbar-nav li.current-menu-item > a
        {
            color: #80bb03;
        }
        .fa-question-circle1:before
        {
            content: "\f059";
            color: #80bb03;
        }
        .jform-header1
        {
            padding-top: 16px;
            padding-bottom: 210px;
        }
        .jpanel-body
        {
            background: #fff;
            padding: 20px;
        }
        .resume-desc
        {
            margin-top: -24px;
        }
        .resume .resume-content .resume-desc .resume-general ul li span
        {
            width: 29%;
            float: left;
            font-weight: 700;
        }
        hr
        {
            margin-bottom: 1.6428571428571em;
            border-top: 1px solid #dedede;
        }
        
        .resume .resume-content .resume-desc .resume-general ul
        {
            list-style: none;
            margin-left: 0;
            padding-left: 0;
            font-size: 13px;
        }
        body
        {
            font-size: 13px;
        }
        .form-group
        {
            margin-bottom: 0.500em;
            position: relative;
        }
        
        
        .resume-timeline1
        {
            padding: -3.857143em 0;
        }
        .form-control1, .form-group .chosen-container-multi .chosen-choices, .form-group .chosen-container-single, .widget_newsletterwidget .newsletter-email, .wpcf7-form-control:not(.wpcf7-submit)
        {
            display: block;
            width: 100%;
            padding: .42857142857142855em 1.2857142857142856em;
            font-size: 14px;
            line-height: 0.7;
            color: #555;
            background-color: #fff;
            background-image: none;
            border: 1px solid #ccc;
            letter-spacing: .5px;
            -webkit-transition: border-color ease-in-out .15s,box-shadow ease-in-out .15s;
            -o-transition: border-color ease-in-out .15s,box-shadow ease-in-out .15s;
            transition: border-color ease-in-out .15s,box-shadow ease-in-out .15s;
        }
        .container-boxed max
        {
            border: 1px solid #dedede;
            padding-top: 7px;
        }
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="cph" runat="Server">
    <div class="container-wrap" style="background-color: #E9EBEE;">
        <div class="main-content container-boxed max offset">
            <div class="row">
                <div class="noo-main col-md-12">
                    <div class="form-horizontal" enctype="multipart/form-data">
                        <div class="candidate-profile-form row">
                            <div class="col-sm-3" style="background-color: white;">
                                <div class="noo-sidebar-wrap">
                                    <div class="widget widget_archive">
                                        <h5 class="widget-title" style="font-size: 15px;">
                                            Profile Completeness</h5>
                                        <div class="container">
                                            <div class="progress progress-striped">
            <div id="MtrProfilePercent" runat="server" class="progress-bar progress-bar-danger"
                                                    role="progressbar" aria-valuenow="80" aria-valuemin="0" aria-valuemax="100" style="width: 15%">
                                                    <span id="lblProfilePercent" runat="server"></span>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="widget widget_categories">
                                        <h4 class="widget-title">
                                            Profile</h4>
                                        <ul>
                                 <li><a href="Employee_Home.aspx">Profile Overview</a> </li>
                                 <li><a href="Employee_Home.aspx?panel=currentEmp">Update Current Employer</a> </li>
                                 <li><a href="Employee_Home.aspx?panel=profileContactDetail">Update Contact Detail</a> </li>
                                 <li><a href="Employee_Home.aspx?panel=sector">Update Hiring Sector/Skills</a> </li>
                                 <li><a href="Employee_Home.aspx?panel=profilePhoto">Update Photo</a> </li>
                                 <li><a href="Employee_Home.aspx?panel=changpwd">Change Password</a> </li>
                                        </ul>
                                    </div>
                                </div>
                            </div>
                            <div class="col-sm-9" style="background-color: white; box-shadow: 0px 0px 5px 0px grey;">
                                <div class="jform">
                                    <div class="jform-header1">
                                        <div class="container-boxed max">
                                        </div>
                                    </div>
                                    <div class="jform-body">
                                        <div class="container-boxed max">
                <form class="form-horizontal">
                <div class="jstep-content">
                    <div class="jpanel jpanel-resume-form">
                        <div class="jpanel-body2">
                            <div class="jpanel jpanel-resume-preview">
                                <div class="jpanel-body1">
                                    <div class="resume-preview">
                                        <div class=" resume-form-detail">
                                            <article class="resume">
												<div class="resume-candidate-profile">
													<div class="row">
														<div class="col-sm-2">
	              <img runat="server" id="ImgEmployeePhoto" alt='' src='images/avatar/anonymous_big.png' height='170' width='150' />
															<h6><a href="Employee_Home.aspx?panel=profilePhoto"> Change your photo</a>
                                                                </h6>
														</div>
																					


				<div class="col-sm-8 candidate-detail">
					<div class="candidate-title clearfix">
						<h2 id="lblContactPerson" runat="server"></h2>
						<a class="btn btn-default pull-right" href="#">
							<i class="fa fa-2x fa-check-circle-o text-info"></i>
							Verified
						</a>
					</div>
					<div class="candidate-info">
						<div class="row">
							<div class="col-sm-6">
								<i class="fa fa-suitcase text-primary1"></i>
								&nbsp;&nbsp;<asp:Literal ID="lblCurrentDesignation" runat="server"></asp:Literal> <br>
								<i class="fa fa-building text-primary1"></i>
								&nbsp;&nbsp;<asp:Literal ID="lblCurrentEmployer" runat="server"></asp:Literal> 
								<br>
								<i class="fa fa-map-marker text-primary1"></i>
								&nbsp;&nbsp;<asp:Literal ID="lblCity" runat="server"></asp:Literal> 
							</div>
																								
							<div class="col-sm-6">
																						
																									
							</div>
																								
																								
																							
																		
					</div>
				</div>	
			</div>
																			</div>
																			<hr/>
																		
																			<div class="resume-content">
																				<div class="row">
																					<div class="col-md-12">
																						<div class="resume-desc">

                <asp:Panel ID="PnlSectorSkillDisplay" runat="server" Visible="false">
                        <div class="resume-general row">																	
																								
						<div class=" col-sm-9">																		
																								
							<h5 class="title-general1">
								<span>Sectors/industry and Skills I hire for   <a href="#" data-toggle="tooltip" title="Specify the sector/industry you are hire for."><i class="fa fa-question-circle" aria-hidden="true"></i>
                                                                    </a>
                            <a class="btn btn-info" href="Employee_Home.aspx?panel=sector">
							<i class="fa fa-pencil"></i> Edit</a>
                                                                     </span>
							</h5>	
																								
																								
							<ul style="list-style-type:inherit;">
								<li>
									<span>Industry:
                                        </span>
									<asp:Literal ID="lblIndustry" runat="server"></asp:Literal>
								</li>
								<li>
									<span>Functional Area:</span>
									<asp:Literal ID="lblFunctionalArea" runat="server"></asp:Literal>
								</li>
								<li>
									<span>Skill:</span>
									<asp:Literal ID="lblSkill" runat="server"></asp:Literal>
								</li>
								                              
							</ul>
						</div>
																						
						</div><hr/>
                      </asp:Panel>
                <asp:Panel ID="PnlContactDetail" runat="server" Visible="false">
					<div class="resume-general row">									
						<div class=" col-sm-9">																		
																								
							<h5 class="title-general1">
								<span>My Contact Details   <a href="#" data-toggle="tooltip" title="Specify your contact Detail !"><i class="fa fa-question-circle" aria-hidden="true"></i>
                                                                    </a>
                              <a class="btn btn-info" href="Employee_Home.aspx?panel=profileContactDetail">
							<i class="fa fa-pencil"></i> Edit</a>
                                                                     </span>
							</h5>	
																								
																								
							<ul style="list-style-type:inherit;">
								<li>
									<span>Email
                                        </span>
									<asp:Literal ID="lblEmailAddress" runat="server"></asp:Literal>
								</li>
                                <li>
									<span>Secondary Email
                                        </span>
									<asp:Literal ID="lblSecondaryEmail" runat="server"></asp:Literal>
								</li>
								<li>
									<span>Phone:</span>
									<asp:Literal ID="lblPhone" runat="server"></asp:Literal>
								</li>
                                <li>
									<span>Mobile:</span>
									<asp:Literal ID="lblMobile" runat="server"></asp:Literal>
								</li>
								<li>
									<span>Facebook profile URL</span>
									<asp:Literal ID="lblFacebookUrl" runat="server"></asp:Literal>
								</li>
								<li>
									<span>LinkedIn profile URL</span>
									<asp:Literal ID="lblLinkedInUrl" runat="server"></asp:Literal>
								</li>
                                <li>
									<span>Google-plus profile URL</span>
									<asp:Literal ID="lblGooglePlus" runat="server"></asp:Literal>
								</li>
							</ul>
						</div>
																						
						</div><hr/>
                        </asp:Panel>
                <asp:Panel ID="PnlCurrentEmployementDetail" runat="server" Visible="false">
                        <div class="resume-general row">																	
																								
						<div class=" col-sm-9">																		
																								
							<h5 class="title-general1">
								<span>Current Employement Detail   <a href="#" data-toggle="tooltip" title="Current Employeement Detail"><i class="fa fa-question-circle" aria-hidden="true"></i>
                                                                    </a>
                                                                    <a class="btn btn-info" href="Employee_Home.aspx?panel=currentEmp">
							<i class="fa fa-pencil"></i> Edit</a>
                                                                     </span>
							</h5>	
																								
																								
							<ul style="list-style-type:inherit;">
								<li>
									<span>Current Designation:
                                        </span>
									<asp:Literal ID="lblCurrentDesignation2" runat="server"></asp:Literal>
								</li>
								<li>
									<span>Current Employer:</span>
									<asp:Literal ID="lblCurrentEmployer2" runat="server"></asp:Literal>
								</li>
                                <li>
									<span>Company Website:</span>
								<asp:Literal ID="lblWebsiteUrl" runat="server"></asp:Literal>
								</li>
								<li>
									<span>Job Profile:</span>
									<asp:Literal ID="lblJobProfile" runat="server"></asp:Literal>
								</li>
								<li>
									<span>Joined In</span>
									<asp:Literal ID="lblJoinedIn" runat="server"></asp:Literal>
								</li>                               
							</ul>
						</div>
																						
						</div><hr/>
                      </asp:Panel>
                <div id="PnlProfileImage" visible="false" runat="server">
                                     <div class="form-title">
                                        <h3>Upload your Latest Photo</h3>
                       <p>Upload your photo (jpg, gif, png format) of less than 2 MB.</p>

                                    </div>
                                    <div class="form-group">
                                        <label for="name" class="col-sm-3 control-label">
                                            <asp:Image ID="ImgEmloyee" Width="170px" Height="190px" runat="server" /></label>
                                        <div class="col-sm-8">
                                       Browse Photo:     <asp:FileUpload ID="FUPhoto" runat="server" />
                                        </div>
                                    </div>
                                    <div class="form-group">
                                       
                                        <div class="col-sm-8">
                <asp:Button ID="BtnSavePhoto" CssClass="btn btn-primary" OnClick="BtnSavePhoto_Click" runat="server" Text="Save" />
                                        </div>
                                    </div>
                                    <p>By uploading a file you certify that this is your photograph, that you have the right to distribute this picture and that it does not violate our guidelines</p>
                                    </div>

                <asp:Panel ID="PnlSector" runat="server" Visible="false">
									<h5 class="title-general1">
												<span>Sectors and Skills/Roles I hire for</span>
											</h5>
																								
									<div class="resume-timeline1 row">
																							
										<div class="resume-form">
		<div class="resume-form-general row">
			<div class="col-sm-12">
				<div class="form-group">
																		
					<label for="title" class="col-sm-4 control-label">Industries</label>
					<div class="col-sm-8">
                    <div style="width:400px; height:300px; padding:2px; overflow:auto; border: 1px solid #ccc;">
                   <asp:CheckBoxList ID="chkIndustris" runat="server"></asp:CheckBoxList>
                   </div>
					</div>
				</div>
				<div class="form-group">
					<label for="language" class="col-sm-4 control-label">Functional Areas</label>
					<div class="col-sm-8">
						 <div style="width:400px; height:300px; padding:2px; overflow:auto; border: 1px solid #ccc;">
                   <asp:CheckBoxList ID="chkFunctionalAreas" runat="server"></asp:CheckBoxList>
                   </div>
					</div>
				</div>
				<div class="form-group">
					<label for="highest_degree" class="col-sm-4 control-label">Skills /Roles i hire for</label>
					<div class="col-sm-8">
                <%--  <asp:TextBox ID="TxtSkill" runat="server" Width="100%" placeholder="enter skill comma seperated"></asp:TextBox>            --%>   
                <asp:ListBox ID="DdnSkills" ClientIDMode="Static" SelectionMode="Multiple" CssClass="chosen-select" runat="server" Width="100%" >
</asp:ListBox>
					</div>
				</div>		
				<div class="form-actions1 resume-preview-actions text-center clearfix">
		<asp:Button ID="BtnSaveSkillHireFor" runat="server" Text="Save" CssClass="btn btn-primary" 
                        onclick="BtnSaveSkillHireFor_Click"></asp:Button>				    <asp:Button ID="BtnCancel" runat="server" Text="Cancel" OnClick="BtnCancel_Click" CssClass="btn btn-warning"></asp:Button>												
				</div>
			</div>
		
																	
		</div>
	</div>
									</div>
									
                                    </asp:Panel>	
                                    
                 <asp:Panel ID="PnlCurrentEmployementEdit" runat="server" Visible="false">
									<h5 class="title-general1">
												<span>Edit Current Employment:</span>
											</h5>
																								
									<div class="resume-timeline1 row">
																							
										<div class="resume-form">
		<div class="resume-form-general row">
			<div class="col-sm-12">
				<div class="form-group">
																		
					<label for="title" class="col-sm-4 control-label">Current Designation:</label>
					<div class="col-sm-8">
                    <asp:TextBox ID="TxtCurrentDesignation" runat="server" Width="100%" placeholder="current designation"></asp:TextBox>         
					</div>
				</div>
				<div class="form-group">
					<label for="language" class="col-sm-4 control-label">Current Employer</label>
					<div class="col-sm-8">
		<asp:TextBox ID="TxtCurrentEmployer" runat="server" Width="100%" placeholder="current employer"></asp:TextBox>
					</div>
				</div>
                <div class="form-group">
																		
					<label for="title" class="col-sm-4 control-label">Company Website:</label>
					<div class="col-sm-8">
                    <asp:TextBox ID="TxtWebsiteUrl" runat="server" Width="100%" placeholder="Enter Company Website Url"></asp:TextBox>         
					</div>
				</div>
				<div class="form-group">
					<label for="highest_degree" class="col-sm-4 control-label">Job Profile</label>
					<div class="col-sm-8">
                  <asp:TextBox ID="TxtJobProfile" runat="server" Width="100%" placeholder="enter job profile"></asp:TextBox>               
					</div>
				</div>
                <div class="form-group">
					<label for="highest_degree" class="col-sm-4 control-label">Joined In</label>
					<div class="col-sm-8">
               <select runat="server" name="currFrmMonth" id="ddljoinMonth" origvalue="05" type="" rel="">
        <option value="0">Month</option>
                        <option value="January">January</option>
                                <option value="February">February</option>
                                <option value="March">March</option>
                                <option value="April">April</option>
                                <option value="May">May</option>
                                <option value="June">June</option>
                                <option value="July">July</option>
                                <option value="August">August</option>
                                <option value="September">September</option>
                                <option value="October">October</option>
                                <option value="November">November</option>
                                <option value="December">December</option>
                    </select>
                    <select runat="server" name="currFrmYear" id="ddljoinYr" origvalue="2016" type="" rel="">
        <option value="0">Year</option>
                        <option value="2016">2016</option>
                                <option value="2015">2015</option>
                                <option value="2014">2014</option>
                                <option value="2013">2013</option>
                                <option value="2012">2012</option>
                                <option value="2011">2011</option>
                                <option value="2010">2010</option>
                                <option value="2009">2009</option>
                                <option value="2008">2008</option>
                                <option value="2007">2007</option>
                                <option value="2006">2006</option>
                                <option value="2005">2005</option>
                                <option value="2004">2004</option>
                                <option value="2003">2003</option>
                                <option value="2002">2002</option>
                                <option value="2001">2001</option>
                                <option value="2000">2000</option>
                                <option value="1999">1999</option>
                                <option value="1998">1998</option>
                                <option value="1997">1997</option>
                                <option value="1996">1996</option>
                                <option value="1995">1995</option>
                                <option value="1994">1994</option>
                                <option value="1993">1993</option>
                                <option value="1992">1992</option>
                                <option value="1991">1991</option>
                                <option value="1990">1990</option>
                                <option value="1989">1989</option>
                                <option value="1988">1988</option>
                                <option value="1987">1987</option>
                                <option value="1986">1986</option>
                                <option value="1985">1985</option>
                                <option value="1984">1984</option>
                                <option value="1983">1983</option>
                                <option value="1982">1982</option>
                                <option value="1981">1981</option>
                                <option value="1980">1980</option>
                                <option value="1979">1979</option>
                                <option value="1978">1978</option>
                                <option value="1977">1977</option>
                                <option value="1976">1976</option>
                                <option value="1975">1975</option>
                                <option value="1974">1974</option>
                                <option value="1973">1973</option>
                                <option value="1972">1972</option>
                                <option value="1971">1971</option>
                                <option value="1970">1970</option>
                                <option value="1969">1969</option>
                                <option value="1968">1968</option>
                                <option value="1967">1967</option>
                                <option value="1966">1966</option>
                    </select>              
					</div>
				</div>			
				<div class="form-actions1 resume-preview-actions text-center clearfix">
		<asp:Button ID="BtnSaveCurrentEmployement" runat="server" Text="Save" CssClass="btn btn-primary" 
                        onclick="BtnSaveCurrentEmployement_Click"></asp:Button>				    <asp:Button ID="Button2" runat="server" Text="Cancel" OnClick="BtnCancel_Click" CssClass="btn btn-warning"></asp:Button>												
				</div>
			</div>
		
																	
		</div>
	</div>
									</div>
									
                                    </asp:Panel>	

                  <asp:Panel ID="PnlContactDetailEdit" runat="server" Visible="false">
									<h5 class="title-general1">
												<span>Edit your Contact Detail:</span>
											</h5>
																								
									<div class="resume-timeline1 row">
																							
										<div class="resume-form">
		<div class="resume-form-general row">
			<div class="col-sm-12">
            
				<div class="form-group">
																		
					<label for="title" class="col-sm-4 control-label">Email Address:</label>
					<div class="col-sm-8">
                    <asp:TextBox ID="TxtEmailAddress" runat="server" Width="100%" placeholder="enter Email address"></asp:TextBox>         
					</div>
				</div>
                <div class="form-group">
																		
					<label for="title" class="col-sm-4 control-label">Secondry Email Address:</label>
					<div class="col-sm-8">
                    <asp:TextBox ID="TxtSecondryEmail" runat="server" Width="100%" placeholder="enter secondry Email address"></asp:TextBox>         
					</div>
				</div>
                <div class="form-group">
																		
					<label for="title" class="col-sm-4 control-label">Phone Number:</label>
					<div class="col-sm-8">
            <asp:TextBox ID="TxtPhoneNumber" runat="server" Width="100%" placeholder="enter phone number"></asp:TextBox>         
					</div>
				</div>
				<div class="form-group">
					<label for="language" class="col-sm-4 control-label">Mobile Number:</label>
					<div class="col-sm-8">
		<asp:TextBox ID="TxtMobileNumber" runat="server" Width="100%" placeholder="enter mobile number"></asp:TextBox>
        this will used by open-campus team.
					</div>
				</div>
				<div class="form-group">
					<label for="highest_degree" class="col-sm-4 control-label">Facebook Profile Url</label>
					<div class="col-sm-8">
                  <asp:TextBox ID="TxtFacebookUrl" runat="server" Width="100%" placeholder="http://www.facebook.com/yourprofilename"></asp:TextBox>               
					</div>
				</div>
                <div class="form-group">
					<label for="highest_degree" class="col-sm-4 control-label">LinkedIn Profile Url</label>
					<div class="col-sm-8">
                   <asp:TextBox ID="TxtLinkedInUrl" runat="server" Width="100%" placeholder="http://www.linkedin.com/profilename"></asp:TextBox>                                  
					</div>
				</div>	
                <div class="form-group">
					<label for="highest_degree" class="col-sm-4 control-label">Google+ Profile Url</label>
					<div class="col-sm-8">
                   <asp:TextBox ID="TxtGooglrUrl" runat="server" Width="100%" placeholder="http://plus.google.com/profilename"></asp:TextBox>                                  
					</div>
				</div>			
				<div class="form-actions1 resume-preview-actions text-center clearfix">
		<asp:Button ID="BtnSaveContactDetail" runat="server" Text="Save" CssClass="btn btn-primary" 
                        onclick="BtnSaveContactDetail_Click"></asp:Button>				    <asp:Button ID="Button3" runat="server" Text="Cancel" OnClick="BtnCancel_Click" CssClass="btn btn-warning"></asp:Button>												
				</div>
			</div>
		
																	
		</div>
	</div>
									</div>
									
                                    </asp:Panel>

                                    <asp:Panel ID="PnlChangePassword" runat="server" Visible="false">
									<h5 class="title-general1">
												<span>Change Account Password:</span>
											</h5>
																								
									<div class="resume-timeline1 row">
																							
										<div class="resume-form">
		<div class="resume-form-general row">
			<div class="col-sm-12">
            
				<div class="form-group">
																		
					<label for="title" class="col-sm-4 control-label">Current Password:</label>
					<div class="col-sm-8">
                    <asp:TextBox ID="TxtCurrentPassword" TextMode="Password" runat="server" Width="100%" placeholder="Enter Current Password"></asp:TextBox>         
					</div>
				</div>
                <div class="form-group">
																		
					<label for="title" class="col-sm-4 control-label">New Password:</label>
					<div class="col-sm-8">
                    <asp:TextBox ID="TxtNewPassword" TextMode="Password" runat="server" Width="100%" placeholder="enter New Password"></asp:TextBox>         
					</div>
				</div>
                <div class="form-group">
																		
					<label for="title" class="col-sm-4 control-label">Confirm New Password:</label>
					<div class="col-sm-8">
            <asp:TextBox ID="TxtConfirmNewPassword" TextMode="Password" runat="server" Width="100%" placeholder="Confirm New Password"></asp:TextBox>         
					</div>
				</div>
				<div class="form-actions1 resume-preview-actions text-center clearfix">
		<asp:Button ID="BtnChangePassword" runat="server" Text="Save" CssClass="btn btn-primary" 
                        onclick="BtnChangePassword_Click"></asp:Button>				    <asp:Button ID="Button4" runat="server" Text="Cancel" OnClick="BtnCancel_Click" CssClass="btn btn-warning"></asp:Button>												
				</div>
			</div>
		
																	
		</div>
	</div>
									</div>
									
                                    </asp:Panel>

                                   
                         		<br /><br />
									</div>
								</div>
							</div>
						</div>
																			
					</article>
                </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            </form>
        </div>
    </div>
</div>
                            </div>
                        </div>
                    </div>
                    <hr />
                </div>
            </div>
        </div>
    </div>
    <asp:HiddenField ID="hfLoginId" runat="server" />
    <asp:HiddenField ID="hfEmpPhoto" runat="server" />
    <asp:HiddenField ID="hfEmployeeId" runat="server" />
</asp:Content>
