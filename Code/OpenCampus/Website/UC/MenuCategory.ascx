﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="MenuCategory.ascx.cs" Inherits="UC_MenuCategory" %>


<div class="member-heading">
				<div class="container-boxed max">
					<div class="member-heading-nav">
		<ul>
			<%--<li id="lnkAllJobs" runat="server"><a href="browse-all-jobs"><i class="fa fa-file-text-o"></i>
           Browse All Jobs</a></li>--%>

	   <li id="lnkJobsByCategory" runat="server"><a href="jobs-by-category"><i class="fa fa-newspaper-o"></i>
    Jobs by Category</a></li>

			<li id="lnkJobsByDesignation" runat="server"><a href="jobs-by-designation"><i class="fa fa-users"></i>
            Jobs by Designation</a></li>

			<li id="lnkJobsBySkills" runat="server"><a href="top-jobs-by-skill"><i class="fa fa-sign-out"></i>
            Jobs by Skills</a></li>

			<li id="lnkJobsByLocation" runat="server"><a href="jobs-by-location"><i class="fa fa-users"></i> Jobs by Location</a></li>
			<li id="lnkJobsByCompany" runat="server"><a href="#"><i class="fa fa-sign-out"></i>
           Jobs by Company</a></li>

		</ul>
					</div>
				</div>
			</div>