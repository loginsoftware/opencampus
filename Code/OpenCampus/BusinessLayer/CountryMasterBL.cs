﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data.Objects;
using DataLayer;


namespace BusinessLayer
{
    public class CountryMasterBL
    {
        public List<CountryMaster> LoadAllCountryMaster()
        {
            DBEntities Obj = new DBEntities();
            return Obj.LoadAllCountryMaster().ToList();
        }
    }
}
