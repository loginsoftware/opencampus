﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using BusinessLayer;

public partial class admin_EmployerDetail : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!Page.IsPostBack)
        {
            TxtFromDate.Text = DateTime.Now.AddDays(-14).ToString("dd-MMM-yyyy");
            TxtToDate.Text = DateTime.Now.ToString("dd-MMM-yyyy");
            LoadCandidateDetail();
        }
    }

    private void LoadCandidateDetail()
    {
        try
        {
            EmployeeMasterBL obj = new EmployeeMasterBL();
            GVEmployer.DataSource = obj.LoadEmployeeMasterBetweenDates(DateTime.Parse(TxtFromDate.Text), DateTime.Parse(TxtToDate.Text), TxtRecruiterName.Text);
            GVEmployer.DataBind();
        }
        catch { }
    }
    protected void BtnSearch_Click(object sender, EventArgs e)
    {
        LoadCandidateDetail();
    }
}