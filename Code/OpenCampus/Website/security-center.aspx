﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.master" AutoEventWireup="true" CodeFile="security-center.aspx.cs" Inherits="Security_Center" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
     <style>
	
	.heading
	{    font-size: 18px;
	}
	.small-heading{
	font-size: 15px;
    font-weight: 100;
	}
	.para{
	font-size: 15px;
	}
	</style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="cph" Runat="Server">
      <div class="noo-page-heading">
				<div class="container-boxed max text-center parallax-content">
					<div class="member-heading-avatar">
						
					</div>
					<div class="page-heading-info ">
						<h1 class="page-title">Security Center</h1>
					</div>
					<div class="page-sub-heading-info">
						We’re <span style="color:
					#80bb03;font-size:19px;">Helping People </span>Find Great Jobs,
and <span style="color:
					#80bb03;font-size:19px;">Helping Employers</span> Build Great Companies
					</div>
				</div> 
				<div class="parallax heading" data-parallax="1" data-parallax_no_mobile="1" data-velocity="0.1"></div>
			</div>
			<div class="member-heading">
				<div class="container-boxed max">
					<div class="member-heading-nav">
						<ul>
							<li><a href="advertise-with-us.aspx"><i class="fa fa-file-text-o"></i> Advertise With Us</a></li>
					        <li><a href="aboutus.aspx"><i class="fa fa-newspaper-o"></i> About Us</a></li>
							<li><a href="contactus.aspx"><i class="fa fa-users"></i> Contact Us</a></li>
							<li><a href="privacy-policy.aspx"><i class="fa fa-sign-out"></i> Privacy Policy</a></li>
							<li><a href="terms-condition.aspx"><i class="fa fa-users"></i> Terms Condition</a></li>
							<li class="active"><a href="security-center.aspx"><i class="fa fa-sign-out"></i> Security Center</a></li>
						</ul>
					</div>
				</div>
			</div>
			<div class="container-wrap">
				<div class="main-content container-boxed max">
					<div class="row">
						<div class="noo-main col-md-12">
							<div class="member-manage" style="margin-bottom: 250px;">
								<h3>Security Center</h3>
							<h5>Welcome to the Open Campus Security Center - </h5>

				<p class="para">	Open Campus is dedicated to providing the safest possible environment for you to search for jobs and manage your career. To assist with that goal, we ask that you keep a few simple security precautions in mind when evaluating job postings on Open Campus and job opportunities that you may receive via email.</p>

	<p class="para">For example, it's possible that you may encounter fraudulent job opportunities when searching for jobs online or you may receive a fraudulent email that has had the sender's email address forged to make it appear as if it was sent from Open Campus. Such practices are a violation of Open Campus's Terms of Use and may be illegal under applicable laws.</p>

	<p class="para">Regrettably, all online companies are susceptible to scams. While Open Campus makes every effort to prevent this abuse, it is not immune to such activity. To help you conduct a safer job search, we've assembled the following security-related articles and resources. Familiarizing yourself with this information will help you better manage your career.</p>

<h5>HOW TO BE A SAFE INTERNET USER?</h5>

	<p class="para">Every Internet site in the world is facing the growing issue of fraudulent usage of information and we want to work with users around the world to stop this practice. Please keep reading to learn more about the warning signs and what you can do.</p>

	<p class="para">Spam email is such a common occurrence today. There are two types of email scams - what's known as 'phishing' and ‘spoofing’. Both practices concern fraudulent email where the 'from address' has been forged to make it appear as if it came from somewhere, or someone, other than the actual source. Below are the warning signs to look for: </p>

<h5>What’s 'phishing' all about – and how do I spot it? </h5>

	<p class="para">Phishing emails are used to fraudulently obtain personal identification and account information. They can also be used to lure the recipient into downloading malicious software. The message will often suggest there are issues with the recipient's account that requires immediate attention. A link will also be provided to a spoof website where the recipient will be asked to provide personal/account information or download malicious software. Open Campus will never ask you to download software in order to access your account or use our services. </p>

<h5>How is it different than 'spoofing'? </h5>

	<p class="para">Spoof emails often include a fraudulent offer of employment and/or the invitation to serve as a go-between for payment processing or money transfers. This scam is primarily directed at a general audience, but it can also reach Open Campus members who have included contact information on their resume. Like with phishing emails, the sender's address is often disguised. </p>

	<p class="para">Beware of fake Agencies/Employer seeking money through cash deposit or wire transfer /other sensitive information. 
We have observed few instances reported to Open Campus that some agencies/individuals are seeking money from jobseeker against a job offer from well-known Brands/Employers/Agencies, whose legitimacy / genuineness can be in question. Usually the contact person seeking such information is with unknown origin with foreign accent. Open Campus advises the users not to rely on the email/offers for employment which is seeking a consideration in cash or in any kind.</p>

<h5>Consumer Advice: How to Avoid Phishing Scams! </h5>

	<p class="para">The number and sophistication of phishing scams sent out to consumers is continuing to increase dramatically. While online banking and e-commerce is very safe, as a general rule you should be careful about giving out your personal financial information over the Internet. The Anti-Phishing Working Group has compiled a list of recommendations that you can use to avoid becoming a victim of these scams.</p>

	<p class="para">Be suspicious of any email with urgent requests for personal financial information
Phishers typically include upsetting or exciting (but false) statements in their emails to get people to react immediately.</p>

	<p class="para">They typically ask for information such as usernames, passwords, credit card numbers, passport number, photograph, date of birth, etc.</p>

	<p class="para">Don't use the links in an email, instant message, or chat to get to any web page if you suspect the message might not be authentic. Instead, call the company on the telephone, or log onto the website directly by typing in the Web address in your browser.</p>

	<p class="para">You should only communicate information such as credit card numbers or account information via a secure website or the telephone.

	<p class="para">Always ensure that you're using a secure website when submitting credit card or other sensitive information via your Web browser.</p>

<h5>If you have more questions, please Contact us at - <a style="color:blue;" href="http://www.opencampus.in/contactus.aspx">http://www.opencampus.in/contactus.aspx</a> </h5>

							</div>
						</div>  
					</div> 
				</div> 
			</div>
</asp:Content>

