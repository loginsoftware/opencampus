﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Candidate.master" AutoEventWireup="true"
    CodeFile="Candidate_profile_basic.aspx.cs" Inherits="Candidate_profile_basic" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
    <style type="text/css">
        .container-boxed.offset
        {
            padding-top: 0px;
        }
        .container{
	width:290px;
	margin-left: -15px;
}
.progress {
	overflow: hidden;
	height: 20px;
	background-color: #ccc;
	border-radius: -1px;
	-webkit-box-shadow: inset 0 1px 2px rgba(0,0,0,.1);
	box-shadow: inset 0 1px 2px rgba(0,0,0,.1);
		margin-bottom: 20px;
}
.progress-bar {
	width: 0;
	height: 100%;
	color: #fff;
	text-align: center;
	background-color: #ee303c;

	
 
}
.progress-striped .progress-bar {
			background-image: -webkit-linear-gradient(45deg,rgba(255,255,255,.15) 25%,transparent 25%,transparent 50%,rgba(255,255,255,.15) 50%,rgba(255,255,255,.15) 75%,transparent 75%,transparent);
			background-image: linear-gradient(45deg,rgba(255,255,255,.15) 25%,transparent 25%,transparent 50%,rgba(255,255,255,.15) 50%,rgba(255,255,255,.15) 75%,transparent 75%,transparent);
			background-size: 40px 40px;
}
.progress.active .progress-bar {
	-webkit-animation: progress-bar-stripes 2s linear infinite;
	animation: progress-bar-stripes 2s linear infinite;
	-moz-animation: progress-bar-stripes 2s linear infinite;
}
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="cph" runat="Server">
    <div class="container-wrap" style="background-color: #E9EBEE;">
        <div class="main-content container-boxed max offset">
            <div class="row">
                <div class="noo-main col-md-12">
                    <div class="form-horizontal" enctype="multipart/form-data">
                        <div class="candidate-profile-form row">
                            <div class="col-sm-3" style="background-color: white;">
                                <div class="noo-sidebar-wrap">
                                 <div class="widget widget_archive">
                                        <h5 class="widget-title" style="font-size:15px;">Profile Completeness</h5>
                                        <div class="container" > 
                    <div class="progress progress-striped">
                      <div id="MtrProfilePercent" runat="server" class="progress-bar progress-bar-danger" role="progressbar" aria-valuenow="80" aria-valuemin="0" aria-valuemax="100" style="width: 0%">
                        <span id="lblProfilePercent" runat="server"> </span>
                      </div>                       
                    </div>                    
                       </div>
                                    </div>
                                    <div class="widget widget_categories">
                                        <h4 class="widget-title">
                                            Profile</h4>
                                        <ul>
                                            <li><a href="Candidate_profile_basic.aspx?type=profilebasic">Profile</a> </li>
                                            <li><a href="Candidate_profile_basic.aspx?type=education">Education</a> </li>
                                            <li><a href="Candidate_profile_basic.aspx?type=designation">Designation</a> </li>
                                         
                                            <li><a href="Candidate_profile_basic.aspx?type=photo">Upload Photo</a> </li>
                                        </ul>
                                    </div>
                                   
                                </div>
                            </div>
                            <div class="col-sm-9" style="background-color: white; box-shadow: 0px 0px 5px 0px grey;">
                                <div id="PnlBasicPofile" visible="false" runat="server" class="col-sm-10">
                                    <div class="form-title">
                                        <h3>
                                            Basic Profile 1</h3>
                                    </div>
                                    <div class="form-group">
                                        <label for="name" class="col-sm-3 control-label">
                                            Full Name</label>
                                        <div class="col-sm-8">
                                            <input id="fullname" runat="server" class="form-control" name="fullname" type="text" />
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label for="profile-email" class="col-sm-3 control-label">
                                            Resume Headline:</label>
                                        <div class="col-sm-8">
                                            <textarea id="resumeheadline" runat="server" class="form-control" cols="20" rows="2"></textarea>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label for="current_job" class="col-sm-3 control-label">
                                            Prefferd Location</label>
                                        <div class="col-sm-8">
                                            <select id="ddlCity" runat="server">
                                                <option value="-1">Select</option>
                                                <option value="2">Ahmedabad</option>
                                                <option value="3">Bengaluru / Bangalore</option>
                                                <option value="4">Chandigarh</option>
                                                <option value="5">Chennai</option>
                                                <option value="6">Delhi</option>
                                                <option value="7">Gurgaon</option>
                                                <option value="8">Hyderabad / Secunderabad</option>
                                                <option value="9">Kolkata</option>
                                                <option value="10">Mumbai</option>
                                                <option value="11">Noida</option>
                                                <option value="12">Pune</option>
                                                <option value="14">Anantapur</option>
                                                <option value="362">Chitoor</option>
                                                <option value="360">Eluru</option>
                                                <option value="359">Gannavaram</option>
                                                <option value="15">Guntakal</option>
                                                <option value="16">Guntur</option>
                                                <option value="265">Kadapa/Cuddapah</option>
                                                <option value="18">Kakinada</option>
                                                <option value="19">Kurnool</option>
                                                <option value="358">Machilipatnam</option>
                                                <option value="363">Nandyal</option>
                                                <option value="20">Nellore</option>
                                                <option value="364">Ongole</option>
                                                <option value="22">Rajahmundry</option>
                                                <option value="365">Tada</option>
                                                <option value="24">Tirupati</option>
                                                <option value="25">Vijayawada</option>
                                                <option value="26">Visakhapatnam</option>
                                                <option value="361">Vizianagaram</option>
                                                <option value="28">Andhra Pradesh - Other</option>
                                                <option value="30">Itanagar</option>
                                                <option value="31">Arunachal Pradesh - Other</option>
                                                <option value="331">Dibrugarh</option>
                                                <option value="33">Guwahati</option>
                                                <option value="34">Silchar</option>
                                                <option value="35">Assam - Other</option>
                                                <option value="37">Bhagalpur</option>
                                                <option value="38">Patna</option>
                                                <option value="39">Bihar - Other</option>
                                                <option value="41">Bhillai</option>
                                                <option value="42">Bilaspur</option>
                                                <option value="334">Korba</option>
                                                <option value="43">Raipur</option>
                                                <option value="247">Raigarh</option>
                                                <option value="44">Chhattisgarh - Other</option>
                                                <option value="46">Panjim / Panaji</option>
                                                <option value="48">Vasco Da Gama</option>
                                                <option value="49">Goa - Other</option>
                                                <option value="51">Ahmedabad</option>
                                                <option value="52">Anand</option>
                                                <option value="53">Ankleshwar</option>
                                                <option value="55">Bharuch</option>
                                                <option value="56">Bhavnagar</option>
                                                <option value="57">Bhuj</option>
                                                <option value="330">Dahej</option>
                                                <option value="332">Gandhidham</option>
                                                <option value="58">Gandhinagar</option>
                                                <option value="59">Gir</option>
                                                <option value="60">Jamnagar</option>
                                                <option value="61">Kandla</option>
                                                <option value="62">Porbandar</option>
                                                <option value="342">Mehsana</option>
                                                <option value="63">Rajkot</option>
                                                <option value="64">Surat</option>
                                                <option value="65">Vadodara / Baroda</option>
                                                <option value="66">Valsad</option>
                                                <option value="67">Vapi</option>
                                                <option value="68">Gujarat - Other</option>
                                                <option value="70">Ambala</option>
                                                <option value="71">Chandigarh</option>
                                                <option value="72">Faridabad</option>
                                                <option value="73">Gurgaon</option>
                                                <option value="74">Hisar</option>
                                                <option value="75">Karnal</option>
                                                <option value="76">Kurukshetra</option>
                                                <option value="77">Panipat</option>
                                                <option value="78">Rohtak</option>
                                                <option value="79">Haryana - Other</option>
                                                <option value="239">Baddi</option>
                                                <option value="81">Dalhousie</option>
                                                <option value="82">Dharmasala</option>
                                                <option value="83">Kulu/Manali</option>
                                                <option value="84">Shimla</option>
                                                <option value="85">Himachal Pradesh - Other</option>
                                                <option value="87">Jammu</option>
                                                <option value="88">Srinagar</option>
                                                <option value="89">Jammu and Kashmir - Other</option>
                                                <option value="91">Bokaro</option>
                                                <option value="92">Dhanbad</option>
                                                <option value="93">Jamshedpur</option>
                                                <option value="94">Ranchi</option>
                                                <option value="95">Jharkhand - Other</option>
                                                <option value="97">Bengaluru / Bangalore</option>
                                                <option value="98">Belgaum</option>
                                                <option value="99">Bellary</option>
                                                <option value="100">Bidar</option>
                                                <option value="348">Davangere</option>
                                                <option value="101">Dharwad</option>
                                                <option value="102">Gulbarga</option>
                                                <option value="103">Hubli</option>
                                                <option value="104">Kolar</option>
                                                <option value="105">Mangalore</option>
                                                <option value="106">Mysoru / Mysore</option>
                                                <option value="107">Karnataka - Other</option>
                                                <option value="109">Calicut</option>
                                                <option value="110">Cochin</option>
                                                <option value="111">Ernakulam</option>
                                                <option value="241">Idukki</option>
                                                <option value="112">Kannur</option>
                                                <option value="242">Kasargode</option>
                                                <option value="113">Kochi</option>
                                                <option value="114">Kollam</option>
                                                <option value="115">Kottayam</option>
                                                <option value="116">Kozhikode</option>
                                                <option value="244">Malappuram</option>
                                                <option value="117">Palakkad</option>
                                                <option value="118">Palghat</option>
                                                <option value="245">Pathanamthitta</option>
                                                <option value="119">Thrissur</option>
                                                <option value="120">Trivandrum</option>
                                                <option value="248">Wayanad</option>
                                                <option value="121">Kerala - Other</option>
                                                <option value="123">Bhopal</option>
                                                <option value="124">Gwalior</option>
                                                <option value="125">Indore</option>
                                                <option value="126">Jabalpur</option>
                                                <option value="335">Katni</option>
                                                <option value="127">Ujjain</option>
                                                <option value="128">Madhya Pradesh - Other</option>
                                                <option value="130">Ahmednagar</option>
                                                <option value="131">Aurangabad</option>
                                                <option value="341">Chandrapur</option>
                                                <option value="132">Jalgaon</option>
                                                <option value="133">Kolhapur</option>
                                                <option value="349">Khopoli</option>
                                                <option value="134">Mumbai</option>
                                                <option value="135">Mumbai Suburbs</option>
                                                <option value="136">Nagpur</option>
                                                <option value="137">Nasik</option>
                                                <option value="138">Navi Mumbai</option>
                                                <option value="139">Pune</option>
                                                <option value="346">Ratnagiri</option>
                                                <option value="140">Solapur</option>
                                                <option value="344">Vasai</option>
                                                <option value="141">Maharashtra - Other</option>
                                                <option value="143">Imphal</option>
                                                <option value="144">Manipur - Other</option>
                                                <option value="146">Shillong</option>
                                                <option value="147">Meghalaya - Other</option>
                                                <option value="149">Aizawl</option>
                                                <option value="150">Mizoram - Other</option>
                                                <option value="152">Dimapur</option>
                                                <option value="153">Nagaland - Other</option>
                                                <option value="155">Bhubaneshwar</option>
                                                <option value="156">Cuttack</option>
                                                <option value="338">Jharsuguda</option>
                                                <option value="157">Paradeep</option>
                                                <option value="158">Puri</option>
                                                <option value="159">Rourkela</option>
                                                <option value="337">Sambalpur</option>
                                                <option value="160">Orissa - Other</option>
                                                <option value="162">Amritsar</option>
                                                <option value="163">Bathinda</option>
                                                <option value="164">Chandigarh</option>
                                                <option value="165">Jalandhar</option>
                                                <option value="166">Ludhiana</option>
                                                <option value="167">Mohali</option>
                                                <option value="168">Pathankot</option>
                                                <option value="169">Patiala</option>
                                                <option value="170">Punjab - Other</option>
                                                <option value="172">Ajmer</option>
                                                <option value="345">Barmer</option>
                                                <option value="240">Bhilwara</option>
                                                <option value="173">Jaipur</option>
                                                <option value="174">Jaisalmer</option>
                                                <option value="175">Jodhpur</option>
                                                <option value="176">Kota</option>
                                                <option value="339">Neemrana</option>
                                                <option value="177">Udaipur</option>
                                                <option value="178">Rajasthan - Other</option>
                                                <option value="180">Gangtok</option>
                                                <option value="181">Sikkim - Other</option>
                                                <option value="183">Chennai</option>
                                                <option value="184">Coimbatore</option>
                                                <option value="185">Cuddalore</option>
                                                <option value="186">Erode</option>
                                                <option value="187">Hosur</option>
                                                <option value="188">Madurai</option>
                                                <option value="189">Nagercoil</option>
                                                <option value="190">Ooty</option>
                                                <option value="191">Salem</option>
                                                <option value="192">Thanjavur</option>
                                                <option value="193">Tirunelveli</option>
                                                <option value="194">Trichy</option>
                                                <option value="195">Tuticorin</option>
                                                <option value="196">Vellore</option>
                                                <option value="197">Tamil Nadu - Other</option>
                                                <option value="249">Adilabad</option>
                                                <option value="368">Bhadrachalam</option>
                                                <option value="369">Godavarikhani</option>
                                                <option value="370">Hanumakonda</option>
                                                <option value="17">Hyderabad / Secunderabad</option>
                                                <option value="286">Karimnagar</option>
                                                <option value="288">Khammam</option>
                                                <option value="371">Kodad</option>
                                                <option value="372">Kothagudem</option>
                                                <option value="373">Mahaboobnagar/Mahabubnagar</option>
                                                <option value="374">Mancherial</option>
                                                <option value="353">Medak</option>
                                                <option value="355">Nalgonda</option>
                                                <option value="21">Nizamabad</option>
                                                <option value="354">Rangareddy</option>
                                                <option value="357">Razole</option>
                                                <option value="375">Sangareddy</option>
                                                <option value="376">Siddipet</option>
                                                <option value="377">Suryapet</option>
                                                <option value="356">Tuni</option>
                                                <option value="27">Warangal</option>
                                                <option value="366">Telangana - Other</option>
                                                <option value="199">Agartala</option>
                                                <option value="200">Tripura - Other</option>
                                                <option value="202">Chandigarh</option>
                                                <option value="203">Dadra &amp; Nagar Haveli - Silvassa</option>
                                                <option value="204">Daman &amp; Diu</option>
                                                <option value="205">Delhi</option>
                                                <option value="243">Lakshadweep</option>
                                                <option value="206">Pondicherry</option>
                                                <option value="208">Agra</option>
                                                <option value="209">Aligarh</option>
                                                <option value="210">Allahabad</option>
                                                <option value="211">Bareilly</option>
                                                <option value="340">Bijnor</option>
                                                <option value="212">Faizabad</option>
                                                <option value="213">Ghaziabad</option>
                                                <option value="214">Gorakhpur</option>
                                                <option value="350">Greater Noida</option>
                                                <option value="215">Kanpur</option>
                                                <option value="216">Lucknow</option>
                                                <option value="217">Mathura</option>
                                                <option value="218">Meerut</option>
                                                <option value="219">Moradabad</option>
                                                <option value="220">Noida</option>
                                                <option value="333">Saharanpur</option>
                                                <option value="221">Varanasi / Banaras</option>
                                                <option value="222">Uttar Pradesh - Other</option>
                                                <option value="224">Dehradun</option>
                                                <option value="336">Haldwani</option>
                                                <option value="343">Kashipur</option>
                                                <option value="225">Roorkee</option>
                                                <option value="226">Uttaranchal - Other</option>
                                                <option value="228">Asansol</option>
                                                <option value="347">Burdwan</option>
                                                <option value="229">Durgapur</option>
                                                <option value="230">Haldia</option>
                                                <option value="231">Kharagpur</option>
                                                <option value="232">Kolkata</option>
                                                <option value="233">Siliguri</option>
                                                <option value="234">West Bengal - Other</option>
                                                <option value="9999">Other</option>
                                            </select>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label for="birthday" class="col-sm-3 control-label">
                                            Birthday</label>
                                        <div class="col-sm-8">
                                            <input type="text" class="form-control" id="birthday" runat="server" value="1989-01-25"
                                                name="birthday">
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label for="gender" class="col-sm-3 control-label">
                                            Gender</label>
                                        <div class="col-sm-8">
                                            <select id="ddlgender" runat="server">
                                                <option value="-1">Select</option>
                                                <option value="Male">Male</option>
                                                <option value="Female">Female</option>
                                            </select>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label for="exper" class="col-sm-3 control-label">
                                            Total Experience:
                                        </label>
                                        <div class="col-sm-8">
                                            <select id="ddlexperienceYear" runat="server">
                                                <option value="-1">Year</option>
                                                <option value="99">Fresher</option>
                                                <option value="00">0</option>
                                                <option value="01">1</option>
                                                <option value="02">2</option>
                                                <option value="03">3</option>
                                                <option value="04">4</option>
                                                <option value="05">5</option>
                                                <option value="06">6</option>
                                                <option value="07">7</option>
                                                <option value="08">8</option>
                                                <option value="09">9</option>
                                                <option value="10">10</option>
                                                <option value="11">11</option>
                                                <option value="12">12</option>
                                                <option value="13">13</option>
                                                <option value="14">14</option>
                                                <option value="15">15</option>
                                                <option value="16">16</option>
                                                <option value="17">17</option>
                                                <option value="18">18</option>
                                                <option value="19">19</option>
                                                <option value="20">20</option>
                                                <option value="21">21</option>
                                                <option value="22">22</option>
                                                <option value="23">23</option>
                                                <option value="24">24</option>
                                                <option value="25">25</option>
                                                <option value="26">26</option>
                                                <option value="27">27</option>
                                                <option value="28">28</option>
                                                <option value="29">29</option>
                                                <option value="30">30</option>
                                                <option value="31">30+</option>
                                            </select>
                                            Years
                                            <select id="ddlexperienceMonth" runat="server">
                                                <option value="-1">Month</option>
                                                <option value="00">0</option>
                                                <option value="01">1</option>
                                                <option value="02">2</option>
                                                <option value="03">3</option>
                                                <option value="04">4</option>
                                                <option value="05">5</option>
                                                <option value="06">6</option>
                                                <option value="07">7</option>
                                                <option value="08">8</option>
                                                <option value="09">9</option>
                                                <option value="10">10</option>
                                                <option value="11">11</option>
                                            </select>
                                            Months
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label for="salary" class="col-sm-3 control-label">
                                            Annual Salary</label>
                                        <div class="col-sm-8">
                                            <select id="ddlctcLacs" runat="server">
                                                <option value="-1">Lacs</option>
                                                <option value="0">0</option>
                                                <option value="1">1</option>
                                                <option value="2">2</option>
                                                <option value="3">3</option>
                                                <option value="4">4</option>
                                                <option value="5">5</option>
                                                <option value="6">6</option>
                                                <option value="7">7</option>
                                                <option value="8">8</option>
                                                <option value="9">9</option>
                                                <option value="10">10</option>
                                                <option value="11">11</option>
                                                <option value="12">12</option>
                                                <option value="13">13</option>
                                                <option value="14">14</option>
                                                <option value="15">15</option>
                                                <option value="16">16</option>
                                                <option value="17">17</option>
                                                <option value="18">18</option>
                                                <option value="19">19</option>
                                                <option value="20">20</option>
                                                <option value="21">21</option>
                                                <option value="22">22</option>
                                                <option value="23">23</option>
                                                <option value="24">24</option>
                                                <option value="25">25</option>
                                                <option value="26">26</option>
                                                <option value="27">27</option>
                                                <option value="28">28</option>
                                                <option value="29">29</option>
                                                <option value="30">30</option>
                                                <option value="31">31</option>
                                                <option value="32">32</option>
                                                <option value="33">33</option>
                                                <option value="34">34</option>
                                                <option value="35">35</option>
                                                <option value="36">36</option>
                                                <option value="37">37</option>
                                                <option value="38">38</option>
                                                <option value="39">39</option>
                                                <option value="40">40</option>
                                                <option value="41">41</option>
                                                <option value="42">42</option>
                                                <option value="43">43</option>
                                                <option value="44">44</option>
                                                <option value="45">45</option>
                                                <option value="46">46</option>
                                                <option value="47">47</option>
                                                <option value="48">48</option>
                                                <option value="49">49</option>
                                                <option value="50">50</option>
                                                <option value="51">50+</option>
                                                <option value="93">55+</option>
                                                <option value="94">60+</option>
                                                <option value="95">65+</option>
                                                <option value="96">70+</option>
                                                <option value="97">75+</option>
                                                <option value="98">80+</option>
                                                <option value="99">85+</option>
                                                <option value="100">90+</option>
                                                <option value="101">95+</option>
                                                <option value="102">100+</option>
                                            </select>
                                            Lakhs
                                            <select id="ddlctcThousands" runat="server">
                                                <option value="-1">Thousands</option>
                                                <option value="0">0</option>
                                                <option value="5">5</option>
                                                <option value="10">10</option>
                                                <option value="15">15</option>
                                                <option value="20">20</option>
                                                <option value="25">25</option>
                                                <option value="30">30</option>
                                                <option value="35">35</option>
                                                <option value="40">40</option>
                                                <option value="45">45</option>
                                                <option value="50">50</option>
                                                <option value="55">55</option>
                                                <option value="60">60</option>
                                                <option value="65">65</option>
                                                <option value="70">70</option>
                                                <option value="75">75</option>
                                                <option value="80">80</option>
                                                <option value="85">85</option>
                                                <option value="90">90</option>
                                                <option value="95">95</option>
                                            </select>
                                            Thousands <span class="pt5 f11 dspIn">
                                                <input type="radio" runat="server" id="ctctype_i" class="chkRadio">
                                                <label>
                                                    Indian Rupees</label>
                                                <input type="radio" runat="server" id="ctctype_u" class="chkRadio">
                                                <label>
                                                    US Dollars</label></span>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label for="mobile" class="col-sm-3 control-label">
                                            Mobile Number</label>
                                        <div class="col-sm-8">
                                            <input type="text" runat="server" class="form-control" id="mobile" value="" name="mobile">
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label for="landline" class="col-sm-3 control-label">
                                            Landline Number</label>
                                        <div class="col-sm-8">
                                            <input type="text" runat="server" class="form-control" id="landline" value="" name="landline">
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label for="address" class="col-sm-3 control-label">
                                            Email Address</label>
                                        <div class="col-sm-8">
                                            <input type="text" runat="server" class="form-control" id="email" name="email">
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label for="address" class="col-sm-3 control-label">
                                            Permanent Address</label>
                                        <div class="col-sm-8">
                                            <input type="text" runat="server" class="form-control" id="address" value="Los Angeles, United States"
                                                name="address">
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label for="city" class="col-sm-3 control-label">
                                            Hometown/City</label>
                                        <div class="col-sm-8">
                                            <input type="text" runat="server" class="form-control" id="city" name="city">
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label for="pincode" class="col-sm-3 control-label">
                                            Pincode</label>
                                        <div class="col-sm-8">
                                            <input type="text" runat="server" class="form-control" id="pincode" name="pincode">
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label for="marital" class="col-sm-3 control-label">
                                            Marital Status</label>
                                        <div class="col-sm-8">
                                            <select runat="server" id="ddlmaritalStatus">
                                                <option value="-1">Select</option>
                                                <option value="Single">Single</option>
                                                <option value="Married">Married</option>
                                                <option value="Widowed">Widowed</option>
                                                <option value="Divorced">Divorced</option>
                                                <option value="Separated">Separated</option>
                                                <option value="Other">Other</option>
                                            </select>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label for="keyskill" class="col-sm-3 control-label">
                                            Key Skill</label>
                                        <div class="col-sm-8">
                                            <input type="text" runat="server" class="form-control" id="keyskill" value="" name="keyskill">
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <asp:Button ID="BtnSave" CssClass="btn btn-primary" runat="server" Text="Save My Profile"
                                            OnClick="BtnSave_Click" />
                                    </div>
                                </div>
                                <div id="PnlEmpDesignation" runat="server" visible="false">
                                    <div class="form-title">
                                        <h3>
                                            Add Current Employer/ Designation</h3>
                                    </div>
                                    <div class="form-group">
                                        <label for="name" class="col-sm-3 control-label">
                                            Company</label>
                                        <div class="col-sm-8">
                                            <input id="company" runat="server" class="form-control" type="text" />
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label for="name" class="col-sm-3 control-label">
                                            Status</label>
                                        <div class="col-sm-8">
                                            <asp:RadioButtonList ID="rbStatus" RepeatDirection="Horizontal" runat="server">
                                                <asp:ListItem Value="Current Employer">Current Employer</asp:ListItem>
                                                <asp:ListItem Value="Previous Employer">Previous Employer</asp:ListItem>
                                                <asp:ListItem Value="Other Employer">Other Employer</asp:ListItem>
                                            </asp:RadioButtonList>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label for="name" class="col-sm-3 control-label">
                                            Duration</label>
                                        <div class="col-sm-8">
                                            <p>
                                                <span class="frErr">
                                                    <select name="startMonth" runat="server" id="startMonth" errid="sDateErr" formrowid="startDateFormRow"
                                                        class="">
                                                        <option value="-1">Month</option>
                                                        <option value="1">Jan</option>
                                                        <option value="2">Feb</option>
                                                        <option value="3">Mar</option>
                                                        <option value="4">Apr</option>
                                                        <option value="5">May</option>
                                                        <option value="6">Jun</option>
                                                        <option value="7">Jul</option>
                                                        <option value="8">Aug</option>
                                                        <option value="9">Sep</option>
                                                        <option value="10">Oct</option>
                                                        <option value="11">Nov</option>
                                                        <option value="12">Dec</option>
                                                    </select>
                                                </span><span class="frErr ml8">
                                                    <select name="startYear" runat="server" id="startYear" errid="sDateErr" formrowid="startDateFormRow"
                                                        class="">
                                                        <option value="-1">Year</option>
                                                        <option value="2016">2016</option>
                                                        <option value="2015">2015</option>
                                                        <option value="2014">2014</option>
                                                        <option value="2013">2013</option>
                                                        <option value="2012">2012</option>
                                                        <option value="2011">2011</option>
                                                        <option value="2010">2010</option>
                                                        <option value="2009">2009</option>
                                                        <option value="2008">2008</option>
                                                        <option value="2007">2007</option>
                                                        <option value="2006">2006</option>
                                                        <option value="2005">2005</option>
                                                        <option value="2004">2004</option>
                                                        <option value="2003">2003</option>
                                                        <option value="2002">2002</option>
                                                        <option value="2001">2001</option>
                                                        <option value="2000">2000</option>
                                                        <option value="1999">1999</option>
                                                        <option value="1998">1998</option>
                                                        <option value="1997">1997</option>
                                                        <option value="1996">1996</option>
                                                        <option value="1995">1995</option>
                                                        <option value="1994">1994</option>
                                                        <option value="1993">1993</option>
                                                        <option value="1992">1992</option>
                                                        <option value="1991">1991</option>
                                                        <option value="1990">1990</option>
                                                        <option value="1989">1989</option>
                                                        <option value="1988">1988</option>
                                                        <option value="1987">1987</option>
                                                        <option value="1986">1986</option>
                                                        <option value="1985">1985</option>
                                                        <option value="1984">1984</option>
                                                        <option value="1983">1983</option>
                                                        <option value="1982">1982</option>
                                                        <option value="1981">1981</option>
                                                        <option value="1980">1980</option>
                                                        <option value="1979">1979</option>
                                                        <option value="1978">1978</option>
                                                        <option value="1977">1977</option>
                                                        <option value="1976">1976</option>
                                                        <option value="1975">1975</option>
                                                        <option value="1974">1974</option>
                                                        <option value="1973">1973</option>
                                                        <option value="1972">1972</option>
                                                        <option value="1971">1971</option>
                                                        <option value="1970">1970</option>
                                                    </select>
                                                </span><span class="f11 fl p_4_Form">To</span> <span class="frErr">
                                                    <select name="endMonth" runat="server" id="endMonth" errid="eDateErr" formrowid="endDateFormRow"
                                                        class="">
                                                        <option value="-1">Month</option>
                                                        <option value="1">Jan</option>
                                                        <option value="2">Feb</option>
                                                        <option value="3">Mar</option>
                                                        <option value="4">Apr</option>
                                                        <option value="5">May</option>
                                                        <option value="6">Jun</option>
                                                        <option value="7">Jul</option>
                                                        <option value="8">Aug</option>
                                                        <option value="9">Sep</option>
                                                        <option value="10">Oct</option>
                                                        <option value="11">Nov</option>
                                                        <option value="12">Dec</option>
                                                    </select></span> <span class="frErr ml8">
                                                        <select name="endYear" runat="server" id="endYear" errid="eDateErr" formrowid="endDateFormRow"
                                                            class=" bdrinp1err">
                                                            <option value="-1">Year</option>
                                                            <option value="0">Present</option>
                                                            <option value="2016">2016</option>
                                                            <option value="2015">2015</option>
                                                            <option value="2014">2014</option>
                                                            <option value="2013">2013</option>
                                                            <option value="2012">2012</option>
                                                            <option value="2011">2011</option>
                                                            <option value="2010">2010</option>
                                                            <option value="2009">2009</option>
                                                            <option value="2008">2008</option>
                                                            <option value="2007">2007</option>
                                                            <option value="2006">2006</option>
                                                            <option value="2005">2005</option>
                                                            <option value="2004">2004</option>
                                                            <option value="2003">2003</option>
                                                            <option value="2002">2002</option>
                                                            <option value="2001">2001</option>
                                                            <option value="2000">2000</option>
                                                            <option value="1999">1999</option>
                                                            <option value="1998">1998</option>
                                                            <option value="1997">1997</option>
                                                            <option value="1996">1996</option>
                                                            <option value="1995">1995</option>
                                                            <option value="1994">1994</option>
                                                            <option value="1993">1993</option>
                                                            <option value="1992">1992</option>
                                                            <option value="1991">1991</option>
                                                            <option value="1990">1990</option>
                                                            <option value="1989">1989</option>
                                                            <option value="1988">1988</option>
                                                            <option value="1987">1987</option>
                                                            <option value="1986">1986</option>
                                                            <option value="1985">1985</option>
                                                            <option value="1984">1984</option>
                                                            <option value="1983">1983</option>
                                                            <option value="1982">1982</option>
                                                            <option value="1981">1981</option>
                                                            <option value="1980">1980</option>
                                                            <option value="1979">1979</option>
                                                            <option value="1978">1978</option>
                                                            <option value="1977">1977</option>
                                                            <option value="1976">1976</option>
                                                            <option value="1975">1975</option>
                                                            <option value="1974">1974</option>
                                                            <option value="1973">1973</option>
                                                            <option value="1972">1972</option>
                                                            <option value="1971">1971</option>
                                                            <option value="1970">1970</option>
                                                        </select></span> <i id="sDateErr-error" style="display: none;"></i><i id="eDateErr-error"
                                                            style="display: none;">Please specify a valid date range</i>
                                            </p>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label for="name" class="col-sm-3 control-label">
                                            Designation</label>
                                        <div class="col-sm-8">
                                            <input id="designation" runat="server" class="form-control" type="text" />
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label for="name" class="col-sm-3 control-label">
                                            Job Profile</label>
                                        <div class="col-sm-8">
                                            <input id="jobprofile" runat="server" class="form-control" type="text" />
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label for="name" class="col-sm-3 control-label">
                                            Notice Period</label>
                                        <div class="col-sm-8">
                                            <select runat="server" id="ddlnoticePeriod" class="w145">
                                                <option value="-1">Select</option>
                                                <option value="6">Serving Notice Period</option>
                                                <option value="1">15 Days or less</option>
                                                <option value="2">1 Months</option>
                                                <option value="3">2 Months</option>
                                                <option value="4">3 Months</option>
                                                <option value="5">More than 3 Months</option>
                                            </select>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                       
                                        <div class="col-sm-8">
                                            <asp:Button ID="BtnSaveDesignation" CssClass="btn btn-primary" runat="server" Text="Save"
                                                OnClick="BtnSaveDesignation_Click" />
                                        </div>
                                    </div>
                                </div>
                                  <div id="PnlEducation" runat="server" visible="false">
                                   <div class="form-title">
                                        <h3>
                                          Education Detail</h3>
                                          
                                    </div>
                                     <div class="form-group">
                                        <label for="name" class="col-sm-3 control-label">
                          Basic / Graduation:</label>
                                        <div class="col-sm-8">
                                          <select runat="server" clientidmode="Static" id="ugcourseId" class="w257" name="ugcourseId" valtype="ugcourseId" onchange="populateSpec('ugcourseId','ugspecId','ug');notPursuingGraduation();showHideOtherBox('ugcourseId','ougCid');showHideOtherBox('ugcourseId','ougSid');">
                                          <option value="-1">Select</option>    <option value="1">Not Pursuing Graduation</option>    <option value="2">B.A</option>    <option value="3">B.Arch</option>    <option value="4">BCA</option>    <option value="5">B.B.A</option>    <option value="6">B.Com</option>    <option value="7">B.Ed</option>    <option value="8">BDS</option>    <option value="9">BHM</option>    <option value="10">B.Pharma</option>    <option value="11">B.Sc</option>    <option value="12">B.Tech/B.E.</option>    <option value="13">LLB</option>    <option value="14">MBBS</option>    <option value="15">Diploma</option>    <option value="16">BVSC</option>    <option value="9999">Other</option>   </select>
                                        </div>
                                    </div>
                                      <div class="form-group">
                                      
                                        <div class="col-sm-8">
                                            <asp:RadioButtonList ID="rbUGStatus" runat="server" RepeatDirection="Horizontal">
                                            <asp:ListItem Value="Full Time" Selected="True">Full Time</asp:ListItem>
                                            <asp:ListItem Value="Part Time">Part Time</asp:ListItem>
                                            <asp:ListItem Value="Correspondence/Distance Learning">Correspondence/Distance Learning</asp:ListItem>
                                            </asp:RadioButtonList>
                                        </div>
                                    </div>
                                     <div class="form-group">
                                        <label for="name" class="col-sm-3 control-label">
                                          Specialization</label>
                                        <div class="col-sm-8">
                                        <select runat="server" clientidmode="Static" id="ugspecId" class="w257 bdrinp1err" name="ugspecId" valtype="ugspecId" onchange="getSelectedValue(this.id);">
	<option value="-1">Select</option>
	<option value="12.1">Agriculture</option>
	<option value="12.2">Automobile</option>
	<option value="12.3">Aviation</option>
	<option value="12.4">Bio-Chemistry/Bio-Technology</option>
	<option value="12.5">Biomedical</option>
	<option value="12.6">Ceramics</option>
	<option value="12.7">Chemical</option>
	<option value="12.8">Civil</option>
	<option value="12.9">Computers</option>
	<option value="12.10">Electrical</option>
	<option value="12.11">Electronics/Telecommunication</option>
	<option value="12.12">Energy</option>
	<option value="12.13">Environmental</option>
	<option value="12.14">Instrumentation</option>
	<option value="12.15">Marine</option>
	<option value="12.16">Mechanical</option>
	<option value="12.17">Metallurgy</option>
	<option value="12.18">Mineral</option>
	<option value="12.19">Mining</option>
	<option value="12.20">Nuclear</option>
	<option value="12.21">Paint/Oil</option>
	<option value="12.22">Petroleum</option>
	<option value="12.23">Plastics</option>
	<option value="12.24">Production/Industrial</option>
	<option value="12.25">Textile</option>
	<option value="12.26">Other Engineering</option>
	<option value="12.9999">Other</option>
</select>
                                            <asp:HiddenField ID="hfugspecId" ClientIDMode="Static" runat="server" />
                                        </div>
                                    </div>
                                     <div class="form-group">
                                        <label for="name" class="col-sm-3 control-label">
                                            University/Institute:</label>
                                        <div class="col-sm-8">
                                           <select runat="server" clientidmode="Static" id="uginstituteId" runat="server">    <option value="-1">Select</option>    <option value="272">Agra University</option>    <option value="1001">Air Hostess Academy (AHA)</option>    <option value="139">Alagappa University</option>    <option value="2">Aligarh Muslim University (AMU)</option>    <option value="140">All India Institute of Management Studies</option>    <option value="1">All India Institute of Medical Sciences (AIIMS), Delhi</option>    <option value="138">All India Management Association (AIMA)</option>    <option value="3">Allahabad University</option>    <option value="1002">Alliance Business Academy (ABA)</option>    <option value="5">Amity Business School (ABS)</option>    <option value="2003">Amity Law School, Noida</option>    <option value="2004">Amity School of Fashion Technology, Noida</option>    <option value="1003">Amity University</option>    <option value="4">Amravati University</option>    <option value="2005">Amrita Institute of Medical Sciences &amp; Research Centre, Kochi</option>    <option value="6">Andhra University</option>    <option value="7">Anna University</option>    <option value="8">Annamalai University</option>    <option value="9">Apeejay School Of Marketing (ASM)</option>    <option value="10">Aptech</option>    <option value="1004">Arena Multimedia</option>    <option value="11">Armed Forces Medical College (AFMC), Pune</option>    <option value="1005">Asansol Engineering College (AEC)</option>    <option value="1006">Asia Pacific Institute of Management (APIM)</option>    <option value="1007">Assam Engineering College (AEC)</option>    <option value="141">Assam University</option>    <option value="1008">Avadh University</option>    <option value="2006">B J Medical College, Pune</option>    <option value="151">B N Mandal University (BNMU)</option>    <option value="1009">Babasaheb Ambedkar Marathwada University (BAMU), Aurangabad</option>    <option value="142">Babasaheb Bhimrao Ambedkar University (BBAU), Lucknow</option>    <option value="12">Banaras Hindu University (BHU), Varanasi</option>    <option value="1010">Banasthali University</option>    <option value="143">Bangalore Institute Of Technology (BIT)</option>    <option value="13">Bangalore Medical College (BMC)</option>    <option value="14">Bangalore University</option>    <option value="144">Barkatullah University</option>    <option value="145">Bengal Engineering College (BEC)</option>    <option value="146">Berhampur University</option>    <option value="1011">Bhagalpur University</option>    <option value="147">Bharathidasan University, Trichy</option>    <option value="1012">Bharatiya Vidya Bhavan (BVB)</option>    <option value="15">Bharathiar University, Coimbatore</option>    <option value="148">Bhavnagar University</option>    <option value="149">Bhopal University</option>    <option value="1013">Bihar University</option>    <option value="1014">Biju Patnaik University of Technology (BPUT)</option>    <option value="1015">Bikaner University</option>    <option value="150">Birla Institute of Management Technology (BIMT)</option>    <option value="17">Birla Institute of Technology (BIT), Ranchi</option>    <option value="16">Birla Institute of Technology and Science (BITS), Pilani</option>    <option value="18">Board of Technical Education (BTE)</option>    <option value="152">Bundelkhand University</option>    <option value="153">Burdwan University</option>    <option value="1017">CIPET</option>    <option value="154">Calcutta Medical College (CMC)</option>    <option value="20">Calcutta University</option>    <option value="19">Calicut University</option>    <option value="21">Centre for Development of Advanced Computing (CDAC)</option>    <option value="1016">Ch Charan Singh University (CCSU), Meerut</option>    <option value="2007">Chandigarh College of Architecture, Chandigarh</option>    <option value="22">Chennai University</option>    <option value="155">Chhatrapati Shahu Ji Maharaj University (CSJMU)</option>    <option value="2008">Christ College, Bangalore</option>    <option value="23">Christian College, Lucknow</option>    <option value="24">Christian Medical College (CMC), Vellore</option>    <option value="2009">Christian Medical College(CMC, Ludhiana), Ludhiana</option>    <option value="157">Cochin University of Science and Technology (CUST)</option>    <option value="158">College of Engineering and Technology (CET), Bhubaneshwar</option>    <option value="2000">College of Engineering, Anna University, Guindy</option>    <option value="273">DAV Institute of Management (DAVIM), Faridabad</option>    <option value="36">DOEACC</option>    <option value="1018">Deen Dayal Upadhyay Gorakhpur University (DDUGU)</option>    <option value="26">Delhi College of Engineering (DCE), Delhi</option>    <option value="25">Delhi University - College of Art</option>    <option value="27">Delhi University - Daulat Ram College </option>    <option value="28">Delhi University - Hansraj College</option>    <option value="29">Delhi University - Hindu College </option>    <option value="30">Delhi University - Lady Shree Ram College (LSR)</option>    <option value="31">Delhi University - Miranda House</option>    <option value="2010">Delhi University - Ramjas College</option>    <option value="32">Delhi University - Shri Ram College of Commerce (SRCC)</option>    <option value="2011">Delhi University - Sri Venkateswara College</option>    <option value="33">Delhi University - St Stephens College</option>    <option value="34">Delhi University - Other</option>    <option value="35">Devi Ahilya Vishwa Vidhyalaya (DAVV), Indore</option>    <option value="1019">Dibrugarh University</option>    <option value="108">Dr. B.R. Ambedkar National Institute of Technology (NIT), Jalandhar</option>    <option value="160">Dr. Bhim Rao Ambedkar University (BRAU), Bihar</option>    <option value="161">Dr. Harisingh Gour University (HGU), Sagar</option>    <option value="162">Dr. MGR Medical University (MGRMU), Chennai</option>    <option value="1020">EMPI Business School</option>    <option value="2012">Elphinstone College, Mumbai</option>    <option value="37">Faculty Of Management Studies</option>    <option value="163">Faculty Of Management Studies (FMS), Delhi</option>    <option value="2013">Faculty of Architecture &amp; Ekistics, JMI, Delhi</option>    <option value="2014">Faculty of Law, Aligarh Muslim University, Aligarh</option>    <option value="38">Fergusson College, Pune</option>    <option value="39">Film and Television Institute of India (FTII), Pune</option>    <option value="164">Fore School of Management (FSM), Delhi</option>    <option value="1021">Fortune Institute of International Business (FIIB)</option>    <option value="2015">Gargi College, Delhi</option>    <option value="166">Goa Institute of Management (GIM)</option>    <option value="40">Goa University</option>    <option value="167">Goenka College of Commerce and Business Administration (GCCBA)</option>    <option value="168">Government College of Engineering (GCE), Pune</option>    <option value="169">Government College of Science (GCS), Raipur</option>    <option value="170">Government Medical College and Hospital (GMCH), Chandigarh</option>    <option value="2016">Grant Medical College, Mumbai</option>    <option value="1097">Great Lakes Institute of Management (GLIM)</option>    <option value="171">Gujarat Agricultural University (GAU)</option>    <option value="2017">Gujarat National Law University, Gandhinagar</option>    <option value="172">Gujarat University</option>    <option value="173">Gulbarga University</option>    <option value="174">Guru Ghasidas University (GGU)</option>    <option value="175">Guru Gobind Singh Indraprastha University (GGSIU)</option>    <option value="1022">Guru Jambheshwar University of Science and Technology (GJUST), Hisar</option>    <option value="176">Guru Nanak Dev University (GNDU)</option>    <option value="1023">Gurukula Kangri University (GKU)</option>    <option value="177">Guwahati Medical College and Hospital (GMCH)</option>    <option value="178">Guwahati University</option>    <option value="165">HNB Garhwal University (HNBGU)</option>    <option value="179">Haldia Institute of Technology (HIT)</option>    <option value="180">Hidayatullah National Law University (HNLU)</option>    <option value="181">Himachal Pradesh University (HPU)</option>    <option value="41">Hyderabad University</option>    <option value="1024">Indian Business Academy (IBA)</option>    <option value="1026">Indian Institute Of Hardware Technology (IIHT)</option>    <option value="1029">Indian Institute Of Packaging (IIP)</option>    <option value="193">Indian Institute of Bankers (IIB)</option>    <option value="1025">Indian Institute of Finance (IIF)</option>    <option value="194">Indian Institute of Foreign Trade (IIFT)</option>    <option value="55">Indian Institute of Hotel Management (IIHM), Aurangabad</option>    <option value="184">Indian Institute of Management (IIM), Ahmedabad</option>    <option value="185">Indian Institute of Management (IIM), Bangalore</option>    <option value="187">Indian Institute of Management (IIM), Indore</option>    <option value="186">Indian Institute of Management (IIM), Kolkatta</option>    <option value="188">Indian Institute of Management (IIM), Kozhikode</option>    <option value="189">Indian Institute of Management (IIM), Lucknow</option>    <option value="56">Indian Institute of Mass Communication (IIMC), Delhi</option>    <option value="1027">Indian Institute of Materials Management (IIMM)</option>    <option value="1028">Indian Institute of Modern Management (IIMM), Pune</option>    <option value="195">Indian Institute of Planning And Management (IIPM)</option>    <option value="196">Indian Institute of Science (IIS), Bangalore</option>    <option value="47">Indian Institute of Social Welfare and Business Management (IISWBM)</option>    <option value="2018">Indian Institute of Technology (IIT), Bhuvaneshwar</option>    <option value="48">Indian Institute of Technology (IIT), Chennai</option>    <option value="49">Indian Institute of Technology (IIT), Delhi</option>    <option value="2020">Indian Institute of Technology (IIT), Gandhinagar</option>    <option value="50">Indian Institute of Technology (IIT), Guwahati</option>    <option value="2021">Indian Institute of Technology (IIT), Hyderabad</option>    <option value="2019">Indian Institute of Technology (IIT), Indore</option>    <option value="2022">Indian Institute of Technology (IIT), Jodhpur</option>    <option value="51">Indian Institute of Technology (IIT), Kanpur</option>    <option value="52">Indian Institute of Technology (IIT), Kharagpur</option>    <option value="2023">Indian Institute of Technology (IIT), Mandi</option>    <option value="53">Indian Institute of Technology (IIT), Mumbai</option>    <option value="2024">Indian Institute of Technology (IIT), Patna</option>    <option value="54">Indian Institute of Technology (IIT), Roorkee</option>    <option value="2025">Indian Institute of Technology (IIT), Ropar</option>    <option value="190">Indian Law Society Law College (ILSLC), Pune</option>    <option value="202">Indian School of Business (ISB), Hyderabad</option>    <option value="203">Indian School of Mines (ISM), Dhanbad</option>    <option value="197">Indian Statistical Institute (ISI), Bangalore</option>    <option value="198">Indian Statistical Institute (ISI), Delhi</option>    <option value="57">Indian Statistical Institute (ISI), Kolkatta</option>    <option value="45">Indira Gandhi National Open University (IGNOU)</option>    <option value="2026">Indraprastha College for Women, Delhi</option>    <option value="1030">Industrial Training Institute (ITI)</option>    <option value="1031">Institute for Technology and Management (ITM)</option>    <option value="1032">Institute of Business Management and Research (IBMR)</option>    <option value="59">Institute of Chartered Accountant of India (ICAI)</option>    <option value="42">Institute of Chartered Financial Analysts of India (ICFAI)</option>    <option value="199">Institute of Company Secretaries of India (ICSI)</option>    <option value="44">Institute of Cost and Works Accountants of India (ICWAI)</option>    <option value="58">Institute of Hotel Management (IHM)</option>    <option value="2027">Institute of Hotel Management (IHM), Ahmedabad</option>    <option value="2028">Institute of Hotel Management (IHM), Aurangabad</option>    <option value="2029">Institute of Hotel Management (IHM), Bangalore</option>    <option value="2030">Institute of Hotel Management (IHM), Chennai</option>    <option value="2031">Institute of Hotel Management (IHM), Delhi</option>    <option value="2032">Institute of Hotel Management (IHM), Kolkata</option>    <option value="1033">Institute of Management Education (IME)</option>    <option value="1034">Institute of Management Studies (IMS), Dehradun</option>    <option value="1035">Institute of Management Studies (IMS), Ghaziabad</option>    <option value="192">Institute of Management Technology (IMT), Ghaziabad</option>    <option value="1036">Institute of Marketing and Management (IMM)</option>    <option value="2033">Institute of Medical Sciences, Varanasi</option>    <option value="1037">Institute of Productivity and Management (IPM)</option>    <option value="201">Institute of Rural Management (IRMA), Anand</option>    <option value="204">Institute of Technology, Banaras Hindu University</option>    <option value="137">International Institute of Information Technology (IIIT), Allahabad</option>    <option value="182">International Institute of Information Technology (IIIT), Bangalore</option>    <option value="46">International Institute of Information Technology (IIIT), Hyderabad</option>    <option value="183">International Institute of Information Technology (IIIT), Kolkatta</option>    <option value="200">International Management Institute (IMI), Delhi</option>    <option value="1038">International School of Business and Media (ISBM)</option>    <option value="2034">J.D. Birla Institute, Kolkata</option>    <option value="60">JJ Institute of Applied Art (JJIAA), Mumbai</option>    <option value="205">Jabalpur University</option>    <option value="206">Jadavpur University</option>    <option value="1039">Jagan Institute of Management Studies (JIMS)</option>    <option value="1040">Jaipuria Institute of Management (JIMS)</option>    <option value="1041">Jamia Hamdard</option>    <option value="61">Jamia Milia Islamia (JMI)</option>    <option value="1042">Jammu University</option>    <option value="207">Jamnalal Bajaj Institute of Management Studies (JLBIM)</option>    <option value="1043">Jawaharlal Nehru Technological University (JNTU)</option>    <option value="62">Jawaharlal Nehru University (JNU)</option>    <option value="2035">Jesus &amp; Mary College (JMC), Delhi</option>    <option value="1044">Jetking</option>    <option value="63">Jipmer, Pondicherry</option>    <option value="208">Jiwaji University</option>    <option value="2036">K J Somaiya College of Arts &amp; Commerce, Mumbai</option>    <option value="209">K.J. Somaiya Institute of Management Studies and Research (KJSIMSR), Mumbai</option>    <option value="210">Kakatiya University</option>    <option value="1045">Kalyani University</option>    <option value="1046">Kannur University</option>    <option value="64">Kanpur University</option>    <option value="1047">Karnataka State Open University (KSOU)</option>    <option value="65">Karnataka University</option>    <option value="1048">Karunya University</option>    <option value="1049">Kashmir University</option>    <option value="66">Kasturba Medical College (KMC), Manipal</option>    <option value="67">Kerala University</option>    <option value="68">King Edward Medical College (KEC), Mumbai</option>    <option value="2037">King George Medical College - Chhatrapati Shahuji Maharaj Medical University, Lucknow</option>    <option value="1050">Kota University</option>    <option value="1051">Kumaun University</option>    <option value="69">Kurukshetra University</option>    <option value="70">Kuvempu University, Karnataka</option>    <option value="71">Lady Hardinge Medical College (LHMC), Delhi</option>    <option value="1052">Lal Bahadur Shastri Institute of Management (LBSIM)</option>    <option value="1053">Lovely Professional University (LPU)</option>    <option value="72">Loyola College, Chennai</option>    <option value="211">Loyola Institute of Business Administration (LIBM), Chennai</option>    <option value="73">Lucknow University</option>    <option value="2038">M S Ramaiah Medical College, Bangalore</option>    <option value="2039">MCM DAV College for Women, Chandigarh</option>    <option value="212">Madan Mohan Malaviya Engineering College (MMMEC)</option>    <option value="75">Madras Christian College (MCC)</option>    <option value="76">Madras Medical College (MMC)</option>    <option value="213">Madras University</option>    <option value="77">Madurai Kamaraj University (MKU)</option>    <option value="214">Magadh University</option>    <option value="215">Maharaja Sayajirao University (MSU), Baroda</option>    <option value="216">Maharana Pratap University of Agriculture and Technology (MPUAT)</option>    <option value="1054">Maharishi Dayanand Saraswati University (MDSU), Ajmer</option>    <option value="217">Maharishi Dayanand University (MDU), Rohtak</option>    <option value="1055">Maharishi Institute of Management (MIM)</option>    <option value="78">Mahatma Gandhi University (MGU)</option>    <option value="1056">Mahatma Jyotiba Phule Rohilkhand University (MJPRS)</option>    <option value="102">Malviya National Institute of Technology (NIT), Jaipur</option>    <option value="223">Management Development Institute (MDI), Gurgaon</option>    <option value="79">Mangalore University</option>    <option value="218">Manipal Academy of Higher Education (MAHE)</option>    <option value="219">Manipal Institute of Management (MIM)</option>    <option value="220">Manipal Institute of Technology (MIT)</option>    <option value="221">Manipal University</option>    <option value="222">Manipur University</option>    <option value="1057">Manonmaniam Sundaranar University (MSU), Thirunelvelli</option>    <option value="80">Maulana Azad Medical College (MAMC), Delhi</option>    <option value="104">Maulana Azad National Institute of Technology (NIT), Bhopal</option>    <option value="2040">Medical College &amp; Hospital, Kolkata</option>    <option value="224">Meerut University</option>    <option value="2041">Mithibai Arts Chauhan Institute, Mumbai</option>    <option value="111">Motilal Nehru Institute National Institute of Technology (NIT), Allahabad</option>    <option value="2042">Mount Carmel College, Bangalore</option>    <option value="225">Mudra Institute of Communications (MICA), Ahmedabad</option>    <option value="81">Mumbai University</option>    <option value="1065">NIILM School of Business (NIILSB)</option>    <option value="87">NIIT</option>    <option value="82">Nagarjuna University</option>    <option value="83">Nagpur University</option>    <option value="227">Narsee Monjee Institute Of Management Studies (NMIMS)</option>    <option value="2043">National Academy of Legal Studies and Research University (NALSAR), Hyderabad</option>    <option value="1058">National Institute of Business Management (NIBM)</option>    <option value="1059">National Institute of Construction Management and Research (NICMR)</option>    <option value="84">National Institute of Design (NID), Ahmedabad</option>    <option value="2044">National Institute of Design (NID), Bangalore</option>    <option value="85">National Institute of Engineering (NIE)</option>    <option value="86">National Institute of Fashion Technology (NIFT)</option>    <option value="2045">National Institute of Fashion Technology (NIFT), Bangalore</option>    <option value="2046">National Institute of Fashion Technology (NIFT), Chennai</option>    <option value="2047">National Institute of Fashion Technology (NIFT), Delhi</option>    <option value="2048">National Institute of Fashion Technology (NIFT), Hyderabad</option>    <option value="2049">National Institute of Fashion Technology (NIFT), Kannur</option>    <option value="2050">National Institute of Fashion Technology (NIFT), Kolkata</option>    <option value="2051">National Institute of Fashion Technology (NIFT), Mumbai</option>    <option value="228">National Institute of Financial Management (NIFM)</option>    <option value="1098">National Institute of Industrial Engineering (NITIE)</option>    <option value="1060">National Institute of Management (NIM)</option>    <option value="1061">National Institute of Sales (NIS)</option>    <option value="2058">National Institute of Technology,(NIT), Agartala</option>    <option value="2052">National Institute of Technology, (NIT), Arunachal Pradesh</option>    <option value="103">National Institute of Technology (NIT), Bharatidasan</option>    <option value="105">National Institute of Technology (NIT), Calicut</option>    <option value="2059">National Institute of Technology,(NIT), Delhi</option>    <option value="106">National Institute of Technology (NIT), Durgapur</option>    <option value="2055">National Institute of Technology,(NIT), Goa</option>    <option value="107">National Institute of Technology (NIT), Hamirpur</option>    <option value="1094">National Institute of Technology (NIT), Jamshedpur</option>    <option value="109">National Institute of Technology (NIT), Karnataka</option>    <option value="110">National Institute of Technology (NIT), Kurukshetra</option>    <option value="2056">National Institute of Technology,(NIT), Manipur</option>    <option value="2060">National Institute of Technology,(NIT), Mizoram</option>    <option value="2057">National Institute of Technology,(NIT), Meghalaya</option>    <option value="2053">National Institute of Technology, (NIT), Nagaland</option>    <option value="1095">National Institute of Technology (NIT), Patna</option>    <option value="1096">National Institute of Technology (NIT), Raipur</option>    <option value="113">National Institute of Technology (NIT), Rourkela</option>    <option value="2054">National Institute of Technology, (NIT), Sikkim</option>    <option value="114">National Institute of Technology (NIT), Silchar</option>    <option value="115">National Institute of Technology (NIT), Srinagar</option>    <option value="117">National Institute of Technology (NIT), Suratkal</option>    <option value="118">National Institute of Technology (NIT), Tiruchirappalli</option>    <option value="119">National Institute of Technology (NIT), Warangal</option>    <option value="120">National Institute of Technology (NIT), Other</option>    <option value="229">National Law Institute University (NLIU), Bhopal</option>    <option value="230">National Law School of India University (NLSIU), Bangalore</option>    <option value="231">National Law University (NLU), Jodhpur</option>    <option value="1062">National Power Training Institute (NPTI)</option>    <option value="232">Netaji Subhas Institute of Technology (NSIT)</option>    <option value="1063">Nettur Technical Training Foundation (NTTF)</option>    <option value="1064">New Delhi Institute of Management (NDIM)</option>    <option value="2061">Nilratan Sircar Medical College, Kolkata</option>    <option value="233">Nirma Institute of Management (NIM), Ahmedabad</option>    <option value="1066">North Bengal University (NBU)</option>    <option value="1067">North Eastern Hill University (NEHU)</option>    <option value="1068">North Gujarat University (NGU)</option>    <option value="88">North Maharashtra University (NMU)</option>    <option value="1069">North Orissa University (NOU)</option>    <option value="2062">Northern India Institute of Fashion Technology (NIIFT), Mohali</option>    <option value="89">Oberoi Centre for Learning and Development (OCLD)</option>    <option value="2063">Osmania Medical College, Hyderabad</option>    <option value="90">Osmania University</option>    <option value="95">PSG, Coimbatore</option>    <option value="98">Panjab University</option>    <option value="234">Patna University</option>    <option value="2065">Pearl Academy, Delhi</option>    <option value="235">Periyar University</option>    <option value="91">Pondicherry University</option>    <option value="92">Pragathi Mahavidyalaya, Hyderabad</option>    <option value="93">Presidency College, Chennai</option>    <option value="94">Presidency College, Kolkatta</option>    <option value="96">Pune University</option>    <option value="1099">Punjab Engineering College (PEC)</option>    <option value="97">Punjab Technical University (PTU)</option>    <option value="1070">Punjabi University, Patiala</option>    <option value="2066">R.A. Podar College of Commerce and Economics, Mumbai</option>    <option value="99">RV College Of Engineering (RVCE)</option>    <option value="1071">Rai Business School (RBS)</option>    <option value="236">Rai University</option>    <option value="100">Rajasthan University</option>    <option value="1072">Rajiv Gandhi Proudyogiki Vishwavidyalaya (RGPV), Bhopal</option>    <option value="101">Ranchi University</option>    <option value="2067">Rani Birla Girls College, Kolkata</option>    <option value="1073">Rani Durgavati Vishwavidyalaya (RDVV), Jabalpur</option>    <option value="1078">SIES College of Management Studies (SIESCMS)</option>    <option value="241">SP Jain Institute of Management Research (SPJIMR), Mumbai</option>    <option value="1081">SRM university (SRMU)</option>    <option value="237">Sambalpur University</option>    <option value="121">Sardar Patel University (SPU)</option>    <option value="116">Sardar Vallabhbhai National Institute of Technology (NIT), Surat</option>    <option value="1074">Sastra University</option>    <option value="1075">Sathyabama University</option>    <option value="238">Saurashtra University</option>    <option value="122">School Of Planning and Architecture (SPA)</option>    <option value="2068">Seth Gordhandas Sunderdas Medical College, Mumbai</option>    <option value="123">Shivaji University, Maharasthra</option>    <option value="1076">Shreemati Nathibai Damodar Thackersey Women's University (SNDTWU)</option>    <option value="2069">Shri Bhagawan Mahaveer Jain College, Bangalore</option>    <option value="2070">Shri Narsee Monjee College of Commerce and Economics, Mumbai</option>    <option value="1077">Siddaganga Institute of Technology (SIT)</option>    <option value="239">Sikkim Manipal University (SMU)</option>    <option value="1079">Siliguri Institute Of Technology (SIT)</option>    <option value="2071">Sir J J College of Architecture, Mumbai</option>    <option value="2072">Sophia College for Women, Mumbai</option>    <option value="2073">Sri Guru Gobind Singh College of Commerce, Delhi</option>    <option value="124">Sri Jayachamarajendran College of Engineering (SJCE)</option>    <option value="1080">Sri Krishnadevaraya University (SKU)</option>    <option value="2074">Sri Ramachandra Medical College &amp; Research Institute, Chennai</option>    <option value="125">Sri Venkateshwara University (SVU), Tirupati</option>    <option value="2075">St. John's Medical College, Bangalore</option>    <option value="2076">St.Joseph's College, Bangalore</option>    <option value="2077">St.Xavier's College, Ahmedabad</option>    <option value="126">St.Xaviers College, Kolkatta</option>    <option value="127">St.Xaviers College, Mumbai</option>    <option value="2078">Stanley Medical College (SMC), Chennai</option>    <option value="242">State Institute of Education Technology (SIET), Lucknow</option>    <option value="243">State Institute of Education Technology (SIET), Patna</option>    <option value="244">State Institute of Education Technology (SIET), Pune</option>    <option value="245">State Institute of Education Technology (SIET), Thiruvananthapuram</option>    <option value="128">Stella Maris College (SMC), Chennai</option>    <option value="2079">Sushant School of Art and Architecture, Gurgaon</option>    <option value="246">Swami Ramanand Teerth Marathawada University (SRTMU)</option>    <option value="248">Symbiosis Centre For Management and HRD (SCMHRD)</option>    <option value="247">Symbiosis Centre for Distance Learning (SCDL)</option>    <option value="249">Symbiosis Institute of Business Management (SIBM), Pune</option>    <option value="1082">Symbiosis Institute of Management Studies (SIMS)</option>    <option value="250">Symbiosis Society Law College (SSLC), Pune</option>    <option value="2080">Symbiosis Society's College of Arts &amp; Commerce, Pune</option>    <option value="251">Symbiosis, Pune</option>    <option value="1084">TASMAC</option>    <option value="252">Ta Pai Management Institute (TAPMI)</option>    <option value="1083">Tamil Nadu Open University (TNOU)</option>    <option value="253">Tata Institute of Social Sciences (TISS)</option>    <option value="254">Tezpur University</option>    <option value="129">Thapar Institute of Engineering and Technology (TIET)</option>    <option value="2081">The Institute of Post Graduate Medical Education &amp; Research (IPGMER), Kolkata</option>    <option value="2082">The Oxford College of Science, Bangalore</option>    <option value="2083">The W.B. National University of Juridical Sciences(NUJS), Kolkata</option>    <option value="1085">Thiruvalluvar University</option>    <option value="1086">Tripura University</option>    <option value="257">University Business School (UBS), Chandigarh</option>    <option value="256">University College of Law (UCL), Bangalore</option>    <option value="2084">University College of Medical Sciences &amp; GTB Hospital, Delhi</option>    <option value="133">University Vishveshvaraya College of Engineering (UVCE)</option>    <option value="132">University of Mysore</option>    <option value="1087">University of North Bengal (UNB)</option>    <option value="2001">University of Petroleum and Energy Studies, Dehradun</option>    <option value="258">Utkal University</option>    <option value="259">Uttar Pradesh Technical University (UPTU)</option>    <option value="260">Veer Bahadur Singh Purvanchal University (VBSPU)</option>    <option value="240">Veer Narmad South Gujarat University (VNSGU)</option>    <option value="134">Vellore Engineering College (VEC)</option>    <option value="261">Vellore Institute of Technology (VIT)</option>    <option value="262">Vidyasagar University</option>    <option value="1088">Vikram University, Ujjain</option>    <option value="1089">Vinayaka Mission University (VMU)</option>    <option value="263">Vinoba Bhave University (VBU)</option>    <option value="112">Visvesvaraya National Institute of Technology (NIT), Nagpur</option>    <option value="266">Visveswaraiah Technological University (VTU)</option>    <option value="131">Welcomgroup Graduate School of Hotel Administration (WGSHA), Manipal</option>    <option value="1090">Welingkar Institute of Management Development and Research (WIMDR)</option>    <option value="267">West Bengal University of Technology (WBUT)</option>    <option value="1091">Wigan and Leigh College (WLC)</option>    <option value="268">Wildlife Institute of India (WII)</option>    <option value="270">Xavier Institute of Social Service (XISS), Ranchi</option>    <option value="271">Xavier Labour Relations Institute (XLRI), Jamshedpur</option>    <option value="269">Xavier's Institute of Management (XIMS), Bhuvaneshwar</option>    <option value="1092">Yashwantrao Chavan Maharashtra Open University (YCMOU)</option>    <option value="135">Young Men Christian Association (YMCA)</option>    <option value="1093">Young Womens Christian Association (YWCA)</option>    <option value="9999">Other</option>   </select>
                                        </div>
                                    </div>
                                     <div class="form-group">
                                        <label for="city" class="col-sm-3 control-label">
                                           Year</label>
                                        <div class="col-sm-8">
                                     <select runat="server" clientidmode="Static"  id="ugyearOfCompletion" runat="server">                                  <option value="-1">Year</option>   <option value="2020">2020</option>   <option value="2019">2019</option>   <option value="2018">2018</option>   <option value="2017">2017</option>   <option value="2016">2016</option>   <option value="2015">2015</option>   <option value="2014">2014</option>   <option value="2013">2013</option>   <option value="2012">2012</option>   <option value="2011">2011</option>   <option value="2010">2010</option>   <option value="2009">2009</option>   <option value="2008">2008</option>   <option value="2007">2007</option>   <option value="2006">2006</option>   <option value="2005">2005</option>   <option value="2004">2004</option>   <option value="2003">2003</option>   <option value="2002">2002</option>   <option value="2001">2001</option>   <option value="2000">2000</option>   <option value="1999">1999</option>   <option value="1998">1998</option>   <option value="1997">1997</option>   <option value="1996">1996</option>   <option value="1995">1995</option>   <option value="1994">1994</option>   <option value="1993">1993</option>   <option value="1992">1992</option>   <option value="1991">1991</option>   <option value="1990">1990</option>   <option value="1989">1989</option>   <option value="1988">1988</option>   <option value="1987">1987</option>   <option value="1986">1986</option>   <option value="1985">1985</option>   <option value="1984">1984</option>   <option value="1983">1983</option>   <option value="1982">1982</option>   <option value="1981">1981</option>   <option value="1980">1980</option>   <option value="1979">1979</option>   <option value="1978">1978</option>   <option value="1977">1977</option>   <option value="1976">1976</option>   <option value="1975">1975</option>   <option value="1974">1974</option>   <option value="1973">1973</option>   <option value="1972">1972</option>   <option value="1971">1971</option>   <option value="1970">1970</option>   <option value="1969">1969</option>   <option value="1968">1968</option>   <option value="1967">1967</option>   <option value="1966">1966</option>   <option value="1965">1965</option>   <option value="1964">1964</option>   <option value="1963">1963</option>   <option value="1962">1962</option>   <option value="1961">1961</option>   <option value="1960">1960</option>   <option value="1959">1959</option>   <option value="1958">1958</option>   <option value="1957">1957</option>   <option value="1956">1956</option>   <option value="1955">1955</option>   <option value="1954">1954</option>   <option value="1953">1953</option>   <option value="1952">1952</option>   <option value="1951">1951</option>   <option value="1950">1950</option>   <option value="1949">1949</option>   <option value="1948">1948</option>   <option value="1947">1947</option>   <option value="1946">1946</option>   <option value="1945">1945</option>   <option value="1944">1944</option>   <option value="1943">1943</option>   <option value="1942">1942</option>   <option value="1941">1941</option>   <option value="1940">1940</option>                                      </select>
                                        </div>
                                    </div>

                                    <div id="PnlPG" style="display:none;" runat="server" clientidmode="Static">
                                   <hr />
                                       <div class="form-group">
                                        <label for="name" class="col-sm-3 control-label">
                          Post Graduation:</label>
                                        <div class="col-sm-8">
                                           
                                           <select runat="server" clientidmode="Static"  id="pgcourseId" class="w257" name="pgcourseId" valtype="pgcourseId" onchange="populateSpec('pgcourseId','pgspecId','pg');showHideOtherBox('pgcourseId','opgCid');showHideOtherBox('pgcourseId','opgSid');">
                                      <option value="-1">Select</option>    <option value="1">CA</option>    <option value="2">CS</option>    <option value="3">ICWA (CMA)</option>    <option value="4">Integrated PG</option>    <option value="5">LLM</option>    <option value="6">M.A</option>    <option value="7">M.Arch</option>    <option value="8">M.Com</option>    <option value="9">M.Ed</option>    <option value="10">M.Pharma</option>    <option value="11">M.Sc</option>    <option value="12">M.Tech</option>    <option value="13">MBA/PGDM</option>    <option value="14">MCA</option>    <option value="15">MS</option>    <option value="16">PG Diploma</option>    <option value="17">MVSC</option>    <option value="18">MCM</option>    <option value="9999">Other</option>   </select>
                                        </div>
                                    </div>
                                      <div class="form-group">
                                      
                                        <div class="col-sm-8">
                                            <asp:RadioButtonList ID="rbPGStatus" runat="server" RepeatDirection="Horizontal">
                                            <asp:ListItem Value="Full Time" Selected="True">Full Time</asp:ListItem>
                                            <asp:ListItem Value="Part Time">Part Time</asp:ListItem>
                                            <asp:ListItem Value="Correspondence/Distance Learning">Correspondence/Distance Learning</asp:ListItem>
                                            </asp:RadioButtonList>
                                        </div>
                                    </div>
                                     <div class="form-group">
                                        <label for="name" class="col-sm-3 control-label">
                                          Specialization</label>
                                        <div class="col-sm-8">
                                        <select runat="server" clientidmode="Static"  id="pgspecId" class="w257" name="pgspecId" valtype="pgspecId" onchange="getSelectedValue(this.id);">
                                       <option value="-1">Select</option>    </select>
                                           <asp:HiddenField ID="hfpgspecId" ClientIDMode="Static" runat="server" />
                                        </div>
                                    </div>
                                     <div class="form-group">
                                        <label for="name" class="col-sm-3 control-label">
                                            University/Institute:</label>
                                        <div class="col-sm-8">
                                           <select runat="server" clientidmode="Static"  id="pginstitute" runat="server">    <option value="-1">Select</option>    <option value="272">Agra University</option>    <option value="1001">Air Hostess Academy (AHA)</option>    <option value="139">Alagappa University</option>    <option value="2">Aligarh Muslim University (AMU)</option>    <option value="140">All India Institute of Management Studies</option>    <option value="1">All India Institute of Medical Sciences (AIIMS), Delhi</option>    <option value="138">All India Management Association (AIMA)</option>    <option value="3">Allahabad University</option>    <option value="1002">Alliance Business Academy (ABA)</option>    <option value="5">Amity Business School (ABS)</option>    <option value="2003">Amity Law School, Noida</option>    <option value="2004">Amity School of Fashion Technology, Noida</option>    <option value="1003">Amity University</option>    <option value="4">Amravati University</option>    <option value="2005">Amrita Institute of Medical Sciences &amp; Research Centre, Kochi</option>    <option value="6">Andhra University</option>    <option value="7">Anna University</option>    <option value="8">Annamalai University</option>    <option value="9">Apeejay School Of Marketing (ASM)</option>    <option value="10">Aptech</option>    <option value="1004">Arena Multimedia</option>    <option value="11">Armed Forces Medical College (AFMC), Pune</option>    <option value="1005">Asansol Engineering College (AEC)</option>    <option value="1006">Asia Pacific Institute of Management (APIM)</option>    <option value="1007">Assam Engineering College (AEC)</option>    <option value="141">Assam University</option>    <option value="1008">Avadh University</option>    <option value="2006">B J Medical College, Pune</option>    <option value="151">B N Mandal University (BNMU)</option>    <option value="1009">Babasaheb Ambedkar Marathwada University (BAMU), Aurangabad</option>    <option value="142">Babasaheb Bhimrao Ambedkar University (BBAU), Lucknow</option>    <option value="12">Banaras Hindu University (BHU), Varanasi</option>    <option value="1010">Banasthali University</option>    <option value="143">Bangalore Institute Of Technology (BIT)</option>    <option value="13">Bangalore Medical College (BMC)</option>    <option value="14">Bangalore University</option>    <option value="144">Barkatullah University</option>    <option value="145">Bengal Engineering College (BEC)</option>    <option value="146">Berhampur University</option>    <option value="1011">Bhagalpur University</option>    <option value="147">Bharathidasan University, Trichy</option>    <option value="1012">Bharatiya Vidya Bhavan (BVB)</option>    <option value="15">Bharathiar University, Coimbatore</option>    <option value="148">Bhavnagar University</option>    <option value="149">Bhopal University</option>    <option value="1013">Bihar University</option>    <option value="1014">Biju Patnaik University of Technology (BPUT)</option>    <option value="1015">Bikaner University</option>    <option value="150">Birla Institute of Management Technology (BIMT)</option>    <option value="17">Birla Institute of Technology (BIT), Ranchi</option>    <option value="16">Birla Institute of Technology and Science (BITS), Pilani</option>    <option value="18">Board of Technical Education (BTE)</option>    <option value="152">Bundelkhand University</option>    <option value="153">Burdwan University</option>    <option value="1017">CIPET</option>    <option value="154">Calcutta Medical College (CMC)</option>    <option value="20">Calcutta University</option>    <option value="19">Calicut University</option>    <option value="21">Centre for Development of Advanced Computing (CDAC)</option>    <option value="1016">Ch Charan Singh University (CCSU), Meerut</option>    <option value="2007">Chandigarh College of Architecture, Chandigarh</option>    <option value="22">Chennai University</option>    <option value="155">Chhatrapati Shahu Ji Maharaj University (CSJMU)</option>    <option value="2008">Christ College, Bangalore</option>    <option value="23">Christian College, Lucknow</option>    <option value="24">Christian Medical College (CMC), Vellore</option>    <option value="2009">Christian Medical College(CMC, Ludhiana), Ludhiana</option>    <option value="157">Cochin University of Science and Technology (CUST)</option>    <option value="158">College of Engineering and Technology (CET), Bhubaneshwar</option>    <option value="2000">College of Engineering, Anna University, Guindy</option>    <option value="273">DAV Institute of Management (DAVIM), Faridabad</option>    <option value="36">DOEACC</option>    <option value="1018">Deen Dayal Upadhyay Gorakhpur University (DDUGU)</option>    <option value="26">Delhi College of Engineering (DCE), Delhi</option>    <option value="25">Delhi University - College of Art</option>    <option value="27">Delhi University - Daulat Ram College </option>    <option value="28">Delhi University - Hansraj College</option>    <option value="29">Delhi University - Hindu College </option>    <option value="30">Delhi University - Lady Shree Ram College (LSR)</option>    <option value="31">Delhi University - Miranda House</option>    <option value="2010">Delhi University - Ramjas College</option>    <option value="32">Delhi University - Shri Ram College of Commerce (SRCC)</option>    <option value="2011">Delhi University - Sri Venkateswara College</option>    <option value="33">Delhi University - St Stephens College</option>    <option value="34">Delhi University - Other</option>    <option value="35">Devi Ahilya Vishwa Vidhyalaya (DAVV), Indore</option>    <option value="1019">Dibrugarh University</option>    <option value="108">Dr. B.R. Ambedkar National Institute of Technology (NIT), Jalandhar</option>    <option value="160">Dr. Bhim Rao Ambedkar University (BRAU), Bihar</option>    <option value="161">Dr. Harisingh Gour University (HGU), Sagar</option>    <option value="162">Dr. MGR Medical University (MGRMU), Chennai</option>    <option value="1020">EMPI Business School</option>    <option value="2012">Elphinstone College, Mumbai</option>    <option value="37">Faculty Of Management Studies</option>    <option value="163">Faculty Of Management Studies (FMS), Delhi</option>    <option value="2013">Faculty of Architecture &amp; Ekistics, JMI, Delhi</option>    <option value="2014">Faculty of Law, Aligarh Muslim University, Aligarh</option>    <option value="38">Fergusson College, Pune</option>    <option value="39">Film and Television Institute of India (FTII), Pune</option>    <option value="164">Fore School of Management (FSM), Delhi</option>    <option value="1021">Fortune Institute of International Business (FIIB)</option>    <option value="2015">Gargi College, Delhi</option>    <option value="166">Goa Institute of Management (GIM)</option>    <option value="40">Goa University</option>    <option value="167">Goenka College of Commerce and Business Administration (GCCBA)</option>    <option value="168">Government College of Engineering (GCE), Pune</option>    <option value="169">Government College of Science (GCS), Raipur</option>    <option value="170">Government Medical College and Hospital (GMCH), Chandigarh</option>    <option value="2016">Grant Medical College, Mumbai</option>    <option value="1097">Great Lakes Institute of Management (GLIM)</option>    <option value="171">Gujarat Agricultural University (GAU)</option>    <option value="2017">Gujarat National Law University, Gandhinagar</option>    <option value="172">Gujarat University</option>    <option value="173">Gulbarga University</option>    <option value="174">Guru Ghasidas University (GGU)</option>    <option value="175">Guru Gobind Singh Indraprastha University (GGSIU)</option>    <option value="1022">Guru Jambheshwar University of Science and Technology (GJUST), Hisar</option>    <option value="176">Guru Nanak Dev University (GNDU)</option>    <option value="1023">Gurukula Kangri University (GKU)</option>    <option value="177">Guwahati Medical College and Hospital (GMCH)</option>    <option value="178">Guwahati University</option>    <option value="165">HNB Garhwal University (HNBGU)</option>    <option value="179">Haldia Institute of Technology (HIT)</option>    <option value="180">Hidayatullah National Law University (HNLU)</option>    <option value="181">Himachal Pradesh University (HPU)</option>    <option value="41">Hyderabad University</option>    <option value="1024">Indian Business Academy (IBA)</option>    <option value="1026">Indian Institute Of Hardware Technology (IIHT)</option>    <option value="1029">Indian Institute Of Packaging (IIP)</option>    <option value="193">Indian Institute of Bankers (IIB)</option>    <option value="1025">Indian Institute of Finance (IIF)</option>    <option value="194">Indian Institute of Foreign Trade (IIFT)</option>    <option value="55">Indian Institute of Hotel Management (IIHM), Aurangabad</option>    <option value="184">Indian Institute of Management (IIM), Ahmedabad</option>    <option value="185">Indian Institute of Management (IIM), Bangalore</option>    <option value="187">Indian Institute of Management (IIM), Indore</option>    <option value="186">Indian Institute of Management (IIM), Kolkatta</option>    <option value="188">Indian Institute of Management (IIM), Kozhikode</option>    <option value="189">Indian Institute of Management (IIM), Lucknow</option>    <option value="56">Indian Institute of Mass Communication (IIMC), Delhi</option>    <option value="1027">Indian Institute of Materials Management (IIMM)</option>    <option value="1028">Indian Institute of Modern Management (IIMM), Pune</option>    <option value="195">Indian Institute of Planning And Management (IIPM)</option>    <option value="196">Indian Institute of Science (IIS), Bangalore</option>    <option value="47">Indian Institute of Social Welfare and Business Management (IISWBM)</option>    <option value="2018">Indian Institute of Technology (IIT), Bhuvaneshwar</option>    <option value="48">Indian Institute of Technology (IIT), Chennai</option>    <option value="49">Indian Institute of Technology (IIT), Delhi</option>    <option value="2020">Indian Institute of Technology (IIT), Gandhinagar</option>    <option value="50">Indian Institute of Technology (IIT), Guwahati</option>    <option value="2021">Indian Institute of Technology (IIT), Hyderabad</option>    <option value="2019">Indian Institute of Technology (IIT), Indore</option>    <option value="2022">Indian Institute of Technology (IIT), Jodhpur</option>    <option value="51">Indian Institute of Technology (IIT), Kanpur</option>    <option value="52">Indian Institute of Technology (IIT), Kharagpur</option>    <option value="2023">Indian Institute of Technology (IIT), Mandi</option>    <option value="53">Indian Institute of Technology (IIT), Mumbai</option>    <option value="2024">Indian Institute of Technology (IIT), Patna</option>    <option value="54">Indian Institute of Technology (IIT), Roorkee</option>    <option value="2025">Indian Institute of Technology (IIT), Ropar</option>    <option value="190">Indian Law Society Law College (ILSLC), Pune</option>    <option value="202">Indian School of Business (ISB), Hyderabad</option>    <option value="203">Indian School of Mines (ISM), Dhanbad</option>    <option value="197">Indian Statistical Institute (ISI), Bangalore</option>    <option value="198">Indian Statistical Institute (ISI), Delhi</option>    <option value="57">Indian Statistical Institute (ISI), Kolkatta</option>    <option value="45">Indira Gandhi National Open University (IGNOU)</option>    <option value="2026">Indraprastha College for Women, Delhi</option>    <option value="1030">Industrial Training Institute (ITI)</option>    <option value="1031">Institute for Technology and Management (ITM)</option>    <option value="1032">Institute of Business Management and Research (IBMR)</option>    <option value="59">Institute of Chartered Accountant of India (ICAI)</option>    <option value="42">Institute of Chartered Financial Analysts of India (ICFAI)</option>    <option value="199">Institute of Company Secretaries of India (ICSI)</option>    <option value="44">Institute of Cost and Works Accountants of India (ICWAI)</option>    <option value="58">Institute of Hotel Management (IHM)</option>    <option value="2027">Institute of Hotel Management (IHM), Ahmedabad</option>    <option value="2028">Institute of Hotel Management (IHM), Aurangabad</option>    <option value="2029">Institute of Hotel Management (IHM), Bangalore</option>    <option value="2030">Institute of Hotel Management (IHM), Chennai</option>    <option value="2031">Institute of Hotel Management (IHM), Delhi</option>    <option value="2032">Institute of Hotel Management (IHM), Kolkata</option>    <option value="1033">Institute of Management Education (IME)</option>    <option value="1034">Institute of Management Studies (IMS), Dehradun</option>    <option value="1035">Institute of Management Studies (IMS), Ghaziabad</option>    <option value="192">Institute of Management Technology (IMT), Ghaziabad</option>    <option value="1036">Institute of Marketing and Management (IMM)</option>    <option value="2033">Institute of Medical Sciences, Varanasi</option>    <option value="1037">Institute of Productivity and Management (IPM)</option>    <option value="201">Institute of Rural Management (IRMA), Anand</option>    <option value="204">Institute of Technology, Banaras Hindu University</option>    <option value="137">International Institute of Information Technology (IIIT), Allahabad</option>    <option value="182">International Institute of Information Technology (IIIT), Bangalore</option>    <option value="46">International Institute of Information Technology (IIIT), Hyderabad</option>    <option value="183">International Institute of Information Technology (IIIT), Kolkatta</option>    <option value="200">International Management Institute (IMI), Delhi</option>    <option value="1038">International School of Business and Media (ISBM)</option>    <option value="2034">J.D. Birla Institute, Kolkata</option>    <option value="60">JJ Institute of Applied Art (JJIAA), Mumbai</option>    <option value="205">Jabalpur University</option>    <option value="206">Jadavpur University</option>    <option value="1039">Jagan Institute of Management Studies (JIMS)</option>    <option value="1040">Jaipuria Institute of Management (JIMS)</option>    <option value="1041">Jamia Hamdard</option>    <option value="61">Jamia Milia Islamia (JMI)</option>    <option value="1042">Jammu University</option>    <option value="207">Jamnalal Bajaj Institute of Management Studies (JLBIM)</option>    <option value="1043">Jawaharlal Nehru Technological University (JNTU)</option>    <option value="62">Jawaharlal Nehru University (JNU)</option>    <option value="2035">Jesus &amp; Mary College (JMC), Delhi</option>    <option value="1044">Jetking</option>    <option value="63">Jipmer, Pondicherry</option>    <option value="208">Jiwaji University</option>    <option value="2036">K J Somaiya College of Arts &amp; Commerce, Mumbai</option>    <option value="209">K.J. Somaiya Institute of Management Studies and Research (KJSIMSR), Mumbai</option>    <option value="210">Kakatiya University</option>    <option value="1045">Kalyani University</option>    <option value="1046">Kannur University</option>    <option value="64">Kanpur University</option>    <option value="1047">Karnataka State Open University (KSOU)</option>    <option value="65">Karnataka University</option>    <option value="1048">Karunya University</option>    <option value="1049">Kashmir University</option>    <option value="66">Kasturba Medical College (KMC), Manipal</option>    <option value="67">Kerala University</option>    <option value="68">King Edward Medical College (KEC), Mumbai</option>    <option value="2037">King George Medical College - Chhatrapati Shahuji Maharaj Medical University, Lucknow</option>    <option value="1050">Kota University</option>    <option value="1051">Kumaun University</option>    <option value="69">Kurukshetra University</option>    <option value="70">Kuvempu University, Karnataka</option>    <option value="71">Lady Hardinge Medical College (LHMC), Delhi</option>    <option value="1052">Lal Bahadur Shastri Institute of Management (LBSIM)</option>    <option value="1053">Lovely Professional University (LPU)</option>    <option value="72">Loyola College, Chennai</option>    <option value="211">Loyola Institute of Business Administration (LIBM), Chennai</option>    <option value="73">Lucknow University</option>    <option value="2038">M S Ramaiah Medical College, Bangalore</option>    <option value="2039">MCM DAV College for Women, Chandigarh</option>    <option value="212">Madan Mohan Malaviya Engineering College (MMMEC)</option>    <option value="75">Madras Christian College (MCC)</option>    <option value="76">Madras Medical College (MMC)</option>    <option value="213">Madras University</option>    <option value="77">Madurai Kamaraj University (MKU)</option>    <option value="214">Magadh University</option>    <option value="215">Maharaja Sayajirao University (MSU), Baroda</option>    <option value="216">Maharana Pratap University of Agriculture and Technology (MPUAT)</option>    <option value="1054">Maharishi Dayanand Saraswati University (MDSU), Ajmer</option>    <option value="217">Maharishi Dayanand University (MDU), Rohtak</option>    <option value="1055">Maharishi Institute of Management (MIM)</option>    <option value="78">Mahatma Gandhi University (MGU)</option>    <option value="1056">Mahatma Jyotiba Phule Rohilkhand University (MJPRS)</option>    <option value="102">Malviya National Institute of Technology (NIT), Jaipur</option>    <option value="223">Management Development Institute (MDI), Gurgaon</option>    <option value="79">Mangalore University</option>    <option value="218">Manipal Academy of Higher Education (MAHE)</option>    <option value="219">Manipal Institute of Management (MIM)</option>    <option value="220">Manipal Institute of Technology (MIT)</option>    <option value="221">Manipal University</option>    <option value="222">Manipur University</option>    <option value="1057">Manonmaniam Sundaranar University (MSU), Thirunelvelli</option>    <option value="80">Maulana Azad Medical College (MAMC), Delhi</option>    <option value="104">Maulana Azad National Institute of Technology (NIT), Bhopal</option>    <option value="2040">Medical College &amp; Hospital, Kolkata</option>    <option value="224">Meerut University</option>    <option value="2041">Mithibai Arts Chauhan Institute, Mumbai</option>    <option value="111">Motilal Nehru Institute National Institute of Technology (NIT), Allahabad</option>    <option value="2042">Mount Carmel College, Bangalore</option>    <option value="225">Mudra Institute of Communications (MICA), Ahmedabad</option>    <option value="81">Mumbai University</option>    <option value="1065">NIILM School of Business (NIILSB)</option>    <option value="87">NIIT</option>    <option value="82">Nagarjuna University</option>    <option value="83">Nagpur University</option>    <option value="227">Narsee Monjee Institute Of Management Studies (NMIMS)</option>    <option value="2043">National Academy of Legal Studies and Research University (NALSAR), Hyderabad</option>    <option value="1058">National Institute of Business Management (NIBM)</option>    <option value="1059">National Institute of Construction Management and Research (NICMR)</option>    <option value="84">National Institute of Design (NID), Ahmedabad</option>    <option value="2044">National Institute of Design (NID), Bangalore</option>    <option value="85">National Institute of Engineering (NIE)</option>    <option value="86">National Institute of Fashion Technology (NIFT)</option>    <option value="2045">National Institute of Fashion Technology (NIFT), Bangalore</option>    <option value="2046">National Institute of Fashion Technology (NIFT), Chennai</option>    <option value="2047">National Institute of Fashion Technology (NIFT), Delhi</option>    <option value="2048">National Institute of Fashion Technology (NIFT), Hyderabad</option>    <option value="2049">National Institute of Fashion Technology (NIFT), Kannur</option>    <option value="2050">National Institute of Fashion Technology (NIFT), Kolkata</option>    <option value="2051">National Institute of Fashion Technology (NIFT), Mumbai</option>    <option value="228">National Institute of Financial Management (NIFM)</option>    <option value="1098">National Institute of Industrial Engineering (NITIE)</option>    <option value="1060">National Institute of Management (NIM)</option>    <option value="1061">National Institute of Sales (NIS)</option>    <option value="2058">National Institute of Technology,(NIT), Agartala</option>    <option value="2052">National Institute of Technology, (NIT), Arunachal Pradesh</option>    <option value="103">National Institute of Technology (NIT), Bharatidasan</option>    <option value="105">National Institute of Technology (NIT), Calicut</option>    <option value="2059">National Institute of Technology,(NIT), Delhi</option>    <option value="106">National Institute of Technology (NIT), Durgapur</option>    <option value="2055">National Institute of Technology,(NIT), Goa</option>    <option value="107">National Institute of Technology (NIT), Hamirpur</option>    <option value="1094">National Institute of Technology (NIT), Jamshedpur</option>    <option value="109">National Institute of Technology (NIT), Karnataka</option>    <option value="110">National Institute of Technology (NIT), Kurukshetra</option>    <option value="2056">National Institute of Technology,(NIT), Manipur</option>    <option value="2060">National Institute of Technology,(NIT), Mizoram</option>    <option value="2057">National Institute of Technology,(NIT), Meghalaya</option>    <option value="2053">National Institute of Technology, (NIT), Nagaland</option>    <option value="1095">National Institute of Technology (NIT), Patna</option>    <option value="1096">National Institute of Technology (NIT), Raipur</option>    <option value="113">National Institute of Technology (NIT), Rourkela</option>    <option value="2054">National Institute of Technology, (NIT), Sikkim</option>    <option value="114">National Institute of Technology (NIT), Silchar</option>    <option value="115">National Institute of Technology (NIT), Srinagar</option>    <option value="117">National Institute of Technology (NIT), Suratkal</option>    <option value="118">National Institute of Technology (NIT), Tiruchirappalli</option>    <option value="119">National Institute of Technology (NIT), Warangal</option>    <option value="120">National Institute of Technology (NIT), Other</option>    <option value="229">National Law Institute University (NLIU), Bhopal</option>    <option value="230">National Law School of India University (NLSIU), Bangalore</option>    <option value="231">National Law University (NLU), Jodhpur</option>    <option value="1062">National Power Training Institute (NPTI)</option>    <option value="232">Netaji Subhas Institute of Technology (NSIT)</option>    <option value="1063">Nettur Technical Training Foundation (NTTF)</option>    <option value="1064">New Delhi Institute of Management (NDIM)</option>    <option value="2061">Nilratan Sircar Medical College, Kolkata</option>    <option value="233">Nirma Institute of Management (NIM), Ahmedabad</option>    <option value="1066">North Bengal University (NBU)</option>    <option value="1067">North Eastern Hill University (NEHU)</option>    <option value="1068">North Gujarat University (NGU)</option>    <option value="88">North Maharashtra University (NMU)</option>    <option value="1069">North Orissa University (NOU)</option>    <option value="2062">Northern India Institute of Fashion Technology (NIIFT), Mohali</option>    <option value="89">Oberoi Centre for Learning and Development (OCLD)</option>    <option value="2063">Osmania Medical College, Hyderabad</option>    <option value="90">Osmania University</option>    <option value="95">PSG, Coimbatore</option>    <option value="98">Panjab University</option>    <option value="234">Patna University</option>    <option value="2065">Pearl Academy, Delhi</option>    <option value="235">Periyar University</option>    <option value="91">Pondicherry University</option>    <option value="92">Pragathi Mahavidyalaya, Hyderabad</option>    <option value="93">Presidency College, Chennai</option>    <option value="94">Presidency College, Kolkatta</option>    <option value="96">Pune University</option>    <option value="1099">Punjab Engineering College (PEC)</option>    <option value="97">Punjab Technical University (PTU)</option>    <option value="1070">Punjabi University, Patiala</option>    <option value="2066">R.A. Podar College of Commerce and Economics, Mumbai</option>    <option value="99">RV College Of Engineering (RVCE)</option>    <option value="1071">Rai Business School (RBS)</option>    <option value="236">Rai University</option>    <option value="100">Rajasthan University</option>    <option value="1072">Rajiv Gandhi Proudyogiki Vishwavidyalaya (RGPV), Bhopal</option>    <option value="101">Ranchi University</option>    <option value="2067">Rani Birla Girls College, Kolkata</option>    <option value="1073">Rani Durgavati Vishwavidyalaya (RDVV), Jabalpur</option>    <option value="1078">SIES College of Management Studies (SIESCMS)</option>    <option value="241">SP Jain Institute of Management Research (SPJIMR), Mumbai</option>    <option value="1081">SRM university (SRMU)</option>    <option value="237">Sambalpur University</option>    <option value="121">Sardar Patel University (SPU)</option>    <option value="116">Sardar Vallabhbhai National Institute of Technology (NIT), Surat</option>    <option value="1074">Sastra University</option>    <option value="1075">Sathyabama University</option>    <option value="238">Saurashtra University</option>    <option value="122">School Of Planning and Architecture (SPA)</option>    <option value="2068">Seth Gordhandas Sunderdas Medical College, Mumbai</option>    <option value="123">Shivaji University, Maharasthra</option>    <option value="1076">Shreemati Nathibai Damodar Thackersey Women's University (SNDTWU)</option>    <option value="2069">Shri Bhagawan Mahaveer Jain College, Bangalore</option>    <option value="2070">Shri Narsee Monjee College of Commerce and Economics, Mumbai</option>    <option value="1077">Siddaganga Institute of Technology (SIT)</option>    <option value="239">Sikkim Manipal University (SMU)</option>    <option value="1079">Siliguri Institute Of Technology (SIT)</option>    <option value="2071">Sir J J College of Architecture, Mumbai</option>    <option value="2072">Sophia College for Women, Mumbai</option>    <option value="2073">Sri Guru Gobind Singh College of Commerce, Delhi</option>    <option value="124">Sri Jayachamarajendran College of Engineering (SJCE)</option>    <option value="1080">Sri Krishnadevaraya University (SKU)</option>    <option value="2074">Sri Ramachandra Medical College &amp; Research Institute, Chennai</option>    <option value="125">Sri Venkateshwara University (SVU), Tirupati</option>    <option value="2075">St. John's Medical College, Bangalore</option>    <option value="2076">St.Joseph's College, Bangalore</option>    <option value="2077">St.Xavier's College, Ahmedabad</option>    <option value="126">St.Xaviers College, Kolkatta</option>    <option value="127">St.Xaviers College, Mumbai</option>    <option value="2078">Stanley Medical College (SMC), Chennai</option>    <option value="242">State Institute of Education Technology (SIET), Lucknow</option>    <option value="243">State Institute of Education Technology (SIET), Patna</option>    <option value="244">State Institute of Education Technology (SIET), Pune</option>    <option value="245">State Institute of Education Technology (SIET), Thiruvananthapuram</option>    <option value="128">Stella Maris College (SMC), Chennai</option>    <option value="2079">Sushant School of Art and Architecture, Gurgaon</option>    <option value="246">Swami Ramanand Teerth Marathawada University (SRTMU)</option>    <option value="248">Symbiosis Centre For Management and HRD (SCMHRD)</option>    <option value="247">Symbiosis Centre for Distance Learning (SCDL)</option>    <option value="249">Symbiosis Institute of Business Management (SIBM), Pune</option>    <option value="1082">Symbiosis Institute of Management Studies (SIMS)</option>    <option value="250">Symbiosis Society Law College (SSLC), Pune</option>    <option value="2080">Symbiosis Society's College of Arts &amp; Commerce, Pune</option>    <option value="251">Symbiosis, Pune</option>    <option value="1084">TASMAC</option>    <option value="252">Ta Pai Management Institute (TAPMI)</option>    <option value="1083">Tamil Nadu Open University (TNOU)</option>    <option value="253">Tata Institute of Social Sciences (TISS)</option>    <option value="254">Tezpur University</option>    <option value="129">Thapar Institute of Engineering and Technology (TIET)</option>    <option value="2081">The Institute of Post Graduate Medical Education &amp; Research (IPGMER), Kolkata</option>    <option value="2082">The Oxford College of Science, Bangalore</option>    <option value="2083">The W.B. National University of Juridical Sciences(NUJS), Kolkata</option>    <option value="1085">Thiruvalluvar University</option>    <option value="1086">Tripura University</option>    <option value="257">University Business School (UBS), Chandigarh</option>    <option value="256">University College of Law (UCL), Bangalore</option>    <option value="2084">University College of Medical Sciences &amp; GTB Hospital, Delhi</option>    <option value="133">University Vishveshvaraya College of Engineering (UVCE)</option>    <option value="132">University of Mysore</option>    <option value="1087">University of North Bengal (UNB)</option>    <option value="2001">University of Petroleum and Energy Studies, Dehradun</option>    <option value="258">Utkal University</option>    <option value="259">Uttar Pradesh Technical University (UPTU)</option>    <option value="260">Veer Bahadur Singh Purvanchal University (VBSPU)</option>    <option value="240">Veer Narmad South Gujarat University (VNSGU)</option>    <option value="134">Vellore Engineering College (VEC)</option>    <option value="261">Vellore Institute of Technology (VIT)</option>    <option value="262">Vidyasagar University</option>    <option value="1088">Vikram University, Ujjain</option>    <option value="1089">Vinayaka Mission University (VMU)</option>    <option value="263">Vinoba Bhave University (VBU)</option>    <option value="112">Visvesvaraya National Institute of Technology (NIT), Nagpur</option>    <option value="266">Visveswaraiah Technological University (VTU)</option>    <option value="131">Welcomgroup Graduate School of Hotel Administration (WGSHA), Manipal</option>    <option value="1090">Welingkar Institute of Management Development and Research (WIMDR)</option>    <option value="267">West Bengal University of Technology (WBUT)</option>    <option value="1091">Wigan and Leigh College (WLC)</option>    <option value="268">Wildlife Institute of India (WII)</option>    <option value="270">Xavier Institute of Social Service (XISS), Ranchi</option>    <option value="271">Xavier Labour Relations Institute (XLRI), Jamshedpur</option>    <option value="269">Xavier's Institute of Management (XIMS), Bhuvaneshwar</option>    <option value="1092">Yashwantrao Chavan Maharashtra Open University (YCMOU)</option>    <option value="135">Young Men Christian Association (YMCA)</option>    <option value="1093">Young Womens Christian Association (YWCA)</option>    <option value="9999">Other</option>   </select>
                                        </div>
                                    </div>
                                     <div class="form-group">
                                        <label for="city" class="col-sm-3 control-label">
                                           Year</label>
                                        <div class="col-sm-8">
                                     <select id="pgyearOfCompletion" runat="server">                                  <option value="-1">Year</option>   <option value="2020">2020</option>   <option value="2019">2019</option>   <option value="2018">2018</option>   <option value="2017">2017</option>   <option value="2016">2016</option>   <option value="2015">2015</option>   <option value="2014">2014</option>   <option value="2013">2013</option>   <option value="2012">2012</option>   <option value="2011">2011</option>   <option value="2010">2010</option>   <option value="2009">2009</option>   <option value="2008">2008</option>   <option value="2007">2007</option>   <option value="2006">2006</option>   <option value="2005">2005</option>   <option value="2004">2004</option>   <option value="2003">2003</option>   <option value="2002">2002</option>   <option value="2001">2001</option>   <option value="2000">2000</option>   <option value="1999">1999</option>   <option value="1998">1998</option>   <option value="1997">1997</option>   <option value="1996">1996</option>   <option value="1995">1995</option>   <option value="1994">1994</option>   <option value="1993">1993</option>   <option value="1992">1992</option>   <option value="1991">1991</option>   <option value="1990">1990</option>   <option value="1989">1989</option>   <option value="1988">1988</option>   <option value="1987">1987</option>   <option value="1986">1986</option>   <option value="1985">1985</option>   <option value="1984">1984</option>   <option value="1983">1983</option>   <option value="1982">1982</option>   <option value="1981">1981</option>   <option value="1980">1980</option>   <option value="1979">1979</option>   <option value="1978">1978</option>   <option value="1977">1977</option>   <option value="1976">1976</option>   <option value="1975">1975</option>   <option value="1974">1974</option>   <option value="1973">1973</option>   <option value="1972">1972</option>   <option value="1971">1971</option>   <option value="1970">1970</option>   <option value="1969">1969</option>   <option value="1968">1968</option>   <option value="1967">1967</option>   <option value="1966">1966</option>   <option value="1965">1965</option>   <option value="1964">1964</option>   <option value="1963">1963</option>   <option value="1962">1962</option>   <option value="1961">1961</option>   <option value="1960">1960</option>   <option value="1959">1959</option>   <option value="1958">1958</option>   <option value="1957">1957</option>   <option value="1956">1956</option>   <option value="1955">1955</option>   <option value="1954">1954</option>   <option value="1953">1953</option>   <option value="1952">1952</option>   <option value="1951">1951</option>   <option value="1950">1950</option>   <option value="1949">1949</option>   <option value="1948">1948</option>   <option value="1947">1947</option>   <option value="1946">1946</option>   <option value="1945">1945</option>   <option value="1944">1944</option>   <option value="1943">1943</option>   <option value="1942">1942</option>   <option value="1941">1941</option>   <option value="1940">1940</option>                                      </select>
                                        </div>
                                    </div>
                                    </div>
                                     <div class="form-group">
                                       
                                        <div class="col-sm-8">
                <asp:Button ID="BtnSaveEducation" CssClass="btn btn-primary" OnClick="BtnSaveEducation_Click" runat="server" Text="Save" />
                      <asp:HyperLink ID="lnkPanelPG" runat="server" onclick="ShowPGPanel();" style="cursor:pointer;">Add Post Graduation</asp:HyperLink>
                                        </div>
                                    </div>
                                  </div>

                                    <div id="PnlProfileImage" visible="false" runat="server" class="col-sm-10">
                                     <div class="form-title">
                                        <h3>Upload your Latest Photo</h3>
                                        <p>A Open-Campus profile with photo has 40% higher chances of getting noticed by employers.</p>

                                    </div>
                                    <div class="form-group">
                                        <label for="name" class="col-sm-3 control-label">
                                            <asp:Image ID="ImgCandidate" Width="170px" Height="190px" runat="server" /></label>
                                        <div class="col-sm-8">
                                       Browse Photo:     <asp:FileUpload ID="FUPhoto" runat="server" />
                                        </div>
                                    </div>
                                    <div class="form-group">
                                       
                                        <div class="col-sm-8">
                <asp:Button ID="BtnSavePhoto" CssClass="btn btn-primary" OnClick="BtnSavePhoto_Click" runat="server" Text="Save" />
                                        </div>
                                    </div>
                                    </div>
                                 <div id="PnlResume" visible="false" runat="server" class="col-sm-10">
                                     <div class="form-title">
                                        <h3>Upload your Updated Resume</h3>
                                        <p>A Open-Campus profile with Resume has 50% higher chances of getting noticed by employers.</p>

                                    </div>
                                    <div class="form-group">
                                        <label for="name" class="col-sm-3 control-label">
                                            <asp:Image ID="Image1" Width="170px" Height="190px" runat="server" ImageUrl="~/images/word-resume.png" /></label><br />
                                       
                                        <div class="col-sm-8">
                                       Browse Resume:     <asp:FileUpload ID="FUResume" runat="server" />
                                        </div>
                                    </div>
                                    <div class="form-group">
                                       
                                        <div class="col-sm-8">
                <asp:Button ID="BtnSaveResume" CssClass="btn btn-primary" OnClick="BtnSaveResume_Click" runat="server" Text="Save" />
                <asp:HyperLink ID="lnkDownloadResume" CssClass="btn btn-warning" runat="server" Text="Download Current Resume" />
                                            
                                        </div>
                                    </div>
                                    </div>
                            </div>
                        </div>
                    </div>
                    <hr />
                </div>
            </div>
        </div>
    </div>
    <script type="text/javascript">
       
        var imgurl = "//static.naukimg.com/s/1/105/";
        window.onload = function ()
        { notPursuingGraduation(); showHideOtherBox('ugcourseId', 'ougCid'); showHideOtherBox('ugcourseId', 'ougSid'); showHideOtherBox('ugspecId', 'ougSid'); showHideOtherBox('uginstituteId', 'ougUid'); showHideOtherBox('pgcourseId', 'opgCid'); showHideOtherBox('pgcourseId', 'opgSid'); showHideOtherBox('pgspecId', 'opgSid'); showHideOtherBox('pginstituteId', 'opgUid'); showHideOtherBox('ppgcourseId', 'oppgCid'); showHideOtherBox('ppgcourseId', 'oppgSid'); showHideOtherBox('ppgspecId', 'oppgSid'); showHideOtherBox('ppginstituteId', 'oppgUid'); }

        function seteId(educationId) {
            eId = educationId;
        }


        function ShowPGPanel() {
            $("#PnlPG").show();
        }
        function getSelectedValue(ctrlId) {
           $("#hf"+ctrlId).val($("#"+ctrlId+" option:selected").text());
        }
       
        </script>
    <asp:HiddenField ID="hfEducation_PG_Pkey" runat="server" />
    <asp:HiddenField ID="hfEducation_UG_Pkey" runat="server" />
    <asp:HiddenField ID="hfPercentCount" runat="server" Value="0" />
    <asp:HiddenField ID="hfCandidateImageUrl" runat="server" />
    <asp:HiddenField ID="hfResumePath" runat="server" />
            <script type='text/javascript' src='https://ajax.googleapis.com/ajax/libs/jquery/2.2.2/jquery.min.js'></script>

    <script src="myJS/EducationDetail.js" type="text/javascript"></script>
    <script src="myJS/mnjComp_14122015.min.js" type="text/javascript"></script>
    <script src="myJS/pgSpec_20012015.js" type="text/javascript"></script>
    <script src="myJS/ppgSpec.js" type="text/javascript"></script>
    <script src="myJS/ugSpec.js" type="text/javascript"></script>
</asp:Content>
