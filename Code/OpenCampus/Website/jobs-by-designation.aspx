﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.master" AutoEventWireup="true" CodeFile="jobs-by-designation.aspx.cs" Inherits="jobs_by_designation" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
    <title> Browse Jobs by Job Titles / Designations – OpenCampus.in</title>
<meta name="robots" content="INDEX,FOLLOW" />
<meta  property="og:description" name="description" content="Browse the Most Popular Job Titles/Designation – Assistant Professor, Business Analyst, Computer Operator, Accountant, SEO Executive, HR Executive, Project Leader, Law Officer, Senior Java Developer, Software Developer."/>

   <meta name="keywords" content=" Senior Web Designer Jobs, SEO Specialist Jobs, Fashion Designer Jobs, Civil Supervisor Jobs, Software Developer Jobs, Senior Interior Designer Jobs, Sales Officer, PHP Developer Jobs, HR Executive Jobs, Electrical Engineer Jobs."/>


   <meta name="application-name" content="opencampus.in - Your job search starts and ends with us."/>

   <meta name="copyright" content="2017 opencampus.in" />

   <meta name="content-language" content="EN" />

   <meta name="author" content="www.opencampus.in" />

   <meta name="distribution" content="GLOBAL" />


   <meta name="robots" content="ALL" />

   <meta name="pragma" content="no-cache" />

   <meta name="revisit-after" content="1 day" />

   <meta name="classification" content=" Browse Jobs by Job Titles / Designations – OpenCampus.in " />

   <meta property="og:image" content="https://opencampus.in/images/logo-opencampus.png"/>

   <link rel="image_src" href="https://opencampus.in/images/logo-opencampus.png" />

   <link href="https://plus.google.com/+opencampus1" rel="publisher" />


	<meta property="og:type" content="website" />

	<meta property="og:url" content="https://opencampus.in" />  

	<meta property="og:title" content="Open Campus" /> 

	<meta property="og:description" content="Open Campus, is India's leading online career and recruitment resource with its cutting edge technology provides relevant profiles to employers"/> 


	<meta property="og:image" content="https://opencampus.in/images/logo-opencampus.png" /> 

	<meta property="og:site_name" content="opencampus.in"/>

<meta name="twitter:card" content="summary" />
<meta name="twitter:url" content="https://www.opencampus.in" />
<meta name="twitter:site" content="@opencampusdot">
<meta name="twitter:title" content=" Browse Jobs by Job Titles / Designations – OpenCampus.in " />
<meta name="twitter:description" content="Browse the Most Popular Job Titles/Designation – Assistant Professor, Business Analyst, Computer Operator, Accountant, SEO Executive, HR Executive, Project Leader, Law Officer, Senior Java Developer, Software Developer." />
<meta name="twitter:image" content="https://pbs.twimg.com/profile_images/872485092932833280/umLVkRsm_400x400.jpg" />

</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="cph" Runat="Server">
<UC:MenuCategory ID="ucMenuCategoryLinks" runat="server" />

			<div class="container-wrap">
				<div class="main-content container-boxed max">
					<div class="row">
                    
                    <div class="noo-main col-md-12">
                        <div class="col-md-8">
                        <h1> Browse Jobs by Job Titles / Designations</h1> 
                        </div>
                        <div class="col-md-4">
                            <input type="search" class="form-control" placeholder="Search by Designation"/>
                        </div>
                    </div>
                     <div class="noo-main col-md-12">
                        <div class="search-links-filter">  
                    <a href="jobs-by-designation">All</a>
                    <a href="jobs-by-designation-a">A</a><a href="jobs-by-designation-b">B</a><a href="jobs-by-designation-c">C</a><a href="jobs-by-designation-d">D</a><a href="jobs-by-designation-e">E</a><a href="jobs-by-designation-f">F</a><a href="jobs-by-designation-g">G</a><a href="jobs-by-designation-h">H</a><a href="jobs-by-designation-i">I</a><a href="jobs-by-designation-j">J</a><a href="jobs-by-designation-k">K</a><a href="jobs-by-designation-l">L</a><a href="jobs-by-designation-m">M</a><a href="jobs-by-designation-n">N</a><a href="jobs-by-designation-o">O</a><a href="jobs-by-designation-p">P</a><a href="jobs-by-designation-q">Q</a><a href="jobs-by-designation-r">R</a><a href="jobs-by-designation-s">S</a><a href="jobs-by-designation-t">T</a><a href="jobs-by-designation-u">U</a><a href="jobs-by-designation-v">V</a><a href="jobs-by-designation-w">W</a><a href="jobs-by-designation-x">X</a><a href="jobs-by-designation-y">Y</a><a href="jobs-by-designation-z">Z</a><a href="jobs-by-designation-09">0-9</a>
                </div>    
                    </div>
						<div class="noo-main col-md-12">
							<div class="member-manage">
								<h5>Browse Jobs by Job Titles / Designations</h5>
							<div class="member-manage-inner">
                             <ul>
                                <asp:Repeater ID="rptrJobsByDesignation" runat="server">
                                <ItemTemplate>
                                 <li><a href="<%# MyUrl(Eval("FilterName").ToString()) %>-jobs" title="<%# Eval("FilterName") %> Jobs"><%# Eval("FilterName") %></a></li>
                                </ItemTemplate>
                                </asp:Repeater>				
                                </ul>						

                                    
                                    </div>
							</div>                           
						</div>  
                        
					</div> 
				</div> 
			</div>
</asp:Content>

