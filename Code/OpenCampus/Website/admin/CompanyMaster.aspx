﻿<%@ Page Title="" Language="C#" MasterPageFile="~/admin/Admin.master" AutoEventWireup="true" CodeFile="CompanyMaster.aspx.cs" Inherits="admin_CompanyMaster" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="cph" Runat="Server">
  <div class="page-header">
							<h1>
								Dashboard
								<small>
									<i class="ace-icon fa fa-angle-double-right"></i>
								Add Company Profile
								</small>
							</h1>
						</div><!-- /.page-header -->

    <div class="row">
        <div class="col-sm-12">
            <div class="widget-box">
											<div class="widget-header">
												<h4 class="widget-title">Create Company
                                                 <span class="pull-right">
                                                     <asp:HyperLink ID="HyperLink1" CssClass="btn btn-info" runat="server" NavigateUrl="~/admin/CompanyMasterDetail.aspx">View All</asp:HyperLink>
                                                 </span> 
                                                 </h4>
											</div>

											<div class="widget-body">
												<div class="widget-main">
													<div class="form-inline">
                                                       <div class="col-sm-3">
                                                        <label>Company Name</label>
                                                           <div>
                                                             <asp:TextBox ID="TxtCompanyName" runat="server" Width="100%"></asp:TextBox>
                                                               </div>
                                                       </div>
                                                         <div class="col-sm-3">
                                                        <label>Address</label>
                                                             <div>
                                                             <asp:TextBox ID="TxtAddress" runat="server" Width="100%"></asp:TextBox>

                                                                 </div>
                                                       </div>
                                                         <div class="col-sm-3">
                                                        <label>Logo</label>
                                                           <div>  <asp:FileUpload ID="FULogo" runat="server" />
                                                               <asp:Image ID="ImgLogo" runat="server" Width="100px" Visible="false" />
                                                         <br />   note: logo size should be 100x100 or 150x150 or 200x200</div>
                                                            
                                                       </div>
                                                        <div class="col-sm-3">
                                                        <label>Company Website</label>
                                                           <div> <asp:TextBox ID="TxtWebsiteUrl" runat="server" Width="100%"></asp:TextBox></div>
                                                            
                                                       </div>
                                                      <div class="clearfix"></div>
                                                         <div class="col-sm-3">
                                                             <br />
                     <asp:LinkButton ID="BtnSave" class="btn btn-info btn-sm" runat="server" OnClick="BtnSave_Click"><i class="ace-icon fa fa-save bigger-110"></i> Save</asp:LinkButton>
                                                       </div>												
														 <div class="clearfix"></div>
													</div>
												</div>
											</div>
										</div>
        </div>
    </div>
    <asp:HiddenField ID="hfPkey" runat="server" />
    <asp:HiddenField ID="hfImagePath" runat="server" />
</asp:Content>

