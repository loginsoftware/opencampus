﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using BusinessLayer;

public partial class jobs_by_location : Utility // System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!Page.IsPostBack)
        {
            string filtr = "";
            try
            {
              filtr = this.Page.RouteData.Values["filtr"].ToString();
            }
            catch { }

            LoadJobLocation(filtr);
        }
    }

    private void LoadJobLocation(string Filter)
    {
        JobPostingBL objF = new JobPostingBL();
        List<DataLayer.JobPostingCityList_Result> lst = objF.LoadAllJobPostingCityList(Filter);

        string _stateCurrent = "";
        string _statePrev = "";
        string _inner = "";
        for (int i = 0; i < lst.Count; i++)
        {
            _stateCurrent = lst[i].StateName;
            if (i == 0)
            {
                _inner += "<div><a href='jobs-in-" + MyUrl(lst[i].Name) + "' title='Jobs in " + lst[i].Name + "'>" + lst[i].Name + "</a></div>";
                _statePrev = _stateCurrent;
            }
            else if (_statePrev == _stateCurrent)
            {
                _inner += "<div><a href='jobs-in-" + MyUrl(lst[i].Name) + "' title='Jobs in " + lst[i].Name + "'>" + lst[i].Name + "</a></div>";
            }
            else if (_statePrev != _stateCurrent)
            {
                LtrJobLocation.Text += "<li><div><span>" + _statePrev + "</span>" + _inner + "</div></li> ";
                _statePrev = _stateCurrent;
                _inner = "";
                _inner += "<div><a href='jobs-in-" + MyUrl(lst[i].Name) + "' title='Jobs in " + lst[i].Name + "'>" + lst[i].Name + "</a></div>";
                _statePrev = _stateCurrent;
            }

            if (i == lst.Count - 1)
            {
                LtrJobLocation.Text += "<li><div><span>" + _statePrev + "</span>" + _inner + "</div></li> ";
            }
           
        }
        
    }
}