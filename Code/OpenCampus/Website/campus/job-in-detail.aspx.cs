﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using BusinessLayer;

public partial class campus_job_in_detail : Utility // System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!Page.IsPostBack)
        {
            try
            {
                string jobId = this.Page.RouteData.Values["JobId"].ToString();
                hfJobId.Value = jobId;
                LoadJobDetail(int.Parse(jobId));
            }
            catch {
                Response.Redirect("http://www.opencampus.in", false);
                Response.StatusCode = 301;
                Response.End(); 
            }
        }
    }

    private void LoadJobDetail(int JobId)
    {
        JobPostingBL obj = new JobPostingBL();
        List<DataLayer.JobPostingJobDetailByJobId_Result> J = obj.LoadJobPostingJobDetailByJobId(JobId);
        Page.Title = J[0].JobTitleDesignation;
        lblJobTitleHeader.Text = J[0].JobTitleDesignation;
        lblExperience.Text = J[0].WorkExperience.ToString().Replace("TO", " to ") + " years";
        lblPostedOn.Text = J[0].CreatedOn.ToString();


        lblSalary.Text = J[0].AnnualCTC.ToString().Replace("TO", " to ") + " " + J[0].OtherSalaryDetail;
        lblExperience2.Text = J[0].WorkExperience.ToString().Replace("TO", " to ") + " years";
        lblIndustryName.Text = J[0].IndustryName;
        lblFunctionalArea.Text = J[0].FunctionalAreaName;

        string[] _skill = J[0].Keywords.Trim(',').ToString().Split(',');
        for (int i = 0; i < _skill.Length; i++)
        {
            lblKeywordSkill.Text += "<label class='badge'>" + _skill[i].ToString() + "</label> ";
        }

        lblNoOfVacancy.Text = J[0].NoOfVacancy.ToString();
        lblReferenceCode.Text = J[0].ReferenceCode;
        lblJobDescription.Text = J[0].JobDescription;
        lblJobLocations.Text = J[0].JobLocation;

        EmployeeMasterBL objE = new EmployeeMasterBL();
        List<DataLayer.VEmployeeMaster> E = objE.LoadEmployeeMasterByEmployeeId(J[0].EmployeeId);
        lblPostedByCompanyName.Text = E[0].CurrentEmployer;
        lblPostedByCompanyAddress.Text = E[0].Address1 + ", " + E[0].Address2 + ", " + E[0].Name;
        lblPostedByCompanyWebsiteUrl.Text = E[0].WebsiteUrl;
        if (E[0].WebsiteUrl.Contains("http://"))
        {
            lblPostedByCompanyWebsiteUrl.NavigateUrl = E[0].WebsiteUrl;
        }
        else
        {
            lblPostedByCompanyWebsiteUrl.NavigateUrl = "http://" + E[0].WebsiteUrl;
        }

        lnkShareFB.HRef = "http://www.facebook.com/sharer/sharer.php?u=" + HttpContext.Current.Request.Url.AbsoluteUri;
        lnkShareTW.HRef = HttpContext.Current.Request.Url.AbsoluteUri;
        lnkShareLN.HRef = HttpContext.Current.Request.Url.AbsoluteUri;

        if (J[0].JobPostCompanyId != null)
        {
            JobPostCompanyBL objP = new JobPostCompanyBL();
            List<DataLayer.JobPostCompany> C = objP.LoadJobPostCompanyByPrimaryKey(int.Parse(J[0].JobPostCompanyId.ToString()));
            lblCompanyName.Text = C[0].CompanyName;
           /// lblCompanyAddress.Text = C[0].CompanyAddress;
            lblCompanyAddress.Text = J[0].JobLocation;
            ImgCompanyLogo.Src = C[0].CompanyLogo;
            ImgCompanyLogo.Alt = J[0].JobTitleDesignation;
        }
        else
        {
            lblCompanyName.Text = E[0].CompanyName;
          //  lblCompanyAddress.Text = E[0].Address1 + ", " + E[0].Address2 + ", " + E[0].Name;
            lblCompanyAddress.Text = J[0].JobLocation;
            if (E[0].ImagePath != "")
            {
                ImgCompanyLogo.Src = E[0].ImagePath;                
            }
            else
            {
                ImgCompanyLogo.Src = GetCompanyLogoUrl();
            }
            ImgCompanyLogo.Alt = J[0].JobTitleDesignation;
        }

        int _industry = 0;
        int _functional = 0;
        if (J[0].Industry != null) _industry = int.Parse(J[0].Industry.ToString());
        if (J[0].FunctionalArea != null) _functional = int.Parse(J[0].FunctionalArea.ToString());
        LoadSimilarJobs(int.Parse(J[0].Industry), int.Parse(J[0].FunctionalArea));

        this.Page.Title = "" + J[0].JobTitleDesignation + " " + J[0].JobId + " Jobs - Opencampus.in";
        this.Page.MetaDescription = "Apply to " + J[0].JobTitleDesignation + " " + J[0].JobId + " job for " + J[0].Keywords + " on Opencampus.in, India's No.1 Job Portal. Explore " + J[0].JobDesignation + " Openings in your desired locations Now!";
    }
    protected void BtnLoginToApply_Click(object sender, EventArgs e)
    {
        if (GetCookie(this.Page, "CandidateId") != null as string)
        {
            int _candidateId = int.Parse(GetCookie(this.Page, "CandidateId"));
            JobApplicationBL objA = new JobApplicationBL();

            try
            {
                objA.InsertJobApplication(int.Parse(hfJobId.Value), _candidateId, DateTime.Now, "Pending");
                ShowMessage("Thank you for apply, we will notify you if selected");
            }
            catch(Exception Ex)
            {
                if (Ex.InnerException.ToString().Contains("UK_JobApplication"))
                {
                    ShowMessage("Oho, OpenCampus feels that you have already apply for this job.");
                }
            }
        }
        else
        {
            string requestUrl = HttpContext.Current.Request.Url.AbsoluteUri;
            Response.Redirect("~/campus/Login.aspx?requestUrl=" + requestUrl, false);
        }
    }

    private void LoadSimilarJobs(int Industry, int FunctionalArea)
    {
        JobPostingBL objP = new JobPostingBL();
        List<DataLayer.JobPosting> J = objP.LoadJobPostingSimilarJob(Industry, FunctionalArea, DateTime.Now, 10);
        if (J.Count > 0)
        {
            for (int i = 0; i < J.Count; i++)
            {
                LtrSimilarJob.Text += "<div class='similar-jb'>	"
                            + "<a href='/jobs/" + MyUrl(J[i].JobTitleDesignation).Trim('-') + "-"+J[i].JobId+"' class='ellipsis font-s-14' target='_blank'>"+J[i].JobTitleDesignation+"</a>"
                            + "<span class='show'><i class='fa fa-suitcase'></i> " + J[i].WorkExperience.ToString().Replace("TO", " to ") + " years" + "</span><span class='show'><i class='fa fa-map-marker'></i> "+J[i].JobLocation+"</span></div>";
            }
        }
    }
}