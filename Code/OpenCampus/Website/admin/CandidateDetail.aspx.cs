﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using BusinessLayer;

public partial class admin_CandidateDetail : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!Page.IsPostBack)
        {
            TxtFromDate.Text = DateTime.Now.AddDays(-14).ToString("dd-MMM-yyyy");
            TxtToDate.Text = DateTime.Now.ToString("dd-MMM-yyyy");
            LoadCandidateDetail();
        }
    }

    private void LoadCandidateDetail()
    {
        try
        {
            CandidateMasterBL obj = new CandidateMasterBL();
            GVCandidate.DataSource = obj.LoadCandidateMasterBetweenDates(DateTime.Parse(TxtFromDate.Text), DateTime.Parse(TxtToDate.Text), TxtCandidateName.Text);
            GVCandidate.DataBind();
        }
        catch { }
    }
    protected void BtnSearch_Click(object sender, EventArgs e)
    {
        LoadCandidateDetail();
    }
}