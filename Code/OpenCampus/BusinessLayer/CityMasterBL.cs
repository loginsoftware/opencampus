﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data.Objects;
using DataLayer;

namespace BusinessLayer
{
    public class CityMasterBL
    {
        public List<CityMaster> LoadCityMasterByPrimaryKey(int UserTypeId)
        {
            DBEntities Obj = new DBEntities();
            return Obj.LoadCityMasterByPrimaryKey(UserTypeId).ToList();
        }

        public List<CityMaster> LoadAllCityMaster()
        {
            DBEntities Obj = new DBEntities();
            return Obj.LoadAllCityMaster().ToList();
        }

        public List<CityMaster> LoadCityMasterByStateID(int StateID)
        {
            DBEntities Obj = new DBEntities();
            return Obj.LoadCityMasterByStateID(StateID).ToList();
        }

        public List<CityMasterSearchName_Result> LoadCityMasterBySearchName(string CityName)
        {
            DBEntities Obj = new DBEntities();
            return Obj.LoadCityMasterBySearchName(CityName).ToList();
        }
    }
}
