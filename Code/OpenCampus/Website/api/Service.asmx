﻿<%@ WebService Language="C#" Class="Service" %>

using System;
using System.Web;
using System.Web.Services;
using System.Web.Services.Protocols;
using System.Collections.Generic;
using BusinessLayer;
using MailChimp;
using MailChimp.Lists;
using MailChimp.Helper;

[WebService(Namespace = "http://tempuri.org/")]
[WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
// To allow this Web Service to be called from script, using ASP.NET AJAX, uncomment the following line. 
 [System.Web.Script.Services.ScriptService]
public class Service  : System.Web.Services.WebService {

    [WebMethod]
    public void RegisterCandidate(string UserNameEmail, string Password)
    {
        string jsonoutput = "";
        string UserTypeId = "1"; //---candidate
        try
        {
            LoginDetailBL objL = new LoginDetailBL();
            int _loginId = objL.InsertLoginDetail(UserNameEmail, Password, int.Parse(UserTypeId), DateTime.Now);
            CandidateMasterBL objC = new CandidateMasterBL();
            int _candidateId = objC.InsertCandidateMaster("", null, "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", false, false, _loginId);
      jsonoutput = "[{\"Result\":\"Success\",\"Message\":\"Registered Successfully\",\"CandidateId\":"+_candidateId+"}]";
        }
        catch(Exception Ex)
        {
            if (Ex.InnerException.Message.Contains("UK_UserNameEmail"))
            {
                jsonoutput = "[{\"Result\":\"Error\",\"Message\":\"Please Enter Another Email Address\"}]";

            }
        }

        HttpContext.Current.Response.Write(jsonoutput);
    }

    [WebMethod]
    public void LoginUser(string UserNameEmail, string Password)
    {
        string jsonoutput = "";
        Utility Ut = new Utility();
        try
        {
            LoginDetailBL objL = new LoginDetailBL();
            List<DataLayer.LoginDetail> L = objL.LoadLoginDetailEmpByUserName(UserNameEmail);
            if (L.Count > 0)
            {
                if (L[0].Password == Password)
                {
                    jsonoutput = "[{\"Result\":\"Success\",\"Message\":\"Logged Successfully\",\"Password\":\"" + Ut.EncryptString(Password) + "\"}]";

                }
                else
                {
                    jsonoutput = "[{\"Result\":\"Error\",\"Message\":\"User or Password incorrect\",\"CandidateId\":" + 0 + "}]";

                }
            }
            else
            {
                jsonoutput = "[{\"Result\":\"Error\",\"Message\":\"User or Password incorrect\",\"CandidateId\":" + 0 + "}]";
            }
            
        }
        catch (Exception Ex)
        {
                jsonoutput = "[{\"Result\":\"Error\",\"Message\":\"Please Enter Another Email Address\"}]";
        }

        HttpContext.Current.Response.Write(jsonoutput);
    }

    [WebMethod]
    public void CandidateProfile(string CandidateId, string FullName, string DOB, string Gender, string MaritalStatus, string Mobile, string Landline, string Address, string HometownCity, string Pincode, string PreferedLocation, string TotalExpe, string AnnualCtc, string Currency, string ResumeHeadline, string KeySkill)
    {
        CandidateMasterBL obj = new CandidateMasterBL();
        obj.UpdateCandidateMasterBasicProfile(int.Parse(CandidateId), FullName, DateTime.Parse(DOB), Gender, MaritalStatus, Mobile, Landline, Address, HometownCity, Pincode, PreferedLocation, TotalExpe, AnnualCtc, Currency, ResumeHeadline, KeySkill);

       string jsonoutput = "[{\"Result\":\"Success\",\"Message\":\"Profile Update Successfully\"}]";
       HttpContext.Current.Response.Write(jsonoutput);
        
    }

    [WebMethod]
    public void SearchCityName(string keyword)
    {
        try
        {
            CityMasterBL obj = new CityMasterBL();
            List<DataLayer.CityMasterSearchName_Result> C = obj.LoadCityMasterBySearchName(keyword);
            string jsonoutput = "";
            for (int i = 0; i < C.Count; i++)
            {
                jsonoutput += "'" + C[i].Name + "'" + ",";
            }
            jsonoutput = jsonoutput.TrimEnd(',');
            HttpContext.Current.Response.Write("[" + jsonoutput.Replace("'", "\"") + "]");
        }
        catch { }
    }

    [WebMethod]
    public void SearchJobDesignation(string keyword)
    {
        FilterMasterBL obj = new FilterMasterBL();
        List<DataLayer.FilterMaster> C = obj.LoadFilterMasterSearchByFilterType(keyword, "DesignationJobTitle");
        string jsonoutput = "";
        for (int i = 0; i < C.Count; i++)
        {
            jsonoutput += "'" + C[i].FilterName + "'" + ",";
        }
        jsonoutput = jsonoutput.TrimEnd(',');
        HttpContext.Current.Response.Write("[" + jsonoutput.Replace("'", "\"") + "]");
    }

    [System.Runtime.Serialization.DataContract]
    public class MyMergeVar : MergeVar
    {
        [System.Runtime.Serialization.DataMember(Name = "FNAME")]
        public string FirstName { get; set; }
        [System.Runtime.Serialization.DataMember(Name = "LNAME")]
        public string LastName { get; set; }
    }
    //------------------- mailchimp.NET api dll read document from github and self chrome bookmark
    [WebMethod]
    public void SubscribeMailchimp(string _Email, string _FirstName, string _LastName)
    {
        MailChimpManager mc = new MailChimpManager("75ea6bf237e400e9a19721a2d96343c5-us11");

        MyMergeVar myMergeVars = new MyMergeVar();
        myMergeVars.FirstName = _FirstName;
        myMergeVars.LastName = _LastName;

        //	Create the email parameter
        EmailParameter email = new EmailParameter()
        {
            Email = _Email    
        };
        

        EmailParameter results = mc.Subscribe("751043c730", email, myMergeVars);
        
        string jsonoutput = "[{\"Result\":\"Success\",\"Message\":\"Added Successfully\"}]";
        HttpContext.Current.Response.Write(jsonoutput);

    }

    [WebMethod]
    public void SaveJobPosting(string JobTitleDesignation, string JobDescription, string keywordsSkills, string MinExperienceTOMaxExperience, string MinSalaryTOMaxSalary, string OtherSalaryDetail, string NoOfVacancy, string JobLocation, string DdnIndustry, string DdnFunctionalArea, string JobResponseOnEmail, string JobResponseOnWalkIn, string JobResponseEmail, string FromDateWalkinTOToDateWalkin, string ReferenceCode,  string isPublished, string LinkToJob, string JobPostCompanyId, string DdnDesignation)
    {
        Utility Ut = new Utility ();
        string jsonoutput = "";
        string _employeeId = Context.Request.Cookies["EmployeeId"].Value;
        string chkIsFeatured = "True";
        try
        {
            JobPostingBL obj = new JobPostingBL();
            int _jobId = obj.InsertJobPosting(int.Parse(_employeeId), JobTitleDesignation, JobDescription.Replace("\r\n", "<br/>"), keywordsSkills.Trim(','), MinExperienceTOMaxExperience, MinSalaryTOMaxSalary, OtherSalaryDetail, NoOfVacancy, JobLocation.Trim(','), DdnIndustry, DdnFunctionalArea, bool.Parse(JobResponseOnEmail), bool.Parse(JobResponseOnWalkIn), JobResponseEmail, FromDateWalkinTOToDateWalkin, ReferenceCode, "", bool.Parse(chkIsFeatured), DateTime.Now, bool.Parse(isPublished), LinkToJob, DateTime.Now.AddDays(15), int.Parse(JobPostCompanyId), DdnDesignation);
            jsonoutput = "[{\"Result\":\"Success\",\"Message\":\"Job Posted Successfully\",\"JobId\":" + _jobId + "}]";
        }
        catch (Exception Ex)
        {

            jsonoutput = "[{\"Result\":\"Error\",\"Message\":\"" + Ex.Message.ToString() + "\"}]";
           
        }

        HttpContext.Current.Response.Write(jsonoutput);
    }
    
}