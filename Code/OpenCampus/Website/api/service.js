﻿var requestUrl = window.location.origin + "/api/Service.asmx";

function RegisterCandidate() {
    var url = requestUrl + "/RegisterCandidate";
    var email = $("[name='user_email']").val();
    var pasword = $("[name='user_password']").val();
    var usertype = $("[name='user_role']").val();
    if (email == "") return;
    if (pasword == "") return;
    if (usertype == "1") {

        $.getJSON(url, { "UserNameEmail": email, "Password": pasword })
                .done(function (data, status) {
                    if (data[0].Result === "Error") {
                        alert(data[0].Message);
                    }
                    if (data[0].Result === "Success") {
                        $.cookie("CandidateId", data[0].CandidateId, { path: '/' });
                        window.location = "../Candidate_profile_basic.aspx?type=profilebasic";
                    }
                })
                .fail(function (data, status) {

                });
    }
    else {
        window.location = "../employer-registration.aspx?reg=emp&email=" + email + "&pass=" + pasword + "";
    }

        };

        $("#btnLogin").click(function() {
            var url = requestUrl + "/LoginUser";
            var email = $("[name='loginemail']").val();
            var pasword = $("[name='loginpassword']").val();
            $.getJSON(url, { "UserNameEmail": email, "Password": pasword })
            .done(function (data, status) {
                if (data[0].Result === "Error") {
                    alert(data[0].Message);
                }
                if (data[0].Result === "Success") {
                    $.cookie("CandidateId", data[0].CandidateId, { path: '/' });
                    window.location = "campus/Login.aspx?action=login&user=" + email + "&pwd=" + data[0].Password + "";
                }
            })
            .fail(function (data, status) {

            });

        });

        function CandidateBasicProfile() {

            var email = $("[name='loginemail']").val();
            var pasword = $("[name='loginpassword']").val();
            var url = requestUrl + "/CandidateProfile";
            $.getJSON(url, { "UserNameEmail": email, "Password": pasword })
            .done(function (data, status) {
                if (data[0].Result === "Error") {
                    alert(data[0].Message);
                }
                if (data[0].Result === "Success") {
                    $.cookie("candidate", data[0].CandidateId);
                    window.location = "Candidate_profile_basic.aspx";
                }
            })
            .fail(function (data, status) {

            });

        };

        $("#LnkSaveAndPublish").click(function () {
            var _isPublished = "True";
            SaveJobPosting(_isPublished);
        });
        $("#LnkSaveAndPublishLater").click(function () {
            var _isPublished = "False";
            SaveJobPosting(_isPublished);
        });

        function SaveJobPosting(_isPublished) {
            //------ job location
            var divLocation = $('#cph_lstbxJobLocation_chosen').find('span');
            var _location = "";
            for (var i = 0, l = divLocation.length; i < l; i++) {
                _location += divLocation[i].innerText + ",";
            }            
            //----- job skills
            var divSkills = $('#DdnSkills_chosen').find('span');
            var _keywordSkill = "";
            for (var i = 0, l = divSkills.length; i < l; i++) {
                _keywordSkill += divSkills[i].innerText + ",";
            }
            if ($("#cph_txtJobTitleDesignation").val() == "") {
                alert("Enter Job Display/Title.");
                return;
            }
            if ($("#cph_txtJobDescription").val() == "") {
                alert("Enter Job Description.");
                return;
            }
            if (_keywordSkill == "") {
                alert("Select Job Skills & Keywords.");
                return;
            }
            if (_location == "") {
                alert("Select Job Locations.");
                return;
            }
            if ($("#DdnIndustry").val() == "--Job Industry Type--") {
                alert("Select Job Industry Type.");
                return;
            }
            if ($("#DdnFunctionalArea").val() == "--Job Functional Area--") {
                alert("Select Job Functional Area.");
                return;
            }            

            var JobResponseOnEmail = "False";
            var JobResponseOnWalkIn = "False";
            var responseType = $("#hfJobResponseType").val();
            if (responseType == "On Email") JobResponseOnEmail = "True";
            if (responseType == "Walk-In") JobResponseOnWalkIn = "True";

            if (responseType == "Walk-In") {
                if ($("#cph_txtFromDateWalkin").val() == "" || $("#cph_txtToDateWalkin").val() == "") {
                    alert("Enter Valid Job Walk-In Date");
                    return;
                }

            }
            if (responseType == "On Email") {
                if ($("#cph_txtJobResponseEmail").val() == "") {
                    alert("Enter job response Email Address.");
                    return;
                }

            }


            var _jobPostCompany = "1";
            if ($("#DdnJobPostCompany").val() != "--Select--") _jobPostCompany = $("#DdnJobPostCompany").val();
            var Designation = $("#DdnDesignation").val();
            var url = requestUrl + "/SaveJobPosting";
            $.getJSON(url, { "JobTitleDesignation": $("#cph_txtJobTitleDesignation").val(), "JobDescription": $("#cph_txtJobDescription").val(), "keywordsSkills": _keywordSkill, "MinExperienceTOMaxExperience": $("#cph_txtMinExperience").val() + "TO" + $("#cph_txtMaxExperience").val(), "MinSalaryTOMaxSalary": $("#cph_txtMinSalary").val() + "TO" + $("#cph_txtMaxSalary").val(), "OtherSalaryDetail": $("#cph_txtOtherSalaryDetail").val(), "NoOfVacancy": $("#cph_txtNoOfVacancy").val(), "JobLocation": _location, "DdnIndustry": $("#DdnIndustry").val(), "DdnFunctionalArea": $("#DdnFunctionalArea").val(), "JobResponseOnEmail": JobResponseOnEmail, "JobResponseOnWalkIn": JobResponseOnWalkIn, "JobResponseEmail": $("#cph_txtJobResponseEmail").val(), "FromDateWalkinTOToDateWalkin": $("#cph_txtFromDateWalkin").val() + "TO" + $("#cph_txtToDateWalkin").val(), "ReferenceCode": $("#cph_txtReferenceCode").val(), "isPublished": _isPublished, "LinkToJob": $("#cph_txtLinkToJob").val(), "JobPostCompanyId": _jobPostCompany, "DdnDesignation": Designation })
           .done(function (data, status) {
               if (data[0].Result === "Error") {
                   alert(data[0].Message);
               }
               if (data[0].Result === "Success") {
                   if (_isPublished == "True") {
                       alert(data[0].Message + " \n Job Reference No# " + data[0].JobId);
                   }
                   else if (_isPublished == "False") {
                       alert("Job Saved, but Not Published, to publish go to active jobs, \n Job Reference No# " + data[0].JobId);
                   }
                   window.location = window.location.href;
               }
           })
           .fail(function (data, status) {

           });

        }