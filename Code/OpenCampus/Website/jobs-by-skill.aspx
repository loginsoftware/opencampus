﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.master" AutoEventWireup="true" CodeFile="jobs-by-skill.aspx.cs" Inherits="jobs_by_skill" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
    <title> Browse Top Jobs by Skills | Best Jobs for Your Skills - OpenCampus.in</title>
<meta name="robots" content="INDEX,FOLLOW" />
<meta  property="og:description" name="description" content="Browse the Most Popular Jobs by IT Skills – Software Testing, .NET, PHP, Oracle, Java, Programming, Ajax, Online Marketing, Web Designing, SQL | Non IT Skills - Banking, Teaching, Finance, Photography, Accounting, Business Analysis."/>

   <meta name="keywords" content=" Banking Jobs, Teaching Jobs, Finance Jobs, Photography Jobs, Accounting Jobs, Business Analysis Jobs, Software Testing Jobs, .NET Jobs, PHP Jobs, Oracle Jobs, Java Developer Jobs, Programming, Ajax, Online Marketing Jobs, Web Designing Jobs "/>


   <meta name="application-name" content="opencampus.in - Your job search starts and ends with us."/>

   <meta name="copyright" content="2017 opencampus.in" />

   <meta name="content-language" content="EN" />

   <meta name="author" content="www.opencampus.in" />

   <meta name="distribution" content="GLOBAL" />


   <meta name="robots" content="ALL" />

   <meta name="pragma" content="no-cache" />

   <meta name="revisit-after" content="1 day" />

   <meta name="classification" content=" Browse Jobs by Job Titles / Designations – OpenCampus.in " />

   <meta property="og:image" content="https://opencampus.in/images/logo-opencampus.png"/>

   <link rel="image_src" href="https://opencampus.in/images/logo-opencampus.png" />

   <link href="https://plus.google.com/+opencampus1" rel="publisher" />


	<meta property="og:type" content="website" />

	<meta property="og:url" content="https://opencampus.in" />  

	<meta property="og:title" content="Open Campus" /> 

	<meta property="og:description" content="Open Campus, is India's leading online career and recruitment resource with its cutting edge technology provides relevant profiles to employers"/> 


	<meta property="og:image" content="https://opencampus.in/images/logo-opencampus.png" /> 

	<meta property="og:site_name" content="opencampus.in"/>

<meta name="twitter:card" content="summary" />
<meta name="twitter:url" content="https://www.opencampus.in" />
<meta name="twitter:site" content="@opencampusdot" />
<meta name="twitter:title" content=" Browse Top Jobs by Skills | Best Jobs for Your Skills - OpenCampus.in " />
<meta name="twitter:description" content="Browse the Most Popular Jobs by IT Skills – Software Testing, .NET, PHP, Oracle, Java, Programming, Ajax, Online Marketing, Web Designing, SQL | Non IT Skills - Banking, Teaching, Finance, Photography, Accounting, Business Analysis." />
<meta name="twitter:image" content="https://pbs.twimg.com/profile_images/872485092932833280/umLVkRsm_400x400.jpg" />

<style>
.text {
    overflow:hidden;
}
.height {
    max-height:1000px;
}
.heightAuto {
    max-height:5000px;
}
</style>
    
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="cph" Runat="Server">
<UC:MenuCategory ID="ucMenuCategoryLinks" runat="server" />

			<div class="container-wrap">
				<div class="main-content container-boxed max">
					<div class="row">
                     <div class="noo-main col-md-12">
                        <div class="col-md-8">
                            <h1>Browse Jobs by Skills</h1>
                        </div>
                        <div class="col-md-4">
                            <input type="search" id="txtSearch" class="form-control" placeholder="Search by Top Skills"/>
                        </div>
                    </div>
                    <div class="noo-main col-md-12">
                        <div class="search-links-filter">  
                    <a href="top-jobs-by-skill">All</a>
                    <a href="top-jobs-by-skill-a">A</a><a href="top-jobs-by-skill-b">B</a><a href="top-jobs-by-skill-c">C</a><a href="top-jobs-by-skill-d">D</a><a href="top-jobs-by-skill-e">E</a><a href="top-jobs-by-skill-f">F</a><a href="top-jobs-by-skill-g">G</a><a href="top-jobs-by-skill-h">H</a><a href="top-jobs-by-skill-i">I</a><a href="top-jobs-by-skill-j">J</a><a href="top-jobs-by-skill-k">K</a><a href="top-jobs-by-skill-l">L</a><a href="top-jobs-by-skill-m">M</a><a href="top-jobs-by-skill-n">N</a><a href="top-jobs-by-skill-o">O</a><a href="top-jobs-by-skill-p">P</a><a href="top-jobs-by-skill-q">Q</a><a href="top-jobs-by-skill-r">R</a><a href="top-jobs-by-skill-s">S</a><a href="top-jobs-by-skill-t">T</a><a href="top-jobs-by-skill-u">U</a><a href="top-jobs-by-skill-v">V</a><a href="top-jobs-by-skill-w">W</a><a href="top-jobs-by-skill-x">X</a><a href="top-jobs-by-skill-y">Y</a><a href="top-jobs-by-skill-z">Z</a><a href="top-jobs-by-skill-09">0-9</a>
                </div>    
                    </div>
						<div class="noo-main col-md-12">
							<div class="member-manage">
								<h5>Browse Jobs by IT Skills</h5>
							<div class="member-manage-inner">
                             <ul>
                                <asp:Repeater ID="rptrJobsByITSkills" runat="server">
                                <ItemTemplate>
                    <li><a href="<%# MyUrl(Eval("FilterName").ToString()) %>-jobs" title="<%# Eval("FilterName") %> Jobs"><%# Eval("FilterName") %></a></li>
                                </ItemTemplate>
                                </asp:Repeater>				
                                </ul>	
                               
                                    </div>
							</div>                           
						</div>  
                        <div class="noo-main col-md-12">
							<div class="member-manage">
								<h5>Browse Jobs by Non IT Skills</h5>
							<div class="member-manage-inner">
                            <ul>
                                <asp:Repeater ID="rptrJobsByNonITSkills" runat="server">
                                <ItemTemplate>
                               <li><a href="<%# MyUrl(Eval("FilterName").ToString()) %>-jobs" title="<%# Eval("FilterName") %> Jobs"><%# Eval("FilterName") %></a></li>
                                </ItemTemplate>
                                </asp:Repeater>				
                                </ul>
                                    
                                    </div>
							</div>                           
						</div>  


					</div> 
				</div> 
			</div>
            <script>
                function changeheight() {
                    var readmore = $('#readmore');
                    if (readmore.text() == 'Read more') {
                        readmore.text("Read less");
                    } else {
                        readmore.text("Read more");
                    }

                    $('.height').toggleClass("heightAuto");
                };
            </script>
</asp:Content>

