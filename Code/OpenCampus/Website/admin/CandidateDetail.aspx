﻿<%@ Page Title="Candidate Detail" Language="C#" MasterPageFile="~/admin/Admin.master" AutoEventWireup="true" CodeFile="CandidateDetail.aspx.cs" Inherits="admin_CandidateDetail" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="cph" Runat="Server">
    <div class="page-header">
							<h1>
								Dashboard
								<small>
									<i class="ace-icon fa fa-angle-double-right"></i>
									Candidate
								</small>
							</h1>
						</div><!-- /.page-header -->

    <div class="row">
        <div class="col-sm-12">
            <div class="widget-box">
											<div class="widget-header">
												<h4 class="widget-title">Filter Candidate</h4>
											</div>

											<div class="widget-body">
												<div class="widget-main">
													<div class="form-inline">
                                                       <div class="col-sm-3">
                                                        <label>Candidate Name</label>
                                                           <div>
                                                             <asp:TextBox ID="TxtCandidateName" runat="server" Width="100%"></asp:TextBox>
                                                               </div>
                                                       </div>
                                                         <div class="col-sm-3">
                                                        <label>From Date</label>
                                                             <div>
                                                             <asp:TextBox ID="TxtFromDate" runat="server" Width="100%"></asp:TextBox>

                                                                 </div>
                                                       </div>
                                                         <div class="col-sm-3">
                                                        <label>To Date</label>
                                                           <div> <asp:TextBox ID="TxtToDate" runat="server" Width="100%"></asp:TextBox></div>
                                                            
                                                       </div>
                                                      
                                                         <div class="col-sm-3">
                                                             <br />
                     <asp:LinkButton ID="BtnSearch" class="btn btn-info btn-sm" runat="server" OnClick="BtnSearch_Click"><i class="ace-icon fa fa-search bigger-110"></i> Search</asp:LinkButton>
                                                       </div>												
														 <div class="clearfix"></div>
													</div>
												</div>
											</div>
										</div>
        </div>
    </div>
    
						<div class="row">
							<div class="col-xs-12">
								<!-- PAGE CONTENT BEGINS -->

                                <div class="row">
									<div class="col-sm-12">
										<div class="widget-box">
											<div class="widget-header">
												<h4 class="widget-title lighter smaller">
													<i class="ace-icon fa fa-users blue"></i>
													Candidate Detail
												</h4>
											</div>
									<div style="overflow-x:scroll;">
       <asp:GridView ID="GVCandidate" runat="server" AutoGenerateColumns="false" CssClass="table  table-bordered table-hover">
           <Columns>
                <asp:TemplateField HeaderText="Sr No">
                   <ItemTemplate>
                      <%# Container.DataItemIndex + 1 %>
                   </ItemTemplate>
               </asp:TemplateField>
               <asp:BoundField DataField="CandidateName" HeaderText="Name" ItemStyle-Wrap="false" />
               <asp:BoundField DataField="MobileNo" HeaderText="Mobile" />
               <asp:BoundField DataField="UserNameEmail" HeaderText="Email" />
               <asp:TemplateField HeaderText="Date" ItemStyle-Wrap="false">
                   <ItemTemplate>
                       <asp:Label ID="lblRegisteredDate" ToolTip='<%# Eval("CreatedOn") %>' runat="server" Text='<%# Eval("CreatedOn", "{0:dd-MMM-yyyy}") %>'></asp:Label>
                   </ItemTemplate>
               </asp:TemplateField>
               <asp:BoundField DataField="Gender" HeaderText="Gender" />
               <asp:BoundField DataField="DOB" HeaderText="DOB" DataFormatString="{0:dd-MMM-yyyy}" ItemStyle-Wrap="false" />
                 <asp:TemplateField HeaderText="Photo">
                   <ItemTemplate>
                       <asp:Label ID="lblPhotoStatus" ToolTip='<%# Eval("ProfileImagePath") %>' runat="server" Text='<%# Eval("ProfileImagePath") == "" ? "No" : "Yes" %>' CssClass='<%# Eval("ProfileImagePath").ToString() == "" ? "label label-danger" : "label label-success" %>'></asp:Label>
                   </ItemTemplate>
               </asp:TemplateField>
                <asp:TemplateField HeaderText="Resume">
                   <ItemTemplate>
                       <asp:Label ID="lblResumeFilePath" ToolTip='<%# Eval("ResumeFilePath") %>' runat="server" Text='<%# Eval("ResumeFilePath") == "" ? "No" : "Yes" %>' CssClass='<%# Eval("ResumeFilePath").ToString() == "" ? "label label-danger" : "label label-success" %>'></asp:Label>
                   </ItemTemplate>
               </asp:TemplateField>
                <asp:TemplateField HeaderText="MobileVerify">
                   <ItemTemplate>
                       <asp:Label ID="lblIsMobileVerified" runat="server" Text='<%# Eval("IsMobileVerified").ToString() == "True" ? "Yes" : "No" %>' CssClass='<%# Eval("IsEmailVerified").ToString() == "True" ? "label label-success" : "label label-danger" %>'></asp:Label>
                   </ItemTemplate>
               </asp:TemplateField>
                <asp:TemplateField HeaderText="EmailVerify">
                   <ItemTemplate>
                       <asp:Label ID="lblIsEmailVerified" runat="server" Text='<%# Eval("IsEmailVerified").ToString() == "True" ? "Yes" : "No" %>' CssClass='<%# Eval("IsEmailVerified").ToString() == "True" ? "label label-success" : "label label-danger" %>'></asp:Label>
                   </ItemTemplate>
               </asp:TemplateField>
           </Columns>

       </asp:GridView>
						</div>

                                            </div>
									</div><!-- /.col -->

								</div><!-- /.row -->

                                	<!-- PAGE CONTENT ENDS -->
							</div><!-- /.col -->
						</div><!-- /.row -->
</asp:Content>

