﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using BusinessLayer;

public partial class EmployeeLogin : Utility // System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!Page.IsPostBack)
        {

        }
    }
    protected void BtnEmpLogin_Click(object sender, EventArgs e)
    {
        string Username = empEmail.Value;
        string Password = empPassword.Value;
        LoginDetailBL obj = new LoginDetailBL();
        List<DataLayer.LoginDetail> L = obj.LoadLoginDetailEmpByUserName(Username);
        if (L.Count > 0)
        {
            if (L[0].Password == Password)
            {
                //Session["LoginId"] = L[0].LoginId.ToString();
                SetCookie(this.Page, "LoginId", L[0].LoginId.ToString());
                Response.Redirect("Employee_Home.aspx");
            }
            else
            {
                ShowMessage("Invalid UserName or Password", this.Page);
            }
        }
        else
        {
            ShowMessage("Invalid UserName or Password", this.Page);
        }
    }
}