﻿<%@ Page Language="C#" EnableEventValidation="false" ValidateRequest="false" AutoEventWireup="true" CodeFile="choosen-example.aspx.cs" Inherits="choosen_example" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
    <link href="css/chosen.css" rel="stylesheet" type="text/css" />
</head>
<body>
    <form id="form1" runat="server">
    <asp:ScriptManager ID="ScriptManager1" runat="server">
    </asp:ScriptManager>
    <div>
        <asp:DropDownList ID="DropDownList1" runat="server" Width="200px" AppendDataBoundItems="true">
        <asp:ListItem>--Select--</asp:ListItem>
        </asp:DropDownList>
        <asp:ListBox ID="ListBox1" runat="server" EnableViewState="false" SelectionMode="Multiple" Width="250px" AppendDataBoundItems="false" AutoPostBack="false">
        </asp:ListBox>
       
        <br />
        <asp:Button ID="Button1" UseSubmitBehavior="false" runat="server" onclick="Button1_Click" Text="Button" />
   
    </div>  
   <br /><br />
   -------------------------------------------------------------------<br />
   <br />
    job Title<asp:TextBox ID="txttitle" runat="server"></asp:TextBox><br />
    job location<asp:TextBox ID="txtlocation" runat="server"></asp:TextBox><br />
    <asp:Button ID="Button2" runat="server" Text="get query" 
        onclick="Button2_Click" />
    <asp:Label ID="lblquery" runat="server" Text=""></asp:Label>

   
    
    </form>
    
     <script src="js/jquery.js" type="text/javascript"></script>
    <script src="js/chosen.jquery.min.js" type="text/javascript"></script>
    <script src="js/gofrendi.chosen.ajaxify.js" type="text/javascript"></script>
     <script type="text/javascript">

         jQuery('#DropDownList1').chosen({ allow_single_deselect: true, width: "200px", search_contains: true });
         chosen_ajaxify('DropDownList1', 'api/service.asmx/SearchCityName?keyword=');

         jQuery('#ListBox1').chosen({ allow_single_deselect: true, width: "200px", search_contains: true });
         chosen_ajaxify('ListBox1', 'api/service.asmx/SearchCityName?keyword=');
        </script>
</body>
</html>
