﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using BusinessLayer;
using System.IO;

public partial class Employee_Home : Utility // System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!Page.IsPostBack)
        {
            //if (Session["LoginId"] != null as string)
            if (GetCookie(this.Page, "LoginId") != null as string)
            {
                // hfLoginId.Value = Session["LoginId"].ToString();
                hfLoginId.Value = GetCookie(this.Page, "LoginId");
                LoadEmployeeInfo(int.Parse(hfLoginId.Value));

                if (Request.QueryString["panel"] != null as string)
                {
                    string request = Request.QueryString["panel"].ToString();
                    if (request == "sector")
                    {
                        PnlSector.Visible = true;
                        FillSkill(DdnSkills);
                        FillSectorBySectorType(chkIndustris, "Industries");
                        FillSectorBySectorType(chkFunctionalAreas, "Functional Areas");
                        LoadSectorInformation(int.Parse(hfLoginId.Value));
                        
                    }
                    else if (request == "currentEmp")
                    {
                        PnlCurrentEmployementEdit.Visible = true;
                        LoadCurrentEmpDetail(int.Parse(hfLoginId.Value));
                    }
                    else if (request == "profilePhoto")
                    {
                        PnlProfileImage.Visible = true;
                        LoadProfileImage(int.Parse(hfLoginId.Value));
                    }
                    else if (request == "profileContactDetail")
                    {
                        PnlContactDetailEdit.Visible = true;
                        LoadProfileContactDetail(int.Parse(hfLoginId.Value));
                    }
                    else if (request == "changpwd")
                    {
                        PnlChangePassword.Visible = true;
                        hfLoginId.Value = EncryptString(GetCookie(this.Page, "LoginId"));
                    }


                }
                else
                {
                    PnlSectorSkillDisplay.Visible = true;
                    PnlContactDetail.Visible = true;
                    PnlCurrentEmployementDetail.Visible = true;
                }

            }
            else
            {
                Response.Redirect("employee-login.aspx");
            }
        }
    }

    private void LoadProfileContactDetail(int LoginId)
    {
        EmployeeMasterBL obj = new EmployeeMasterBL();
        List<DataLayer.VEmployeeMaster> E = obj.LoadEmployeeMasterByLoginId(LoginId);
        TxtEmailAddress.Text = E[0].UserNameEmail;
        TxtEmailAddress.Enabled = false;
        TxtPhoneNumber.Text = E[0].PhoneNo;
        TxtFacebookUrl.Text = E[0].FacebookUrl;
        TxtLinkedInUrl.Text = E[0].LinkedInUrl;
        TxtGooglrUrl.Text = E[0].GooglePlusUrl;
        if (E[0].MobileNo != "")
        {
            string[] emailMobile = E[0].MobileNo.Split('$');
            TxtMobileNumber.Text = emailMobile[0].ToString();
            TxtSecondryEmail.Text = emailMobile[1].ToString();
        }
    }

    private void LoadProfileImage(int LoginId)
    {
        EmployeeMasterBL obj = new EmployeeMasterBL();
        List<DataLayer.VEmployeeMaster> E = obj.LoadEmployeeMasterByLoginId(LoginId);
        ImgEmloyee.ImageUrl = E[0].ImagePath;
    }

    private void LoadEmployeeInfo(int LoginId)
    {
        EmployeeMasterBL obj = new EmployeeMasterBL();
        List<DataLayer.VEmployeeMaster> E = obj.LoadEmployeeMasterByLoginId(LoginId);
        if (E.Count > 0)
        {
            FillProgressBar(40);
            hfEmployeeId.Value = EncryptString(E[0].EmployeeId.ToString());
            SetCookie(this.Page, "EmployeeId", E[0].EmployeeId.ToString());
            lblContactPerson.InnerHtml = E[0].ContactPersone;
            lblCurrentEmployer.Text = E[0].CurrentEmployer;
            lblCity.Text = E[0].Name;
            // -- contact details -- //
            lblEmailAddress.Text = E[0].UserNameEmail;
            lblPhone.Text = E[0].PhoneNo;
            lblFacebookUrl.Text = E[0].FacebookUrl;
            lblLinkedInUrl.Text = E[0].LinkedInUrl;
            lblGooglePlus.Text = E[0].GooglePlusUrl;
            if (E[0].MobileNo.Contains('$'))
            {
                string[] emailMobile = E[0].MobileNo.Split('$');
                lblMobile.Text = emailMobile[0].ToString();
                lblSecondaryEmail.Text = emailMobile[1].ToString();

            }
            // -- current employement detail -- //
            lblCurrentDesignation.Text = E[0].CurrentDesignation;
            lblCurrentDesignation2.Text = E[0].CurrentDesignation;
            lblCurrentEmployer2.Text = E[0].CurrentEmployer;
            lblJobProfile.Text = E[0].JobProfile;
            lblJoinedIn.Text = E[0].JoinedIn;
            lblWebsiteUrl.Text = "<a target='_blank' href='" + E[0].WebsiteUrl + "'>" + E[0].WebsiteUrl + "</a>";

            if (E[0].SkillRolesIHireFor.Contains("@"))
            {
                string[] skilroll = E[0].SkillRolesIHireFor.Split('@');
                lblIndustry.Text = skilroll[0].ToString().Replace("$", ", ");
                lblFunctionalArea.Text = skilroll[1].ToString().Replace("$", ", ");
                lblSkill.Text = skilroll[2].ToString();
            }

            // photo // 
            if (E[0].ImagePath != "")
            {
                ImgEmployeePhoto.Attributes.Add("src", E[0].ImagePath);
                Image ImgEmployee = (Image)Master.FindControl("ImgEmployee");
                ImgEmployee.ImageUrl = E[0].ImagePath;
            }

            if (E[0].MobileNo != "") FillProgressBar(45);
            else return;
            if (E[0].ImagePath != "") FillProgressBar(55);
            else return;
            if (E[0].CurrentDesignation != "") FillProgressBar(60);
            else return;
            if (E[0].WebsiteUrl != "") FillProgressBar(65);
            else return;
            if (E[0].FacebookUrl != "" || E[0].LinkedInUrl != "" || E[0].GooglePlusUrl != "") FillProgressBar(70);
            else return;
            if (E[0].JoinedIn != "") FillProgressBar(75);
            else return;
            if (E[0].SkillRolesIHireFor != "" && E[0].SkillRolesIHireFor.Contains('@'))
            {
                string[] _skillRoll = E[0].SkillRolesIHireFor.Split('@');
                if (_skillRoll.Length == 3)
                {
                    FillProgressBar(100);
                }
            }
        }
        else
        {
            Response.Redirect("campus/Login.aspx", false);
        }

        
    }

    private void LoadCurrentEmpDetail(int loginId)
    {
        EmployeeMasterBL obj = new EmployeeMasterBL();
        List<DataLayer.VEmployeeMaster> E = obj.LoadEmployeeMasterByLoginId(loginId);
        TxtCurrentDesignation.Text = E[0].CurrentDesignation;
        TxtCurrentEmployer.Text = E[0].CurrentEmployer;
        TxtJobProfile.Text = E[0].JobProfile;
        if (E[0].JoinedIn != "")
        {
            string[] joinedIn = E[0].JoinedIn.Split('-');
            ddljoinMonth.Items.FindByValue(joinedIn[0]).Selected = true;
            ddljoinYr.Items.FindByValue(joinedIn[1]).Selected = true;
        }
        TxtWebsiteUrl.Text = E[0].WebsiteUrl;
    }

    private void LoadSectorInformation(int loginId)
    {
        EmployeeMasterBL obj = new EmployeeMasterBL();
        List<DataLayer.VEmployeeMaster> E = obj.LoadEmployeeMasterByLoginId(loginId);
        if (E[0].SkillRolesIHireFor.Contains("@"))
        {
            string[] skilroll = E[0].SkillRolesIHireFor.Split('@');
            string[] _industries = skilroll[0].ToString().Split('$');
            string[] _functionalarea = skilroll[1].ToString().Split('$');
            string _skills = skilroll[2].ToString();
            var valueArray = _skills.TrimEnd(',').Split(',');
            var list = valueArray.ToList();
            foreach (ListItem i in DdnSkills.Items)
            {
                i.Selected = list.Contains(i.Text);
            }
           
            for (int i = 0; i < chkIndustris.Items.Count; i++)
            {
                for (int j = 0; j < _industries.Length; j++)
                {
                    if (chkIndustris.Items[i].Text == _industries[j].ToString())
                    {
                        chkIndustris.Items[i].Selected = true;
                        break;
                    }
                }
            }
            for (int i = 0; i < chkFunctionalAreas.Items.Count; i++)
            {
                for (int j = 0; j < _functionalarea.Length; j++)
                {
                    if (chkFunctionalAreas.Items[i].Text == _functionalarea[j].ToString())
                    {
                        chkFunctionalAreas.Items[i].Selected = true;
                        break;
                    }
                }
            }
        }
    }

    protected void BtnSavePhoto_Click(object sender, EventArgs e)
    {
        if (FUPhoto.HasFile)
        {
            string FileName = Path.GetFileName(FUPhoto.PostedFile.FileName);
            if (CheckFileType(FileName) == false)
            {
                ShowMessage("Invalid File", this.Page);
                return;
            }
            else
            {
                FileName = CheckFileNameExist("ReadWrite/EmployeePhoto", FileName);
                FUPhoto.SaveAs(Server.MapPath("~//ReadWrite//EmployeePhoto//" + FileName));

                if (hfEmpPhoto.Value != "")
                {
                    DeleteFileFromFolder(hfEmpPhoto.Value);
                }
                hfEmpPhoto.Value = "~/ReadWrite/EmployeePhoto/" + FileName;
                ImgEmloyee.ImageUrl = "~/ReadWrite/EmployeePhoto/" + FileName;

                EmployeeMasterBL obj = new EmployeeMasterBL();
                obj.UpdateEmployeeMasterPhoto(int.Parse(DecryptString(hfEmployeeId.Value)), hfEmpPhoto.Value);
              
                ScriptManager.RegisterStartupScript(this, this.GetType(), "AlertMsg",
"alert('Photo Updated Successfully'); window.location='" +
Request.Url.AbsoluteUri + "';", true);
            }
        }
        else
        {
            ShowMessage("Please Select Photo", this.Page);
        }
    }

    protected void BtnSaveSkillHireFor_Click(object sender, EventArgs e)
    {
        string industries = "";
        string functional_areas = "";
        for (int i = 0; i < chkIndustris.Items.Count; i++)
        {
            if (chkIndustris.Items[i].Selected == true)
            {
                industries += "$" + chkIndustris.Items[i].Text;
            }
        }
        if (industries == "")
        {
            ShowMessage("please select minimum 3 working industries you hiring for", this.Page);
            return;
        }
        for (int i = 0; i < chkFunctionalAreas.Items.Count; i++)
        {
            if (chkFunctionalAreas.Items[i].Selected == true)
            {
                functional_areas += "$" + chkFunctionalAreas.Items[i].Text;
            }
        }
        if (functional_areas == "")
        {
            ShowMessage("please select minimum 3 functional areas you hiring for", this.Page);
            return;
        }
        string keywordsSkills = "";
        foreach (ListItem listItem in DdnSkills.Items)
        {
            if (listItem.Selected == true)
            {
                keywordsSkills += listItem.Text + ",";
            }
        }
        EmployeeMasterBL obj = new EmployeeMasterBL();
        obj.UpdateEmployeeMasterSkillRolesIHireFor(int.Parse(DecryptString(hfEmployeeId.Value)), industries.TrimStart('$') + "@" + functional_areas.TrimStart('$') + "@" + keywordsSkills);

        ScriptManager.RegisterStartupScript(this, this.GetType(), "AlertMsg",
 "alert('Hiring information updated successfully'); window.location='" +
 Request.Url.AbsoluteUri + "';", true);
    }

    protected void BtnSaveCurrentEmployement_Click(object sender, EventArgs e)
    {
        EmployeeMasterBL obj = new EmployeeMasterBL();
        obj.UpdateEmployeeMasterCurrentEmpDetail(int.Parse(DecryptString(hfEmployeeId.Value)), TxtCurrentEmployer.Text, TxtCurrentDesignation.Text, TxtJobProfile.Text, ddljoinMonth.Items[ddljoinMonth.SelectedIndex].Value + "-" + ddljoinYr.Items[ddljoinYr.SelectedIndex].Value, TxtWebsiteUrl.Text);

        ShowMessage("Employement Detail updated successfully", this.Page);
    }

    protected void BtnSaveContactDetail_Click(object sender, EventArgs e)
    {
        EmployeeMasterBL obj = new EmployeeMasterBL();
        obj.UpdateEmployeeMasterContactDetail(int.Parse(DecryptString(hfEmployeeId.Value)), TxtPhoneNumber.Text, TxtMobileNumber.Text + "$" + TxtSecondryEmail.Text, TxtFacebookUrl.Text, TxtLinkedInUrl.Text, TxtGooglrUrl.Text);

        ShowMessage("Contact Detail updated successfully", this.Page);

    }

    protected void BtnChangePassword_Click(object sender, EventArgs e)
    {
        LoginDetailBL obj = new LoginDetailBL();
        List<DataLayer.LoginDetail> L = obj.LoadLoginDetailByPrimaryKey(int.Parse(DecryptString(hfLoginId.Value)));
        if (L.Count > 0)
        {
            if (L[0].Password == TxtCurrentPassword.Text)
            {
                if (TxtNewPassword.Text == TxtConfirmNewPassword.Text)
                {
                    obj.UpdateLoginDetailPassword(int.Parse(DecryptString(hfLoginId.Value)), TxtNewPassword.Text);
                    ShowMessage("Password Changed Successfully", this.Page);
                }
                else
                {
                    ShowMessage("Confirm Password Not Matched", this.Page);
                }
            }
            else
            {
                ShowMessage("Current Password Not Matched", this.Page);
            }
        }
    }

    private void DeleteFileFromFolder(string path)
    {
        string Deletefile = Server.MapPath(path);
        FileInfo file = new FileInfo(Deletefile);
        if (file.Exists)
        {
            file.Delete();
        }
    }

    protected void BtnCancel_Click(object sender, EventArgs e)
    {
        Response.Redirect("Employee_Home.aspx");
    }

    private void FillProgressBar(int Value)
    {

        MtrProfilePercent.Style.Add("width", Value.ToString() + "%");
        lblProfilePercent.InnerHtml = "" + Value + "% Complete";
    }
}