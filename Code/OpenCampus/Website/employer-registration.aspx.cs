﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using BusinessLayer;

public partial class Employer_Registration : Utility // System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!Page.IsPostBack)
        {
            FillCountry(DdnCountry);
            if (Request.QueryString["reg"] != null as string)
            {
                if (Request.QueryString["reg"].ToString() == "emp")
                {
                    email.Value = Request.QueryString["email"].ToString();
                }
            }           
        }
    }
    protected void DdnCountry_SelectedIndexChanged(object sender, EventArgs e)
    {
        FillState(DdnState, int.Parse(DdnCountry.SelectedValue));
    }
    protected void DdnState_SelectedIndexChanged(object sender, EventArgs e)
    {
        FillCity(DdnCity, int.Parse(DdnState.SelectedValue));
    }
    protected void BtnRegister_Click(object sender, EventArgs e)
    {
        if (!chkAll()) return;
        try
        {
            EmployeeMasterBL obj = new EmployeeMasterBL();
            LoginDetailBL objL = new LoginDetailBL ();
            int _loginId = objL.InsertLoginDetail(email.Value, password.Value,2, DateTime.Now);
            int _empId = obj.InsertEmployeeMaster(cname.Value, rbCompanyType.SelectedValue, indType.Items[indType.SelectedIndex].Text, cPers.Value, addLine1.Value, addLine2.Value, int.Parse(DdnCity.SelectedValue), zipcode.Value, phone.Value, "", "", cname.Value, "", true, DateTime.Now, _loginId, "", "", "", "", "", "", "");

            //Session["EmployeeId"] = _empId.ToString();
            SetCookie(this.Page, "EmployeeId", _empId.ToString());
            Response.Redirect("Employee_Home.aspx", false);
        }
        catch(Exception Ex)
        {
            if (Ex.InnerException.Message.Contains("UK_UserNameEmail"))
            {
                email_error.InnerHtml = "This Email already registered with us.";
                email.Focus();
            }
        }
    }

    private bool chkAll()
    {
        if (email.Value == "")
        {
            email_error.InnerHtml = "Please Enter email Address";
            email.Focus();
            return false;
        }
        if (!IsEmail(email.Value))
        {
            email_error.InnerHtml = "Please Enter valid email Address";
            email.Focus();
            return false;
        }
        if (password.Value == "")
        {
            password_error.InnerHtml = "Please create your password";
            password.Focus();
            return false;
        }
        if (password.Value.Length < 6)
        {
            password_error.InnerHtml = "Password must be 6 character long";
            password.Focus();
            return false;
        }
        if (cname.Value == "")
        {
            cName_error.InnerHtml = "Please enter company name.";
            return false;
        }
        if (cPers.Value == "")
        {
            cPers_error.InnerHtml = "Please Contact persone Name";
            cPers.Focus();
            return false;
        }
        if (indType.SelectedIndex == 0)
        {
            indType_error.InnerHtml = "Please select Industry Type";
            indType.Focus();
            return false;
        }
        if (addLine1.Value == "")
        {
            addLine1_error.InnerHtml = "please enter address";
            addLine1.Focus();
            return false;
        }
        if (DdnCity.SelectedIndex == 0)
        {
            city_error.InnerHtml = "Select your City";
            DdnCity.Focus();
            return false;
        }
        if (zipcode.Value == "")
        {
            pin_error.InnerHtml = "enter pin/zip code";
            zipcode.Focus();
            return false;
        }
        if (phone.Value == "")
        {
            phone_error.InnerHtml = "Please enter phone number";
            phone.Focus();
            return false;
        }
        if (chkIAgreeTermsCond.Checked == false)
        {
            terms_error.InnerHtml = "Please agree terms and condition.";
            chkIAgreeTermsCond.Focus();
            return false;
        }

        return true;
    }
}