﻿<%@ Page Title="Employer Registration | OpenCampus" Language="C#" MasterPageFile="~/Site.master" AutoEventWireup="true" CodeFile="employer-registration.aspx.cs" Inherits="Employer_Registration" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
    <link href="css/custome_login_software.css" rel="stylesheet" type="text/css" />
    <style type="text/css">
        @media (max-width: 767px)
        {
            .noo-header
            {
                margin-top: 0px;
            }
        }
        @media (min-width: 768px)
        {
            .noo-header
            {
                margin-top: 0px;
            }
        }
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="cph" Runat="Server">
<div class="container-wrap" style="background-color: #E9EBEE;">
        <div class="main-content container-boxed max offset" style="padding-top: 0px;">
            <div class="row">
                <div class="noo-main col-md-12">
                    <div class="form-horizontal" enctype="multipart/form-data">
                        <div class="candidate-profile-form row">
                            <asp:UpdatePanel ID="UpdatePanel1" runat="server">
                            <ContentTemplate>
                            <div class="col-sm-12" style="background-color: white; box-shadow: 0px 0px 5px 0px grey;">

                            <div class="smbndtxt">
<h1 class="fh1 mt10">Open Campus Client Registration</h1>
</div>

                            <div class="cls bg wid745">

<div class="p10"> Start hiring online for entry level position from colleges across India with a recruiters account of OpenCampus. Please fill in the mandatory details in the form below to proceed.<br>
  <br>
<div class="fright"><span class="red">*</span> Mandatory Fields</div>
<div class="cls"><img src="images/spacer.gif" alt="" height="10"></div>
<h3 class="cls">Account Information</h3>
	<div class="rowN">
		<label for="email"><span class="red">*</span>Email ID:</label>
		<p><input name="email" runat="server" value="" id="email" type="text" maxlength="50" class="w250">
		<span id="email_error" runat="server" class="error1"></span>
		</p>
	</div>
	<div class="rowN">
		<label for="pass"><span class="red">*</span>Password:</label>
		<p><input name="pass" runat="server" id="password" type="password" value="" maxlength="50" class="w250">
		<span class="ltGrey1 dispBlk">(Minimum 6 Characters)</span>
		<span id="password_error" runat="server" class="error1"></span></p>
	</div>
	
	<div class="cls"><img src="images/spacer.gif" alt="" height="10"></div>
	<h3 class="cls">Company/Recruiter Details </h3>
	<div class="rowN">
		<label for="cname"><span class="red">*</span>Company Name:</label>
		<p><input name="cname" runat="server" id="cname" value="" type="text" class="w250" maxlength="100">
		<span id="cName_error" runat="server" class="error1"></span></p>
	</div>
	<div class="rowN">
		<label for="cPers"><span class="red">*</span>Contact Person:</label>
		<p><input name="cPers" runat="server" id="cPers" type="text" value="" class="w250" maxlength="35">
		<span id="cPers_error" runat="server" class="error1"></span></p>
	</div>
	<div class="rowN">
		<div class="lCol"><span class="red">*</span>Company Type:</div>
		<p>
            <asp:RadioButtonList ID="rbCompanyType" RepeatDirection="Horizontal" runat="server">
            <asp:ListItem Value="Company" Selected="True">Company</asp:ListItem>
            <asp:ListItem Value="Consultant">Consultant</asp:ListItem>
            </asp:RadioButtonList>
        </p>
	</div>
	
	<div class="rowN">
		<label for="indType"><span class="red">*</span>Industry Type:</label>
		<p>      <select name="indType" id="indType" runat="server">
            <option value="">Select industry type</option>
	        					<option value="2">Accessories/Apparel/Fashion Design</option>    
							<option value="3">Accounting/Consulting/Taxation</option>    
							<option value="4">Advertising/Event Management/PR</option>    
							<option value="5">Agriculture/Dairy Technology</option>    
							<option value="6">Airlines/Hotel/Hospitality/Travel/Tourism/Restaurants</option>    
							<option value="7">Architectural Services/ Interior Designing</option>    
							<option value="8">Auto Ancillary/Automobiles/Components</option>    
							<option value="9">Banking/Financial Services/Stockbroking/Securities</option>    
							<option value="10">Biotechnology/Pharmaceutical/Clinical Research</option>    
							<option value="11">Cement/Construction/Engineering/Metals/Steel/Iron</option>    
							<option value="12">Chemicals/Petro Chemicals/Plastics</option>    
							<option value="13">Consumer FMCG/Foods/Beverages</option>    
							<option value="14">Consumer Goods - Durables</option>    
							<option value="15">Courier/Freight/Transportation/Warehousing</option>    
							<option value="16">CRM/ IT Enabled Services/BPO</option>    
							<option value="17">Education/Training/Teaching</option>    
							<option value="18">Employment Firms/Recruitment Services Firms</option>    
							<option value="19">Entertainment/Media/Publishing/Dotcom</option>    
							<option value="20">Export Houses/Textiles/Merchandise</option>    
							<option value="21">Gems and Jewellery</option>    
							<option value="22">Computer Hardware/Networking</option>    
							<option value="23">Healthcare/Medicine</option>    
							<option value="24">Insurance</option>    
							<option value="25">Law/Legal Firms</option>    
							<option value="26">Machinery/Equipment Manufacturing/Industrial Products</option>    
							<option value="27">NGO/Social Services</option>    
							<option value="28">Office Automation</option>    
							<option value="29">Petroleum/Oil and Gas/Projects/Infrastructure/Power/Non-conventional Energy</option>    
							<option value="30">Printing/Packaging</option>    
							<option value="31">Real Estate</option>    
							<option value="32">Retailing</option>    
							<option value="33">Security/Law Enforcement</option>    
							<option value="34">Software Services</option>    
							<option value="35">Telecom Equipment/Electronics/Electronic Devices/RF/Mobile Network/Semi-conductor</option>    
							<option value="36">Telecom/ISP</option>    
							<option value="37">Others/Engg. Services/Service Providers</option>    
							<option value="38">Shipping/Marine</option>    
							<option value="40">Animation / Gaming</option>    
							<option value="41">Banking/FinancialServices/Broking</option>    
							<option value="43">Brewery/Distillery</option>    
							<option value="44">Ceramics/Sanitaryware</option>    
							<option value="45">Government/Defence</option>    
							<option value="46">Electricals/Switchgears</option>    
							<option value="48">FacilityManagement</option>    
							<option value="49">Fertilizers/Pesticides</option>    
							<option value="50">FoodProcessing</option>    
							<option value="51">Glass</option>    
							<option value="52">HeatVentilation/AirConditioning</option>    
							<option value="53">KPO/Research/Analytics</option>    
							<option value="54">Mining</option>    
							<option value="55">Publishing</option>    
							<option value="56">Steel</option>    
							<option value="57">Strategy/ManagementConsultingFirms</option>    
							<option value="58">Tyres</option>    
							<option value="59">WaterTreatment/WasteManagement</option>    
							<option value="60">Wellness/Fitness/Sports</option>    
				
	</select>

		<span id="indType_error" runat="server" class="error1"></span>
		</p>
	</div>
	
  <div class="rowN">
                <label for=""><span class="red"></span></label>

                <p></p>
        </div>

	<h3 class="cls">Contact Information</h3>
	<div class="rowN">
		<label for="addLine1"><span class="red">*</span>Address Line 1:</label>
		<p><input name="addLine1" id="addLine1" runat="server" value="" type="text" class="w250" maxlength="50">
		<span id="addLine1_error" runat="server" class="error1"></span></p>
	</div>
	
	<div class="rowN">
		<label for="addLine2">Address Line 2:</label>
		<p><input name="addLine2" runat="server" id="addLine2" type="text" value="" class="w250" maxlength="50"></p>
	</div>
		
	<div class="rowN">
		<label for="country"><span class="red">*</span>Country:</label>
		<p>     
            <asp:DropDownList ID="DdnCountry" runat="server" AppendDataBoundItems="true" 
                AutoPostBack="True" onselectedindexchanged="DdnCountry_SelectedIndexChanged">
            <asp:ListItem>--Select Country--</asp:ListItem>
            </asp:DropDownList>

		<span id="country_error" runat="server" class="error1"></span></p>
	</div>

	<div class="rowN">
		<label for="state"><span class="red">*</span>State:</label>
		<p>
        <asp:DropDownList ID="DdnState" runat="server" AppendDataBoundItems="true" 
                AutoPostBack="True" onselectedindexchanged="DdnState_SelectedIndexChanged">
            <asp:ListItem>--Select State--</asp:ListItem>
            </asp:DropDownList>
		<span id="state_error" runat="server" class="error1"></span></p>
	</div>


	<div class="rowN">
		<label for="city"><span class="red">*</span>City:</label>
		<p>
        <asp:DropDownList ID="DdnCity" runat="server" AppendDataBoundItems="true">
            <asp:ListItem>--Select City--</asp:ListItem>
            </asp:DropDownList>
        <span id="ocitySpan" style="visibility: hidden;"><label for="ocity">Other:</label>
			<input name="ocity" type="text" value="" id="ocity" maxlength="100" disabled=""></span>

		<span id="city_error" runat="server" class="error1"></span></p>
	</div>
	
	<div class="rowN">
		<label for="pin"><span class="red">*</span>PIN/ZIP:</label>
		<p>
        <input name="zipcode" id="zipcode" runat="server" type="text" value="" class="w250" maxlength="6">
       	<span id="pin_error" runat="server" class="error1"></span></p>
	</div>

	
	<div class="rowN">
		<label for="pin"><span class="red">*</span>Phone No:</label>
		<p>
	 <input name="phone" id="phone" type="text" runat="server" value="" class="w250" maxlength="13">
		<span id="phone_error" runat="server" class="error1"></span></p>
	</div>
	
	<div class="rowN">
		<div class="lCol">&nbsp;</div>
	  <p>
          <asp:CheckBox ID="chkIAgreeTermsCond" runat="server" /> <label for="terms">I have read and I agree to the <a href="#" class="udrL">terms of service</a></label>
	 <span runat="server" id="terms_error" class="error1"></span>
</p>
	</div>
	
	<div class="cls"><img src="images/spacer.gif" alt=""></div>
	<div class="p173 pdtp15"><span class="blBdrBg">
        <asp:Button ID="BtnRegister" runat="server" Text="Save and Proceed" 
            onclick="BtnRegister_Click" /></span></div>
	<div class="cls"><img src="images/spacer.gif" alt=""></div>

</div>
<div class="cls"><img src="images/spacer.gif" alt="" height="10"></div>
</div>
    






                            </div>
                            </ContentTemplate>
                            </asp:UpdatePanel>

                        </div>
                    </div>
                    <hr />
                </div>
            </div>
        </div>
    </div>
</asp:Content>

