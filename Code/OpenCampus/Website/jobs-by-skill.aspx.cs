﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using BusinessLayer;

public partial class jobs_by_skill : Utility // System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!Page.IsPostBack)
        {
            string filtr = "";
            try
            {
                filtr = this.Page.RouteData.Values["filtr"].ToString();
            }
            catch { }
            LoadJobSkills(filtr);
        }
    }

    private void LoadJobSkills(string Filter)
    {
        FilterMasterBL objF = new FilterMasterBL();
        rptrJobsByITSkills.DataSource = objF.LoadFilterMasterByFilterType("SkillsIT", Filter);
        rptrJobsByITSkills.DataBind();

        rptrJobsByNonITSkills.DataSource = objF.LoadFilterMasterByFilterType("SkillsNonIT", Filter);
        rptrJobsByNonITSkills.DataBind();
    }
}