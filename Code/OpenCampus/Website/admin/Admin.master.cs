﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class admin_Admin :  System.Web.UI.MasterPage
{
    Utility Ut = new Utility();
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!Page.IsPostBack)
        {
            if (Session["AdminLoginId"] != null as string)
            {
                lblWelcomeName.InnerText =Ut.GetCookie(this.Page, "AdminFullName");
            }
            else
            {
                Session.Abandon();
                Response.Redirect("Default.aspx");
            }
        }
    }
}
