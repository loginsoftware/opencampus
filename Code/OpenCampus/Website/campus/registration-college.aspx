﻿<%@ Page Title="" Language="C#" MasterPageFile="~/campus/College.master" AutoEventWireup="true" CodeFile="registration-college.aspx.cs" Inherits="campus_registration_college" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="asp" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
<style>
	.btn-group, .btn-group button {
		width:100% !important;	
		text-align:left
	}
	
	form ul.dropdown-menu li a:hover {
		background:#EEEEEE!important;
		color:#414141!important;
		
	}
	.multiselect-container label.radio {
		padding-left:15px;
	}	
	.multiselect-container li input[type="radio"] {
		display:none	
	}
	.head {
		padding:0 0 10px	
	}
	.head-t {
		position:absolute; top:20px; left:20px
	}
	.f-list {
		border-right: 5px solid rgb(228, 185, 82);
		padding-right: 20px;
		position: absolute;
		right: 40px;
		text-align: right;
		top: 250px;
	}
	.f-list p {
		font-size:16px !important;
		color:#FFF !important;
		line-height:35px !important;
	}
	
	h2.division span { 
		background:#fff; 
		padding:0 10px;
		font-size: 16px;
		color:#AAA;
	}
	.col-md-3 {
		background: transparent url('images/reg-feat.jpg') no-repeat scroll 0% 0% / contain ; position: absolute; left: 45px ! important; z-index: 9; top: 75px;
	}
	.col-md-5 {
		box-shadow:0px 1px 2px 2px #ccc !important;	
	}

	@media screen and (max-width: 990px) and (min-width: 240px) { 
		.col-md-3 {
			position: relative ;
    		left: 0 !important;
    		top: 0;
		}
	}
.shadow{
	
	
	
	    box-shadow: 0px 1px 2px 2px #ccc;
}
.top {
    margin-top: 141px;    background-color: white;
}
.cd-btn {
    position: fixed;
    width: 100%;
    box-shadow: 1px -6px 16px 1px black;
}
.middle{
    font-weight: 700;
    margin: -2% 3%;

}
</style>
<style type="text/css">

.modal
{
    position: fixed;
    z-index: 999;
    height: 100%;
    width: 100%;
    top: 0;
    background-color: Black;
    filter: alpha(opacity=60);
    opacity: 0.6;
    -moz-opacity: 0.8;
}
.center
{
    z-index: 1000;
    margin: 300px auto;
    padding: 10px;
    width: 130px;
    background-color: White;
    border-radius: 10px;
    filter: alpha(opacity=100);
    opacity: 1;
    -moz-opacity: 1;
}
.center img
{
    height: 128px;
    width: 128px;
}
</style>

</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="cph" Runat="Server">
<div class="container-fluid">
	<div class="row">	
  <div class="col-md-12"><br><br>
  	<asp:UpdateProgress ID="UpdateProgress1" runat="server" AssociatedUpdatePanelID="UpdatePanel1">
<ProgressTemplate>
    <div class="modal">
        <div class="center">
            <img alt="" src="images/loader.gif" />
        </div>
    </div>
</ProgressTemplate>
</asp:UpdateProgress>
    
    <asp:UpdatePanel ID="UpdatePanel1" runat="server">
    <ContentTemplate>
    <div class="col-md-6 top border-r shadow col-md-offset-3 padd-all"><br>
	<h1 class="middle"><b>Register your College in OpenCampus.in </b></h1>
	<hr>
      
      
      <div class="text-l">
        
            
        <div class="col-md-12">
          <div class="row">
           <h3>College / University Details</h3>
		  <hr>
          <div class="form-group col-md-12 f-name">
          <div class="col-md-6">
           <label class="clr67 form-text requiredClass">Select Country</label>
              <div class="clearfix"></div>
    <asp:DropDownList ID="DdnCountry" AutoPostBack="true" OnSelectedIndexChanged="DdnCountry_SelectedIndexChanged" runat="server" class="form-control">
    </asp:DropDownList>
          </div>
           <div class="col-md-6">
            <label class="clr67 form-text requiredClass">Select State</label>
              <div class="clearfix"></div>
    <asp:DropDownList ID="DdnState" AutoPostBack="true" runat="server" OnSelectedIndexChanged="DdnState_SelectedIndexChanged" class="form-control">
    </asp:DropDownList>
          </div>		 
             
               
              <div class="clearfix"></div>
          </div>
          <div class="form-group col-md-12">
          <label class="clr67 form-text requiredClass">College City</label>
                   <asp:DropDownList ID="DdnCity" runat="server" class="form-control">
    </asp:DropDownList>
          <div class="clearfix"></div>
        </div>
 <div class="form-group col-md-12">
          <label class="clr67 form-text requiredClass">College Name</label>
       <input type="text" id="TxtCollegeName" runat="server" class="form-control" required title="enter College / University Name" x-moz-errormessage="Enter College Name" value="" placeholder="Enter College Name">
          <div class="clearfix"></div>
        </div>	
 <div class="form-group col-md-12">
          <label class="clr67 form-text requiredClass">About college</label>
                
				<textarea rows="2" id="TxtAboutCollege" runat="server"  class="form-control"  cols="50" placeholder="Describe yourself here..." required>
</textarea>

          <div class="clearfix"></div>
        </div>	
 <div class="form-group col-md-12">
          <label class="clr67 form-text requiredClass">Address</label>
                
				 <input type="text" id="TxtAddress" runat="server" class="form-control" required title="enter College / University address" x-moz-errormessage="Enter College Address" value="" placeholder="Enter Address">

          <div class="clearfix"></div>
        </div>	
		
		        <div class="col-md-12">
          <div class="row">
            <div class="form-group col-md-6">
              <label class="clr67 form-text requiredClass show"> Pincode</label>
              <input type="text" id="TxtPinCode" runat="server" class="form-control" placeholder="94043" required>


              <div class="clearfix"></div>
            </div>
            <div class="form-group col-md-6">
              <label class="clr67 form-text requiredClass">Phone</label>
              <input id="TxtPhoneNumber" runat="server" type="text" class="form-control" required maxlength="30" title="Enter Phone Number" x-moz-errormessage="Enter Phone Number" value="" placeholder="99263372622">
              <div class="clearfix"></div>
            </div>
			  
			
            <div class="clearfix"></div>

          </div>
        </div>
		<div class="form-group col-md-12">
          <label class="clr67 form-text requiredClass">College Website</label>
                
		<input id="TxtWebsite" runat="server" type="text" class="form-control" placeholder="www.collegename.com" required>

          <div class="clearfix"></div>
        </div>	
	<div class="form-group col-md-12">
          <label class="clr67 form-text requiredClass">University name</label>
                
		<input id="TxtUniversityName" runat="server" type="text"  class="form-control" required>

          <div class="clearfix"></div>
        </div>
<div class="form-group col-md-12">
          <label class="clr67 form-text requiredClass">Affiliations</label>
                
		<input type="text" id="TxtAffiliation" runat="server" class="form-control" required>
        Enter primary affiliations of college with other universities if any.
          <div class="clearfix"></div>
        </div>			
		<div class="col-md-12">
          <div class="row">
            <div class="form-group col-md-6">
			<h3>Dean / Director / Principal Details</h3>
			<hr>
              <label class="clr67 form-text requiredClass show"> Dean / Director / Principal</label>
              <input type="text" id="TxtDeanDirectorName" runat="server" class="form-control" required maxlength="30" title="Enter Director name" x-moz-errormessage="Enter Director name" value="" >
              <div class="clearfix"></div>
            </div>
			<div class="form-group col-md-12">
          <label class="clr67 form-text requiredClass">About Dean</label>
                
				<textarea id="TxtAboutDean" runat="server" rows="2"  class="form-control"  cols="50" placeholder="Describe about Dean here..." required>
</textarea>

          <div class="clearfix"></div>
        </div>	
		<div class="form-group col-md-12">
          <label class="clr67 form-text requiredClass">Upload Photo of Dean</label>
                
        <asp:AsyncFileUpload ID="FUDean" runat="server" ThrobberID="myThrobber" OnUploadedComplete="FUDean_OnUploadedComplete" />
        <%--<asp:Label ID="lblerrorFUDean" runat="server" Text="Please Upload Only .jpg, .jpeg, .png, .gif, .xlsx , .docx, .doc, .pptx , .xls"></asp:Label>--%>
        <asp:HiddenField ID="hfFUDean" runat="server" />
          <div class="clearfix"></div>
        </div>	     
        
            <div class="clearfix"></div>
          </div>
        </div>
		<div class="col-md-12">
          <div class="row">
            <div class="form-group col-md-6">
			<h3>Placement Cell Contact Details</h3>
			<hr>
              <label class="clr67 form-text requiredClass show"> Placement Coordinator</label>
              <input type="text" id="TxtPlacementCordinator" runat="server" class="form-control" maxlength="30" title="Enter name of placement co-ordiator" x-moz-errormessage="Enter name of placement co-ordiator" value="" >
              <div class="clearfix"></div>
            </div>
			        <div class="col-md-12">
          <div class="row">
            <div class="form-group col-md-6">
              <label class="clr67 form-text requiredClass show"> Mobile No *</label>
              <input type="text" id="TxtPlacementPhone" runat="server"  class="form-control" placeholder="Enter Mobile Number" required >
  

              <div class="clearfix"></div>
            </div>
            
			  <div class="form-group col-md-6">
              <label class="clr67 form-text requiredClass show">Alternate Mobile No *</label>
              <input type="text" id="TxtPlacementPhoneAlternate" runat="server"  class="form-control" placeholder="Enter Alternate Mobile Number">
  

              <div class="clearfix"></div>
            </div> 
			
            <div class="clearfix"></div>

          </div>
        </div>
		
		
		<div class="form-group col-md-12">
          <label class="clr67 form-text requiredClass">Email Address</label>
                
				<input id="TxtEmailId" runat="server" type="text" class="form-control" placeholder="Type your email" required>



          <div class="clearfix"></div>
        </div>	

            <div class="clearfix"></div>
          </div>
        </div>
		
	<div class="col-md-12">
          <div class="row">
            
			        <div class="col-md-12">
								<h3>College Display Options
</h3>
			<hr>
          <div class="row">
		  
            <div class="form-group col-md-6">
              <label class="clr67 form-text requiredClass show"> Upload College Logo</label>
          <asp:AsyncFileUpload ID="FULogo" runat="server" OnUploadedComplete="FULogo_OnUploadedComplete"/>
           <asp:HiddenField ID="hfFULogo" runat="server" />
		 <hr>
 

              <div class="clearfix"></div>
            </div>
			
			
			    <div class="form-group col-md-6">
              <label class="clr67 form-text requiredClass show"> Upload College Cover Photo</label>
        <asp:AsyncFileUpload ID="FUCollegeCoverPhoto" runat="server" OnUploadedComplete="FUCollegeCoverPhoto_OnUploadedComplete"/>
         <asp:HiddenField ID="hfFUCollegeCover" runat="server" />
		<hr>
 

              <div class="clearfix"></div>
            </div>
                
                <div class="form-group col-md-6">
              <label class="clr67 form-text requiredClass show"> Upload Presentations</label>
          <asp:AsyncFileUpload ID="FUPresentation" runat="server" OnUploadedComplete="FUPresentation_OnUploadedComplete"/>
		 Supported Formats: .ppt, .pps, .pdf. Limit 5MB
          <asp:HiddenField ID="hfFUPresentation" runat="server" />
		 <hr>
		 
 <div class="clearfix"></div>
            </div>
                <div class="form-group col-md-6">
              <label class="clr67 form-text requiredClass show"> Upload Placement Statistics</label>
      <asp:AsyncFileUpload ID="FUPlacementStatic" runat="server" OnUploadedComplete="FUPlacementStatic_OnUploadedComplete"/>
		Supported Formats: .jpg, .png, .gif. Limit 200KB
  <asp:HiddenField ID="hfFUPlacementStatic" runat="server" />

              <div class="clearfix"></div>
            </div>
            <div class="form-group col-md-12">
              <label class="clr67 form-text requiredClass show"> College Video(Youtube Url)</label>
       	<input id="TxtVideoUrl" runat="server" type="text" class="form-control" placeholder="Paste Video Youtube Url">
		 <hr>
		 
 <div class="clearfix"></div>
            </div>
            
			  
			
          </div>
        </div>
		
		
		
            <div class="clearfix"></div>
          </div>
        </div>
	
          	   
          </div>
        </div>
        <div class="col-md-12">
          <div class="row">
            <div class="form-group col-md-6">
			<h3>Login details</h3>
			<hr>
              <label class="clr67 form-text requiredClass show">Username *</label>
              <input type="text" id="TxtUsername" runat="server" class="form-control" required maxlength="30" title="enter login username" x-moz-errormessage="Enter Login Username" value="" placeholder="Username">
              <div class="clearfix"></div>
            </div>
               <div class="form-group col-md-12">
          <label class="clr67 form-text requiredClass">Password *</label>
              <input type="password" id="TxtPassword" runat="server" class="form-control" required title="Enter your password" x-moz-errormessage="Enter your password" value="">
          <div class="clearfix"></div>
        </div>   
            <div class="clearfix"></div>
          </div>
        </div>
        
        <div class="col-md-12"> <br>
          <br>
		  
          <p><input type="checkbox" name="iagree" value="iagree">     By clicking Join Now, you agree to opencampus <a href="../terms-condition.aspx">Terms</a>, <a href="../privacy-policy.aspx">Privacy Policy</a>, and <a href="../privacy-policy.aspx">Cookie Policy</a>.</p>
          <%--<button type="submit" class="btn btn-lg btn-primary" name="btnCreateAccount" value="true">Submit</button>--%>
                <asp:Button ID="BtnRegister" runat="server" 
                CssClass="btn btn-lg btn-primary" Text="Register" onclick="BtnRegister_Click" />
          <br>
          <br>
        </div>
      </div>
        <asp:HiddenField ID="hfLatitude" runat="server" />
        <asp:HiddenField ID="hfLongitude" runat="server" />
    </div>

    </ContentTemplate>
    </asp:UpdatePanel>
  </div>
  </div>
</div>
</asp:Content>

