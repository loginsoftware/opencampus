﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using BusinessLayer;
using System.Text.RegularExpressions;
using AjaxControlToolkit;

public partial class campus_CollegeProfile : Utility // System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!Page.IsPostBack)
        {
            if (GetCookie(this.Page, "CollegeId") != null as string)
            {
                int _collegeId = int.Parse(GetCookie(this.Page, "CollegeId"));
                hfCollegeId.Value = EncryptString(GetCookie(this.Page, "CollegeId"));
                LoadCollegeMaster(_collegeId);
            }
            else
            {
                Response.Redirect("Login.aspx", false);
            }
           
        }
    }

    private void LoadCollegeMaster(int collegeId)
    {
        CollegeMasterBL objC = new CollegeMasterBL();
        List<DataLayer.VCollege> C = objC.LoadCollegeMasterByPrimaryKey(collegeId);
        this.Page.Title = C[0].CollegeName + " " + C[0].CityName;
        lblCollegeName.InnerText = C[0].CollegeName;
        
        lblCollegeAddress.Text = C[0].Address + ",<br>" + C[0].CityName + ", " + C[0].StateName + "<br>" + C[0].CountryName + "-" + C[0].Pincode;
        lblPhoneNo.Text = C[0].PhoneNumber;
        lnkWebsite.NavigateUrl = C[0].CollegeWebsite;
        lnkWebsite.Text = C[0].CollegeWebsite;
        if (C[0].UploadLogoImagePath != "") ImgCollegeLogo.ImageUrl = C[0].UploadLogoImagePath;
        lblAboutCollege.Text = C[0].AboutCollege.Replace("<br/>", "\r\n");
        lblAboutCollegeTag.Text = "About " + C[0].CollegeName;
        lblDeanDirectorName.Text = C[0].DeanDirectorPrincipal;
        lblPlacementCoName.Text = C[0].PlacementCoordinator;
        if (C[0].PlacementPhoneNo.Contains("{oc}"))
        {
            string[] phn = Regex.Split(C[0].PlacementPhoneNo, "{oc}");
            lblPlacementPhoneNo.Text = phn[0].ToString() + ", " + phn[1].ToString();
        }
        else
        {
            lblPlacementPhoneNo.Text = C[0].PlacementPhoneNo;
        }
        lblPlacementEmailId.Text = C[0].PlacementEmailId;
        lblAffiliation.Text = C[0].Affiliations;
        lblUniversity.Text = C[0].UniversityName;

        if (C[0].VideoUrl != "")
        {
            string[] vd = Regex.Split(C[0].VideoUrl, "v=");
            string url = "https://www.youtube.com/embed/" + vd[1].ToString();
            lblYoutubeIframe.Text = "<iframe width='530' height='315' src='" + url + "' frameborder='0' allowfullscreen></iframe>";
        }
        lnkCollegePresentation.NavigateUrl = C[0].UploadPresentationPath;
        lnkCollegePlacementStatistic.NavigateUrl = C[0].UploadPlacementStatisticsPath;
        lnkViewCollegeMap.Attributes.Add("onclick", "showLocationMap('"+C[0].CollegeName + ", " + C[0].CityName + "')");

        hfuserHash.Value = EncryptString(C[0].Password);
        hfuserLogin.Value = EncryptString(C[0].LoginId.ToString());

        TxtCollegeName.Value = C[0].CollegeName;
        TxtAddress.Value = C[0].Address;
        TxtPhoneNumber.Value = C[0].PhoneNumber;
        TxtPinCode.Value = C[0].Pincode;
        TxtWebsite.Value = C[0].CollegeWebsite;
        ImgCollegeLogoUpdate.ImageUrl = C[0].UploadLogoImagePath;
        hfFULogo.Value = C[0].UploadLogoImagePath;
        ImgCoverPhotoUpdate.ImageUrl = C[0].UploadPictureImagePath;
        hfFUCollegeCover.Value = C[0].UploadPictureImagePath;

        TxtDeanDirectorName.Value = C[0].DeanDirectorPrincipal;
        TxtAboutCollege.Value = C[0].AboutCollege.Replace("<br/>", "\r\n");
        hfFUDean.Value = C[0].UploadDeanImagePath;
        ImgCollegeDeanUpdate.ImageUrl = C[0].UploadDeanImagePath;
        TxtVideoUrl.Value = C[0].VideoUrl;

        TxtPlacementCordinator.Value = C[0].PlacementCoordinator;
        if (C[0].PlacementPhoneNo.Contains("{oc}"))
        {
            string[] phn = Regex.Split(C[0].PlacementPhoneNo, "{oc}");
            TxtPlacementPhone.Value = phn[0].ToString();
            TxtPlacementPhoneAlternate.Value = phn[1].ToString();
        }
        else
        {
           TxtPlacementPhone.Value = C[0].PlacementPhoneNo;
        }
        TxtEmailId.Value = C[0].PlacementEmailId;
        TxtAffiliation.Value = C[0].Affiliations;
        TxtUniversityName.Value = C[0].UniversityName;
        hfFUPresentation.Value = C[0].UploadPresentationPath;
        hfFUPlacementStatic.Value = C[0].UploadPlacementStatisticsPath;

    }

    protected void BtnUpdateCollege_Click(object sender, EventArgs e)
    {
        try
        {
            CollegeMasterBL objC = new CollegeMasterBL();
            objC.UpdateCollegeMasterProfile(int.Parse(DecryptString(hfCollegeId.Value)), TxtCollegeName.Value, TxtAddress.Value, TxtPinCode.Value, TxtPhoneNumber.Value, TxtWebsite.Value, TxtUniversityName.Value, TxtAffiliation.Value, TxtAboutCollege.Value, TxtDeanDirectorName.Value, hfAboutDean.Value, hfFUDean.Value, TxtPlacementCordinator.Value, TxtPlacementPhone.Value + "{oc}" + TxtPlacementPhoneAlternate.Value, TxtEmailId.Value, hfFULogo.Value, hfFUCollegeCover.Value, hfFUPresentation.Value, hfFUPlacementStatic.Value, TxtVideoUrl.Value, hflat.Value, hflng.Value, DateTime.Now);

            LoadCollegeMaster(int.Parse(DecryptString(hfCollegeId.Value)));
            ShowMessage("Profile Updated Successfully", this.Page);


        }
        catch (Exception Ex)
        {
           
        }


    }

    protected void BtnChangePassword_Click(object sender, EventArgs e)
    {
       
        if (TxtCurrentPassword.Value == DecryptString(hfuserHash.Value))
        {
            if (TxtNewPassword.Value == TxtConfirmPassword.Value)
            {
                LoginDetailBL objL = new LoginDetailBL();
                objL.UpdateLoginDetailPassword(int.Parse(DecryptString(hfuserLogin.Value)), TxtNewPassword.Value);
                ShowMessage("Password Changed Successffully", this.Page);
            }
            else
            {
                ShowMessage("Confirm Password not matched.", this.Page);
            }
        }
        else
        {
            ShowMessage("inValid Current Password", this.Page);
        }
    }

    protected void FULogo_OnUploadedComplete(object sender, AsyncFileUploadEventArgs e)
    {
        string filename = System.IO.Path.GetFileName(e.FileName);
        if (CheckFileType(filename) == false)
        {
            ShowMessage("InValid File Type !", this.Page);
        }
        else
        {
            string strUploadPath = "~/ReadWrite/College/";
            FULogo.SaveAs(Server.MapPath(strUploadPath) + TxtCollegeName.Value.Replace(" ", "") + filename);
            ScriptManager.RegisterClientScriptBlock(this, this.GetType(), "filePath", "top.$get(\"" + hfFULogo.ClientID + "\").value = '" + strUploadPath + TxtCollegeName.Value.Replace(" ", "") + filename + "';", true);
                        
        }

    }

    protected void FUDean_OnUploadedComplete(object sender, AsyncFileUploadEventArgs e)
    {
        string filename = System.IO.Path.GetFileName(e.FileName);
        if (CheckFileType(filename) == false)
        {
            ShowMessage("InValid File Type !", this.Page);

        }
        else
        {
            string strUploadPath = "~/ReadWrite/College/";
            FUDean.SaveAs(Server.MapPath(strUploadPath) + TxtCollegeName.Value.Replace(" ", "") + filename);
            ScriptManager.RegisterClientScriptBlock(this, this.GetType(), "filePath", "top.$get(\"" + hfFUDean.ClientID + "\").value = '" + strUploadPath + TxtCollegeName.Value.Replace(" ", "") + filename + "';", true);
        }
    }

    protected void FUPresentation_OnUploadedComplete(object sender, AsyncFileUploadEventArgs e)
    {
        string filename = System.IO.Path.GetFileName(e.FileName);
        if (CheckFileType(filename) == false)
        {
            ShowMessage("InValid File Type !", this.Page);
        }
        else
        {
            string strUploadPath = "~/ReadWrite/College/";
            FUPresentation.SaveAs(Server.MapPath(strUploadPath) + TxtCollegeName.Value.Replace(" ", "") + filename);
            ScriptManager.RegisterClientScriptBlock(this, this.GetType(), "filePath", "top.$get(\"" + hfFUPresentation.ClientID + "\").value = '" + strUploadPath + TxtCollegeName.Value.Replace(" ", "") + filename + "';", true);
        }

    }
    protected void FUPlacementStatic_OnUploadedComplete(object sender, AsyncFileUploadEventArgs e)
    {
        string filename = System.IO.Path.GetFileName(e.FileName);
        if (CheckFileType(filename) == false)
        {
            ShowMessage("InValid File Type !", this.Page);
        }
        else
        {
            string strUploadPath = "~/ReadWrite/College/";
            FUPlacementStatic.SaveAs(Server.MapPath(strUploadPath) + TxtCollegeName.Value.Replace(" ", "") + filename);
            ScriptManager.RegisterClientScriptBlock(this, this.GetType(), "filePath", "top.$get(\"" + hfFUPlacementStatic.ClientID + "\").value = '" + strUploadPath + TxtCollegeName.Value.Replace(" ", "") + filename + "';", true);
        }

    }

    protected void FUCollegeCoverPhoto_OnUploadedComplete(object sender, AsyncFileUploadEventArgs e)
    {
        string filename = System.IO.Path.GetFileName(e.FileName);
        if (CheckFileType(filename) == false)
        {
            ShowMessage("InValid File Type !", this.Page);
        }
        else
        {
            string strUploadPath = "~/ReadWrite/College/";
            FUCollegeCoverPhoto.SaveAs(Server.MapPath(strUploadPath) + TxtCollegeName.Value.Replace(" ", "") + filename);
            ScriptManager.RegisterClientScriptBlock(this, this.GetType(), "filePath", "top.$get(\"" + hfFUCollegeCover.ClientID + "\").value = '" + strUploadPath + TxtCollegeName.Value.Replace(" ", "") + filename + "';", true);
        }

    }
}