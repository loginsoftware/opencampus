﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data.Objects;
using DataLayer;

namespace BusinessLayer
{
    public class CandidateDesignationBL
    {
        public int InsertCandidateDesignation(int CandidateId, string CompanyName, string Status, string Duration, string Designation, string JobProfile, string NoticePeriod)
        {
            ObjectParameter CandidateDesignationId = new ObjectParameter("CandidateDesignationId", typeof(Int32));
            DBEntities Obj = new DBEntities();

            Obj.InsertCandidateDesignation(CandidateDesignationId, CandidateId, CompanyName, Status, Duration, Designation, JobProfile, NoticePeriod);
            return int.Parse(CandidateDesignationId.Value.ToString());
        }

        public int UpdateCandidateDesignation(int CandidateDesignationId, int CandidateId, string CompanyName, string Status, string Duration, string Designation, string JobProfile, string NoticePeriod)
        {
            DBEntities Obj = new DBEntities();
            Obj.UpdateCandidateDesignation(CandidateDesignationId, CandidateId, CompanyName, Status, Duration, Designation, JobProfile, NoticePeriod);
            return CandidateDesignationId;
        }

        public int DeleteCandidateDesignation(int CandidateDesignationId)
        {
            DBEntities Obj = new DBEntities();
            Obj.DeleteCandidateDesignation(CandidateDesignationId);
            return CandidateDesignationId;
        }

        public List<CandidateDesignation> LoadCandidateDesignationByPrimaryKey(int CandidateDesignationId)
        {
            DBEntities Obj = new DBEntities();
            return Obj.LoadCandidateDesignationByPrimaryKey(CandidateDesignationId).ToList();
        }

        public List<CandidateDesignation> LoadAllCandidateDesignation()
        {
            DBEntities Obj = new DBEntities();
            return Obj.LoadAllCandidateDesignation().ToList();
        }

        public int UpdateCandidateDesignationByCandidateId(int CandidateId, string CompanyName, string Status, string Duration, string Designation, string JobProfile, string NoticePeriod)
        {
            DBEntities Obj = new DBEntities();
            Obj.UpdateCandidateDesignationByCandidateId(CandidateId, CompanyName, Status, Duration, Designation, JobProfile, NoticePeriod);
            return CandidateId;
        }

        public List<CandidateDesignation> LoadCandidateDesignationByCandidateId(int CandidateId)
        {
            DBEntities Obj = new DBEntities();
            return Obj.LoadCandidateDesignationByCandidateId(CandidateId).ToList();
        }
    }
}
