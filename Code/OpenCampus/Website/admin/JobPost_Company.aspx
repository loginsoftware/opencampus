﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="JobPost_Company.aspx.cs" Inherits="admin_JobPost_Company" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
    <style type="text/css">
        .auto-style1
        {
            width: 80%;
            border-collapse: collapse;
            border: 1px solid #000000;
        }
    </style>
</head>
<body>
    <form id="form1" runat="server">
    <div style="text-align:center;">
        <img src="../images/logo-opencampus.png" />
    </div>
        <div>

            <table align="center" cellpadding="5" class="auto-style1">
                <tr>
                    <td>Comapny Name</td>
                    <td>
                        <asp:TextBox ID="TxtCompanyName" runat="server" Width="50%"></asp:TextBox></td>
                </tr>
                <tr>
                    <td>Address</td>
                    <td><asp:TextBox ID="TxtCompanyAddress" runat="server" Width="50%"></asp:TextBox></td>
                </tr>
                <tr>
                    <td>Company Logo</td>
                    <td>
                        <asp:FileUpload ID="FULogo" runat="server" /> note: logo size should be 100x100 or 150x150 or 200x200</td>
                </tr>
                <tr>
                    <td>Company Website</td>
                    <td><asp:TextBox ID="TxtCompanyWebsite" runat="server" Width="50%"></asp:TextBox></td>
                </tr>
                <tr>
                    <td>&nbsp;</td>
                    <td>
                        <asp:Button ID="BtnSave" runat="server" Text="Save" OnClick="BtnSave_Click" /></td>
                </tr>
            </table>

        </div>
        <asp:HiddenField ID="hfImagePath" runat="server" />
    </form>
</body>
</html>
