﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Configuration;
using System.Data.SqlClient;
using System.Data;

public partial class choosen_example : Utility // System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        
        if (!Page.IsPostBack)
        {
            GetCustomersPageWise(1);
        }
    }
    protected void Button1_Click(object sender, EventArgs e)
    {


        ShowMessage(Request.Form[ListBox1.UniqueID].ToString(), this.Page);


       
    }

    private void GetCustomersPageWise(int pageIndex)
    {
        string constring = ConfigurationManager.ConnectionStrings["loginsoftware_opencampusConnectionString"].ConnectionString;
        using (SqlConnection con = new SqlConnection(constring))
        {
            using (SqlCommand cmd = new SqlCommand("proc_JobPostingLoadByJobDesignationAndSkill", con))
            {
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.AddWithValue("@PageIndex", pageIndex);
                cmd.Parameters.AddWithValue("@PageSize", 20);
                cmd.Parameters.Add("@RecordCount", SqlDbType.Int, 4);
                cmd.Parameters.Add("@DesignationAndSkill", "ajax");
                cmd.Parameters["@RecordCount"].Direction = ParameterDirection.Output;
                con.Open();
                DataTable dt = new DataTable();
               SqlDataReader idr = cmd.ExecuteReader();
               dt.Load(idr);
                //GridView1.DataSource = idr;
                //GridView1.DataBind();
                idr.Close();
                con.Close();
                int recordCount = Convert.ToInt32(cmd.Parameters["@RecordCount"].Value);
               // this.PopulatePager(recordCount, pageIndex);
            }
        }
    }

    protected void Button2_Click(object sender, EventArgs e)
    {
        string _location = "Like '%" + txtlocation.Text.Replace(",","%' or joblocation like '%'") + "%'";
        string _title = "Like '%" + txttitle.Text.Replace(",","%' or Jobtitledesignation like '%'") + "%'";

        lblquery.Text = "select * from jobposting where (joblocation "+_location+") and (Jobtitledesignation "+_title+")";
       
    }
}