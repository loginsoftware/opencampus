﻿<%@ Page Title="Open Campus India - Government Job Vacancies | Private Jobs in India" Language="C#" MasterPageFile="~/Site.master" AutoEventWireup="true" CodeFile="contactus.aspx.cs" Inherits="ContactUs" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">

   <meta name="robots" content="INDEX,FOLLOW" />

   <meta  property="og:description" name="description" content="Apply to Millions of job opportunities on India’s No.1 job site. Find & apply for the Job vacancies in Government sector - OpenCampus is a leading Job site for Freshers who seek employment opportunities in both Private and Government sectors in India."/>

   <meta name="keywords" content="government job vacancies, govt jobs, job sites, private jobs, jobs for freshers, it jobs, jobs india"/>


   <meta name="application-name" content="opencampus.in - Your job search starts and ends with us."/>

   <meta name="copyright" content="2017 opencampus.in" />

   <meta name="content-language" content="EN" />

   <meta name="author" content="www.opencampus.in" />

   <meta name="distribution" content="GLOBAL" />


   <meta name="robots" content="ALL" />

   <meta name="pragma" content="no-cache" />

   <meta name="revisit-after" content="1 day" />

   <meta name="classification" content="Freshers jobs, Software Jobs, IT Jobs, Technical Jobs, Govt Jobs" />

   <meta property="og:image" content="https://opencampus.in/images/logo-opencampus.png"/>

   <link rel="image_src" href="https://opencampus.in/images/logo-opencampus.png" />

   <link href="https://plus.google.com/+opencampus1" rel="publisher" />


	<meta property="og:type" content="website" />

	<meta property="og:url" content="https://opencampus.in" />  

	<meta property="og:title" content="Open Campus India - Government Job Vacancies | Private Jobs in India" /> 

	<meta property="og:description" content="Open Campus, is India's leading online career and recruitment resource with its cutting edge technology provides relevant profiles to employers"/> 


	<meta property="og:image" content="https://opencampus.in/images/logo-opencampus.png" /> 

	<meta property="og:site_name" content="opencampus.in"/>

<meta name="twitter:card" content="summary">
<meta name="twitter:url" content="https://www.opencampus.in">
<meta name="twitter:site" content="@opencampusdot">
<meta name="twitter:title" content="Open Campus India - Government Job Vacancies | Private Jobs in India">
<meta name="twitter:description" content="Apply to Millions of job opportunities on India’s No.1 job site. Find & apply for the Job vacancies in Government sector - OpenCampus is a leading Job site for Freshers who seek employment opportunities in both Private and Government sectors in India.">
<meta name="twitter:image" content="https://pbs.twimg.com/profile_images/872485092932833280/umLVkRsm_400x400.jpg">

<link rel="canonical" href="https://www.opencampus.in/contactus.aspx" />
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="cph" Runat="Server">
    	<div class="noo-page-heading">
				<div class="container-boxed max parallax-content">
					<div class="page-heading-info">
						<h1 class="page-title">Contact Us</h1>
					</div>
				</div>
				<div class="parallax heading" data-parallax="1" data-parallax_no_mobile="1" data-velocity="0.1"></div>
			</div>
			<div class="container-wrap">
				<div class="container-boxed max offset main-content single-noo_job">
					<div class="row">
						<div class="col-md-5">
							<iframe src="https://www.google.com/maps/embed?pb=!1m14!1m8!1m3!1d8700.354552862038!2d77.81778957450722!3d23.53075876343698!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x0%3A0x7affd35153f5c564!2sopencampus.in!5e0!3m2!1sen!2sin!4v1484482559230" width="600" height="450" frameborder="0" style="border:0" allowfullscreen></iframe>
						</div>  
						<div class="noo-sidebar col-md-7">
							<div class="noo-sidebar-wrap">
								<div class="widget widget_noo_advanced_search_widget">
									<h4 class="title-general1 back-b">
                                        It feels great serving you!</h4>
                                    <h4>Employer's and Job Seeker's Mail us at: <a href="mailto:info@opencampus.in">info@opencampus.in</a></h4>
<p>If there is any sort of discrepancy and something does not sound right to you, we are here to clear the clouds, help us to help you!</p>
									<form class="widget-advanced-search">
										<div class="form-group">
											<label for="title" class="label-style">Name</label>
									   <asp:TextBox ID="TxtFullName" runat="server" placeholder="Full Name" autofocus Width="80%"></asp:TextBox>
										</div>
										<div class="form-group">
											<label for="title" class="label-style">Email</label>
											<div class="advance-search-form-control">
											   <asp:TextBox ID="TxtEmail" runat="server" placeholder="Email Address" autofocus Width="80%"></asp:TextBox>
											</div>
										</div>
										<div class="form-group">
											<label for="title" class="label-style">Mobile No</label>
											<div class="advance-search-form-control">
									   <asp:TextBox ID="TxtMobileNo" runat="server" placeholder="Mobile Number" autofocus  Width="80%"></asp:TextBox>
											</div>
										</div>
										<div class="form-group">
											<label for="language" class="label-style">Message</label>
											<div class="advance-search-form-control">
									   <asp:TextBox ID="TxtMessage" runat="server" placeholder="Enter Your Message" Height="100px" TextMode="MultiLine" autofocus  Width="80%"></asp:TextBox>

											</div>
										</div>
										
                                        <asp:Button ID="BtnSaveContact" runat="server" CssClass="btn btn-primary btn-search-submit" Text="Submit" OnClick="BtnSaveContact_Click" />
									</form>
								</div>
								
								
							</div>
						</div>
					</div>  
				</div>  
			</div>
</asp:Content>

