﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using BusinessLayer;
using System.IO;

public partial class Candidate_profile_basic : Utility // System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!Page.IsPostBack)
        {
            try
            {
                string _type = Request.QueryString["type"].ToString();
                if (_type == "profilebasic")
                {
                    PnlBasicPofile.Visible = true;
                    LoadBasicProfile();
                }
                if (_type == "designation")
                {
                    PnlEmpDesignation.Visible = true;
                    LoadCandidateDesignation();
                }
                if (_type == "education")
                {
                    PnlEducation.Visible = true;
                    LoadEducationDetail();
                }
                if (_type == "photo")
                {
                    LoadPhoto();
                    PnlProfileImage.Visible = true;
                }
                if (_type == "resume")
                {
                    LoadResume();
                    PnlResume.Visible = true;
                }
            }
            catch { }

            LoadProfilePercent();

        }
        
       
    }

    private void LoadProfilePercent()
    {
        string candi_id = GetCookie(this.Page, "CandidateId");
        CandidateMasterBL obj = new CandidateMasterBL();
        List<DataLayer.CandidateMaster> C = obj.LoadCandidateMasterByPrimaryKey(int.Parse(candi_id));
        if (C[0].CandidateName == "" || C[0].MobileNo == "" || C[0].PermanentAddress == "" || C[0].HomeTownCity == "")
        {
            hfPercentCount.Value = (int.Parse(hfPercentCount.Value) + 30).ToString();
        }
        if (C[0].ProfileImagePath == "")
        {
            hfPercentCount.Value = (int.Parse(hfPercentCount.Value) + 40).ToString();
        }
        else
        {
            Image ImgCandidate = (Image)Master.FindControl("ImgCandidate");
            ImgCandidate.ImageUrl = C[0].ProfileImagePath;
        }
        CandidateDesignationBL obj2 = new CandidateDesignationBL();
        List<DataLayer.CandidateDesignation> D = obj2.LoadCandidateDesignationByCandidateId(int.Parse(candi_id));
        if (D.Count == 0)
        {
            hfPercentCount.Value = (int.Parse(hfPercentCount.Value) + 10).ToString();
        }
        CandidateEducationBL obj3 = new CandidateEducationBL();
        List<DataLayer.CandidateEducation> E = obj3.LoadCandidateEducationByCandidateId(int.Parse(candi_id));
        if (E.Count == 0)
        {
            hfPercentCount.Value = (int.Parse(hfPercentCount.Value) + 20).ToString();
        }
        if (hfPercentCount.Value == "0")
        {
            MtrProfilePercent.Style.Add("width", "100%");
            lblProfilePercent.InnerHtml = "100% Complete";
        }
        else
        {
            int _count = 100 - int.Parse(hfPercentCount.Value);
            MtrProfilePercent.Style.Add("width", _count.ToString()+"%");
            lblProfilePercent.InnerHtml = "" + _count + "% Complete";
        }
    }


    private void LoadBasicProfile()
    {
        string candi_id = GetCookie(this.Page, "CandidateId");
        CandidateMasterBL obj = new CandidateMasterBL();
        List<DataLayer.CandidateMaster> C = obj.LoadCandidateMasterByPrimaryKey(int.Parse(candi_id));
        if (C.Count > 0)
        {
            fullname.Value = C[0].CandidateName;
            resumeheadline.Value = C[0].ResumeHeadline;
            if(C[0].PreferredLocation != "") ddlCity.Items.FindByText(C[0].PreferredLocation).Selected = true;
            birthday.Value = DateTime.Parse(C[0].DOB.ToString()).ToString("dd-MMM-yyyy");
            if (C[0].Gender != "") ddlgender.Items.FindByValue(C[0].Gender).Selected = true;
            if (C[0].TotalExperience != "")
            {
                string[] _exp = C[0].TotalExperience.Split('$');
                ddlexperienceYear.Items.FindByValue(_exp[0].ToString()).Selected = true;
                ddlexperienceMonth.Items.FindByValue(_exp[1].ToString()).Selected = true;
            }
            if (C[0].AnnualSalary != "")
            {
                string[] _ctc = C[0].AnnualSalary.Split('$');
                ddlctcLacs.Items.FindByValue(_ctc[0].ToString()).Selected = true;
                ddlctcThousands.Items.FindByValue(_ctc[1].ToString()).Selected = true;
            }
            if (C[0].Currency != "")
            {
                string _curr = C[0].Currency;
                if (_curr == "INR") ctctype_i.Checked = true;
                else ctctype_u.Checked = true;
            }
            mobile.Value = C[0].MobileNo;
            landline.Value = C[0].LandlineNo;
            address.Value = C[0].PermanentAddress;
            city.Value = C[0].HomeTownCity;
            pincode.Value = C[0].PinCode;
            if (C[0].MaritalStatus != "") ddlmaritalStatus.Items.FindByValue(C[0].MaritalStatus).Selected = true;
            keyskill.Value = C[0].KeySkill;
        }
    }

    private void SaveProfile(int _cadidateId)
    {
        CandidateMasterBL obj = new CandidateMasterBL();
        string currency = "";
        if(ctctype_i.Checked == true) currency = "INR";
        else currency = "US";
        obj.UpdateCandidateMasterBasicProfile(_cadidateId, fullname.Value, DateTime.Parse(birthday.Value), ddlgender.Items[ddlgender.SelectedIndex].Value, ddlmaritalStatus.Items[ddlmaritalStatus.SelectedIndex].Value, mobile.Value, landline.Value, address.Value, city.Value, pincode.Value, ddlCity.Items[ddlCity.SelectedIndex].Text, ddlexperienceYear.Items[ddlexperienceYear.SelectedIndex].Value + "$" + ddlexperienceMonth.Items[ddlexperienceMonth.SelectedIndex].Value, ddlctcLacs.Items[ddlctcLacs.SelectedIndex].Value + "$" + ddlctcThousands.Items[ddlctcThousands.SelectedIndex].Value, currency, resumeheadline.Value, keyskill.Value);

        ShowMessage("Profile Updated successfully", this.Page);
    }


    protected void BtnSave_Click(object sender, EventArgs e)
    {
        string candi_id = GetCookie(this.Page, "CandidateId");

        SaveProfile(int.Parse(candi_id));
        LoadProfilePercent();
    }


    private void LoadCandidateDesignation()
    {
        string candi_id = GetCookie(this.Page, "CandidateId");
        CandidateDesignationBL obj = new CandidateDesignationBL();
        List<DataLayer.CandidateDesignation> D = obj.LoadCandidateDesignationByCandidateId(int.Parse(candi_id));
        if (D.Count > 0)
        {
            company.Value = D[0].CompanyName;
            rbStatus.SelectedValue = D[0].Status;
            string[] duration = D[0].Duration.Split('$');
            startMonth.Items.FindByText(duration[0].ToString()).Selected = true;
            startYear.Items.FindByText(duration[1].ToString()).Selected = true;
            endMonth.Items.FindByText(duration[2].ToString()).Selected = true;
            endYear.Items.FindByText(duration[3].ToString()).Selected = true;
            designation.Value = D[0].Designation;
            jobprofile.Value = D[0].JobProfile;
            ddlnoticePeriod.Items.FindByText(D[0].NoticePeriod.ToString()).Selected = true;
            BtnSaveDesignation.Text = "Update";
        }
        else
        {
            BtnSaveDesignation.Text = "Save";
        }
    }

    protected void BtnSaveDesignation_Click(object sender, EventArgs e)
    {
        try
        {
            string candi_id = GetCookie(this.Page, "CandidateId");
            CandidateDesignationBL obj = new CandidateDesignationBL();
            if (BtnSaveDesignation.Text == "Save")
            {
                obj.InsertCandidateDesignation(int.Parse(candi_id), company.Value, rbStatus.SelectedValue, startMonth.Items[startMonth.SelectedIndex].Text + "$" + startYear.Items[startYear.SelectedIndex].Text + "$" + endMonth.Items[endMonth.SelectedIndex].Text + "$" + endYear.Items[endYear.SelectedIndex].Text, designation.Value, jobprofile.Value, ddlnoticePeriod.Items[ddlnoticePeriod.SelectedIndex].Text);
            }
            else if (BtnSaveDesignation.Text == "Update")
            {
                obj.UpdateCandidateDesignationByCandidateId(int.Parse(candi_id), company.Value, rbStatus.SelectedValue, startMonth.Items[startMonth.SelectedIndex].Text + "$" + startYear.Items[startYear.SelectedIndex].Text + "$" + endMonth.Items[endMonth.SelectedIndex].Text + "$" + endYear.Items[endYear.SelectedIndex].Text, designation.Value, jobprofile.Value, ddlnoticePeriod.Items[ddlnoticePeriod.SelectedIndex].Text);
            }

            ShowMessage("Updated Successfully", this.Page);

            LoadProfilePercent();
        }
        catch { }
    }


    private void LoadEducationDetail()
    {
        string candi_id = GetCookie(this.Page, "CandidateId");
        CandidateEducationBL obj = new CandidateEducationBL();
        List<DataLayer.CandidateEducation> E = obj.LoadCandidateEducationByCandidateId(int.Parse(candi_id));
        if (E.Count > 0)
        {
            ugcourseId.Items.FindByText(E[0].EducationName).Selected = true;
            rbUGStatus.SelectedValue = E[0].EducationType;
            ugspecId.Items.Add(E[0].Specialization);
            ugspecId.Items.FindByText(E[0].Specialization).Selected = true;
            uginstituteId.Items.FindByText(E[0].UniversityInstitute).Selected = true;
            ugyearOfCompletion.Items.FindByValue(E[0].PassingYear).Selected = true;
            hfugspecId.Value = E[0].Specialization;

            pgcourseId.Items.FindByText(E[1].EducationName).Selected = true;
            rbPGStatus.SelectedValue = E[1].EducationType;
            pgspecId.Items.Add(E[1].Specialization);
            pgspecId.Items.FindByText(E[1].Specialization).Selected = true;
            pginstitute.Items.FindByText(E[1].UniversityInstitute).Selected = true;
            pgyearOfCompletion.Items.FindByValue(E[1].PassingYear).Selected = true;
            hfpgspecId.Value = E[1].Specialization;

            hfEducation_PG_Pkey.Value = E[1].CandidateEducationId.ToString();
            hfEducation_UG_Pkey.Value = E[0].CandidateEducationId.ToString();
            BtnSaveEducation.Text = "Update";
            lnkPanelPG.Visible = false;
            PnlPG.Style.Add("display", "block");
        }
        else
        {
            BtnSaveEducation.Text = "Save";
        }

    }

    protected void BtnSaveEducation_Click(object sender, EventArgs e)
    {
        try
        {
            string candi_id = GetCookie(this.Page, "CandidateId");
            CandidateEducationBL obj = new CandidateEducationBL();
            if (BtnSaveEducation.Text == "Update")
            {
                obj.UpdateCandidateEducation(int.Parse(hfEducation_UG_Pkey.Value), int.Parse(candi_id), ugcourseId.Items[ugcourseId.SelectedIndex].Text, rbUGStatus.SelectedValue, hfugspecId.Value, uginstituteId.Items[uginstituteId.SelectedIndex].Text, ugyearOfCompletion.Items[ugyearOfCompletion.SelectedIndex].Value, "UG");
                obj.UpdateCandidateEducation(int.Parse(hfEducation_PG_Pkey.Value), int.Parse(candi_id), pgcourseId.Items[pgcourseId.SelectedIndex].Text, rbPGStatus.SelectedValue, hfpgspecId.Value, pginstitute.Items[pginstitute.SelectedIndex].Text, pgyearOfCompletion.Items[pgyearOfCompletion.SelectedIndex].Value, "PG");
            }
            else if (BtnSaveEducation.Text == "Save")
            {
                obj.InsertCandidateEducation(int.Parse(candi_id), ugcourseId.Items[ugcourseId.SelectedIndex].Text, rbUGStatus.SelectedValue, ugspecId.Items[ugspecId.SelectedIndex].Text, uginstituteId.Items[uginstituteId.SelectedIndex].Text, ugyearOfCompletion.Items[ugyearOfCompletion.SelectedIndex].Value, "UG");
                obj.InsertCandidateEducation(int.Parse(candi_id), pgcourseId.Items[pgcourseId.SelectedIndex].Text, rbPGStatus.SelectedValue, pgspecId.Items[pgspecId.SelectedIndex].Text, pginstitute.Items[pginstitute.SelectedIndex].Text, pgyearOfCompletion.Items[pgyearOfCompletion.SelectedIndex].Value, "PG");
            }
            ShowMessage("Education Detail Saved Successfully", this.Page);

            LoadProfilePercent();
        }
        catch
        { }
    }

    private void LoadPhoto()
    {
        string candi_id = GetCookie(this.Page, "CandidateId");
        CandidateMasterBL obj = new CandidateMasterBL();
        List<DataLayer.CandidateMaster> C = obj.LoadCandidateMasterByPrimaryKey(int.Parse(candi_id));
        if (C.Count > 0)
        {
            ImgCandidate.ImageUrl = C[0].ProfileImagePath;
        }
    }

    private void LoadResume()
    {
        string candi_id = GetCookie(this.Page, "CandidateId");
        CandidateMasterBL obj = new CandidateMasterBL();
        List<DataLayer.CandidateMaster> C = obj.LoadCandidateMasterByPrimaryKey(int.Parse(candi_id));
        if (C.Count > 0)
        {
            lnkDownloadResume.NavigateUrl = C[0].ResumeFilePath;
        }
    }

    protected void BtnSavePhoto_Click(object sender, EventArgs e)
    {
        if (FUPhoto.HasFile)
        {
            string FileName = Path.GetFileName(FUPhoto.PostedFile.FileName);
            if (CheckFileType(FileName) == false)
            {
                ShowMessage("Invalid File", this.Page);
                return;
            }
            else
            {
                FileName = CheckFileNameExist("ReadWrite/CandidatePhoto", FileName);
                FUPhoto.SaveAs(Server.MapPath("~//ReadWrite//CandidatePhoto//" + FileName));

                if (hfCandidateImageUrl.Value != "")
                {
                    DeleteFileFromFolder(hfCandidateImageUrl.Value);
                }
                hfCandidateImageUrl.Value = "~/ReadWrite/CandidatePhoto/" + FileName;
                ImgCandidate.ImageUrl = "~/ReadWrite/CandidatePhoto/" + FileName;

                CandidateMasterBL obj = new CandidateMasterBL();
                 string candi_id = GetCookie(this.Page, "CandidateId");
                 obj.UpdateCandidateMasterProfileImagePath(int.Parse(candi_id), hfCandidateImageUrl.Value);
                 ShowMessage("Photo Updated Successfully", this.Page);

                 LoadProfilePercent();
            }
        }
        else
        {
            ShowMessage("Please Select Photo", this.Page);
        }
    }

    protected void BtnSaveResume_Click(object sender, EventArgs e)
    {
        if (FUResume.HasFile)
        {
            string FileName = Path.GetFileName(FUResume.PostedFile.FileName);
            if (CheckFileType(FileName) == false)
            {
                ShowMessage("Invalid File", this.Page);
                return;
            }
            else
            {
                FileName = CheckFileNameExist("ReadWrite/Resume", FileName);
                FUResume.SaveAs(Server.MapPath("~//ReadWrite//Resume//" + FileName));

                if (hfResumePath.Value != "")
                {
                    DeleteFileFromFolder(hfResumePath.Value);
                }
                hfResumePath.Value = "~/ReadWrite/Resume/" + FileName;
                lnkDownloadResume.NavigateUrl = "~/ReadWrite/Resume/" + FileName;

                CandidateMasterBL obj = new CandidateMasterBL();
                string candi_id = GetCookie(this.Page, "CandidateId");
                obj.UpdateCandidateMasterResumePath(int.Parse(candi_id), hfResumePath.Value);
                ShowMessage("Resume Updated Successfully", this.Page);

                LoadProfilePercent();
            }
        }
        else
        {
            ShowMessage("Please Select Resume", this.Page);
        }
    }

    private void DeleteFileFromFolder(string path)
    {
        string Deletefile = Server.MapPath(path);
        FileInfo file = new FileInfo(Deletefile);
        if (file.Exists)
        {
            file.Delete();
        }
    }

}