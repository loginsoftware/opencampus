﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Universal.master" AutoEventWireup="true" CodeFile="SearchJobListing.aspx.cs" Inherits="SearchJobListing" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="cph" Runat="Server">
<div class="container profile-new"> <br/>
  
  
  <div class="row row3 padd-all">
  	<div class="col-md-12 advSearch" style="margin:2% 0">
    <div id="searchjobs" name="searchform">
        <span class="keyblock col-md-5 padding0">
        	<label>Keywords</label>
        	<input type="text" class="form-control" placeholder="Designation, skills, company" id="Keywords" value="accountant" autocomplete="off">
        </span>
        <span class="keyblock col-md-5 padding0">
        	<label>Location</label>
        	<input type="text" class="form-control" placeholder="Country, state, city." id="SearchLocation" value="" autocomplete="off" style="border-left:1px solid #ddd">
        </span>
		<span class="keyblock col-md-2 padding0" style="margin-top:40px">
        	<button id="btnSearchJobs" class="font-s-12 search-btn btn btn-default btn-yellow" type="submit"><i class="fa fa-search"></i> Search Jobs</button>
        </span>
        <div class="clearfix"></div>
      </div>
      <div class="clearfix"></div>
    </div>
    <div class="col-md-9 col-md-offset-3 job-tit"><span><div class="margin0 f-white txt-t font-s-20 padd5">
        <asp:Label ID="lblJobCount" runat="server" Text=""></asp:Label>
     Jobs Found for</div><h1 class="margin0 f-white txt-t font-s-18 padd5">
      <asp:Label ID="lblJobSearchName" runat="server" Text="" style="text-transform:capitalize;"></asp:Label>
     </h1></span></div>
    
    <div class="col-md-3 refine-j"> 
      <!--<a href="advanced-job-search.php" class="btn btn-primary a-search box-shadow border-r show"><i class="fa fa-rocket"></i> Advance Search</a>-->
      
      <nav class="navbar profile-nav border-r">
        <div class="container-fluid padding0">
          <!-- Brand and toggle get grouped for better mobile display -->
          <div class="navbar-header">
            <button type="button" class="navbar-toggle collapsed bg-dgray" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1" aria-expanded="false"> <span class="sr-only">Toggle navigation</span> <span class="icon-bar"></span> <span class="icon-bar"></span> <span class="icon-bar"></span> </button>
          </div>
          <!-- Collect the nav links, forms, and other content for toggling -->
          <div class="collapse navbar-collapse padding0" id="bs-example-navbar-collapse-1">
                      <div method="get" action="accountant-jobs?Keywords=accountant">      
      	<div class="search-side bg-white border-r box-shadow">
        <div class="filt-head filt-head-active">
          <span class="black font-s-14 margin0 pull-left">Industry</span>
          <span class="font-s-14 black pull-right">-</span>
          <div class="clearfix"></div>
        </div>
        <div class="check-list">
        	<div class="scroll-panes padd10">
			  <label class="ellipsis"><input type="checkbox" name="SubIndustry[]" value="1" >Cement</label><label class="ellipsis"><input type="checkbox" name="SubIndustry[]" value="2" >Agriculture</label><label class="ellipsis"><input type="checkbox" name="SubIndustry[]" value="3" >Automobile / Auto-ancillary / Tyre</label><label class="ellipsis"><input type="checkbox" name="SubIndustry[]" value="5" >Financial Services / Banking / Broking / Forex / Investment</label><label class="ellipsis"><input type="checkbox" name="SubIndustry[]" value="8" >Engineering / Infrastructure / Construction / EPC</label><label class="ellipsis"><input type="checkbox" name="SubIndustry[]" value="9" >Consumer Durables / Electronic Appliances / White Goods</label><label class="ellipsis"><input type="checkbox" name="SubIndustry[]" value="11" >Export / Import / Trading</label><label class="ellipsis"><input type="checkbox" name="SubIndustry[]" value="12" >Accounting / Audit / Taxation</label><label class="ellipsis"><input type="checkbox" name="SubIndustry[]" value="14" >FMCG / Foods / Beverages / Food Processing</label><label class="ellipsis"><input type="checkbox" name="SubIndustry[]" value="15" >Hotel / Restaurants</label><label class="ellipsis"><input type="checkbox" name="SubIndustry[]" value="16" >Insurance</label><label class="ellipsis"><input type="checkbox" name="SubIndustry[]" value="17" >IT - Hardware & Networking</label><label class="ellipsis"><input type="checkbox" name="SubIndustry[]" value="18" >IT - Software Services</label><label class="ellipsis"><input type="checkbox" name="SubIndustry[]" value="19" >Healthcare / Medical / Hospital</label><label class="ellipsis"><input type="checkbox" name="SubIndustry[]" value="20" >Metals  / Mining / Minerals (Ferrous / Non-Ferrous / Coal)</label><label class="ellipsis"><input type="checkbox" name="SubIndustry[]" value="22" >Petrochemicals / Chemical / Dyes and Stuff / Plastic / Rubber</label><label class="ellipsis"><input type="checkbox" name="SubIndustry[]" value="23" >Pharma / Biotech / Life Science</label><label class="ellipsis"><input type="checkbox" name="SubIndustry[]" value="24" >Real Estate</label><label class="ellipsis"><input type="checkbox" name="SubIndustry[]" value="25" >Retailing / Malls / Supermarts / Stores</label><label class="ellipsis"><input type="checkbox" name="SubIndustry[]" value="26" >Shipping / Ship building / Marine</label><label class="ellipsis"><input type="checkbox" name="SubIndustry[]" value="27" >Telecom / Internet</label><label class="ellipsis"><input type="checkbox" name="SubIndustry[]" value="28" >Textiles / Garments</label><label class="ellipsis"><input type="checkbox" name="SubIndustry[]" value="29" >Travel / Tourism / Ticketing</label><label class="ellipsis"><input type="checkbox" name="SubIndustry[]" value="30" >Advertising / Publishing / Events / PR / MR</label><label class="ellipsis"><input type="checkbox" name="SubIndustry[]" value="32" >Architecture / Interior Design</label><label class="ellipsis"><input type="checkbox" name="SubIndustry[]" value="34" >Aviation / Aerospace / Airlines / MRO</label><label class="ellipsis"><input type="checkbox" name="SubIndustry[]" value="35" >Ceramics / Sanitary Ware / Homedecor / Building Material / Glass</label><label class="ellipsis"><input type="checkbox" name="SubIndustry[]" value="36" >Certification / Quality</label><label class="ellipsis"><input type="checkbox" name="SubIndustry[]" value="37" >Consulting</label><label class="ellipsis"><input type="checkbox" name="SubIndustry[]" value="38" >Courier / Transport / Freight / Logistics</label><label class="ellipsis"><input type="checkbox" name="SubIndustry[]" value="39" >Dairy / Poultry / Animal Farming / Fish Farming</label><label class="ellipsis"><input type="checkbox" name="SubIndustry[]" value="40" >Distillery / Liquor</label><label class="ellipsis"><input type="checkbox" name="SubIndustry[]" value="41" >Diversified</label><label class="ellipsis"><input type="checkbox" name="SubIndustry[]" value="42" >Education / Training</label><label class="ellipsis"><input type="checkbox" name="SubIndustry[]" value="43" >Electricals / Switchgears</label><label class="ellipsis"><input type="checkbox" name="SubIndustry[]" value="44" >Entertainment / Media</label><label class="ellipsis"><input type="checkbox" name="SubIndustry[]" value="45" >Facility Management</label><label class="ellipsis"><input type="checkbox" name="SubIndustry[]" value="46" >Fertilizers / Pesticides</label><label class="ellipsis"><input type="checkbox" name="SubIndustry[]" value="47" >Freshers / Trainee</label><label class="ellipsis"><input type="checkbox" name="SubIndustry[]" value="48" >Gems and Jewellery</label><label class="ellipsis"><input type="checkbox" name="SubIndustry[]" value="49" >Government / Defense / PSU</label><label class="ellipsis"><input type="checkbox" name="SubIndustry[]" value="50" >Health / Wellness / Fitness / Sports / Beauty / SPA / Fashion</label><label class="ellipsis"><input type="checkbox" name="SubIndustry[]" value="51" >Heavy Engineering / Industrial Products / Equipment Manufacturing</label><label class="ellipsis"><input type="checkbox" name="SubIndustry[]" value="52" >HR / Recruitment / Staffing / Manpower</label><label class="ellipsis"><input type="checkbox" name="SubIndustry[]" value="53" >Legal</label><label class="ellipsis"><input type="checkbox" name="SubIndustry[]" value="54" >ITES / BPO / KPO / Outsourcing</label><label class="ellipsis"><input type="checkbox" name="SubIndustry[]" value="55" >Leather</label><label class="ellipsis"><input type="checkbox" name="SubIndustry[]" value="56" >Manufacturing</label><label class="ellipsis"><input type="checkbox" name="SubIndustry[]" value="57" >NGO / Social Service / Politics</label><label class="ellipsis"><input type="checkbox" name="SubIndustry[]" value="58" >Office Equipment / Automation</label><label class="ellipsis"><input type="checkbox" name="SubIndustry[]" value="59" >Oil / Gas / Petroleum</label><label class="ellipsis"><input type="checkbox" name="SubIndustry[]" value="60" >Paint</label><label class="ellipsis"><input type="checkbox" name="SubIndustry[]" value="61" >Paper</label><label class="ellipsis"><input type="checkbox" name="SubIndustry[]" value="63" >Power / Energy / Electricity</label><label class="ellipsis"><input type="checkbox" name="SubIndustry[]" value="64" >Printing / Packaging</label><label class="ellipsis"><input type="checkbox" name="SubIndustry[]" value="65" >Security</label><label class="ellipsis"><input type="checkbox" name="SubIndustry[]" value="66" >Semiconductors / Electronics / Communications</label><label class="ellipsis"><input type="checkbox" name="SubIndustry[]" value="69" >Sugar</label><label class="ellipsis"><input type="checkbox" name="SubIndustry[]" value="70" >Water Treatment / Waste Management / ETP</label><label class="ellipsis"><input type="checkbox" name="SubIndustry[]" value="71" >Other</label><label class="ellipsis"><input type="checkbox" name="SubIndustry[]" value="72" >Space & Aeronautics</label><label class="ellipsis"><input type="checkbox" name="SubIndustry[]" value="73" >Astrology</label><label class="ellipsis"><input type="checkbox" name="SubIndustry[]" value="74" >Iron/ Steel</label><label class="ellipsis"><input type="checkbox" name="SubIndustry[]" value="75" >HVAC / Electromechanical</label><label class="ellipsis"><input type="checkbox" name="SubIndustry[]" value="9999" >Any</label>            </div>
        </div>
      </div>
      	<div class="search-side bg-white border-r box-shadow">
        <div class="filt-head">
          <span class="black font-s-14 margin0 pull-left">Functional Area</span>
          <span class="font-s-14 black pull-right">-</span>
          <div class="clearfix"></div>
        </div>
        <div class="check-list">
          <div class="scroll-panes padd10" style="display:none">
          	<label class="ellipsis"><input type="checkbox" name="SubFunctionalArea[]" value="1" >Accounting / Tax / Company Secretary / Audit</label><label class="ellipsis"><input type="checkbox" name="SubFunctionalArea[]" value="5" >Banking / Insurance</label><label class="ellipsis"><input type="checkbox" name="SubFunctionalArea[]" value="8" >Export / Import</label><label class="ellipsis"><input type="checkbox" name="SubFunctionalArea[]" value="13" >Front Office Staff / Secretarial / Computer Operator</label><label class="ellipsis"><input type="checkbox" name="SubFunctionalArea[]" value="15" >Hotels / Restaurant Management</label><label class="ellipsis"><input type="checkbox" name="SubFunctionalArea[]" value="17" >HR / Admin / PM / IR / Training</label><label class="ellipsis"><input type="checkbox" name="SubFunctionalArea[]" value="18" >IT</label><label class="ellipsis"><input type="checkbox" name="SubFunctionalArea[]" value="19" >Other</label><label class="ellipsis"><input type="checkbox" name="SubFunctionalArea[]" value="25" >Production / Service Engineering / Manufacturing / Maintenance</label><label class="ellipsis"><input type="checkbox" name="SubFunctionalArea[]" value="26" >Project Management / Site Engineers</label><label class="ellipsis"><input type="checkbox" name="SubFunctionalArea[]" value="29" >Sales / Business Development / Client Servicing</label><label class="ellipsis"><input type="checkbox" name="SubFunctionalArea[]" value="43" >Teaching / Education / Language Specialist</label>          </div>
        </div>
      </div>
      	<div class="search-side bg-white border-r box-shadow">
        <div class="filt-head">
          <span class="black font-s-14 margin0 pull-left">Location</span>
          <span class="font-s-14 black pull-right">-</span>
          <div class="clearfix"></div>
        </div>
        <div class="check-list">
          <div class="scroll-panes padd10" style="display:none">
          	<label class="ellipsis"><input type="checkbox" name="SubLocation[]" value="2004" >Afghanistan</label><label class="ellipsis"><input type="checkbox" name="SubLocation[]" value="2024" >Angola</label><label class="ellipsis"><input type="checkbox" name="SubLocation[]" value="2048" >Bahrain</label><label class="ellipsis"><input type="checkbox" name="SubLocation[]" value="2072" >Botswana</label><label class="ellipsis"><input type="checkbox" name="SubLocation[]" value="2156" >China</label><label class="ellipsis"><input type="checkbox" name="SubLocation[]" value="2178" >Congo</label><label class="ellipsis"><input type="checkbox" name="SubLocation[]" value="2262" >Djibouti</label><label class="ellipsis"><input type="checkbox" name="SubLocation[]" value="2288" >Ghana</label><label class="ellipsis"><input type="checkbox" name="SubLocation[]" value="2356" >India</label><label class="ellipsis"><input type="checkbox" name="SubLocation[]" value="2360" >Indonesia</label><label class="ellipsis"><input type="checkbox" name="SubLocation[]" value="2368" >Iraq</label><label class="ellipsis"><input type="checkbox" name="SubLocation[]" value="2404" >Kenya</label><label class="ellipsis"><input type="checkbox" name="SubLocation[]" value="2414" >Kuwait</label><label class="ellipsis"><input type="checkbox" name="SubLocation[]" value="2422" >Lebanon</label><label class="ellipsis"><input type="checkbox" name="SubLocation[]" value="2454" >Malawi</label><label class="ellipsis"><input type="checkbox" name="SubLocation[]" value="2458" >Malaysia</label><label class="ellipsis"><input type="checkbox" name="SubLocation[]" value="2508" >Mozambique</label><label class="ellipsis"><input type="checkbox" name="SubLocation[]" value="2512" >Oman</label><label class="ellipsis"><input type="checkbox" name="SubLocation[]" value="2566" >Nigeria</label><label class="ellipsis"><input type="checkbox" name="SubLocation[]" value="2634" >Qatar</label><label class="ellipsis"><input type="checkbox" name="SubLocation[]" value="2646" >Rwanda</label><label class="ellipsis"><input type="checkbox" name="SubLocation[]" value="2682" >Saudi Arabia</label><label class="ellipsis"><input type="checkbox" name="SubLocation[]" value="2710" >South Africa</label><label class="ellipsis"><input type="checkbox" name="SubLocation[]" value="2764" >Thailand</label><label class="ellipsis"><input type="checkbox" name="SubLocation[]" value="2784" >United Arab Emirates</label><label class="ellipsis"><input type="checkbox" name="SubLocation[]" value="2792" >Turkey</label><label class="ellipsis"><input type="checkbox" name="SubLocation[]" value="2800" >Uganda</label><label class="ellipsis"><input type="checkbox" name="SubLocation[]" value="2834" >Tanzania</label><label class="ellipsis"><input type="checkbox" name="SubLocation[]" value="2894" >Zambia</label><label class="ellipsis"><input type="checkbox" name="SubLocation[]" value="20453" >Andhra Pradesh</label><label class="ellipsis"><input type="checkbox" name="SubLocation[]" value="20456" >Delhi</label><label class="ellipsis"><input type="checkbox" name="SubLocation[]" value="20458" >Haryana</label><label class="ellipsis"><input type="checkbox" name="SubLocation[]" value="20460" >Karnataka</label><label class="ellipsis"><input type="checkbox" name="SubLocation[]" value="20461" >Kerala</label><label class="ellipsis"><input type="checkbox" name="SubLocation[]" value="20464" >Madhya Pradesh</label><label class="ellipsis"><input type="checkbox" name="SubLocation[]" value="20950" >Moscow</label><label class="ellipsis"><input type="checkbox" name="SubLocation[]" value="21342" >Chandigarh</label><label class="ellipsis"><input type="checkbox" name="SubLocation[]" value="21564" >Lagos</label><label class="ellipsis"><input type="checkbox" name="SubLocation[]" value="1000010" >Abu Dhabi</label><label class="ellipsis"><input type="checkbox" name="SubLocation[]" value="1000011" >Ajman</label><label class="ellipsis"><input type="checkbox" name="SubLocation[]" value="1000013" >Dubai</label><label class="ellipsis"><input type="checkbox" name="SubLocation[]" value="1007740" >Hyderabad</label><label class="ellipsis"><input type="checkbox" name="SubLocation[]" value="1007751" >New Delhi</label><label class="ellipsis"><input type="checkbox" name="SubLocation[]" value="1007753" >Ahmedabad</label><label class="ellipsis"><input type="checkbox" name="SubLocation[]" value="1007756" >Gandhinagar</label><label class="ellipsis"><input type="checkbox" name="SubLocation[]" value="1007761" >Vadodara</label><label class="ellipsis"><input type="checkbox" name="SubLocation[]" value="1007765" >Gurgaon</label><label class="ellipsis"><input type="checkbox" name="SubLocation[]" value="1007768" >Bangalore</label><label class="ellipsis"><input type="checkbox" name="SubLocation[]" value="1007776" >Kochi</label><label class="ellipsis"><input type="checkbox" name="SubLocation[]" value="1007780" >Aurangabad</label><label class="ellipsis"><input type="checkbox" name="SubLocation[]" value="1007785" >Mumbai</label><label class="ellipsis"><input type="checkbox" name="SubLocation[]" value="1007788" >Pune</label><label class="ellipsis"><input type="checkbox" name="SubLocation[]" value="1007796" >Indore</label><label class="ellipsis"><input type="checkbox" name="SubLocation[]" value="1007802" >Ludhiana</label><label class="ellipsis"><input type="checkbox" name="SubLocation[]" value="1007809" >Chennai</label><label class="ellipsis"><input type="checkbox" name="SubLocation[]" value="1007819" >Dehradun</label><label class="ellipsis"><input type="checkbox" name="SubLocation[]" value="1007826" >Noida</label><label class="ellipsis"><input type="checkbox" name="SubLocation[]" value="1007828" >Kolkata</label><label class="ellipsis"><input type="checkbox" name="SubLocation[]" value="1012088" >Riyadh</label><label class="ellipsis"><input type="checkbox" name="SubLocation[]" value="1012089" >Jeddah</label><label class="ellipsis"><input type="checkbox" name="SubLocation[]" value="1026824" >Sudan</label><label class="ellipsis"><input type="checkbox" name="SubLocation[]" value="9040212" >Ernakulam</label><label class="ellipsis"><input type="checkbox" name="SubLocation[]" value="9040215" >Thrissur</label><label class="ellipsis"><input type="checkbox" name="SubLocation[]" value="9040216" >Palakkad</label><label class="ellipsis"><input type="checkbox" name="SubLocation[]" value="9040229" >Pimpri Chinchwad</label><label class="ellipsis"><input type="checkbox" name="SubLocation[]" value="9041083" >Dubai</label><label class="ellipsis"><input type="checkbox" name="SubLocation[]" value="9047099" >Sharjah</label><label class="ellipsis"><input type="checkbox" name="SubLocation[]" value="9050511" >Mohali,Sahibzada Ajit Singh Nagar</label><label class="ellipsis"><input type="checkbox" name="SubLocation[]" value="9051475" >Dammam</label><label class="ellipsis"><input type="checkbox" name="SubLocation[]" value="9051476" >Al Khobar</label><label class="ellipsis"><input type="checkbox" name="SubLocation[]" value="9059999" >Doha,Qatar</label><label class="ellipsis"><input type="checkbox" name="SubLocation[]" value="9061363" >Africa</label><label class="ellipsis"><input type="checkbox" name="SubLocation[]" value="9061364" >Dar Es Salaam</label><label class="ellipsis"><input type="checkbox" name="SubLocation[]" value="9061366" >South Sudan,Juba</label>          </div>
        </div>
      </div>
      	<div class="search-side bg-white border-r box-shadow">
        <div class="filt-head">
          <span class="black font-s-14 margin0 pull-left">Companies</span>
          <span class="font-s-14 black pull-right">-</span>
          <div class="clearfix"></div>
        </div>
        <div class="check-list">
          <div class="scroll-panes padd10" style="display:none">
          	<label class="ellipsis"><input type="checkbox" name="SubCompany[]" value="1" >Universal Hunt</label><label class="ellipsis"><input type="checkbox" name="SubCompany[]" value="26" >Al Ramooz Group</label><label class="ellipsis"><input type="checkbox" name="SubCompany[]" value="125" >Vsplash Tech Labs Pvt Ltd</label><label class="ellipsis"><input type="checkbox" name="SubCompany[]" value="126" >MSL</label><label class="ellipsis"><input type="checkbox" name="SubCompany[]" value="143" >VWF INDUSTRIES PVT LTD</label><label class="ellipsis"><input type="checkbox" name="SubCompany[]" value="208" >Al-Thanayyan International Co.FZE</label><label class="ellipsis"><input type="checkbox" name="SubCompany[]" value="226" >National Bags Factory LLC</label><label class="ellipsis"><input type="checkbox" name="SubCompany[]" value="297" >Seajin Tech</label><label class="ellipsis"><input type="checkbox" name="SubCompany[]" value="540" >Swani Furniture Point Pvt Ltd</label><label class="ellipsis"><input type="checkbox" name="SubCompany[]" value="584" >Health Sanctuary Pvt. Ltd.</label><label class="ellipsis"><input type="checkbox" name="SubCompany[]" value="592" >R D Shah &amp; Co</label><label class="ellipsis"><input type="checkbox" name="SubCompany[]" value="601" >Ascot Contracting llc</label><label class="ellipsis"><input type="checkbox" name="SubCompany[]" value="612" >MAHARSHI VISION</label><label class="ellipsis"><input type="checkbox" name="SubCompany[]" value="620" >ABW INFRASTRUCTURE LIMITED</label><label class="ellipsis"><input type="checkbox" name="SubCompany[]" value="663" >FJCare Technical Services</label><label class="ellipsis"><input type="checkbox" name="SubCompany[]" value="701" >POWERTECH</label><label class="ellipsis"><input type="checkbox" name="SubCompany[]" value="719" >Just scents pvt ltd</label><label class="ellipsis"><input type="checkbox" name="SubCompany[]" value="826" >Oswal Minerals Limited</label><label class="ellipsis"><input type="checkbox" name="SubCompany[]" value="831" >ALUSOL ROLLING SHUTTER INDUSTRIES</label><label class="ellipsis"><input type="checkbox" name="SubCompany[]" value="835" >Al Sanaya Technical Equipment LLC</label><label class="ellipsis"><input type="checkbox" name="SubCompany[]" value="1291" >KADEVI INDUSTRIES LTD.</label><label class="ellipsis"><input type="checkbox" name="SubCompany[]" value="1784" >Drake &amp; Scull Water &amp; Energy India Pvt Ltd</label><label class="ellipsis"><input type="checkbox" name="SubCompany[]" value="2338" >Gold Seal Engineering Products Pvt. Ltd.</label><label class="ellipsis"><input type="checkbox" name="SubCompany[]" value="2546" >Lakhani&amp;Lakhani CA Firm</label><label class="ellipsis"><input type="checkbox" name="SubCompany[]" value="2603" >Rhizome Hospitality Solutions</label><label class="ellipsis"><input type="checkbox" name="SubCompany[]" value="2824" >Kushal Technologies</label><label class="ellipsis"><input type="checkbox" name="SubCompany[]" value="3018" >Sama Jewellery Pvt Ltd</label><label class="ellipsis"><input type="checkbox" name="SubCompany[]" value="3875" >Delgence Technologies Pvt Ltd</label><label class="ellipsis"><input type="checkbox" name="SubCompany[]" value="4550" >Yousef Mohammad Al-Briek Al-Dossary</label><label class="ellipsis"><input type="checkbox" name="SubCompany[]" value="4803" >TRIFECTA BROADCASTING PVT LTD</label><label class="ellipsis"><input type="checkbox" name="SubCompany[]" value="4930" >VIJAY SECURITY SYSTEMS PVT LIMITED</label><label class="ellipsis"><input type="checkbox" name="SubCompany[]" value="4937" >PRIME PLACEMENT</label><label class="ellipsis"><input type="checkbox" name="SubCompany[]" value="4973" >Job Achievers</label><label class="ellipsis"><input type="checkbox" name="SubCompany[]" value="5237" >Kamal Leela Developers</label><label class="ellipsis"><input type="checkbox" name="SubCompany[]" value="5288" >Royal Chains Pvt. Ltd</label><label class="ellipsis"><input type="checkbox" name="SubCompany[]" value="5336" >GOBD</label><label class="ellipsis"><input type="checkbox" name="SubCompany[]" value="5813" >Health Sanctuary pvt.ltd</label><label class="ellipsis"><input type="checkbox" name="SubCompany[]" value="5911" >Catallysts Constellations</label><label class="ellipsis"><input type="checkbox" name="SubCompany[]" value="6239" >Diamond International Inex Pvt Ltd</label><label class="ellipsis"><input type="checkbox" name="SubCompany[]" value="6671" >Rayvat Intelligent Outsourcing</label><label class="ellipsis"><input type="checkbox" name="SubCompany[]" value="6802" >kainth consultants</label><label class="ellipsis"><input type="checkbox" name="SubCompany[]" value="7081" >Zenith India Pvt Ltd</label><label class="ellipsis"><input type="checkbox" name="SubCompany[]" value="7215" >Fortes Education</label><label class="ellipsis"><input type="checkbox" name="SubCompany[]" value="7284" >Three Star Consultancy Services</label><label class="ellipsis"><input type="checkbox" name="SubCompany[]" value="7747" >Bestow</label><label class="ellipsis"><input type="checkbox" name="SubCompany[]" value="7785" >A One Placement</label><label class="ellipsis"><input type="checkbox" name="SubCompany[]" value="7859" >Disha Career Services</label><label class="ellipsis"><input type="checkbox" name="SubCompany[]" value="7899" >Nessa Illumination TEchnologies Pvt Ltd</label>          </div>
        </div>
      </div>
      <div class="search-side bg-white border-r box-shadow">
        <div class="filt-head">
          <span class="black font-s-14 margin0 pull-left">Experience (Yrs)</span>
          <span class="font-s-14 black pull-right">-</span>
          <div class="clearfix"></div>
        </div>
        <div class="check-list">
          <div class="scroll-panes padd10" style="display:none">
          	<select name="ExperienceFrom" id="ExperienceFrom" class="form-control" style="float: left; width: 48%;">
              <option value="">From...</option>
              <option value="0" >0</option><option value="1" >1</option><option value="2" >2</option><option value="3" >3</option><option value="4" >4</option><option value="5" >5</option><option value="6" >6</option><option value="7" >7</option><option value="8" >8</option><option value="9" >9</option><option value="10" >10</option><option value="11" >11</option><option value="12" >12</option><option value="13" >13</option><option value="14" >14</option><option value="15" >15</option><option value="16" >16</option><option value="17" >17</option><option value="18" >18</option><option value="19" >19</option><option value="20" >20</option><option value="21" >21</option><option value="22" >22</option><option value="23" >23</option><option value="24" >24</option><option value="25" >25</option><option value="26" >26</option><option value="27" >27</option><option value="28" >28</option><option value="29" >29</option><option value="30" >30</option>            </select>
            <select name="ExperienceTo" id="ExperienceTo" class="form-control" style="float: left; width: 48%; margin-left: 2%;">
              <option value="">To...</option>
              <option value="0" >0</option><option value="1" >1</option><option value="2" >2</option><option value="3" >3</option><option value="4" >4</option><option value="5" >5</option><option value="6" >6</option><option value="7" >7</option><option value="8" >8</option><option value="9" >9</option><option value="10" >10</option><option value="11" >11</option><option value="12" >12</option><option value="13" >13</option><option value="14" >14</option><option value="15" >15</option><option value="16" >16</option><option value="17" >17</option><option value="18" >18</option><option value="19" >19</option><option value="20" >20</option><option value="21" >21</option><option value="22" >22</option><option value="23" >23</option><option value="24" >24</option><option value="25" >25</option><option value="26" >26</option><option value="27" >27</option><option value="28" >28</option><option value="29" >29</option><option value="30" >30</option>            </select>
          </div>
        </div>
      </div>
      	        <input type="submit" name="btnSearch" value="Search" class="btn btn-primary" style="width:100%">
      </div>
      <div class="clearfix"></div>
          </div>
          <!-- /.navbar-collapse -->
          <div class="clearfix"></div>
        </div>
        <!-- /.container-fluid -->
      </nav>

      
    </div>


    <div class="col-md-6" id="job-result-list">
        <asp:Repeater ID="lstJobLiting" runat="server">
        <ItemTemplate>
        <div class="profile-body bg-white border-r box-shadow job-s">
        <div class=" padding0 profile-snap">
          <div class="col-md-2 padd10">
              <asp:Image ID="imgCompanyLogo" runat="server" ImageUrl='<%# Eval("CompanyLogo") %>' AlternateText='<%# Eval("CompanyName") %>' class="img-responsive img-thumbnail" />
            <br>
          </div> 	
          <div class="col-md-7 padd10">
            <a href='jobs/<%# MyUrl(Eval("JobTitleDesignation").ToString()).Trim('-') + "-" + Eval("JobId") %>' class="ellipsis font-s-14" target="_blank"><span class="blue font-s-14"><%# Eval("JobTitleDesignation") %></span></a>
				<div class="jobv">
            		<span class="show"><%# Eval("CompanyName") %></span>
                    <span class="show"><i class="fa fa-suitcase"></i> <%# Eval("WorkExperience").ToString().Replace("TO", " to ") + " years"%></span>
                    <span class="show"><i class="fa fa-map-marker"></i> <%# Eval("JobLocation")%></span>
                    <span class="show ellipsis"><strong>Keyskills</strong>: <%# Eval("Keywords")%>                   </span>
                </div>
            	<div>
                	   </div>
                    
          </div>
          <div class="col-md-3"> 
            <br><br>
            <div class="apply pull-right">
              <a href='jobs/<%# MyUrl(Eval("JobTitleDesignation").ToString()).Trim('-') + "-" + Eval("JobId") %>' title='<%# Eval("JobTitleDesignation")%>' target="_blank"><button type="button" class="btn pull-left border-r">View Application</button></a>              
              <ul class="dropdown-menu text-right" aria-labelledby="dropdownMenu1">
                                  
              </ul>
            </div>
           </div>
          <div class="clearfix"></div>
        </div>
        <div class="clearfix"></div>
      </div>
      </ItemTemplate>
        </asp:Repeater>
          
     
            
     
     
            <div class="col-md-12 text-center">
		<ul class="pagination">
			 <li>
           <asp:HyperLink ID="lnkPagingPrevious" runat="server" class="btn-gray" Visible="false">Previous</asp:HyperLink>
                 <asp:HyperLink ID="lnkPagingNext" runat="server" class="btn-gray" Visible="false">Next</asp:HyperLink>
             </li>        </ul>
                <a href="https://in.jooble.org" target="blank">Jooble</a>
    </div>
        
    </div>

    <div class="col-md-3">
    	<div class="profile-body bg-white border-r box-shadow job-s padd-all">
        	<h2 class="font-s-20 margin0">Share Accountant Jobs </h2>
        	<div class="row padd10" style="margin-top:12px">  
            	<a href="http://www.facebook.com/sharer/sharer.php?u=accountant-jobs" class="col-md-4"><img src="campus/images/icons/social-facebook.png" width="48" alt="Share with facebook"></a>
            	<a href="https://twitter.com/share?url=accountant-jobs" class="col-md-4"><img src="campus/images/icons/social-twitter.png" width="48"  alt="Share with twitter"></a>
                <a href="https://www.linkedin.com/cws/share?url=accountant-jobs" class="col-md-4"><img src="campus/images/icons/social-linkedin.png" width="48" alt="Share with linkedin"></a>           
            </div>
            <hr>
			           
            <a href="#" class="btn btn-primary show"> Get Similar Job Alert</a>	
                        <hr>
            <h3 class="font-s-20">Popular Searches</h3>

                                    <a href="associate-jobs" class="show d-gray"><span>&raquo; Associate Jobs </span></a>
            <a href="human-resources-jobs" class="show d-gray"><span>&raquo; Human Resources Jobs </span></a>
            <a href="consultant-jobs" class="show d-gray"><span>&raquo; Consultant Jobs </span></a>
            <a href="project-manager-jobs" class="show d-gray"><span>&raquo; Project Manager Jobs </span></a>
            <a href="analyst-jobs" class="show d-gray"><span>&raquo; Analyst Jobs </span></a>
            <a href="sales-and-marketing-jobs" class="show d-gray"><span>&raquo; Sales & Marketing Jobs </span></a>
            <a href="civil-engineer-jobs" class="show d-gray"><span>&raquo; Civil Engineer Jobs </span></a>
            <a href="electrician-jobs" class="show d-gray"><span>&raquo; Electrician Jobs </span></a>
            <a href="architect-jobs" class="show d-gray"><span>&raquo; Architect Jobs </span></a>
            <a href="associate-jobs" class="show d-gray"><span>&raquo; Associate Jobs </span></a>
            <a href="programmer-analyst-jobs" class="show d-gray"><span>&raquo; Programmer Analyst Jobs </span></a>
            <a href="consultant-jobs" class="show d-gray"><span>&raquo; Consultant Jobs </span></a>
            <a href="accountant-jobs" class="show d-gray"><span>&raquo; Accountant Jobs </span></a>
            <a href="financial-analyst" class="show d-gray"><span>&raquo; Financial Analyst Jobs </span></a>
            <a href="chief-financial-officer-jobs" class="show d-gray"><span>&raquo; Chief Financial Officer Jobs </span></a>
            <a href="mechanical-engineer-jobs" class="show d-gray"><span>&raquo; Mechanical Engineer Jobs </span></a>
            <a href="electrical-engineer-jobs" class="show d-gray"><span>&raquo; Electrical Engineer Jobs </span></a>
            <a href="chemical-engineer-jobs" class="show d-gray"><span>&raquo; Chemical Engineer Jobs </span></a>
            <a href="sales-executive-jobs" class="show d-gray"><span>&raquo; Sales Executive Jobs </span></a>
            <a href="account-manager-jobs" class="show d-gray"><span>&raquo; Account Manager Jobs </span></a>
        </div>
    </div>
    </div>
  </div>
</asp:Content>

