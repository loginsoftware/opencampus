﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.master" AutoEventWireup="true" CodeFile="advertise-with-us.aspx.cs" Inherits="Advertise_With_Us" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
    <style type="text/css">
	.center{
	position: relative;
    padding: 0;
text-align:center;
    margin-top: 10px;
    z-index: 0;
}
	
	
	.noo-step-icon1{
	
	  /* background-color: red;*/
    box-shadow: rgba(0,0,0,0.15) 1px 1px 5px;
    padding-top: 60px;
    padding-bottom: 30px;
	
	
	}
	    .center1
	    {
	        text-align: left;
	    }
	</style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="cph" Runat="Server">
      <div class="noo-page-heading">
				<div class="container-boxed max text-center parallax-content">
					<div class="member-heading-avatar">
						
					</div>
					<div class="page-heading-info ">
						<h1 class="page-title">Advertise With Us</h1>
					</div>
					<div class="page-sub-heading-info">
						We’re <span style="color:
					#80bb03;font-size:19px;">Helping People </span>Find Great Jobs,
and <span style="color:
					#80bb03;font-size:19px;">Helping Employers</span> Build Great Companies
					</div>
				</div> 
				<div class="parallax heading" data-parallax="1" data-parallax_no_mobile="1" data-velocity="0.1"></div>
			</div>
			<div class="member-heading">
				<div class="container-boxed max">
					<div class="member-heading-nav">
						<ul>
							<li class="active"><a href="advertise-with-us.aspx"><i class="fa fa-file-text-o"></i> Advertise With Us</a></li>
					<li><a href="aboutus.aspx"><i class="fa fa-newspaper-o"></i> About Us</a></li>
							<%--<li><a href="employer-manage-job.html"><i class="fa fa-file-text-o"></i> Hire Us</a></li>--%>
							<li><a href="contactus.aspx"><i class="fa fa-users"></i> Contact Us</a></li>
							<li><a href="privacy-policy.aspx"><i class="fa fa-sign-out"></i> Privacy Policy</a></li>
							<li><a href="terms-condition.aspx"><i class="fa fa-users"></i> Terms Condition</a></li>
							<li><a href="security-center.aspx"><i class="fa fa-sign-out"></i> Security Center</a></li>
						</ul>
					</div>
				</div>
			</div>
			<div class="container-wrap">
			
				<div class="main-content container-boxed max offset">
					
							<div class="row ">
								<div class="col-md-12">
								<div class="noo-step-icon1 clearfix">
									<div id="section" class="dark"><div class="container"><div class="row"><div class="col-md-10 col-md-offset-1"><!-- Start Feature Three -->
									
									<div class="row"><h3 class="center" style="color: black;
    text-align: center;
    font-size: 18px;
    font-weight: 100;
">
																Advertising on Opencampus helps businesses of <br>any size achieve their goals.
															</h3>
									
									
                                        <div class="col-md-3 col-sm-3 feature-three center"><div class="img-feature"><span class="count"></span><img src="images/adv_1.png"/></div><h5 style="color:black;font-size: 18px;
    font-weight: 100;">
															Unlock a unique audience
														</h5><p></p>
														
 <ul class="center1" >
<li>Reach more than 450M active professional members</li>
<li>Promote your brand in a professional context</li>
<li>
More
targeting options</li></ul></div>
									
														
														<div class="col-md-3 col-sm-3 feature-three center"><div class="img-feature"><span class="count"></span><img src="images/adv_2.png"/></div><h5 style="color:black;font-size: 18px;
    font-weight: 100;">
															 Target the right 
															 <br>people
														</h5><p></p>
														
<ul class="center1" >
<li>By job title, function and seniority</li>
<li>By industry and company size</li>
<li>
More
targeting options</li></ul></div>
														
														<div class="col-md-3 col-sm-3 feature-three center"><div class="img-feature"><span class="count"></span><img src="images/adv_3.png"/></div><h5 style="color:black;    font-size: 18px;
    font-weight: 100;">
														Create easy and <br>effective ads
														</h5><p></p>
														
														<ul style="" class="center1">
<li>Boost your content across all devices with&nbsp;</li>
<li>Deliver relevant, personalized messages with </li>
<li>Generate quality leads in minutes with&nbsp;</li></ul></div>
														
														<div class="col-md-3 col-sm-3 feature-three center"><div class="img-feature"><span class="count"></span><img src="images/adv_4.png"/></div><h5 style="color:black;    font-size: 18px;
    font-weight: 100;">
															 Drive real <br>business results
														</h5><p></p>
														
														<ul class="center1">
<li>Set your own budget</li>
<li>Pay by clicks or impressions</li>
<li>Stop your ads at any time</li>
<li>
from your ads</li></ul></div>
														
														</div><!-- End Feature Three --></div></div></div></div>
									</div>
								</div>
							</div>
				</div> 
			</div>
</asp:Content>

