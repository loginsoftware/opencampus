﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using BusinessLayer;

public partial class jobs_by_designation : Utility // System.Web.UI.Page
{
   
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!Page.IsPostBack)
        {
            string filtr = "";
            try
            {
                filtr = this.Page.RouteData.Values["filtr"].ToString();
            }
            catch { }
            LoadJobDesignation(filtr);
        }
    }

    private void LoadJobDesignation(string filterTxt)
    {
        FilterMasterBL objF = new FilterMasterBL();
        rptrJobsByDesignation.DataSource = objF.LoadFilterMasterByFilterType("DesignationJobTitle", filterTxt);
        rptrJobsByDesignation.DataBind();
    }
}