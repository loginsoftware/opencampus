﻿<%@ Page Title="" Language="C#" MasterPageFile="~/campus/College.master" AutoEventWireup="true" CodeFile="Copy of job-in-detail.aspx.cs" Inherits="campus_job_in_detail" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">


</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="cph" Runat="Server">
    <div class="container"> <br/>
  <div class="row row3 padd-all">
    <div class="col-md-12 advSearch f-white" style="margin:4% 0">
      <div class="col-md-2 jd-r text-center padding0">
            <a href="#">
      <img id="ImgCompanyLogo" runat="server" alt="" class="img-circle" src="" width="100" style="box-shadow: 1px 1px 20px 8px white" /></a>
      </div>
      <div class="col-md-7 jd-l">
        <h1 class="media-heading f-white">
        <asp:Literal ID="lblJobTitleHeader" runat="server"></asp:Literal></h1>
        <h2 class="f-white">
            <a href="#" class="f-white" target="_blank"> <asp:Literal ID="lblCompanyName" runat="server"></asp:Literal></a></h2>
        <span><i class="fa fa-suitcase"></i>  <asp:Literal ID="lblExperience" runat="server"></asp:Literal></span>
        <h3 class="f-white"><i class="fa fa-map-marker"></i> <asp:Literal ID="lblCompanyAddress" runat="server"></asp:Literal></h3>
        

      </div>
      <div class="col-md-3 text-center">

      	<br/>
          
          <%--<a href="javascript:;" id="LoginToApply" class="font-s-16 search-btn btn btn-default btn-yellow f-white" title="Login to Apply" style="padding:15px 45px;">Apply</a>--%>
            <asp:LinkButton ID="BtnLoginToApply" runat="server" CssClass="font-s-16 search-btn btn btn-default btn-yellow f-white" ToolTip="Click Here to Apply for this job" OnClick="BtnLoginToApply_Click">Apply</asp:LinkButton>

      </div>
      <div class="clearfix"></div>
    </div>
    
    <div class="col-md-9 padding0">
    	<div class="profile-body bg-white border-r box-shadow job-s padd-all">
            <div class="media job-summary bg-lgray padd-all text-center">
            	<h4 class="margin0">Get the best matching Jobs through email!</h4><br>
				<a href="javascript:;" data-toggle="modal" data-target="#myReg" class="btn btn-primary fadeInUpBig"><i class="fa fa-envelope"></i> Set Job Alert</a>
            </div>
        	<hr>
       		<div class="row">
            <div class="col-md-12">
                            <div class="row">
                <div class="col-md-3">
                  <p class="gray">Salary</p>
                </div>
                <div class="col-md-9">
                  <p class="black">
            <asp:Literal ID="lblSalary" runat="server"></asp:Literal></p>
                </div>
              </div>
                            <div class="row">
                <div class="col-md-3">
                  <p class="gray">Experience</p>
                </div>
                <div class="col-md-9">
                  <p class="black"><asp:Literal ID="lblExperience2" runat="server"></asp:Literal></p>
                </div>
              </div>
                            <div class="row">
                <div class="col-md-3">
                  <p class="gray">Industry</p>
                </div>
                <div class="col-md-9">
                  <p class="black"><asp:Literal ID="lblIndustryName" runat="server"></asp:Literal></p>
                </div>
              </div>
              <div class="row">
                <div class="col-md-3">
                  <p class="gray">Functional Area</p>
                </div>
                <div class="col-md-9">
                  <p class="black"><asp:Literal ID="lblFunctionalArea" runat="server"></asp:Literal></p>
                </div>
              </div>
              <div class="row">
                <div class="col-md-3">
                  <p class="gray">Key Skills</p>
                </div>
                <div class="col-md-9 product-row padding0" style="box-shadow:none;border:none">
                    <asp:Literal ID="lblKeywordSkill" runat="server"></asp:Literal>
                     

                </div>
              </div>
                            <div class="row">
                <div class="col-md-3">
                  <p class="gray">No of Positions</p>
                </div>
                <div class="col-md-9">
                  <p class="black">  <asp:Literal ID="lblNoOfVacancy" runat="server"></asp:Literal></p>
                </div>
              </div>
                 <div class="row">
                <div class="col-md-3">
                  <p class="gray">Job Locations</p>
                </div>
                <div class="col-md-9">
                  <p class="black">  <asp:Literal ID="lblJobLocations" runat="server"></asp:Literal></p>
                </div>
              </div>
                            <div class="row">
                <div class="col-md-3">
                  <p class="gray">Reference No./Job Code</p>
                </div>
                <div class="col-md-9">
                  <p class="black">  <asp:Literal ID="lblReferenceCode" runat="server"></asp:Literal></p>
                </div>
              </div>
                          </div>
            <div class="clearfix"></div>
          </div>
      		<br/>
            
             <asp:LinkButton ID="BtnLoginApply2" runat="server" CssClass="btn btn-end  animated fadeInUpBig margin0" ToolTip="Click Here to Apply for this job" OnClick="BtnLoginToApply_Click">Click here to Apply</asp:LinkButton>

           
            
            <br /><br />
              <h4 class="d-gray txt-t">Job Description</h4><br>
        	  <p>  <asp:Literal ID="lblJobDescription" runat="server"></asp:Literal></p>
              <input type="hidden" name="JobId" id="JobId" value="37072">
        </div><br>
                <div class="profile-body bg-white border-r box-shadow job-s padd-all">
            
			<h4 class="txt-t">Job Posted By</h4><br>
            <p class="black margin0">
                <a href="#" class="black" target="_blank">
                    <asp:Literal ID="lblPostedByCompanyName" runat="server"></asp:Literal></a></p>
			<p class="black margin0"><asp:Literal ID="lblPostedByCompanyAddress" runat="server"></asp:Literal><br></p>
			<p class="black margin0">
                        <asp:HyperLink ID="lblPostedByCompanyWebsiteUrl" runat="server" Target="_blank"></asp:HyperLink>
               
                </p>
                <p class="black margin0">
                          Posted On:   <asp:Literal ID="lblPostedOn" runat="server"></asp:Literal>
               
                </p>    
                    <br>
           
        </div>
    </div>
   
    <div class="pplsearch-header col-md-3">
    	<div class="profile-body bg-white border-r box-shadow job-s padd-all">
            <h4 class="txt-t text-center">Share This Job</h4>
            <div class="row padd10" style="margin-top:12px">  
            	<a id="lnkShareFB" runat="server" rel="nofollow" href="#" onclick="return fbs_click()" target="_blank" class="fb_share_link col-md-4">
                <img src='<%= Page.ResolveUrl("images/icons/social-facebook.png") %>' width="48"></a>
            	<a id="lnkShareTW" runat="server" href="#" class="col-md-4" onclick="window.open('https://twitter.com/share?url=' + encodeURIComponent(this.href), 'sharer', 'toolbar=0,status=0,width=626,height=436');return false;"><img src='<%= Page.ResolveUrl("images/icons/social-twitter.png") %>' width="48"></a>
                <a  id="lnkShareLN" runat="server" href="#"  onclick="window.open('https://www.linkedin.com/cws/share?url=' + encodeURIComponent(this.href), 'sharer', 'toolbar=0,status=0,width=626,height=436');return false;" class="col-md-4"><img src='<%= Page.ResolveUrl("images/icons/social-linkedin.png") %>' width="48"></a>           
            </div>
        </div>
        <div class="profile-body bg-white border-r box-shadow job-s padd-all">
            <h4 class="txt-t text-center">Similar Jobs</h4><br />
                        
                              <asp:Literal ID="LtrSimilarJob" runat="server"></asp:Literal>
            
           
              


        </div>
    </div>
    
  </div>
</div>
    <script>function fbs_click()
{
    u = location.href; t = document.title; window.open('http://www.facebook.com/sharer.php?u=' + encodeURIComponent(u) + '&t=' + encodeURIComponent(t), 'sharer', 'toolbar=0,status=0,width=626,height=436'); return false;
}</script>
    <asp:HiddenField ID="hfJobId" runat="server" />
</asp:Content>

