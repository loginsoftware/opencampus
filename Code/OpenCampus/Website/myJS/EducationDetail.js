﻿
function IsInteger(x) {
    var temp = parseInt(x);
    if (temp == x) {
        return true;
    }
    return false;
}

function populateSpec(CourseId, SpecId, CourseType) {
    if (CourseType == 'ug') {
        var arrSpec = arrUGSpec;
    }
    else if (CourseType == 'pg') {
        var arrSpec = arrPGSpec;
    }
    else if (CourseType == 'ppg') {
        var arrSpec = arrPPGSpec;
    }

    specID = CourseType + "specId";
    ospecID = "o" + specID;
    ospecID = ospecID.substring(0, ospecID.length - 2);
    spec = gbi(specID);
    ospec = gbi(ospecID);

    var courseEle = gbi(CourseId);
    var specEle = gbi(SpecId);

    // Remove existing elements from spec
    specEle.length = 1;
    //ospec will be displayed only if course selected is other
    //ospec.style.display = "none";
    //no new options to be made if "SELECT" in course is selected
    var zz = 0;
    if (courseEle.value != -1) {
        zz = 0;
        //alert(courseEle.value );
        if (courseEle.value != "9999") {
            CourseVal = courseEle.value;
            var opt = new Option();
            opt.text = "Select";
            opt.value = "-1";

            specEle[zz++] = opt;
            for (var j in arrSpec[CourseVal]) {
                if (arrSpec[CourseVal][j] != 'Other' && isInteger(j)) {
                    var opt = new Option();
                    // alert(CourseVal);
                    // alert(j);
                    //          alert(arrSpec[CourseVal][j]);
                    opt.text = arrSpec[CourseVal][j];
                    //        alert(opt.text);
                    opt.value = CourseVal + "." + j;

                    specEle[zz++] = opt;
                }
            }
        }
        var optOth = new Option();
        optOth.text = "Other";
        //alert(CourseVal);
        optOth.value = courseEle.value + ".9999";
        specEle[zz] = optOth;
        if (courseEle.value == "9999") {
            optOth.selected = true;
            ospec.style.display = "block";
        } /**/
    }
}

function showPGFields() {
    gbi("gradBdrIdPg").style.display = "block";
    gbi("pgButton").style.display = "none";
    gbi("pgCid").style.display = "block";
    gbi("pgYid").style.display = "block";
    gbi("pgRid").style.display = "block";
    gbi("pgSid").style.display = "block";
    gbi("pgUid").style.display = "block";
    gbi("ppgButton").style.display = "block";

}
function showPPGFields() {
    gbi("gradBdrId").style.display = "block";
    gbi("ppgButton").style.display = "none";
    gbi("ppgCid").style.display = "block";
    gbi("ppgYid").style.display = "block";
    gbi("ppgRid").style.display = "block";
    gbi("ppgSid").style.display = "block";
    gbi("ppgUid").style.display = "block";

}

function showCertificationBoxes() {
    gbi("certId1").style.display = "block";
    gbi("certId2").style.display = "block";
    gbi("certId3").style.display = "block";
    gbi("certiButton").style.display = "none";
}

function notPursuingGraduation() {

    /*    if(gbi("ugcourseId").value == 1) {//Not Pursuing graduation option
    gbi("pgButton").style.display="none";
    }
    */

    if (gbi("ugcourseId").value == 1) {//Not Pursuing graduation option
        //        gbi("notPursuingGraduation").style.display = "none";
        gbi("pgButton").style.display = "none";
        gbi("ougcourse").value = "";
        gbi("uginstituteId").value = -1;
        //gbi("ouginstitute").style.display = 'none';
        gbi("ouginstitute").value = "";
        gbi("ugspecId").value = -1;
        //gbi("ougspec").style.display = 'none';
        gbi("ougspec").value = "";
        gbi("ugyearOfCompletion").value = -1;
        /*
        gbi("pgcourseId").value = -1;
        gbi("opgcourse").value = "";
        gbi("ppgcourseId").value = -1;
        gbi("oppgcourse").value = "";
        gbi("pgyearOfCompletion").value = -1;
        gbi("ppgyearOfCompletion").value = -1;
        gbi("pginstituteId").value = -1;
        gbi("opginstitute").value = "";
        gbi("ppginstituteId").value = -1;
        gbi("oppginstitute").value = "";
        gbi("pgspecId").value = -1;
        gbi("opgspec").value = "";
        gbi("ppgspecId").value = -1;
        gbi("oppgspec").value = "";
        gbi("certId1").value = "";
        gbi("certId2").value = "";
        gbi("certId3").value = "";
        */
    } else {
        gbi("notPursuingGraduation").style.display = "block";
    }

}

function notPursuingGraduationForNewProfile() {

    if (gbi("ugcourseId").value == 1) {//Not Pursuing graduation option
        gbi("notPursuingGraduation").style.display = "none";
        if (gbi("notPursuingGraduationCourseType")) {
            gbi("notPursuingGraduationCourseType").style.display = "none";
        }
        gbi("ougcourse").value = "";
        if (gbi("pgcourseId")) {
            gbi("pgcourseId").value = -1;
            gbi("opgcourse").value = "";
        }
        if (gbi("ppgcourseId")) {
            gbi("ppgcourseId").value = -1;
            gbi("oppgcourse").value = "";
        }

        if (gbi("certId1")) {
            gbi("certId1").value = "";
            gbi("certId2").value = "";
            gbi("certId3").value = "";
        }


    } else {
        gbi("notPursuingGraduation").style.display = "";
        if (gbi("notPursuingGraduationCourseType")) {
            gbi("notPursuingGraduationCourseType").style.display = "";
        }
    }
}

function showHideOtherBox(fieldName, otherFieldName, fieldToFocus) {
    var field = gbi(fieldName).value;
    if (field.indexOf(".") != -1) {
        field = field.split(".")[1];
    }
    if (field == 9999) {
        gbi(otherFieldName).style.display = "";
        if (fieldToFocus) {
            gbi(fieldToFocus).focus();
        }
    } else {
        //   gbi(otherFieldName).style.display = "none";
        gbi(otherFieldName).style.display = "none";
    }
}
function shouwHideCertificateBoxes(countCertificates) {
    //   alert(countCertificates);
    if (countCertificates == 0) {
        gbi("addCertiButton").style.display = "block";
        gbi("certId1").style.display = "none";
        gbi("certId2").style.display = "none";
        gbi("certId3").style.display = "none";
    } else {
        gbi("addCertiButton").style.display = "none";
        gbi("certId1").style.display = "block";
        gbi("certId2").style.display = "block";
        gbi("certId3").style.display = "block";
    }
}

function shouwHidePgBoxes(maxEducationTypeId) {
    if (maxEducationTypeId == 1) {
        gbi("pgButton").style.display = "block";
    } else {
        gbi("pgButton").style.display = "none";
    }

    if (maxEducationTypeId >= 2) {
        gbi("gradBdrIdPg").style.display = "block";
        if (gbi("pgDeleteLink")) {
            gbi("pgDeleteLink").style.display = "block";
        }
        gbi("pgCid").style.display = "block";
        gbi("pgYid").style.display = "block";
        gbi("pgRid").style.display = "block";
        gbi("pgSid").style.display = "block";
        gbi("pgUid").style.display = "block";
    } else {
        //gbi("pgDeleteLink").style.display="none";
        gbi("pgCid").style.display = "none";
        gbi("pgYid").style.display = "none";
        gbi("pgRid").style.display = "none";
        gbi("pgSid").style.display = "none";
        gbi("pgUid").style.display = "none";
    }
}
function shouwHidePpgBoxes(maxEducationTypeId) {
    if (maxEducationTypeId == 2) {
        gbi("ppgButton").style.display = "block";
    } else {
        gbi("ppgButton").style.display = "none";
    }

    if (maxEducationTypeId == 3) {
        gbi("gradBdrId").style.display = "block";
        gbi("ppgDeleteLink").style.display = "block";
        gbi("ppgCid").style.display = "block";
        gbi("ppgYid").style.display = "block";
        gbi("ppgRid").style.display = "block";
        gbi("ppgSid").style.display = "block";
        gbi("ppgUid").style.display = "block";
    } else {
        gbi("ppgDeleteLink").style.display = "none";
        gbi("ppgCid").style.display = "none";
        gbi("ppgYid").style.display = "none";
        gbi("ppgRid").style.display = "none";
        gbi("ppgSid").style.display = "none";
        gbi("ppgUid").style.display = "none";
    }
}
