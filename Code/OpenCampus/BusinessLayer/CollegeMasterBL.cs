﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data.Objects;
using DataLayer;

namespace BusinessLayer
{
    public class CollegeMasterBL
    {
        public int InsertCollegeMaster(string CollegeName, int CityId, string Address, string Pincode, string PhoneNumber, string CollegeWebsite, string UniversityName, string Affiliations, string AboutCollege, string DeanDirectorPrincipal, string AboutDean, string UploadDeanImagePath, string PlacementCoordinator, string PlacementPhoneNo, string PlacementEmailId, string UploadLogoImagePath, string UploadPictureImagePath, string UploadPresentationPath, string UploadPlacementStatisticsPath, string VideoUrl, string Latitute, string Longitude, DateTime CreatedOn, DateTime UpdatedOn, int LoginId)
        {
            ObjectParameter CollegeId = new ObjectParameter("CollegeId", typeof(Int32));
            DBEntities Obj = new DBEntities();

            Obj.InsertCollegeMaster(CollegeId, CollegeName, CityId, Address, Pincode, PhoneNumber, CollegeWebsite, UniversityName, Affiliations, AboutCollege, DeanDirectorPrincipal, AboutDean, UploadDeanImagePath, PlacementCoordinator, PlacementPhoneNo, PlacementEmailId, UploadLogoImagePath, UploadPictureImagePath, UploadPresentationPath, UploadPlacementStatisticsPath, VideoUrl, Latitute, Longitude, CreatedOn, UpdatedOn, LoginId);
            return int.Parse(CollegeId.Value.ToString());
        }

        public int UpdateCollegeMaster(int CollegeId, string CollegeName, int CityId, string Address, string Pincode, string PhoneNumber, string CollegeWebsite, string UniversityName, string Affiliations, string AboutCollege, string DeanDirectorPrincipal, string AboutDean, string UploadDeanImagePath, string PlacementCoordinator, string PlacementPhoneNo, string PlacementEmailId, string UploadLogoImagePath, string UploadPictureImagePath, string UploadPresentationPath, string UploadPlacementStatisticsPath, string VideoUrl, string Latitute, string Longitude, DateTime CreatedOn, DateTime UpdatedOn, int LoginId)
        {
            DBEntities Obj = new DBEntities();
            Obj.UpdateCollegeMaster(CollegeId, CollegeName, CityId, Address, Pincode, PhoneNumber, CollegeWebsite, UniversityName, Affiliations, AboutCollege, DeanDirectorPrincipal, AboutDean, UploadDeanImagePath, PlacementCoordinator, PlacementPhoneNo, PlacementEmailId, UploadLogoImagePath, UploadPictureImagePath, UploadPresentationPath, UploadPlacementStatisticsPath, VideoUrl, Latitute, Longitude, CreatedOn, UpdatedOn, LoginId);
            return CollegeId;
        }

        public int DeleteCollegeMaster(int CollegeId)
        {
            DBEntities Obj = new DBEntities();
            Obj.DeleteCollegeMaster(CollegeId);
            return CollegeId;
        }

        public List<VCollege> LoadCollegeMasterByPrimaryKey(int CollegeId)
        {
            DBEntities Obj = new DBEntities();
            return Obj.LoadCollegeMasterByPrimaryKey(CollegeId).ToList();
        }

        public List<CollegeMaster> LoadAllCollegeMaster()
        {
            DBEntities Obj = new DBEntities();
            return Obj.LoadAllCollegeMaster().ToList();
        }

        public int UpdateCollegeMasterProfile(int CollegeId, string CollegeName, string Address, string Pincode, string PhoneNumber, string CollegeWebsite, string UniversityName, string Affiliations, string AboutCollege, string DeanDirectorPrincipal, string AboutDean, string UploadDeanImagePath, string PlacementCoordinator, string PlacementPhoneNo, string PlacementEmailId, string UploadLogoImagePath, string UploadPictureImagePath, string UploadPresentationPath, string UploadPlacementStatisticsPath, string VideoUrl, string Latitute, string Longitude, DateTime UpdatedOn)
        {
            DBEntities Obj = new DBEntities();
            Obj.UpdateCollegeMasterProfile(CollegeId, CollegeName, Address, Pincode, PhoneNumber, CollegeWebsite, UniversityName, Affiliations, AboutCollege, DeanDirectorPrincipal, AboutDean, UploadDeanImagePath, PlacementCoordinator, PlacementPhoneNo, PlacementEmailId, UploadLogoImagePath, UploadPictureImagePath, UploadPresentationPath, UploadPlacementStatisticsPath, VideoUrl, Latitute, Longitude, UpdatedOn);
            return CollegeId;
        }


        public List<VCollege> LoadCollegeMasterByLoginId(int LoginId)
        {
            DBEntities Obj = new DBEntities();
            return Obj.LoadCollegeMasterByLoginId(LoginId).ToList();
        }

        public List<VCollege> LoadCollegeMasterTop(int CollegeId)
        {
            DBEntities Obj = new DBEntities();
            return Obj.LoadCollegeMasterTop(CollegeId).ToList();
        }

        public List<VCollege> LoadCollegeMasterBetweenDates(DateTime FromDate, DateTime ToDate, string CollegeName)
        {
            DBEntities Obj = new DBEntities();
            return Obj.LoadCollegeMasterBetweenDates(FromDate, ToDate, CollegeName).ToList();
        }
    }
}
