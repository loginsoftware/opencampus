﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data.Objects;
using DataLayer;

namespace BusinessLayer
{
    public class JobPostCompanyBL
    {
        public int InsertJobPostCompany(string CompanyName, string CompanyAddress, string CompanyLogo, string CompanyWebsite, DateTime CreatedOn, string CompanyAboutUs, string CompanyPhoneNumber, string CompanyFaxNumber, string CompanyAdminEmail, string CompanyPasswordhash, string CompanyPasswordSalt, string Latitude, string Longitutde, string Image1, string Image2, string Image3, string CompanyFacebookPageUrl)
        {
            ObjectParameter JobPostCompanyId = new ObjectParameter("JobPostCompanyId", typeof(Int32));
            DBEntities Obj = new DBEntities();

            Obj.InsertJobPostCompany(JobPostCompanyId, CompanyName, CompanyAddress, CompanyLogo, CompanyWebsite, CreatedOn, CompanyAboutUs, CompanyPhoneNumber, CompanyFaxNumber, CompanyAdminEmail, CompanyPasswordhash, CompanyPasswordSalt, Latitude, Longitutde, Image1, Image2, Image3, CompanyFacebookPageUrl);
            return int.Parse(JobPostCompanyId.Value.ToString());
        }

        public int UpdateJobPostCompany(int JobPostCompanyId, string CompanyName, string CompanyAddress, string CompanyLogo, string CompanyWebsite, DateTime CreatedOn, string CompanyAboutUs, string CompanyPhoneNumber, string CompanyFaxNumber, string CompanyAdminEmail, string CompanyPasswordhash, string CompanyPasswordSalt, string Latitude, string Longitutde, string Image1, string Image2, string Image3, string CompanyFacebookPageUrl)
        {
            DBEntities Obj = new DBEntities();
            Obj.UpdateJobPostCompany(JobPostCompanyId, CompanyName, CompanyAddress, CompanyLogo, CompanyWebsite, CreatedOn, CompanyAboutUs, CompanyPhoneNumber, CompanyFaxNumber, CompanyAdminEmail, CompanyPasswordhash, CompanyPasswordSalt, Latitude, Longitutde, Image1, Image2, Image3, CompanyFacebookPageUrl);
            return JobPostCompanyId;
        }

        public int DeleteJobPostCompany(int JobPostCompanyId)
        {
            DBEntities Obj = new DBEntities();
            Obj.DeleteJobPostCompany(JobPostCompanyId);
            return JobPostCompanyId;
        }

        public List<JobPostCompany> LoadJobPostCompanyByPrimaryKey(int JobPostCompanyId)
        {
            DBEntities Obj = new DBEntities();
            return Obj.LoadJobPostCompanyByPrimaryKey(JobPostCompanyId).ToList();
        }

        public List<JobPostCompany> LoadAllJobPostCompany()
        {
            DBEntities Obj = new DBEntities();
            return Obj.LoadAllJobPostCompany().ToList();
        }

        public List<JobPostCompany> LoadJobPostCompanyByComapnyName(string CompanyName)
        {
            DBEntities Obj = new DBEntities();
            return Obj.LoadJobPostCompanyByComapnyName(CompanyName).ToList();
        }
    }
}
