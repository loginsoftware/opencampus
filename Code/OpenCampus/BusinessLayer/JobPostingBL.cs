﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data.Objects;
using DataLayer;

namespace BusinessLayer
{
    public class JobPostingBL
    {
        public int InsertJobPosting(int EmployeeId, string JobTitleDesignation, string JobDescription, string Keywords, string WorkExperience, string AnnualCTC, string OtherSalaryDetail, string NoOfVacancy, string JobLocation, string Industry, string FunctionalArea, bool JobResponseOnEmail, bool IsWalkin, string ResponseEmailId, string WalkinDate, string ReferenceCode, string JobImagePath, bool IsFeatured, DateTime CreatedOn, bool IsPublished, string LinkToJob, DateTime JobExpiryDate, Nullable<int> JobPostCompanyId, string JobDesignation)
        {
            ObjectParameter JobId = new ObjectParameter("JobId", typeof(Int32));
            DBEntities Obj = new DBEntities();

            Obj.InsertJobPosting(JobId, EmployeeId, JobTitleDesignation, JobDescription, Keywords, WorkExperience, AnnualCTC, OtherSalaryDetail, NoOfVacancy, JobLocation, Industry, FunctionalArea, JobResponseOnEmail, IsWalkin, ResponseEmailId, WalkinDate, ReferenceCode, JobImagePath, IsFeatured, CreatedOn, IsPublished, LinkToJob, JobExpiryDate, JobPostCompanyId, JobDesignation);
            return int.Parse(JobId.Value.ToString());
        }

        public int UpdateJobPosting(int JobId, int EmployeeId, string JobTitleDesignation, string JobDescription, string Keywords, string WorkExperience, string AnnualCTC, string OtherSalaryDetail, string NoOfVacancy, string JobLocation, string Industry, string FunctionalArea, bool JobResponseOnEmail, bool IsWalkin, string ResponseEmailId, string WalkinDate, string ReferenceCode, string JobImagePath, bool IsFeatured, DateTime CreatedOn, bool IsPublished, string LinkToJob, DateTime JobExpiryDate, Nullable<int> JobPostCompanyId, string JobDesignation)
        {
            DBEntities Obj = new DBEntities();
            Obj.UpdateJobPosting(JobId, EmployeeId, JobTitleDesignation, JobDescription, Keywords, WorkExperience, AnnualCTC, OtherSalaryDetail, NoOfVacancy, JobLocation, Industry, FunctionalArea, JobResponseOnEmail, IsWalkin, ResponseEmailId, WalkinDate, ReferenceCode, JobImagePath, IsFeatured, CreatedOn, IsPublished, LinkToJob, JobExpiryDate, JobPostCompanyId,JobDesignation);
            return JobId;
        }

        public int DeleteJobPosting(int JobId)
        {
            DBEntities Obj = new DBEntities();
            Obj.DeleteJobPosting(JobId);
            return JobId;
        }

        public List<JobPosting> LoadJobPostingByPrimaryKey(int JobId)
        {
            DBEntities Obj = new DBEntities();
            return Obj.LoadJobPostingByPrimaryKey(JobId).ToList();
        }

        public List<JobPosting> LoadAllJobPosting()
        {
            DBEntities Obj = new DBEntities();
            return Obj.LoadAllJobPosting().ToList();
        }

        public List<JobPostingByEmployeeId_Result> LoadJobPostingByEmployeeId(int EmployeeId)
        {
            DBEntities Obj = new DBEntities();
            return Obj.LoadJobPostingByEmployeeId(EmployeeId).ToList();
        }

        public List<JobPostingJobDetailByJobId_Result> LoadJobPostingJobDetailByJobId(int JobId)
        {
            DBEntities Obj = new DBEntities();
            return Obj.LoadJobPostingJobDetailByJobId(JobId).ToList();
        }

        public List<JobPosting> LoadJobPostingSimilarJob(int Industry, int FunctionalArea, DateTime TodayDate, int Count)
        {
            DBEntities Obj = new DBEntities();
            return Obj.LoadJobPostingSimilarJob(Industry, FunctionalArea, TodayDate, Count).ToList();
        }

        public List<VJobListHomePage> LoadJobPostingJobForHomePage()
        {
            DBEntities Obj = new DBEntities();
            return Obj.LoadJobPostingJobForHomePage().ToList();
        }

        public int UpdateJobPostingUnPublish(int JobId, bool IsPublished)
        {
            DBEntities Obj = new DBEntities();
            Obj.UpdateJobPostingUnPublish(JobId, IsPublished);
            return JobId;
        }

        public List<VJobListing> LoadJobPostingByJobLocation(out int recordCount, int PageIndex, int PageSize, string JobLocation)
        {
            DBEntities Obj = new DBEntities();
            ObjectParameter RecordCount = new ObjectParameter("RecordCount", typeof(Int32));
            List<VJobListing> lst =Obj.LoadJobPostingByJobLocation(PageIndex, PageSize, RecordCount, JobLocation).ToList();
            recordCount = int.Parse(RecordCount.Value.ToString());
            return lst;
        }
        public List<VJobListing> LoadJobPostingByJobDesignationAndSkill(out int recordCount, int PageIndex, int PageSize, string DesignationAndSkill)
        {
            DBEntities Obj = new DBEntities();
            ObjectParameter RecordCount = new ObjectParameter("RecordCount", typeof(Int32));
            List<VJobListing> lst = Obj.LoadJobPostingByJobDesignationAndSkill(PageIndex, PageSize, RecordCount, DesignationAndSkill).ToList();
            recordCount = int.Parse(RecordCount.Value.ToString());
            return lst;
        }

        public List<JobPosting> LoadJobPostingRecentJobs(int Count)
        {
            DBEntities Obj = new DBEntities();
            return Obj.LoadJobPostingRecentJobs(Count).ToList();
        }

        public List<VJobListing> LoadAllJobPostingJobsByPaging(out int recordCount, int PageIndex, int PageSize)
        {
            DBEntities Obj = new DBEntities();
            ObjectParameter RecordCount = new ObjectParameter("RecordCount", typeof(Int32));
            List<VJobListing> lst = Obj.LoadAllJobPostingJobsByPaging(PageIndex, PageSize, RecordCount).ToList();
            recordCount = int.Parse(RecordCount.Value.ToString());
            return lst;
        }

        public List<JobPostingCityList_Result> LoadAllJobPostingCityList(string StartText)
        {
            DBEntities Obj = new DBEntities();
            return Obj.LoadAllJobPostingCityList(StartText).ToList();
        }
    }
}
