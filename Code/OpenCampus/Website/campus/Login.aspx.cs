﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using BusinessLayer;
using System.Threading;
using System.IO;

public partial class campus_Login : Utility // System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!Page.IsPostBack)
        {
            
            if (Request.QueryString["action"] != null as string)
            {
                string _action = Request.QueryString["action"].ToString();
                if (_action == "login")
                {
                    TxtUserNameEmail.Text = Request.QueryString["user"].ToString();
                    TxtPassword.Text = DecryptString(Request.QueryString["pwd"].ToString());
                    BtnLogin_Click(null, EventArgs.Empty);
                }
                else if (_action == "forgotpassword")
                {
                    loginform.Visible = false;
                    forgotform.Visible = true;
                }
            }
        }
    }
    protected void BtnLogin_Click(object sender, EventArgs e)
    {
        LoginDetailBL objL = new LoginDetailBL();
        List<DataLayer.LoginDetail> L = objL.LoadLoginDetailEmpByUserName(TxtUserNameEmail.Text);
        if (L.Count > 0)
        {
            if (L[0].Password == TxtPassword.Text)
            {
                if (L[0].UserTypeId == 1) // 1 for candidate
                {
                    CandidateMasterBL objC = new CandidateMasterBL();
                    List<DataLayer.CandidateMaster> C = objC.LoadCandidateMasterByLoginId(L[0].LoginId);
                    SetCookie(this.Page, "CandidateId", C[0].CandidateId.ToString());
                    if (Request.QueryString["requestUrl"] != null as string)
                    {
                        Response.Redirect(Request.QueryString["requestUrl"].ToString(), false);
                    }
                    else
                    {
                        Response.Redirect("~/Candidate_profile_basic.aspx?type=profilebasic", false);
                    }
                }
                else if(L[0].UserTypeId == 2) // 2 for employee
                {
                    EmployeeMasterBL objE = new EmployeeMasterBL();
                    List<DataLayer.VEmployeeMaster> E = objE.LoadEmployeeMasterByLoginId(L[0].LoginId);
                    SetCookie(this.Page, "LoginId", L[0].LoginId.ToString());
                    SetCookie(this.Page, "EmployeeId", E[0].EmployeeId.ToString());
                    Response.Redirect("~/Employee_Home.aspx", false);
                }
                else if (L[0].UserTypeId == 3)
                {
                    CollegeMasterBL objCL = new CollegeMasterBL();
                    List<DataLayer.VCollege> CL = objCL.LoadCollegeMasterByLoginId(L[0].LoginId);
                    SetCookie(this.Page, "LoginId", L[0].LoginId.ToString());
                    SetCookie(this.Page, "CollegeId", CL[0].CollegeId.ToString());
                    Response.Redirect("~/campus/CollegeProfile.aspx", false);
                }
            }
            else
            {
                ShowMessage("Invalid UserName or Password", this.Page);
            }
        }
        else
        {
            ShowMessage("Invalid UserName or Password", this.Page);
        }
    }
    protected void BtnSendOtp_Click(object sender, EventArgs e)
    {
         LoginDetailBL objL = new LoginDetailBL();
        List<DataLayer.LoginDetail> L = objL.LoadLoginDetailEmpByUserName(TxtEmailForgotPassword.Text);
        if (L.Count > 0)
        {
            StreamReader reader = new StreamReader(Server.MapPath("~/EmailTemp/OTP.html"));
            string readFile = reader.ReadToEnd();
            string myString = readFile;
            myString = myString.Replace("$$Password$$", L[0].Password);
            myString = myString.Replace("$$UserName$$", L[0].UserNameEmail);
            Thread email = new Thread(delegate()
            {
                SendMail(L[0].UserNameEmail, "Opencampus Password", myString);

            });

            email.IsBackground = true;
            email.Start();

            forgotform.Visible = false;
            loginform.Visible = true;

            ShowMessage("Your Password Has been Sent to your Email Id", this.Page);
        }
        else
        {
            ShowMessage("Invalid Email or UserName", this.Page);
        }
    }
}