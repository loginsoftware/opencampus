function showAlert(message, type, closeDelay) {
    if ($("#alerts-container").length == 0) {
        $("body").append($('<div id="alerts-container" style="position: fixed;width: 50%; left: 25%; top:1.5%">'));
    }
    type = type || "info";
    var alert = $('<div class="alert alert-' + type + ' fade in">').append($('<button type="button" class="close" data-dismiss="alert">').append("&times;")).append(message);
    $("#alerts-container").prepend(alert);
    if (closeDelay)
        window.setTimeout(function() {
            alert.alert("close")
        }, closeDelay);
}

function urlify(text) {
    var urlRegex = /(https?:\/\/[^\s]+)/g;
    return text.replace(urlRegex, function(url) {
        return '<a href="' + url + '" target="_blank">' + url + '</a>';
    })
}
//jQuery(function($) {
//    var preloader = $('.preloader');
//    $(window).load(function() {

//       // var startNotifications = document.getElementById('notification-counter1');
//		
//       // if (typeof(startNotifications) != 'undefined' && startNotifications != null) {
//			
//            if (typeof(Worker) !== "undefined") 
//			{
//				if (typeof(w) == "undefined") {
//                    w = new Worker("notifications.js");
//                }
//                w.onmessage = function(event) {
//                    if (event.data != 0) {
//                        var notificationDetails = (event.data).split('[]');
//                        var totalNotifications = notificationDetails[0];
//                        var notificationList = notificationDetails[1];
//                        var element = document.getElementById('notification-counter');
//                        if (typeof(element) != 'undefined' && element != null) {
//                            document.getElementById("notification-counter").style.display = 'block';
//                            document.getElementById("notification-counter").innerHTML = totalNotifications;
//                            document.getElementById("jq-dropdown-suggest").innerHTML = notificationList;
//                        }
//                    } else {
//                        var element = document.getElementById('notification-counter');
//                        if (typeof(element) != 'undefined' && element != null) {
//                            document.getElementById("notification-counter").style.display = 'none';
//                            document.getElementById("jq-dropdown-suggest").innerHTML = '';
//                        }
//                    }
//                };
//            } else {
//				
//                var element = document.getElementById('jq-dropdown-suggest');
//                if (typeof(element) != 'undefined' && element != null) {
//                    document.getElementById("jq-dropdown-suggest").innerHTML = "Sorry, your browser does not support Web Workers...";
//                }
//            }
//        //}
//    });
//    $('ul.navbar-nav > li').bind('mouseover', openSubMenu);
//    $('ul.navbar-nav > li').bind('mouseout', closeSubMenu);

    function openSubMenu() {
        if ($(this).hasClass('join')) {} else {
            $(this).addClass('activeMenu');
        }
        $(this).find('ul').show();
    };

    function closeSubMenu() {
        if ($(this).hasClass('activeMenu')) {
            $(this).removeClass('activeMenu');
        }
        $(this).find('ul').hide();
    };
