﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data.Objects;
using DataLayer;

namespace BusinessLayer
{
    public class EmployeeMasterBL
    {
        public int InsertEmployeeMaster(string CompanyName, string CompanyType, string IndustryType, string ContactPersone, string Address1, string Address2, int CityId, string ZipCode, string PhoneNo, string MobileNo, string ImagePath, string CurrentEmployer, string CurrentDesignation, bool IsActive, DateTime UpdatedOn, int LoginId, string WebsiteUrl, string FacebookUrl, string LinkedInUrl, string GooglePlusUrl, string SkillRolesHireFor, string JobProfile, string JoinedIn)
        {
             ObjectParameter EmployeeId = new ObjectParameter("EmployeeId", typeof(Int32));
            DBEntities Obj = new DBEntities();

            Obj.InsertEmployeeMaster(EmployeeId, CompanyName, CompanyType, IndustryType, ContactPersone, Address1, Address2, CityId, ZipCode, PhoneNo, MobileNo, ImagePath, CurrentEmployer, CurrentDesignation, IsActive, UpdatedOn, LoginId, WebsiteUrl, FacebookUrl, LinkedInUrl, GooglePlusUrl, SkillRolesHireFor, JobProfile, JoinedIn);
            return int.Parse(EmployeeId.Value.ToString());
        }

        public int UpdateEmployeeMaster(int EmployeeId, string CompanyName, string CompanyType, string IndustryType, string ContactPersone, string Address1, string Address2, int CityId, string ZipCode, string PhoneNo, string MobileNo, string ImagePath, string CurrentEmployer, string CurrentDesignation, bool IsActive, DateTime UpdatedOn, int LoginId, string WebsiteUrl, string FacebookUrl, string LinkedInUrl, string GooglePlusUrl, string SkillRolesHireFor, string JobProfile, string JoinedIn)
        {
            DBEntities Obj = new DBEntities();
            Obj.UpdateEmployeeMaster(EmployeeId, CompanyName, CompanyType, IndustryType, ContactPersone, Address1, Address2, CityId, ZipCode, PhoneNo, MobileNo, ImagePath, CurrentEmployer, CurrentDesignation, IsActive, UpdatedOn, LoginId, WebsiteUrl, FacebookUrl, LinkedInUrl, GooglePlusUrl, SkillRolesHireFor, JobProfile, JoinedIn);
            return EmployeeId;
        }

        public int DeleteEmployeeMaster(int EmployeeId)
        {
            DBEntities Obj = new DBEntities();
            Obj.DeleteEmployeeMaster(EmployeeId);
            return EmployeeId;
        }

        public List<EmployeeMaster> LoadEmployeeMasterByPrimaryKey(int EmployeeId)
        {
            DBEntities Obj = new DBEntities();
            return Obj.LoadEmployeeMasterByPrimaryKey(EmployeeId).ToList();
        }

        public List<EmployeeMaster> LoadAllEmployeeMaster()
        {
            DBEntities Obj = new DBEntities();
            return Obj.LoadAllEmployeeMaster().ToList();
        }

        public List<VEmployeeMaster> LoadEmployeeMasterByEmployeeId(int EmployeeId)
        {
            DBEntities Obj = new DBEntities();
            return Obj.LoadEmployeeMasterByEmployeeId(EmployeeId).ToList();
        }

        public List<VEmployeeMaster> LoadEmployeeMasterByLoginId(int LoginId)
        {
            DBEntities Obj = new DBEntities();
            return Obj.LoadEmployeeMasterByLoginId(LoginId).ToList();
        }

        public int UpdateEmployeeMasterSkillRolesIHireFor(int EmployeeId, string SkillRolesHireFor)
        {
            DBEntities Obj = new DBEntities();
            Obj.UpdateEmployeeMasterSkillRolesIHireFor(EmployeeId, SkillRolesHireFor);
            return EmployeeId;
        }

        public int UpdateEmployeeMasterCurrentEmpDetail(int EmployeeId, string CurrentEmployer, string CurrentDesignation, string JobProfile, string JoinedIn, string WebsiteUrl)
        {
            DBEntities Obj = new DBEntities();
            Obj.UpdateEmployeeMasterCurrentEmpDetail(EmployeeId, CurrentEmployer, CurrentDesignation, JobProfile, JoinedIn, WebsiteUrl);
            return EmployeeId;
        }
        public int UpdateEmployeeMasterPhoto(int EmployeeId, string ImagePath)
        {
            DBEntities Obj = new DBEntities();
            Obj.UpdateEmployeeMasterPhoto(EmployeeId, ImagePath);
            return EmployeeId;
        }

        public int UpdateEmployeeMasterContactDetail(int EmployeeId, string PhoneNo, string Mobile, string FacebookUrl, string LinkedInUrl, string GoogleUrl)
        {
            DBEntities Obj = new DBEntities();
            Obj.UpdateEmployeeMasterContactDetail(EmployeeId, PhoneNo, Mobile, DateTime.Now, FacebookUrl, LinkedInUrl, GoogleUrl);
            return EmployeeId;
        }

        public List<VEmployeeMaster> LoadEmployeeMasterTop(int EmployeeId)
        {
            DBEntities Obj = new DBEntities();
            return Obj.LoadEmployeeMasterTop(EmployeeId).ToList();
        }

        public List<VEmployeeMaster> LoadEmployeeMasterBetweenDates(DateTime FromDate, DateTime ToDate, string CompanyNameContactPerson)
        {
            DBEntities Obj = new DBEntities();
            return Obj.LoadEmployeeMasterBetweenDates(FromDate, ToDate, CompanyNameContactPerson).ToList();
        }
    }
}
