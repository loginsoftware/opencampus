﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using BusinessLayer;

public partial class Job_Published : Utility // System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!Page.IsPostBack)
        {
            
                FillDataGrid();
           
        }
    }

    private void FillDataGrid()
    {
        JobPostingBL obj = new JobPostingBL();
        if (GetCookie(this.Page, "EmployeeId") != null)
        {
            GVDetail.DataSource = obj.LoadJobPostingByEmployeeId(int.Parse(GetCookie(this.Page, "EmployeeId")));
            GVDetail.DataBind();
        }
    }

    protected void lnkbtnUnPublish_Click(object sender, EventArgs e)
    {
        LinkButton LnkBtn = (LinkButton)sender;
        GridViewRow gvRow = (GridViewRow)LnkBtn.NamingContainer;
        int rowIndex = gvRow.RowIndex;
        int Pkey = int.Parse(GVDetail.DataKeys[rowIndex].Value.ToString());
        JobPostingBL obj = new JobPostingBL();
        obj.UpdateJobPostingUnPublish(Pkey, false);
        ShowMessage("Job UnPublished Successfully", this.Page);
        FillDataGrid();
    }
    protected void lnkbtnPublish_Click(object sender, EventArgs e)
    {
        LinkButton LnkBtn = (LinkButton)sender;
        GridViewRow gvRow = (GridViewRow)LnkBtn.NamingContainer;
        int rowIndex = gvRow.RowIndex;
        int Pkey = int.Parse(GVDetail.DataKeys[rowIndex].Value.ToString());
        JobPostingBL obj = new JobPostingBL();
        obj.UpdateJobPostingUnPublish(Pkey, true);
        ShowMessage("Job Published Successfully", this.Page);
        FillDataGrid();
    }

    protected void GVDetail_PageIndexChanging(object sender, GridViewPageEventArgs e)
    {
        GVDetail.PageIndex = e.NewPageIndex;
        FillDataGrid();
    }
}