﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class ContactUs : Utility // System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!Page.IsPostBack)
        {

        }
    }
    protected void BtnSaveContact_Click(object sender, EventArgs e)
    {
        StreamReader reader = new StreamReader(Server.MapPath("~/EmailTemp/ContactUs.html"));
        string readFile = reader.ReadToEnd();
        string myString = readFile;
        myString = myString.Replace("$$Name$$", TxtFullName.Text);
        myString = myString.Replace("$$Email$$", TxtEmail.Text);
        myString = myString.Replace("$$Mobile$$",TxtMobileNo.Text);
        myString = myString.Replace("$$Message$$", TxtMessage.Text);
        Thread email = new Thread(delegate()
        {
            SendMail("info@opencampus.in", "Opencampus ContactUs Query", myString);

        });

        email.IsBackground = true;
        email.Start();

        ShowMessage("Your Message sent, we will get touch with you shortly.", this.Page);
    }
}