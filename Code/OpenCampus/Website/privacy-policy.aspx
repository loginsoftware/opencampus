﻿<%@ Page Title="Privacy Policy | Opencampus" Language="C#" MasterPageFile="~/Site.master" AutoEventWireup="true" CodeFile="privacy-policy.aspx.cs" Inherits="Privacy_Policy" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
    <style>
	
	.heading
	{    font-size: 18px;
	}
	.small-heading{
	font-size: 15px;
    font-weight: 100;
	}
	.para{
	font-size: 15px;
	}
	</style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="cph" Runat="Server">
    <div class="noo-page-heading">
				<div class="container-boxed max text-center parallax-content">
					<div class="member-heading-avatar">
						
					</div>
					<div class="page-heading-info ">
						<h1 class="page-title">Privacy Policy</h1>
					</div>
					<div class="page-sub-heading-info">
						We’re <span style="color:
					#80bb03;font-size:19px;">Helping People </span>Find Great Jobs,
and <span style="color:
					#80bb03;font-size:19px;">Helping Employers</span> Build Great Companies
					</div>
				</div> 
				<div class="parallax heading" data-parallax="1" data-parallax_no_mobile="1" data-velocity="0.1"></div>
			</div>
    <div class="member-heading">
				<div class="container-boxed max">
					<div class="member-heading-nav">
						<ul>
							<li><a href="advertise-with-us.aspx"><i class="fa fa-file-text-o"></i> Advertise With Us</a></li>
					<li><a href="aboutus.aspx"><i class="fa fa-newspaper-o"></i> About Us</a></li>
							<%--<li><a href="employer-manage-job.html"><i class="fa fa-file-text-o"></i> Hire Us</a></li>--%>
							<li><a href="contactus.aspx"><i class="fa fa-users"></i> Contact Us</a></li>
							<li class="active"><a href="privacy-policy.aspx"><i class="fa fa-sign-out"></i> Privacy Policy</a></li>
							<li><a href="terms-condition.aspx"><i class="fa fa-users"></i> Terms Condition</a></li>
							<li><a href="security-center.aspx"><i class="fa fa-sign-out"></i> Security Center</a></li>
						</ul>
					</div>
				</div>
			</div>
			<div class="container-wrap">
				<div class="main-content container-boxed max">
					<div class="row">
						<div class="noo-main col-md-12" style="margin-bottom:250px;">
							<div class="member-manage">
								<h3>Privacy Policy</h3>
							<h5>Open Campus's Privacy Commitment Overview - </h5>
							<p class="para">Open Campus is committed to protecting the privacy of our users, and strives to provide a safe, secure user experience. This Privacy Statement sets forth the online data collection and usage policies and practices that apply to this web site. </p>
                                <p class="para">
                                    The following statement explains Open Campus's commitment to managing your personal information and sets out how and when personal information is collected, held, used or disclosed. This Privacy Commitment only applies to data gathered on the opencampus.in web site (the Website), and does not apply to any other information or website.
                                </p>

							
							<p class="para">By registering or by using this site, you explicitly accept, without limitation or qualification, the collection, use and transfer of the personal information provided by you in the manner described in this Statement. Please read this Statement carefully as it affects your rights and liabilities under law. If you do not accept the Privacy Statement stated herein or disagree with the way we collect and process personal information collected on the Web site, please do not use it.
</p>

<h5>Posting Your Information to Public Areas of Open Campus - </h5>
							
							<p class="para">Please remember that if you post any of your personal information in public areas of Open Campus, such as in online forums, public profiles, chat rooms, or in the searchable resume database, such information may be collected and used by others over whom Open Campus has no control. We cannot control the use made by third parties of information you post or otherwise make available in public areas of Open Campus.
</p>
<h5>Updating Account Information -</h5>
							
							<p class="para">We wish to ensure your personal information is complete, accurate and up to date. Open Campus allows you to change or correct your personal information at any time. To do so, simply log into your My Open Campus account, and you will find options for editing the information you have submitted.
</p>
<h5>Advertising</h5>
							
							<p class="para">We use third-party advertising companies to serve ads when you visit our Web site. These companies may use information about you and your visits to this and other Web sites in order to provide advertisements on this site and other sites about goods and services of interest to you.
</p>
<h5>Access to Personal Information -</h5>
							
							<p class="para">You can gain access to the personal information you have provided by visiting our Web Site. If you need assistance to login you can contact us by calling or emailing info@Opencampus.in. If you have not accessed your resume for some time we may also contact you to check your information is still correct. A request for access to the personal information we hold about you, or in relation to an inquiry about privacy, should be sent to info@opencampusindia.in for the attention of the relevant officer.</p>
                                <p class="para">We retain indefinitely all the information we gather about you in an effort to make your repeat use of the Web Site more efficient, practical and relevant. You can delete your resume from Open Campus's online database, close your account at any time in which event we will remove all our copies of your resume and your account information from the Web Site, except for an archival copy which is not accessible on the internet.</p>
                                <p class="para">SSL makes it very difficult for your information to be stolen or intercepted while being transferred. However, we cannot guarantee encryption or the privacy of your personal details and credit card information. 
                                </p>
                                <p class="para">When we receive your order, it is kept encrypted until we are ready to process it. We will take reasonable steps to keep the information you provide us with secure.</p>

<h5>Open Campus Database - </h5>
							
							<p class="para">The information you provide is stored on the Open Campus database. To provide you with the highest quality service the Open Campus database is located in Singapore. </p>
                                <p class="para">When providing information you agree this may be transferred to an Open Campus database in another country.</p>

<h5>Feedback -  </h5>
							
							<p class="para">Open Campus has areas on the Web Site where you can submit feedback. Any feedback that is submitted through this area becomes the property of Open Campus. </p>
                                <p class="para">We may use this feedback at any time for any purposes, such as success stories for marketing purposes, or to contact you for further feedback on the Web Site.</p>

<h5>Legal and Contact Information - </h5>
							
							<p class="para">If, at any time, you have questions or concerns about this Privacy Statement or believe that we have not adhered to this Privacy Statement, please feel free to contact us online at info@opencampus.in or at - </p>
                                <h5>By post / mail - <br />
Maa Hospital Road,<br />
New Colony Mohangiri, <br />
Vidisha, 464001, M.P., India <br />
By Mobile No. -  +91 8871413396    </h5>
<p>We will use reasonable efforts to answer promptly your question or resolve your problem.</p>

<h5>Security -</h5>
							
					<p class="para">	You are responsible for ensuring that your login credentials (your username and password) are kept confidential.</p>

<p class="para">Open Campus has implemented technical and organizational measures designed to secure your personal information from accidental loss and from unauthorized access, use, alteration or disclosure. 

<p class="para">Notwithstanding such measures, the Internet is an open system and we cannot guarantee that unauthorized third parties will not be able to defeat those measures or use your personal information for improper purposes. </p>

<p class="para">Further, your resume should not contain any sensitive data that you would not want made public. You should be aware that resumes may be monitored by your current employer.</p>

<h5>Cookies - </h5>
						<p class="para">	Open Campus uses "cookies" to help personalize and maximize your online experience and time online, including for storing user preferences, improving search results and ad selection, and tracking user trends.</p>

<p class="para">A cookie is a text file that is either stored in your computer's memory temporarily (a "session" cookie) or placed on your hard drive (a "persistent" cookie) by a Web page server. Cookies are not used to run programs or deliver viruses to your computer. Cookies are uniquely assigned to your computer and can only be read by a web server in the domain that issued the cookie to you.</p>

<p class="para">When you return to the Open Campus site, cookies enable us to retrieve the information you previously provided, so you can easily use the features that you customized. Because of our use of cookies, we can deliver faster and more accurate results and a more personalized site experience. For example, if you personalize Open Campus pages, or register for services, a cookie helps us to recall your specific information (such as user name, password and preferences).</p>

  <p class="para">  We predominately use "session" cookies, which are not permanently stored on your hard drive. Session cookies expire after two hours of inactivity or when you close your browser. When you carry out a job search or apply online a large amount of data is processed by our system and transferred through each page of the search or application process. This information includes your job search criteria, data that identifies your resume, covering letter, the job you are applying for, your right to work in the relevant country and the results of any questionnaire you have completed as part of the online application process as well as demographic information that is not unique to you. "Session" cookies allow us to keep track of this data until you have completed your search or application. We also use cookies to identify users who have been banned from using our forums for behavior that contradicts our terms of use and to track click streams, for load balancing and to enable you to navigate through the site using redirection pages.</p>

<p class="para">We use "persistent" cookies primarily to ensure that we do not show you pages that you have already seen and to personalize your site experience and save you time. "Persistent" Cookies enable us to recognize whether you have responded to any poll or seen an advertisement or information notice placed on the site so that we do not show you the poll, advertisement or notice again the next time you visit our site. If you are a recruiter and have an alliance arrangement in place with Open Campus we also use "persistent" cookies to recall your alliance status and display your correct pricing information when you access our site. The "persistent" cookies we use will remain on your computer after you have left our site and will expire when you log out of the Site or 60 days after your last visit for security cookies, 90 days after your last visit for poll response and alliance cookies, or two years after your last visit for advertisement and information notice cookies. We also allow persistent third party cookies as part of online advertising campaigns to determine and track site traffic landing from Open Campus's advertising banners that have been placed on the websites of third parties. The data collected via these cookies is anonymous and the information is not linked to your personally identifiable information without your permission. The "persistent" cookies placed by our third party ad servers will remain on your computer after you have left our site for up to 20 years unless you choose to delete them.</p>

<p class="para">We also allow other companies to display advertisements to you while you are visiting our Sites. Because your web browser must request these advertisements from the other company servers, these companies can view, edit or set their own cookies, just as if you had requested a web page from their site. Open Campus has no control over, and is not responsible for, the practices of those third party advertisers. Open Campus encourages you to review the policies of such advertisers. Third parties, such as Google, Doubleclick, Right media and other advertiser platforms, may also use information that they gather from your activity on our Web Site to select which Open Campus advertisement should be displayed to you on web sites other than our Web Site.</p>

<p class="para">You can opt-out of customized advertising by third parties by setting your browser to decline third party cookies or some browsers allow you to block cookies from a particular third party whose customized advertising you do not wish to receive. You can also delete the advertiser's cookie each time after it has been served. In addition, third party advertisers, including advertising networks, may offer an opt-out of their customized advertising. If you visit an advertiser's own web site, you will be able to see their policies. If you wish, you may opt out of Google's use of customized advertising by visiting the Google advertising opt-out page, you can opt out of DoubleClick's use of customized advertising by visiting the DoubleClick opt-out page, and you can opt out of Right Media's use of customized advertising by visiting Right Media opt out page.</p>

<p class="para">In addition, many of these third parties are members of the Network Advertising Initiative, which requires its members to provide a permanent opt-out from their customized ads. You can opt-out of customized advertising by all NAI members by visiting the NAI site. However, since opting out is enforced by serving a blocking cookie, if you later delete all cookies, you will also delete your blocking cookie and will need to set it again. In addition, advertiser opt-out cookies may expire after a set period of time and will need to be re-set following their expiration.</p>

<p class="para">If you opt-out of customized advertising by third parties, you will still see ads on our Web Site and Open Campus ads elsewhere on websites other than our Web Site, however, such ads will be not be customized to your interests and may be less relevant to you.</p>

<p class="para">Please note that if you opt-out of cookies by changing your browser settings, your opt-out will be saved but it may not be carried with you if you choose to use another browser or log-in from another computer. You should check your browser settings each time you open a new browser or log-in from a different computer.</p>

<p class="para">This website uses Google Analytics, a web analytics service provided by Google, Inc. ("Google"). Google Analytics uses "cookies", which are text files placed on your computer, to help the website analyze how users use the site. The information generated by the cookie about your use of the website (including your IP address) will be transmitted to and stored by Google on servers in the United States. Google will use this information for the purpose of evaluating your use of the website, compiling reports on website activity for website operators and providing other services relating to website activity and internet usage. Google may also transfer this information to third parties where required to do so by law, or where such third parties process the information on Google's behalf. Google will not associate your IP address with any other data held by Google. You may refuse the use of cookies by selecting the appropriate settings on your browser, however please note that if you do this you may not be able to use the full functionality of this website. By using this website, you consent to the processing of data about you by Google in the manner and for the purposes set out above.</p>

  <h5>  Changes to Privacy Statement - </h5>

<p class="para">If we decide to materially change the substance of this Privacy Statement, we will, where required, contact you via the email address that you maintain in your profile. We will also post those changes through a prominent notice on the web site so that you will always know what information we gather, how we might use that information, and to whom we will disclose it.</p>

<p class="para">Thank you for using Open Campus. We are committed to providing you with the best tools and advice to help you get ahead in your career. We welcome your feedback and suggestions. Please contact us by calling or email info@opencampus.in </p>

							</div>
						</div>  
					</div> 
				</div> 
			</div>
</asp:Content>

