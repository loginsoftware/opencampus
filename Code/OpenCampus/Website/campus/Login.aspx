﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="Login.aspx.cs" Inherits="campus_Login" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>Login | OpenCampus</title>
    <link href="css/bootstrap.min.css" rel="stylesheet" property="stylesheet">
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.6.1/css/font-awesome.min.css">
<link href="css/opencampus.css" rel="stylesheet" property="stylesheet">
<link href='https://fonts.googleapis.com/css?family=Source+Sans+Pro:400,600,700,300,900' rel='stylesheet' type='text/css'>
<link href="css/redefined.css" rel="stylesheet" property="stylesheet">

<!--[if lt IE 9]>
    <script src="js/html5shiv.js"></script>
    <script src="js/respond.min.js"></script>
  <![endif]-->

<script type="text/javascript" src="js/jquery-1.11.1.min.js"></script>
<link rel="shortcut icon" href="images/favicon.ico"><link href="theme/css/font-awesome.css" rel="stylesheet">
<link href="css/bootstrap-social.css" rel="stylesheet" >
<style>
body {
	background:url('images/bg.jpg') no-repeat top fixed!important;
	background-size:cover !important;
}
.panel-heading {
    padding: 21px 15px;
    border-bottom: 1px solid transparent;
    border-top-left-radius: 3px;
    border-top-right-radius: 3px;
}
.panel-info>.panel-heading {
    color: #31708f;
    background-color: #f2f2f1;
    border-color: #f9f9f9;
}
.logo-img {
	text-align: center;
	margin: 2% 0px 0px;
}
.panel {
    margin-bottom: 20px;
    background-color: #fff;
    background-color: rgba(253, 253, 253, 0.9);
    border: 1px solid transparent;
    border-radius: 2px;
    -webkit-box-shadow: 0 1px 1px rgba(0,0,0,.05);
    box-shadow: 0 1px 1px rgba(0,0,0,.05);
}
</style>
</head>
<body>
    <form id="form1" runat="server">
    <div class="container-fluid links" style="background:#80bb03; height:3px;"></div>

    <div class="container">    
        <div id="loginbox" style="margin-top: 137px;" class="mainbox col-md-6 col-md-offset-3 col-sm-8 col-sm-offset-2">                    
            <div class="panel panel-info" >
                    <div class="panel-heading">
                        <div class="panel-title logo-img"><a href="../index.aspx"><img src="images/logo.png"></a> </div>
                   
                    </div>     

                    <div style="padding-top:30px" class="panel-body" >

                        <div style="display:none" id="login-alert" class="alert alert-danger col-sm-12"></div>
                            
                        <div runat="server" id="loginform" class="form-horizontal" role="form">
                                    
                            <div style="margin-bottom: 25px" class="input-group">
                                        <span class="input-group-addon"><i class="glyphicon glyphicon-user"></i></span>
                                   
                                <asp:TextBox ID="TxtUserNameEmail" CssClass="form-control" runat="server" placeholder="username or email"></asp:TextBox>                                    
                                    </div>
                                
                            <div style="margin-bottom: 25px" class="input-group">
                                        <span class="input-group-addon"><i class="glyphicon glyphicon-lock"></i></span>
                       <asp:TextBox ID="TxtPassword" TextMode="Password" CssClass="form-control" runat="server" placeholder="Password"></asp:TextBox>              
                                    </div>
                                    

                                
                                <div style="float:right; font-size: 80%; position: relative; top:-10px"><a href="Login.aspx?action=forgotpassword">Forgot password?</a></div>


                                <div style="margin-top:10px" class="form-group">
                                    <!-- Button -->

                                    <div class="col-sm-12 controls">
                                     <asp:LinkButton ID="BtnLogin" runat="server" CssClass="btn btn-success" OnClick="BtnLogin_Click"> Login </asp:LinkButton>

                                    </div>
                                </div>


                                <div class="form-group">
                                    <div class="col-md-12 control">
                                        <div style="border-top:1px solid#d2d2d2; padding-top:15px; font-size:85%" >
                                            Don't have an account! 
                                        <a href="Register.aspx" >
                                            Sign Up Here
                                        </a>
                                        </div>
                                    </div>
                                </div>    
                            </div>    
                        
                        
                        <div runat="server" id="forgotform" class="form-horizontal" role="form" visible="false">
                                    
                            <div style="margin-bottom: 25px" class="input-group">
                                        <span class="input-group-addon"><i class="glyphicon glyphicon-user"></i></span>
                                   
                                <asp:TextBox ID="TxtEmailForgotPassword" CssClass="form-control" runat="server" placeholder="Enter username or email"></asp:TextBox>                                    
                                    </div>
                                
                                         <div style="float:right; font-size: 80%; position: relative; top:-10px"><a href="Login.aspx">--- Back to Login</a></div>              
                                <div style="margin-top:10px" class="form-group">
                                    <!-- Button -->

                                    <div class="col-sm-12 controls">
                                     <asp:LinkButton ID="BtnSendOtp" runat="server" CssClass="btn btn-success" OnClick="BtnSendOtp_Click"> Send OTP </asp:LinkButton>

                                    </div>
                                </div>
                                                           
                            </div> 



                        </div>                     
                    </div>  
        </div>
        <asp:HiddenField ID="hfOTP" runat="server" />
        
    </form>
</body>
</html>
