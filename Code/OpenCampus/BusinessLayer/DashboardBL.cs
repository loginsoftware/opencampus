﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data.Objects;
using DataLayer;

namespace BusinessLayer
{
    public class DashboardBL
    {
        public List<DashboardTotalState_Result> LoadAllDashboardTotalState()
        {
            DBEntities Obj = new DBEntities();
            return Obj.LoadAllDashboardTotalState().ToList();
        }

        public List<DashboardChartState_Result> LoadDashboardChartState()
        {
            DBEntities Obj = new DBEntities();
            return Obj.LoadDashboardChartState().ToList();
        }

        public List<DashboardChartStateWeek_Result> LoadDashboardChartStateWeek(DateTime TodayDate)
        {
            DBEntities Obj = new DBEntities();
            return Obj.LoadDashboardChartStateWeek(TodayDate).ToList();
        }
    }
}
