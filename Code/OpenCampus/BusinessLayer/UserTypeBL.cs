﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data.Objects;
using DataLayer;

namespace BusinessLayer
{
    public class UserTypeBL
    {
        public int InsertUserType(int UserTypeId, string UserTypeName)
        {
           // ObjectParameter UserTypeId = new ObjectParameter("UserTypeId", typeof(Int32));
            DBEntities Obj = new DBEntities();

            Obj.InsertUserType(UserTypeId, UserTypeName);
            return UserTypeId;
        }

        public int UpdateUserType(int UserTypeId, string UserTypeName)
        {
            DBEntities Obj = new DBEntities();
            Obj.UpdateUserType(UserTypeId, UserTypeName);
            return UserTypeId;
        }

        public int DeleteUserType(int UserTypeId)
        {
            DBEntities Obj = new DBEntities();
            Obj.DeleteUserType(UserTypeId);
            return UserTypeId;
        }

        public List<UserType> LoadUserTypeByPrimaryKey(int UserTypeId)
        {
            DBEntities Obj = new DBEntities();
            return Obj.LoadUserTypeByPrimaryKey(UserTypeId).ToList();
        }

        public List<UserType> LoadAllUserType()
        {
            DBEntities Obj = new DBEntities();
            return Obj.LoadAllUserType().ToList();
        }
    }
}
