﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Employer.master" AutoEventWireup="true" CodeFile="Job_Published.aspx.cs" Inherits="Job_Published" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="cph" Runat="Server">
    <div class="container-wrap">
				<div class="main-content container-boxed max offset">
					<div class="row">
						<div class="noo-main col-md-12">
							<div class="member-manage">
								<h3>Published Jobs</h3>
								<em><strong>Note:</strong> Expired listings will be automatically removed after 30 days.</em>
								
									
									<div class="member-manage-table">
										<%--<table class="table">
											<thead>
												<tr>
													<th class="check-column">
														<div class="form-control-flat">
															<label class="checkbox">
																<input type="checkbox"><i></i>
															</label>
														</div>
													</th>
													<th>Title</th>
													<th class="hidden-xs">&nbsp;</th>
													<th class="hidden-xs hidden-sm">Location</th>
													<th class="hidden-xs">Closing</th>
													<th class="text-center">Apps</th>
													<th class="text-center hidden-xs">Status</th>
													<th class="text-center hidden-xs">Action</th>
												</tr>
											</thead>
											<tbody>
												<tr>
													<td class="check-column">
														<div class="form-control-flat">
															<label class="checkbox">
																<input type="checkbox" name="ids[]" value="">
																<i></i>
															</label>
														</div>
													</td>
													<td>
														<a href="#"><strong>Senior Designer</strong></a>
													</td>
													<td class="hidden-xs">
														<a href="#">
															<span class="noo-job-feature not-featured" data-toggle="tooltip"  title="Set Featured">
																<i class="fa fa-star-o"></i>
															</span>
														</a>
													</td>
													<td class="hidden-xs hidden-sm">
														<i class="fa fa-map-marker"></i>&nbsp;<em></em>
													</td>
													<td class="job-manage-expires hidden-xs">
														<span><i class="fa fa-calendar"></i>&nbsp;<em>Nov 19, 2015</em></span>
													</td>
													<td class="job-manage-app text-center">
														<span>1</span>
													</td>
													<td class="text-center">
														<span class="job-application-status job-application-status-publish">
															Active
														</span>
													</td>
													<td class="member-manage-actions hidden-xs text-center">
														<a href="#" class="member-manage-action" data-toggle="tooltip" title="Edit Job">
															<i class="fa fa-pencil"></i>
														</a>
														<a href="#" class="member-manage-action action-delete" data-toggle="tooltip" title="Delete Job">
															<i class="fa fa-trash-o"></i>
														</a>
													</td>
												</tr>
												<tr>
													<td class="check-column">
														<div class="form-control-flat">
															<label class="checkbox">
																<input type="checkbox" name="ids[]" value="">
																<i></i>
															</label>
														</div>
													</td>
													<td>
														<a href="#"><strong>Senior Designer</strong></a>
													</td>
													<td class="hidden-xs">
														<a href="#">
															<span class="noo-job-feature not-featured" data-toggle="tooltip"  title="Set Featured">
																<i class="fa fa-star-o"></i>
															</span>
														</a>
													</td>
													<td class="hidden-xs hidden-sm">
														<i class="fa fa-map-marker"></i>&nbsp;<em></em>
													</td>
													<td class="job-manage-expires hidden-xs">
														<span><i class="fa fa-calendar"></i>&nbsp;<em>Nov 19, 2015</em></span>
													</td>
													<td class="job-manage-app text-center">
														<span>1</span>
													</td>
													<td class="text-center">
														<span class="job-application-status job-application-status-publish">
															Active
														</span>
													</td>
													<td class="member-manage-actions hidden-xs text-center">
														<a href="#" class="member-manage-action" data-toggle="tooltip" title="Edit Job">
															<i class="fa fa-pencil"></i>
														</a>
														<a href="#" class="member-manage-action action-delete" data-toggle="tooltip" title="Delete Job">
															<i class="fa fa-trash-o"></i>
														</a>
													</td>
												</tr>
												<tr>
													<td class="check-column">
														<div class="form-control-flat">
															<label class="checkbox">
																<input type="checkbox" name="ids[]" value="">
																<i></i>
															</label>
														</div>
													</td>
													<td>
														<a href="#"><strong>Senior Designer</strong></a>
													</td>
													<td class="hidden-xs">
														<a href="#">
															<span class="noo-job-feature not-featured" data-toggle="tooltip"  title="Set Featured">
																<i class="fa fa-star-o"></i>
															</span>
														</a>
													</td>
													<td class="hidden-xs hidden-sm">
														<i class="fa fa-map-marker"></i>&nbsp;<em></em>
													</td>
													<td class="job-manage-expires hidden-xs">
														<span><i class="fa fa-calendar"></i>&nbsp;<em>Nov 19, 2015</em></span>
													</td>
													<td class="job-manage-app text-center">
														<span>1</span>
													</td>
													<td class="text-center">
														<span class="job-application-status job-application-status-publish">
															Active
														</span>
													</td>
													<td class="member-manage-actions hidden-xs text-center">
														<a href="#" class="member-manage-action" data-toggle="tooltip" title="Edit Job">
															<i class="fa fa-pencil"></i>
														</a>
														<a href="#" class="member-manage-action action-delete" data-toggle="tooltip" title="Delete Job">
															<i class="fa fa-trash-o"></i>
														</a>
													</td>
												</tr>
												<tr>
													<td class="check-column">
														<div class="form-control-flat">
															<label class="checkbox">
																<input type="checkbox" name="ids[]" value="">
																<i></i>
															</label>
														</div>
													</td>
													<td>
														<a href="#"><strong>Senior Designer</strong></a>
													</td>
													<td class="hidden-xs">
														<a href="#">
															<span class="noo-job-feature not-featured" data-toggle="tooltip"  title="Set Featured">
																<i class="fa fa-star-o"></i>
															</span>
														</a>
													</td>
													<td class="hidden-xs hidden-sm">
														<i class="fa fa-map-marker"></i>&nbsp;<em></em>
													</td>
													<td class="job-manage-expires hidden-xs">
														<span><i class="fa fa-calendar"></i>&nbsp;<em>Nov 19, 2015</em></span>
													</td>
													<td class="job-manage-app text-center">
														<span>1</span>
													</td>
													<td class="text-center">
														<span class="job-application-status job-application-status-publish">
															Active
														</span>
													</td>
													<td class="member-manage-actions hidden-xs text-center">
														<a href="#" class="member-manage-action" data-toggle="tooltip" title="Edit Job">
															<i class="fa fa-pencil"></i>
														</a>
														<a href="#" class="member-manage-action action-delete" data-toggle="tooltip" title="Delete Job">
															<i class="fa fa-trash-o"></i>
														</a>
													</td>
												</tr>
												<tr>
													<td class="check-column">
														<div class="form-control-flat">
															<label class="checkbox">
																<input type="checkbox" name="ids[]" value="">
																<i></i>
															</label>
														</div>
													</td>
													<td>
														<a href="#"><strong>Senior Designer</strong></a>
													</td>
													<td class="hidden-xs">
														<a href="#">
															<span class="noo-job-feature not-featured" data-toggle="tooltip"  title="Set Featured">
																<i class="fa fa-star-o"></i>
															</span>
														</a>
													</td>
													<td class="hidden-xs hidden-sm">
														<i class="fa fa-map-marker"></i>&nbsp;<em></em>
													</td>
													<td class="job-manage-expires hidden-xs">
														<span><i class="fa fa-calendar"></i>&nbsp;<em>Nov 19, 2015</em></span>
													</td>
													<td class="job-manage-app text-center">
														<span>1</span>
													</td>
													<td class="text-center">
														<span class="job-application-status job-application-status-publish">
															Active
														</span>
													</td>
													<td class="member-manage-actions hidden-xs text-center">
														<a href="#" class="member-manage-action" data-toggle="tooltip" title="Edit Job">
															<i class="fa fa-pencil"></i>
														</a>
														<a href="#" class="member-manage-action action-delete" data-toggle="tooltip" title="Delete Job">
															<i class="fa fa-trash-o"></i>
														</a>
													</td>
												</tr>
											</tbody>
										</table>--%>
                                        <asp:GridView ID="GVDetail" AutoGenerateColumns="false" runat="server" AllowPaging="true" 
            DataKeyNames="JobId" CssClass="table table-responsive" onpageindexchanging="GVDetail_PageIndexChanging">
            <Columns>
                 <asp:TemplateField HeaderText="Sr.No">
               <ItemTemplate>
                  <%# Container.DataItemIndex + 1 %>
               </ItemTemplate>
               </asp:TemplateField>
                 <asp:TemplateField HeaderText="Title">
               <ItemTemplate>
                   <a target="_blank" href='campus/<%# Eval("JobTitleDesignation").ToString().Replace(" ", "-").ToLower() %>-<%# Eval("JobId") %>.html'>
                   <asp:Label ID="lblJobTitleDesignation" runat="server" Text='<%# Eval("JobTitleDesignation") %>'></asp:Label>
                       </a>
               </ItemTemplate>
               </asp:TemplateField>
                 <asp:TemplateField HeaderText="KeyWords">
               <ItemTemplate>
                   <asp:Label ID="lblKeywords" runat="server" Text='<%# Eval("Keywords") %>'></asp:Label>
               </ItemTemplate>
               </asp:TemplateField>
                 <asp:TemplateField HeaderText="Experience">
               <ItemTemplate>
                   <asp:Label ID="lblWorkExperience" runat="server" Text='<%# Eval("WorkExperience").ToString().Replace("TO"," - ") %>'></asp:Label>
               </ItemTemplate>
               </asp:TemplateField>
                 <asp:TemplateField HeaderText="AnnualCTC">
               <ItemTemplate>
                   <asp:Label ID="lblAnnualCTC" runat="server" Text='<%# Eval("AnnualCTC").ToString().Replace("TO"," - ") %>'></asp:Label>
               </ItemTemplate>
               </asp:TemplateField>
                 <asp:TemplateField HeaderText="NoOfVacancy">
               <ItemTemplate>
                   <asp:Label ID="lblNoOfVacancy" runat="server" Text='<%# Eval("NoOfVacancy") %>'></asp:Label>
               </ItemTemplate>
               </asp:TemplateField>
                <asp:TemplateField HeaderText="JobLocation">
               <ItemTemplate>
                   <asp:Label ID="lblJobLocation" runat="server" Text='<%# Eval("JobLocation") %>'></asp:Label>
               </ItemTemplate>
               </asp:TemplateField>
                 <asp:TemplateField HeaderText="No.of Applied">
               <ItemTemplate>
                   <asp:LinkButton ID="lnkbtnViewApplication" runat="server" CssClass="btn btn-yellow">(<%# Eval("TotalApplied") %>)</asp:LinkButton>
               </ItemTemplate>
               </asp:TemplateField>
                 <asp:TemplateField HeaderText="UnPublish">
               <ItemTemplate>
                   <asp:LinkButton ID="lnkbtnUnPublish" runat="server" CssClass="btn btn-success" OnClick="lnkbtnUnPublish_Click" OnClientClick="return confirm('Are you sure to UnPublish this Job ?') ;" Visible='<%# Eval("IsPublished") %>'><i class="fa fa-unlink"></i></asp:LinkButton>
               </ItemTemplate>
               </asp:TemplateField>
                 <asp:TemplateField HeaderText="Publish">
               <ItemTemplate>
                   <asp:LinkButton ID="lnkbtnPublish" runat="server" CssClass="btn btn-danger" OnClick="lnkbtnPublish_Click" OnClientClick="return confirm('Are you sure to Publish this Job ?') ;" Visible='<%# Boolean.Parse(Eval("IsPublished").ToString()) == false ? true : false %>'><i class="fa fa-link"></i></asp:LinkButton>
               </ItemTemplate>
               </asp:TemplateField>
            </Columns>
                             
		</asp:GridView>
									</div>
									
								
							</div>
						</div>  
					</div> 
				</div> 
			</div>
    
</asp:Content>

