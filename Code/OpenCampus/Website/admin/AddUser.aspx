﻿<%@ Page Title="" Language="C#" MasterPageFile="~/admin/Admin.master" AutoEventWireup="true" CodeFile="AddUser.aspx.cs" Inherits="admin_AddUser" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="cph" Runat="Server">
     <div class="page-header">
							<h1>
								Dashboard
								<small>
									<i class="ace-icon fa fa-angle-double-right"></i>
								Add Admin User
								</small>
							</h1>
						</div><!-- /.page-header -->

    <div class="row">
        <div class="col-sm-12">
            <div class="widget-box">
											<div class="widget-header">
												<h4 class="widget-title">Create Admin's</h4>
											</div>

											<div class="widget-body">
												<div class="widget-main">
													<div class="form-inline">
                                                       <div class="col-sm-3">
                                                        <label>Full Name</label>
                                                           <div>
                                                             <asp:TextBox ID="TxtFullName" runat="server" Width="100%"></asp:TextBox>
                                                               </div>
                                                       </div>
                                                         <div class="col-sm-3">
                                                        <label>Email</label>
                                                             <div>
                                                             <asp:TextBox ID="TxtEmail" runat="server" Width="100%"></asp:TextBox>

                                                                 </div>
                                                       </div>
                                                         <div class="col-sm-3">
                                                        <label>Password</label>
                                                           <div> <asp:TextBox ID="TxtPassword" TextMode="Password" runat="server" Width="100%"></asp:TextBox></div>
                                                            
                                                       </div>
                                                        <div class="col-sm-3">
                                                        <label>Confirm Password</label>
                                                           <div> <asp:TextBox ID="TxtConfirmPassword" TextMode="Password" runat="server" Width="100%"></asp:TextBox></div>
                                                            
                                                       </div>
                                                      
                                                         <div class="col-sm-3">
                                                             <br />
                     <asp:LinkButton ID="BtnSave" class="btn btn-info btn-sm" runat="server" OnClick="BtnSave_Click"><i class="ace-icon fa fa-save bigger-110"></i> Save</asp:LinkButton>
                                                       </div>												
														 <div class="clearfix"></div>
													</div>
												</div>
											</div>
										</div>
        </div>
    </div>
</asp:Content>

