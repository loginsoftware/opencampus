﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using BusinessLayer;
using AjaxControlToolkit;

public partial class campus_registration_college : Utility // System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
       

        if (!Page.IsPostBack)
        {
            TxtEmailId.Attributes["type"] = "email";
            FillCountry(DdnCountry);
        }
    }

    protected void DdnCountry_SelectedIndexChanged(object sender, EventArgs e)
    {
        FillState(DdnState, int.Parse(DdnCountry.SelectedValue));
    }
    protected void DdnState_SelectedIndexChanged(object sender, EventArgs e)
    {
        FillCity(DdnCity, int.Parse(DdnState.SelectedValue));
    }
    protected void FUDean_OnUploadedComplete(object sender, AsyncFileUploadEventArgs e)
    {
        string filename = System.IO.Path.GetFileName(e.FileName);
        if (CheckFileType(filename) == false)
        {
            ShowMessage("InValid File Type !",this.Page);
               
        }
        else
        {
            string strUploadPath = "~/ReadWrite/College/";
            FUDean.SaveAs(Server.MapPath(strUploadPath) + TxtCollegeName.Value.Replace(" ", "") + filename);
            ScriptManager.RegisterClientScriptBlock(this, this.GetType(), "filePath", "top.$get(\"" + hfFUDean.ClientID + "\").value = '" + strUploadPath + TxtCollegeName.Value.Replace(" ", "") + filename + "';", true);
        }
    }
    protected void FULogo_OnUploadedComplete(object sender, AsyncFileUploadEventArgs e)
    {
        string filename = System.IO.Path.GetFileName(e.FileName);
        if (CheckFileType(filename) == false)
        {
            ShowMessage("InValid File Type !", this.Page);
        }
        else
        {
            string strUploadPath = "~/ReadWrite/College/";
            FULogo.SaveAs(Server.MapPath(strUploadPath) + TxtCollegeName.Value.Replace(" ", "") + filename);
            ScriptManager.RegisterClientScriptBlock(this, this.GetType(), "filePath", "top.$get(\"" + hfFULogo.ClientID + "\").value = '" + strUploadPath + TxtCollegeName.Value.Replace(" ", "") + filename + "';", true);
        }
       
    }
    protected void FUCollegeCoverPhoto_OnUploadedComplete(object sender, AsyncFileUploadEventArgs e)
    {
        string filename = System.IO.Path.GetFileName(e.FileName);
        if (CheckFileType(filename) == false)
        {
            ShowMessage("InValid File Type !", this.Page);
        }
        else
        {
            string strUploadPath = "~/ReadWrite/College/";
            FUCollegeCoverPhoto.SaveAs(Server.MapPath(strUploadPath) + TxtCollegeName.Value.Replace(" ", "") + filename);
            ScriptManager.RegisterClientScriptBlock(this, this.GetType(), "filePath", "top.$get(\"" + hfFUCollegeCover.ClientID + "\").value = '" + strUploadPath + TxtCollegeName.Value.Replace(" ", "") + filename + "';", true);
        }

    }
    protected void FUPresentation_OnUploadedComplete(object sender, AsyncFileUploadEventArgs e)
    {
        string filename = System.IO.Path.GetFileName(e.FileName);
        if (CheckFileType(filename) == false)
        {
            ShowMessage("InValid File Type !", this.Page);
        }
        else
        {
            string strUploadPath = "~/ReadWrite/College/";
            FUPresentation.SaveAs(Server.MapPath(strUploadPath) + TxtCollegeName.Value.Replace(" ", "") + filename);
            ScriptManager.RegisterClientScriptBlock(this, this.GetType(), "filePath", "top.$get(\"" + hfFUPresentation.ClientID + "\").value = '" + strUploadPath + TxtCollegeName.Value.Replace(" ", "") + filename + "';", true);
        }

    }
    protected void FUPlacementStatic_OnUploadedComplete(object sender, AsyncFileUploadEventArgs e)
    {
        string filename = System.IO.Path.GetFileName(e.FileName);
        if (CheckFileType(filename) == false)
        {
            ShowMessage("InValid File Type !", this.Page);
        }
        else
        {
            string strUploadPath = "~/ReadWrite/College/";
            FUPlacementStatic.SaveAs(Server.MapPath(strUploadPath) + TxtCollegeName.Value.Replace(" ", "") + filename);
            ScriptManager.RegisterClientScriptBlock(this, this.GetType(), "filePath", "top.$get(\"" + hfFUPlacementStatic.ClientID + "\").value = '" + strUploadPath + TxtCollegeName.Value.Replace(" ", "") + filename + "';", true);
        }

    }
    protected void BtnRegister_Click(object sender, EventArgs e)
    {
        if (!chkAll()) return;
        try
        {
            LoginDetailBL objL = new LoginDetailBL();
            CollegeMasterBL objC = new CollegeMasterBL();
            int _LoginId = objL.InsertLoginDetail(TxtUsername.Value, TxtPassword.Value, 3, DateTime.Now);
            int _collegeId = objC.InsertCollegeMaster(TxtCollegeName.Value, int.Parse(DdnCity.SelectedValue), TxtAddress.Value, TxtPinCode.Value, TxtPhoneNumber.Value, TxtWebsite.Value, TxtUniversityName.Value, TxtAffiliation.Value, TxtAboutCollege.Value, TxtDeanDirectorName.Value, TxtAboutDean.Value, hfFUDean.Value, TxtPlacementCordinator.Value, TxtPlacementPhone.Value + "{oc}" + TxtPlacementPhoneAlternate.Value, TxtEmailId.Value, hfFULogo.Value, hfFUCollegeCover.Value, hfFUPresentation.Value, hfFUPlacementStatic.Value, TxtVideoUrl.Value, hfLatitude.Value, hfLongitude.Value, DateTime.Now, DateTime.Now, _LoginId);


            Utility util = new Utility();
            SetCookie(this.Page, "CollegeId", _collegeId.ToString());

            Response.Redirect("CollegeProfile.aspx?college=" + TxtCollegeName.Value.ToString().Replace(" ", "-") + "", false);
        }
        catch (Exception Ex)
        {
            if (Ex.InnerException.Message.Contains("UK_UserNameEmail"))
            {
                ShowMessage("This UserName already registered with us, please choose another one.", this.Page);
            }
        }


    }

    private bool chkAll()
    {
        if (!IsEmail(TxtUsername.Value))
        {
            ShowMessage("Enter valid Email in Username", this.Page);
            return false;
        }
        return true;
    }
}