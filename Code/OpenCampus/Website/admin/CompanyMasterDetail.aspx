﻿<%@ Page Title="" Language="C#" MasterPageFile="~/admin/Admin.master" AutoEventWireup="true" CodeFile="CompanyMasterDetail.aspx.cs" Inherits="admin_CompanyMasterDetail" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="cph" Runat="Server">
 <div class="page-header">
							<h1>
								Dashboard
								<small>
									<i class="ace-icon fa fa-angle-double-right"></i>
									Company Detail
								</small>
							</h1>
						</div><!-- /.page-header -->

    <div class="row">
        <div class="col-sm-12">
            <div class="widget-box">
											<div class="widget-header">
												<h4 class="widget-title">Filter Company</h4>
											</div>

											<div class="widget-body">
												<div class="widget-main">
													<div class="form-inline">
                                                       <div class="col-sm-3">
                                                        <label>Company Name</label>
                                                           <div>
                                                             <asp:TextBox ID="TxtCompanyName" runat="server" Width="100%"></asp:TextBox>
                                                               </div>
                                                       </div>
                                                         <div class="clearfix"></div>
                                                         <div class="col-sm-3">
                                                         <br />
                     <asp:LinkButton ID="BtnSearch" class="btn btn-info btn-sm" runat="server" OnClick="BtnSearch_Click"><i class="ace-icon fa fa-search bigger-110"></i> Search</asp:LinkButton>
                                                       </div>												
														 <div class="clearfix"></div>
													</div>
												</div>
											</div>
										</div>
        </div>
    </div>
    
						<div class="row">
							<div class="col-xs-12">
								<!-- PAGE CONTENT BEGINS -->

                                <div class="row">
									<div class="col-sm-12">
										<div class="widget-box">
											<div class="widget-header">
												<h4 class="widget-title lighter smaller">
													<i class="ace-icon fa fa-users blue"></i>
													Company's Detail
												</h4>
											</div>
									<div style="overflow-x:scroll;">
       <asp:GridView ID="GVCompany" runat="server" AutoGenerateColumns="false" CssClass="table  table-bordered table-hover" AllowPaging="true" PageSize="50" DataKeyNames="JobPostCompanyId">
           <Columns>
                <asp:TemplateField HeaderText="Sr No">
                   <ItemTemplate>
                      <%# Container.DataItemIndex + 1 %>
                   </ItemTemplate>
               </asp:TemplateField>
                <asp:TemplateField HeaderText="Logo" ItemStyle-Wrap="false">
                   <ItemTemplate>
                       <asp:Image ID="lblCompanyLogo" runat="server" ImageUrl='<%# Eval("CompanyLogo") %>' Width="50px" />
                   </ItemTemplate>
               </asp:TemplateField>
               <asp:BoundField DataField="CompanyName" HeaderText="Company" ItemStyle-Wrap="false" />
               <asp:BoundField DataField="CompanyAddress" HeaderText="Address" ItemStyle-Wrap="false" />
               <asp:BoundField DataField="CompanyWebsite" HeaderText="Website" />
               <asp:TemplateField HeaderText="Action">
                   <ItemTemplate>
                    <asp:LinkButton ID="LnkBtnEdit" CssClass="btn btn-success" runat="server" onclick="LnkBtnEdit_Click"
                            ToolTip="Click here to Edit"><i class="fa fa-pencil"></i></asp:LinkButton>  
                   </ItemTemplate>
               </asp:TemplateField>
           </Columns>

       </asp:GridView>
						</div>

                                            </div>
									</div><!-- /.col -->

								</div><!-- /.row -->

                                	<!-- PAGE CONTENT ENDS -->
							</div><!-- /.col -->
						</div><!-- /.row -->
</asp:Content>

