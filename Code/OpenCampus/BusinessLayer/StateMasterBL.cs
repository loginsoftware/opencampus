﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data.Objects;
using DataLayer;

namespace BusinessLayer
{
    public class StateMasterBL
    {
        public List<StateMaster> LoadStateMasterByPrimaryKey(int UserTypeId)
        {
            DBEntities Obj = new DBEntities();
            return Obj.LoadStateMasterByPrimaryKey(UserTypeId).ToList();
        }

        public List<StateMaster> LoadAllStateMaster()
        {
            DBEntities Obj = new DBEntities();
            return Obj.LoadAllStateMaster().ToList();
        }

        public List<StateMaster> LoadStateMasterByCountryID(int CountryID)
        {
            DBEntities Obj = new DBEntities();
            return Obj.LoadStateMasterByCountryID(CountryID).ToList();
        }
    }
}
