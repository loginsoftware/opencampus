﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using BusinessLayer;

public partial class admin_Default : Utility // System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!Page.IsPostBack)
        {

        }
    }
    protected void BtnLogin_Click(object sender, EventArgs e)
    {
        AdminLoginBL obj = new AdminLoginBL();
        List<DataLayer.AdminLogin> A = obj.LoadAdminLoginByEmail(TxtUserName.Text);
        if (A.Count > 0)
        {
            string PasswordHash = A[0].PasswordHash;
            string PasswordSalt = A[0].PasswordSaltKey;

            string _Password = CreateHash(TxtPassword.Text, PasswordSalt);
            if (_Password == PasswordHash)
            {
                Session["AdminLoginId"] = A[0].AdminLoginId.ToString();
                SetCookie(this.Page, "AdminFullName", A[0].EmployeeName);
                Response.Redirect("Dashboard.aspx");
            }
            else
            {
                ShowMessage("Invalid email or passsword", this.Page);
            }
        }
        else
        {
            ShowMessage("Invalid email or passsword", this.Page);
        }

    }
}