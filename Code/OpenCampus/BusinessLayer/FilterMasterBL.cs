﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data.Objects;
using DataLayer;

namespace BusinessLayer
{
    public class FilterMasterBL
    {
        public int InsertFilterMaster(string FilterName, string FilterType, string MetaTitle, string MetaDescription)
        {
            ObjectParameter FilterId = new ObjectParameter("FilterId", typeof(Int32));
            DBEntities Obj = new DBEntities();

            Obj.InsertFilterMaster(FilterId, FilterName, FilterType, MetaTitle, MetaDescription);
            return int.Parse(FilterId.Value.ToString());
        }

        public int UpdateFilterMaster(int FilterId, string FilterName, string FilterType, string MetaTitle, string MetaDescription)
        {
            DBEntities Obj = new DBEntities();
            Obj.UpdateFilterMaster(FilterId, FilterName, FilterType, MetaTitle, MetaDescription);
            return FilterId;
        }

        public int DeleteFilterMaster(int FilterId)
        {
            DBEntities Obj = new DBEntities();
            Obj.DeleteFilterMaster(FilterId);
            return FilterId;
        }

        public List<FilterMaster> LoadFilterMasterByPrimaryKey(int FilterId)
        {
            DBEntities Obj = new DBEntities();
            return Obj.LoadFilterMasterByPrimaryKey(FilterId).ToList();
        }

        public List<FilterMaster> LoadAllFilterMaster()
        {
            DBEntities Obj = new DBEntities();
            return Obj.LoadAllFilterMaster().ToList();
        }

        public List<FilterMaster> LoadFilterMasterByFilterType(string FilterType, string StartText)
        {
            DBEntities Obj = new DBEntities();
            return Obj.LoadFilterMasterByFilterType(FilterType, StartText).ToList();
        }
        public List<FilterMaster> LoadFilterMasterSearchByFilterType(string SearchText, string FilterType)
        {
            DBEntities Obj = new DBEntities();
            return Obj.LoadFilterMasterSearchByFilterType(SearchText, FilterType).ToList();
        }

        public List<FilterMasterSkillCityDesignationForHomePage_Result> LoadFilterMasterSkillCityDesignationForHomePage()
        {
            DBEntities Obj = new DBEntities();
            return Obj.LoadFilterMasterSkillCityDesignationForHomePage().ToList();
        }
    }
}
