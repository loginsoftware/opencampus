﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.IO;

public partial class UC_MenuCategory : System.Web.UI.UserControl
{
    protected void Page_Load(object sender, EventArgs e)
    {
        string pageName = Path.GetFileName(Request.Path);
        if (pageName == "jobs-by-category")
        {
            lnkJobsByCategory.Attributes.Add("class", "active");
        }
        else if (pageName == "jobs-by-designation")
        {
            lnkJobsByDesignation.Attributes.Add("class", "active");
        }
        else if (pageName == "top-jobs-by-skill")
        {
            lnkJobsBySkills.Attributes.Add("class", "active");
        }
        else if (pageName == "jobs-by-location")
        {
            lnkJobsByLocation.Attributes.Add("class", "active");
        }
        else
        {
            lnkJobsByCompany.Attributes.Add("class", "active");
        }
    }
}