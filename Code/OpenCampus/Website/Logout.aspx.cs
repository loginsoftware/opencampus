﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class Logout : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        Session.Abandon();
        if (Request.Cookies["CollegeId"] != null)
        {
            Response.Cookies["CollegeId"].Expires = DateTime.Now.AddDays(-1);
        }
        if (Request.Cookies["LoginId"] != null)
        {
            Response.Cookies["LoginId"].Expires = DateTime.Now.AddDays(-1);
        }
        Response.Redirect("index.aspx");
    }
}