﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using BusinessLayer;

public partial class admin_AddUser : Utility // System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!Page.IsPostBack)
        {

        }
    }

    protected void BtnSave_Click(object sender, EventArgs e)
    {
        if (!ChkAll()) return;
        try
        {
            string SaltKey = CreateSalt(10);
            string PasswordHash = CreateHash(TxtPassword.Text, SaltKey);
            AdminLoginBL obj = new AdminLoginBL();
            obj.InsertAdminLogin(TxtFullName.Text, TxtEmail.Text, PasswordHash, SaltKey, 4, DateTime.Now);
            ShowMessage("User Admin Created Successfully", this.Page);
        }
        catch (Exception Ex)
        {
            ShowMessage(Ex.InnerException.Message, this.Page);
        }
    }

    private bool ChkAll()
    {
        if (IsEmpty(TxtFullName.Text))
        {
            ShowMessage("Enter Employee FullName", this.Page);
            return false;
        }
        if (IsEmpty(TxtEmail.Text))
        {
            ShowMessage("Enter Employee Email Address", this.Page);
            return false;
        }
        if (IsEmail(TxtEmail.Text))
        {
            ShowMessage("Enter Valid Email Address", this.Page);
            return false;
        }
        if (IsEmpty(TxtPassword.Text))
        {
            ShowMessage("Enter Password", this.Page);
            return false;
        }
        if (TxtPassword.Text.Length >= 6)
        {
            ShowMessage("Password Length Should be Minimun 6 Character.", this.Page);
            return false;
        }
        if (IsEmpty(TxtConfirmPassword.Text))
        {
            ShowMessage("Enter Confirm Password", this.Page);
            return false;
        }
        if (TxtPassword.Text != TxtConfirmPassword.Text)
        {
            ShowMessage("Confirm Password Not Matched", this.Page);
            return false;
        }
        return true;
    }
}