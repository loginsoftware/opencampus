﻿<%@ Page Title="" Language="C#" MasterPageFile="~/campus/College.master" AutoEventWireup="true" CodeFile="CollegeProfile.aspx.cs" Inherits="campus_CollegeProfile" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="asp" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
<link rel="stylesheet" href="css/bootstrap-social.css" type="text/css">
<script src="js/jobs.js"></script>
<link type="text/css" href="css/default.css" rel="stylesheet" media="all" />
<script src="js/jquery.nicescroll.min.js"></script>
<style>

#map-canvas {
  height: 30em;
  width: 100%;
}
.modal-color{
background-color:white;
}
.proportion{
padding:0% 0%;

}
.big{
font-size: 23px;
    color: #333333;

}

</style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="cph" Runat="Server">
<div class="container profile-new"> <br/>
  
  
  <div class="row1 padd-all">
  	<div class="row pro-snap">
      <div class="col-md-6 profile-body padding0 box-shadow">
        <div class="profile-contentbox">
          <div class="col-md-4 padding0">
           <asp:Image id="ImgCollegeLogo" runat="server" ImageUrl="images/avtar_6041941471331070.jpg" class="preview" Width="198px" Height="198px" />
          
           </div>
          <div class="col-md-8 profile-snapshot">
            <div class="pre-btn">
            	            		<br>
            	            </div>
            <div>
              
              <p class="font-s-14 black "><b><h3 class="big" id="lblCollegeName" runat="server"></h3></b>
              </p>
            </div>
            <div>
              <p class="font-s-12 black margin0 ellipsis"><i class="fa fa-map-marker font-s-14 l-gray"></i>
    <asp:Literal ID="lblCollegeAddress" runat="server"></asp:Literal> </p>   
   
            </div>
          </div>
          <div class="clearfix"></div>
        </div>        
      </div>
 <div class="col-md-6 bg-white  box-shadow" id="__notifications" style="min-height:198px; background-color: #f7f7f7;">          <div class="col-md-8 profile-snapshot">
            <div class="pre-btn">
            	            		<br> 
            	            </div> 
            <div>
			<h1 class="black font-bold"></h1>
              <p class="font-s-14 black"><b>Contact Details</b></p>
            </div>
            <div>
              <p class="font-s-12 black margin0 ellipsis"><i class="fa fa-phone-square font-s-14 l-gray"></i> <asp:Literal ID="lblPhoneNo" runat="server"></asp:Literal></p>
              <p class="font-s-12 black margin0 ellipsis"><i class="fa fa-globe font-s-14 l-gray"></i> 
        <asp:HyperLink ID="lnkWebsite" runat="server" Target="_blank"></asp:HyperLink></p>
              <p class="font-s-12 black margin0"><i class="fa fa-map-marker text-danger font-s-14" aria-hidden="true"></i> Location Map: 
            <asp:HyperLink ID="lnkViewCollegeMap" runat="server" data-toggle="modal" data-target="#mpLocation">View</asp:HyperLink></p>
            </div>
           
          </div>
        <p style="float:right; padding:1px;">   <asp:HyperLink ID="lnkEditCollegeInfo" runat="server" CssClass="btn btn-success" data-toggle="modal" data-target="#mpCollegeInfo"><i class="fa fa-pencil"></i> Edit</asp:HyperLink></p>
          </div>
    </div>
    <div class="col-md-9 col-md-offset-3 job-tit"><span><div class="margin0 f-white txt-t font-s-20 padd5"></div><h1 class="margin0 f-white txt-t font-s-18 padd5"></h1></span></div>
    
    <div class="col-md-3 refine-j"> 
     
      
      <nav class="navbar profile-nav border-r">
        <div class="container-fluid padding0">
          <!-- Brand and toggle get grouped for better mobile display -->
          <div class="navbar-header">
            <button type="button" class="navbar-toggle collapsed bg-dgray" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1" aria-expanded="false"> <span class="sr-only">Toggle navigation</span> <span class="icon-bar"></span> <span class="icon-bar"></span> <span class="icon-bar"></span> </button>
          </div>
          <!-- Collect the nav links, forms, and other content for toggling -->
          <div class="collapse navbar-collapse padding0" id="bs-example-navbar-collapse-1">
                      <div>  
                      <div class="search-side bg-white border-r box-shadow">
        <div class="filt-head filt-head-active">
          <span class="black font-s-14 margin0 pull-left">Batch & Courses</span>
          <span class="font-s-14 black pull-right">-</span>          
          <div class="clearfix"></div>
        </div>    
        <div id="infoCoursesSpec" runat="server" style="margin-left:10px;">
        to show your college courses and specializations Please email us: info@opencampus.in
        </div>    
      </div>    
      <div class="search-side bg-white border-r box-shadow">
        <div class="filt-head">
          <span class="black font-s-14 margin0 pull-left">Settings</span>
          <span class="font-s-14 black pull-right">-</span>          
          <div class="clearfix"></div>
        </div>
         <div class="check-list">    
        <div class="scroll-panes padd10" runat="server" style="margin-left:10px; display:none;">
        <asp:HyperLink ID="HyperLink3" style="cursor:pointer;" runat="server" data-toggle="modal" data-target="#mpChangePwd">Change Password</asp:HyperLink>
        </div>  
        </div>  
      </div>    
      	<%--<div class="search-side bg-white border-r box-shadow">
        <div class="filt-head filt-head-active">
          <span class="black font-s-14 margin0 pull-left">B.E/B.Tech</span>
          <span class="font-s-14 black pull-right">-</span>
          <div class="clearfix"></div>
        </div>
        <div class="check-list">
        	<div class="scroll-panes padd10">
			  <label class="ellipsis"><input type="checkbox" name="SubIndustry[]" value="1" >Cement</label><label class="ellipsis"><input type="checkbox" name="SubIndustry[]" value="2" >Agriculture</label><label class="ellipsis"><input type="checkbox" name="SubIndustry[]" value="3" >Automobile / Auto-ancillary / Tyre</label><label class="ellipsis"><input type="checkbox" name="SubIndustry[]" value="5" >Financial Services / Banking / Broking / Forex / Investment</label><label class="ellipsis"><input type="checkbox" name="SubIndustry[]" value="8" >Engineering / Infrastructure / Construction / EPC</label>          </div>
        </div>
      </div>--%>
      	<%--<div class="search-side bg-white border-r box-shadow">
        <div class="filt-head filt-head-active">
          <span class="black font-s-14 margin0 pull-left">Functional Area</span>
          <span class="font-s-14 black pull-right">-</span>
          <div class="clearfix"></div>
        </div>
        <div class="check-list">
          <div class="scroll-panes padd10">
          	<label class="ellipsis"><input type="checkbox" name="SubFunctionalArea[]" value="1" >Accounting / Tax / Company Secretary / Audit</label><label class="ellipsis"><input type="checkbox" name="SubFunctionalArea[]" value="5" >Banking / Insurance</label><label class="ellipsis"><input type="checkbox" name="SubFunctionalArea[]" value="8" >Export / Import</label><label class="ellipsis"><input type="checkbox" name="SubFunctionalArea[]" value="13" >Front Office Staff / Secretarial / Computer Operator</label><label class="ellipsis"><input type="checkbox" name="SubFunctionalArea[]" value="15" >Hotels / Restaurant Management</label>   </div>
        </div>
      </div>--%>
      	        
      </div>
      <div class="clearfix"></div>
          </div>
          <!-- /.navbar-collapse -->
          <div class="clearfix"></div>
        </div>
        <!-- /.container-fluid -->
      </nav>

      
    </div>
    <div class="col-md-6" id="job-result-list">
          <div class="profile-body bg-white border-r box-shadow job-s">
        <div class=" padding0 profile-snap">
          <div class="col-md-1 padd10">
          	
                        
          </div> 	
          <div class="col-md-10 padd10">
            <span class="blue font-s-14"><h1>About Campus &nbsp;&nbsp;&nbsp;&nbsp;<asp:HyperLink ID="HyperLink1" runat="server" CssClass="btn btn-success" data-toggle="modal" data-target="#mpAboutCollege"><i class="fa fa-pencil"></i> Edit</asp:HyperLink></h1>
            
            </span>
				<div class="jobv">
            		<span class="show"><b><asp:Literal ID="lblAboutCollegeTag" runat="server"></asp:Literal></b></span>
                    <span class="show">
                <asp:Literal ID="lblAboutCollege" runat="server"></asp:Literal></span>
                                  
                </div>
            	<div>
               </div>
                    
          </div>
          
          <div class="clearfix"></div>
        </div>
        <div class="clearfix"></div>
      </div>
     
            <div class="profile-body bg-white border-r box-shadow job-s">
			<div class=" padding0 profile-snap">
          <div class="col-md-1 padd10">
          	
                        
          </div> 	
          <div class="col-md-10 padd10">
            <span class="blue font-s-14"><h1>Dean / Director / Principal</h1></span>
				<div class="jobv">
            		<span class="show"><b>
                    <asp:Image ID="ImgDeanDirector" Width="85px" Height="100px" runat="server" />
                    <asp:Literal ID="lblDeanDirectorName" runat="server"></asp:Literal></b></span>
                    <span class="show"></span>
                                 
                </div>
            	<div>
               </div>
                    
          </div>
          
          <div class="clearfix"></div>
        </div>
       
        <div class="clearfix"></div>
      </div>
     
            <div class="profile-body bg-white border-r box-shadow job-s">
			<div class=" padding0 profile-snap">
          <div class="col-md-1 padd10">
          	
                        
          </div> 	
          <div class="col-md-10 padd10">
            <span class="blue font-s-14"><h1>College Pictures and Video</h1></span>
				<div class="jobv" style="margin-left: -60px;">
            		<span class="show">
                        <asp:Literal ID="lblYoutubeIframe" runat="server"></asp:Literal>
                    
                    </span>
                  
                </div>
            	<div>
               </div>
                    
          </div>
          
          <div class="clearfix"></div>
        </div>
   
        <div class="clearfix"></div>
      </div>
     
            
     
            
     
            <div class="profile-body bg-white border-r box-shadow job-s">
   
        <div class="clearfix"></div>
      </div>
     
            <div class="profile-body bg-white border-r box-shadow job-s">

        <div class="clearfix"></div>
      </div>
     
         
    </div>
    <div class="col-md-3">
    	<div class="profile-body bg-white border-r box-shadow job-s padd-all">
        <asp:HyperLink ID="HyperLink2" runat="server" CssClass="btn btn-success" data-toggle="modal" data-target="#mpAboutPlacement"><i class="fa fa-pencil"></i> Edit</asp:HyperLink>
        	<h3 class="font-s-20">Placement Coordinator</h3>
    <a class="show d-gray"><span> <i class="fa fa-user"></i> &nbsp;&nbsp;<asp:Literal ID="lblPlacementCoName" runat="server"></asp:Literal> </span></a>
<a class="show d-gray"><span> <i class="fa fa-phone"></i> &nbsp;&nbsp;<asp:Literal ID="lblPlacementPhoneNo" runat="server"></asp:Literal> </span></a>
<a class="show d-gray"><span> <i class="fa fa-envelope"></i> &nbsp;&nbsp;<asp:Literal ID="lblPlacementEmailId" runat="server"></asp:Literal> </span></a>
        	<hr>
			<h3 class="font-s-20">University</h3>
          <span> <asp:Literal ID="lblUniversity" runat="server"></asp:Literal> </span>
<h3 class="font-s-20">Affilations</h3>
   <span>
                        <asp:Literal ID="lblAffiliation" runat="server"></asp:Literal> </span>
             
            
			          
                        <hr>
                        <h3 class="font-s-20">College Presentation</h3>
                          <asp:Image ID="Image2" runat="server" ImageUrl="images/college-ppt.png" Width="80%" />
                            <asp:HyperLink ID="lnkCollegePresentation" runat="server">Download</asp:HyperLink>

                             <hr>
                        <h3 class="font-s-20">Placement Statistics</h3>
                          <asp:Image ID="Image3" runat="server" ImageUrl="images/college-placement-static.png" Width="100%" />
                            <asp:HyperLink ID="lnkCollegePlacementStatistic" runat="server">Download</asp:HyperLink>
         
        </div>
    </div>
    </div>
  </div>
  
   <div class="modal proportion" id="mpCollegeInfo" tabindex="-1" role="dialog">
    <div class="modal-dialog" role="document">
      <div class="modal-content">
       <div class="modal-body sign-in padding0">
      <div class="col-md-12" style="background-color:White; padding:20px;">
              <h4 class="d-gray margin0">Update College Information</h4><br />
              <fieldset>              

                  <div class="form-group col-md-12">
          <label class="clr67 form-text requiredClass">College Name</label>
       <input type="text" id="TxtCollegeName" runat="server" class="form-control" title="enter College / University Name" x-moz-errormessage="Enter College Name" value="" placeholder="Enter College Name" />
          <div class="clearfix"></div>
        </div>
                  <div class="form-group col-md-12">
          <label class="clr67 form-text requiredClass">Address</label>
                
				 <input type="text" id="TxtAddress" runat="server" class="form-control" title="enter College / University address" x-moz-errormessage="Enter College Address" value="" placeholder="Enter Address" />
          <div class="clearfix"></div>
        </div>	
          <div class="col-md-12">
          <div class="row">
            <div class="form-group col-md-6">
              <label class="clr67 form-text requiredClass show"> Pincode</label>
              <input type="text" id="TxtPinCode" runat="server" class="form-control" placeholder="94043" />


              <div class="clearfix"></div>
            </div>
            <div class="form-group col-md-6">
              <label class="clr67 form-text requiredClass">Phone</label>
              <input id="TxtPhoneNumber" runat="server" type="text" class="form-control" maxlength="30" title="Enter Phone Number" x-moz-errormessage="Enter Phone Number" value="" placeholder="99263372622" />
              <div class="clearfix"></div>
            </div>
			  
			
            <div class="clearfix"></div>

          </div>
        </div>
        <div class="form-group col-md-12">
          <label class="clr67 form-text requiredClass">College Website</label>
                
		<input id="TxtWebsite" runat="server" type="text" class="form-control" placeholder="www.collegename.com" />

          <div class="clearfix"></div>
        </div>	
         <div class="col-md-12">
        <div class="form-group col-md-6">                
          <label class="clr67 form-text requiredClass show"> Upload College Logo</label>
          <asp:AsyncFileUpload ID="FULogo" runat="server" OnUploadedComplete="FULogo_OnUploadedComplete"/>
           <asp:HiddenField ID="hfFULogo" runat="server" />
                <asp:Image ID="ImgCollegeLogoUpdate" runat="server"  Width="50px" Height="50px"/>

          <div class="clearfix"></div>
        </div>	
         <div class="form-group col-md-6">
              <label class="clr67 form-text requiredClass show"> Upload College Cover Photo</label>
        <asp:AsyncFileUpload ID="FUCollegeCoverPhoto" runat="server" OnUploadedComplete="FUCollegeCoverPhoto_OnUploadedComplete"/>
         <asp:HiddenField ID="hfFUCollegeCover" runat="server" />
          <asp:Image ID="ImgCoverPhotoUpdate" runat="server"  Width="50px" Height="50px"/>
		<hr>
 

              <div class="clearfix"></div>
            </div>
            </div>

         <div class="form-group col-md-12">
         
                <asp:Button ID="BtnSaveCollegeInfo" runat="server" Text="Update"  CssClass="btn btn-primary" OnClick="BtnUpdateCollege_Click"/>
          <div class="clearfix"></div>
        </div>	
              </fieldset>
           
          </div>
            </div>
      </div>
    </div>
  </div>

  <div class="modal proportion" id="mpAboutCollege" tabindex="-1" role="dialog">
    <div class="modal-dialog" role="document">
      <div class="modal-content">
       <div class="modal-body sign-in padding0">
      <div class="col-md-12" style="background-color:White; padding:20px;">
              <h4 class="d-gray margin0">About College</h4><br />
              <fieldset>               
                  <div class="form-group col-md-12">
          <label class="clr67 form-text requiredClass">About college</label>
                
				<textarea rows="2" id="TxtAboutCollege" runat="server"  class="form-control"  cols="50" placeholder="Describe yourself here...">
</textarea>

          <div class="clearfix"></div>
        </div>
                  <div class="form-group col-md-12">
              <label class="clr67 form-text requiredClass show"> Dean / Director / Principal</label>
              <input type="text" id="TxtDeanDirectorName" runat="server" class="form-control" maxlength="30" title="Enter Director name" x-moz-errormessage="Enter Director name" value="" />
                    <asp:HiddenField ID="hfAboutDean" runat="server" />
              <div class="clearfix"></div>
            </div>	
      <div class="form-group col-md-12">
          <label class="clr67 form-text requiredClass">Upload Photo of Dean</label>
                
        <asp:AsyncFileUpload ID="FUDean" runat="server" ThrobberID="myThrobber" OnUploadedComplete="FUDean_OnUploadedComplete" />
        <%--<asp:Label ID="lblerrorFUDean" runat="server" Text="Please Upload Only .jpg, .jpeg, .png, .gif, .xlsx , .docx, .doc, .pptx , .xls"></asp:Label>--%>
        <asp:HiddenField ID="hfFUDean" runat="server" />
            <asp:Image ID="ImgCollegeDeanUpdate" runat="server" Width="50px" Height="50px" />
          <div class="clearfix"></div>
        </div>	
        <div class="form-group col-md-12">
              <label class="clr67 form-text requiredClass show"> College Video(Youtube Url)</label>
       	<input id="TxtVideoUrl" runat="server" type="text" class="form-control" placeholder="Paste Video Youtube Url" />
			 
 <div class="clearfix"></div>
            </div>  
         <div class="form-group col-md-12">
         
                <asp:Button ID="BtnSaveAboutCollege" runat="server" Text="Update" CssClass="btn btn-primary" OnClick="BtnUpdateCollege_Click"/>
          <div class="clearfix"></div>
        </div>	
              </fieldset>
           
          </div>
            </div>
      </div>
    </div>
  </div>

  <div class="modal proportion" id="mpAboutPlacement" tabindex="-1" role="dialog">
    <div class="modal-dialog" role="document">
      <div class="modal-content">
       <div class="modal-body sign-in padding0">
      <div class="col-md-12" style="background-color:White; padding:20px;">          
              <fieldset>               
                  <div class="form-group col-md-12">
			<h3>Placement Cell Contact Details</h3>
			<hr>
              <label class="clr67 form-text requiredClass show"> Placement Coordinator</label>
              <input type="text" id="TxtPlacementCordinator" runat="server" class="form-control" maxlength="30" title="Enter name of placement co-ordiator" x-moz-errormessage="Enter name of placement co-ordiator" />
              <div class="clearfix"></div>
            </div>
			        <div class="col-md-12">
          <div class="row">
            <div class="form-group col-md-6">
              <label class="clr67 form-text requiredClass show"> Mobile No *</label>
              <input type="text" id="TxtPlacementPhone" runat="server"  class="form-control" placeholder="Enter Mobile Number" />
  

              <div class="clearfix"></div>
            </div>
            
			  <div class="form-group col-md-6">
              <label class="clr67 form-text requiredClass show">Alternate Mobile No *</label>
              <input type="text" id="TxtPlacementPhoneAlternate" runat="server"  class="form-control" placeholder="Enter Alternate Mobile Number" />
  

              <div class="clearfix"></div>
            </div> 
			
          </div>
        </div>	
		
		<div class="form-group col-md-12">
          <label class="clr67 form-text requiredClass">Email Address</label>
                
				<input id="TxtEmailId" runat="server" type="text" class="form-control" placeholder="Type your email" />
          <div class="clearfix"></div>
        </div>	
            	<div class="form-group col-md-12">
          <label class="clr67 form-text requiredClass">University name</label>
                
		<input id="TxtUniversityName" runat="server" type="text"  class="form-control" />

          <div class="clearfix"></div>
        </div>
             <div class="form-group col-md-12">
          <label class="clr67 form-text requiredClass">Affiliations</label>
                
		<input type="text" id="TxtAffiliation" runat="server" class="form-control" />
        Enter primary affiliations of college with other universities if any.
          <div class="clearfix"></div>
        </div>	     

        <div class="col-md-12">
          <div class="row">
            <div class="form-group col-md-6">
             <label class="clr67 form-text requiredClass show"> Upload Presentations</label>
          <asp:AsyncFileUpload ID="FUPresentation" runat="server" OnUploadedComplete="FUPresentation_OnUploadedComplete"/>
		 Supported Formats: .ppt, .pps, .pdf. Limit 5MB
          <asp:HiddenField ID="hfFUPresentation" runat="server" />
		 <hr>
		 
 <div class="clearfix"></div>
            </div>
            
			  <div class="form-group col-md-6">
              <label class="clr67 form-text requiredClass show"> Upload Placement Statistics</label>
      <asp:AsyncFileUpload ID="FUPlacementStatic" runat="server" OnUploadedComplete="FUPlacementStatic_OnUploadedComplete"/>
		Supported Formats: .jpg, .png, .gif. Limit 200KB
  <asp:HiddenField ID="hfFUPlacementStatic" runat="server" />

              <div class="clearfix"></div>
            </div> 
			
          </div>
        </div>
         <div class="form-group col-md-12">
         
                <asp:Button ID="BtnSaveAboutPlacement" runat="server" Text="Update"  CssClass="btn btn-primary" OnClick="BtnUpdateCollege_Click"/>
          <div class="clearfix"></div>
        </div>	
              </fieldset>
           
          </div>
            </div>
      </div>
    </div>
  </div>

  <div class="modal proportion" id="mpChangePwd" tabindex="-1" role="dialog">
    <div class="modal-dialog" role="document">
      <div class="modal-content">
       <div class="modal-body sign-in padding0">
                    <asp:UpdatePanel ID="UpdatePanel1" runat="server">
                    <ContentTemplate>
      <div class="col-md-12" style="background-color:White; padding:20px;">
              <h4 class="d-gray margin0">Change Password</h4><br />
              <fieldset>              

                  <div class="form-group col-md-12">
          <label class="clr67 form-text requiredClass">Enter Current Password</label>
       <input type="password" id="TxtCurrentPassword" runat="server" class="form-control" value="" placeholder="Current Password" />
          <div class="clearfix"></div>
        </div>

         <div class="form-group col-md-12">
          <label class="clr67 form-text requiredClass">New Password</label>
       <input type="password" id="TxtNewPassword" runat="server" class="form-control" value="" placeholder="New Password" />
          <div class="clearfix"></div>
        </div>
               <div class="form-group col-md-12">
          <label class="clr67 form-text requiredClass">Confirm Password</label>
       <input type="password" id="TxtConfirmPassword" runat="server" class="form-control" value="" placeholder="Confirm Password" />
          <div class="clearfix"></div>
        </div>    	
          
        	
         

         <div class="form-group col-md-12">
         
                <asp:Button ID="BtnChangePassword" runat="server" Text="Update"  CssClass="btn btn-primary" OnClick="BtnChangePassword_Click"/>
          <div class="clearfix"></div>
        </div>	
              </fieldset>
           
          </div>

          
                    </ContentTemplate>
                    </asp:UpdatePanel>

            </div>
      </div>
    </div>
  </div>

  <div class="modal proportion" id="mpLocation" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog" role="document">
      <div class="modal-content">
       <div id="map-canvas" style="width:100%;"></div>
      </div>
    </div>
  </div>
    <asp:HiddenField ID="hfCollegeId" runat="server" />
    <script type="text/javascript">
        $(document).ready(function () {
            $('.cd-btn').on('click', function (event) {
                event.preventDefault();
                $('.cd-panel').addClass('is-visible');
            });
            //clode the lateral panel
            $('.cd-panel').on('click', function (event) {
                if ($(event.target).is('.cd-panel') || $(event.target).is('.cd-panel-close')) {
                    $('.cd-panel').removeClass('is-visible');
                    event.preventDefault();
                }
            });
            $('.filt-head').click(function () {
                var itemList = this;
                $('.scroll-panes').css('display', 'none');
                $('.filt-head').removeClass('filt-head-active');
                $(itemList).parent('div.search-side').find('.scroll-panes').css('display', 'block');
                $(itemList).addClass('filt-head-active');
            });
        });
</script>
  <script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyCgxY6DqJ4TxnRfKjlZR8SfLSQRtOSTxEU"></script>
<script type="text/javascript">
    
    function showLocationMap() {
        setTimeout(function () {
            var latval = $('input[id=hflat]').val();
            var lngval = $('input[id=hflng]').val();

            var mapOptions = {
                zoom: 8,
                center: new google.maps.LatLng(latval, lngval),
                mapTypeId: google.maps.MapTypeId.ROADMAP
            };
            var map = new google.maps.Map(document.getElementById('map-canvas'), mapOptions);
            var marker = new google.maps.Marker({
                position: new google.maps.LatLng(latval, lngval),
                map: map,
                title: 'Hello World!'
            })
        }, 1000);
    }
</script>
    <asp:HiddenField ID="hflat" runat="server" ClientIDMode="Static" Value="23.30803" />
    <asp:HiddenField ID="hflng" runat="server" ClientIDMode="Static" Value="77.3844337" />
    <asp:HiddenField ID="hfuserLogin" runat="server" />
    <asp:HiddenField ID="hfuserHash" runat="server" />

</asp:Content>

