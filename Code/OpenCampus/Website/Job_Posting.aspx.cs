﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using BusinessLayer;
using System.IO;

public partial class Job_Posting : Utility // System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if(!Page.IsPostBack)
        {
            FillSkill(DdnSkills);
            txtMinExperience.Attributes.Add("Type", "number");
            txtMaxExperience.Attributes.Add("Type", "number");
            FillIndustry(DdnIndustry);
            FillFunctionalArea(DdnFunctionalArea);
            FillJobPostCompany(DdnJobPostCompany);

            //if (Request.QueryString["result"] != null as string)
            //{
            //    string _result = Request.QueryString["result"].ToString();
            //    if (_result == "published")
            //    {
            //        ShowMessage("Job Published Successfully", this.Page);
            //    }
            //    else if (_result == "draft")
            //    {
            //        ShowMessage("Job Saved but not Published, to publish this Job go to Draft Page.", this.Page);
            //    }
            //}

        }
    }
    //protected void LnkSaveAndPublish_Click(object sender, EventArgs e)
    //{
    //    SaveJob(true);
    //}
    //protected void LnkSaveAndPublishLater_Click(object sender, EventArgs e)
    //{
    //    SaveJob(false);
    //}

    //private void SaveJob(bool param)
    //{
    //    if (!ChkAll()) return;
    //    SaveImages();
    //    bool _isPublished = param;
    //    int _EmployeeId = int.Parse(GetCookie(this.Page, "EmployeeId"));
    //    string _jobLocation = Request.Form[lstbxJobLocation.UniqueID].ToString();
    //    JobPostingBL obj = new JobPostingBL();

    //    string keywordsSkills = "";
    //    foreach (ListItem listItem in DdnSkills.Items)
    //    {
    //        if (listItem.Selected == true)
    //        {
    //            keywordsSkills += listItem.Text + ",";
    //        }
    //    }
    //    bool JobResponseOnEmail = false;
    //    bool JobResponseOnWalkIn = false;
    //    if (rbResponseType.SelectedValue == "On Email") JobResponseOnEmail = true;
    //    if (rbResponseType.SelectedValue == "Walk-In") JobResponseOnWalkIn = true;
    //    int? _jobPostCompany = 1;
    //    if (DdnJobPostCompany.SelectedIndex > 0) _jobPostCompany = int.Parse(DdnJobPostCompany.SelectedValue);

    //    int _result = obj.InsertJobPosting(_EmployeeId, txtJobTitleDesignation.Value, txtJobDescription.Value.Replace("\r\n", "<br/>"), keywordsSkills, txtMinExperience.Value + "TO" + txtMaxExperience.Value, txtMinSalary.Value + "TO" + txtMaxSalary.Value, txtOtherSalaryDetail.Value, txtNoOfVacancy.Value, _jobLocation, DdnIndustry.SelectedValue, DdnFunctionalArea.SelectedValue, JobResponseOnEmail, JobResponseOnWalkIn, txtJobResponseEmail.Text, txtFromDateWalkin.Text + "TO" + txtToDateWalkin.Text, txtReferenceCode.Text, hfImagePath.Value, chkIsFeatured.Checked, DateTime.Now, _isPublished, txtLinkToJob.Text, DateTime.Now.AddDays(15), _jobPostCompany, DdnDesignation.SelectedItem.Text);

    //    if (_isPublished == true)
    //    {
    //        Response.Redirect("Job_Posting.aspx?result=published");
    //    }
    //    else
    //    {
    //        Response.Redirect("Job_Posting.aspx?result=draft");
    //    }
    //}

    //private bool ChkAll()
    //{
    //    //if (txtJobTitleDesignation.Value == "")
    //    //{
    //    //    ShowMessage("Enter Job Title you want to display", this.Page);
    //    //    return false;
    //    //}
    //    //if (DdnDesignation.SelectedValue == "")
    //    //{
    //    //    ShowMessage("Select Job Desination.", this.Page);
    //    //    return false;
    //    //}
    //    //if (txtJobDescription.Value == "")
    //    //{
    //    //    ShowMessage("Enter Job Description.", this.Page);
    //    //    return false;
    //    //}
    //    //if (DdnSkills.SelectedValue == "")
    //    //{
    //    //    ShowMessage("Select job Skills, keywords, designations", this.Page);
    //    //    return false;
    //    //}
    //    //if (txtMinExperience.Value == "" || txtMaxExperience.Value == "")
    //    //{
    //    //    ShowMessage("Enter required Experience, if not enter NA", this.Page);
    //    //    return false;
    //    //}
    //    //if (txtMinSalary.Value == "" || txtMaxSalary.Value == "")
    //    //{
    //    //    ShowMessage("Enter Annual CTC Salary, if not enter NA", this.Page);
    //    //    return false;
    //    //}
    //    //if (txtOtherSalaryDetail.Value == "")
    //    //{
    //    //    ShowMessage("Enter Other Salary Detail.", this.Page);
    //    //    return false;
    //    //}
    //    try
    //    {
    //        string _jobLocation = Request.Form[lstbxJobLocation.UniqueID].ToString();
    //    }
    //    catch {
    //        ShowMessage("Enter job Locations", this.Page);
    //        return false;
    //    }
       
    //    if (DdnIndustry.SelectedIndex == 0)
    //    {
    //        ShowMessage("Select Job Industry.", this.Page);
    //        return false;
    //    }
    //    if (DdnFunctionalArea.SelectedIndex == 0)
    //    {
    //        ShowMessage("Select Job Functional Area.", this.Page);
    //        return false;
    //    }
    //    if (rbResponseType.SelectedValue == "Walk-In")
    //    {
    //        if (txtFromDateWalkin.Text == "" || txtToDateWalkin.Text == "")
    //        {
    //            ShowMessage("Enter Valid Job Walk-In Date", this.Page);
    //            return false;
    //        }
            
    //    }
    //    if (rbResponseType.SelectedValue == "On Email")
    //    {
    //        if (txtJobResponseEmail.Text == "")
    //        {
    //            ShowMessage("Enter job response Email Address.", this.Page);
    //            return false;
    //        }

    //    }

    //    return true;
    //}

    //private void SaveImages()
    //{
    //    if (FUImage.HasFile)
    //    {
    //        string FileName = Path.GetFileName(FUImage.PostedFile.FileName);

    //        FileName = CheckFileNameExist("ReadWrite/JobPost", FileName);
    //        FUImage.SaveAs(Server.MapPath("~//ReadWrite//JobPost//" + FileName));

    //        if (hfImagePath.Value != "")
    //        {
    //            DeleteFileFromFolder(hfImagePath.Value);
    //        }
    //        hfImagePath.Value = "~/ReadWrite/JobPost/" + FileName;
    //    }
    //}
}