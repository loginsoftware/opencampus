﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using BusinessLayer;
using System.IO;

public partial class admin_JobPost_Company : Utility // System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!Page.IsPostBack)
        {
            if (Request.QueryString["result"] != null as string)
            {
                string _result = Request.QueryString["result"].ToString();
                if (_result == "success")
                {
                    ShowMessage("Record Saved Successfully", this.Page);
                }
                else if (_result == "error")
                {
                    ShowMessage("Record Not Saved, Try Again", this.Page);
                }
            }
        }
    }
    protected void BtnSave_Click(object sender, EventArgs e)
    {
        JobPostCompanyBL obj = new JobPostCompanyBL();
        SaveImages();
        //int _result = obj.InsertJobPostCompany(TxtCompanyName.Text, TxtCompanyAddress.Text, hfImagePath.Value, TxtCompanyWebsite.Text, DateTime.Now);

        //if (_result > 0)
        //{
        //    Response.Redirect("JobPost_Company.aspx?result=success", false);
        //}
        //else
        //{
        //    Response.Redirect("JobPost_Company.aspx?result=error", false);
        //}
        
    }

    private void SaveImages()
    {
        if (FULogo.HasFile)
        {
            string FileName = Path.GetFileName(FULogo.PostedFile.FileName);

            FileName = CheckFileNameExist("ReadWrite/CompanyLogo", FileName);
            FULogo.SaveAs(Server.MapPath("~//ReadWrite//CompanyLogo//" + FileName));

            if (hfImagePath.Value != "")
            {
                DeleteFileFromFolder(hfImagePath.Value);
            }
            hfImagePath.Value = "~/ReadWrite/CompanyLogo/" + FileName;
        }
    }
}