﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.master" AutoEventWireup="true" CodeFile="index.aspx.cs" Inherits="index" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">

  <title>Open Campus India - Job Vacancies in India | Freshers Jobs Online</title>

   <meta name="robots" content="INDEX,FOLLOW" />

   <meta  property="og:description" name="description" content="Search for job opportunities across India for fresher’s & experienced, be part of our recruitment, Search for latest Jobs posted by top companies - Stay connected with opencampus.in."/>

   <meta name="keywords" content="jobs, online jobs, recruitment, part time jobs, Engineering Jobs, Management Jobs, Technical Jobs, BPO, Finance & Accounting Jobs, fresher jobs, experienced jobs, latest jobs"/>


   <meta name="application-name" content="opencampus.in - Your job search starts and ends with us."/>

   <meta name="copyright" content="2017 opencampus.in" />

   <meta name="content-language" content="EN" />

   <meta name="author" content="www.opencampus.in" />

   <meta name="distribution" content="GLOBAL" />


   <meta name="robots" content="ALL" />

   <meta name="pragma" content="no-cache" />

   <meta name="revisit-after" content="1 day" />

   <meta name="classification" content="Freshers jobs, Software Jobs, IT Jobs, Technical Jobs, Govt Jobs" />

   <meta property="og:image" content="https://opencampus.in/images/logo-opencampus.png"/>

   <link rel="image_src" href="https://opencampus.in/images/logo-opencampus.png" />

   <link href="https://plus.google.com/+opencampus1" rel="publisher" />


	<meta property="og:type" content="website" />

	<meta property="og:url" content="https://opencampus.in" />  

	<meta property="og:title" content="Open Campus" /> 

	<meta property="og:description" content="Open Campus, is India's leading online career and recruitment resource with its cutting edge technology provides relevant profiles to employers"/> 


	<meta property="og:image" content="https://opencampus.in/images/logo-opencampus.png" /> 

	<meta property="og:site_name" content="opencampus.in"/>

<meta name="twitter:card" content="summary">
<meta name="twitter:url" content="https://www.opencampus.in">
<meta name="twitter:site" content="@opencampusdot">
<meta name="twitter:title" content="Open Campus India - Job Vacancies in India | Freshers Jobs Online">
<meta name="twitter:description" content="Search for job opportunities across India for fresher’s & experienced, be part of our recruitment, Search for latest Jobs posted by top companies - Stay connected with opencampus.in.">
<meta name="twitter:image" content="https://pbs.twimg.com/profile_images/872485092932833280/umLVkRsm_400x400.jpg">

<link rel="canonical" href="https://www.opencampus.in" />
    <script type="application/ld+json">
             { 
               "@context" : "http://schema.org", 
               "@type" : "Organization", 
               "name" : "Open Campus", 
               "url" : "https://www.opencampus.in/", 
               "logo" : "https://www.opencampus.in/images/logo-opencampus.png",
               "description": "Search for job opportunities across India for fresher’s & experienced, be part of our recruitment, Search for latest Jobs posted by top companies - Stay connected with opencampus.in.",
               "brand" : "Open Campus",
               "sameAs" : [ "https://www.facebook.com/opencampus.in", "https://twitter.com/opencampusdot", "https://plus.google.com/+opencampus1", "https://www.linkedin.com/company/opencampus.in"] 
             }
       </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="cph" Runat="Server">
<div class="container-wrap">
				<div class="main-content container-fullwidth">
					<div class="row">
						<div class="noo-main col-md-12 banner-main" style="background:url('images/1.jpg') no-repeat;">
                        
						<%--<video controls loop id="bgvid" playsinline autoplay muted>
  <!-- WCAG general accessibility recommendation is that media such as background video play through only once. Loop turned on for the purposes of illustration; if removed, the end of the video will fade in the same way created by pressing the "Pause" button  
<source src="http://thenewcode.com/assets/videos/polina.webm" type="video/webm">-->
<source src="images/1.mp4" type="video/mp4">
</video>--%>

							
      <h1 class="f-white text-center margin0 cv">Join us & Explore thousands of Jobs .</h1>
      <span class="font-s-14 f-white text-center show padd10 cv ">Your job search starts and ends with us.</span>
      <div class="col-md-8 col-md-offset-2 advSearch" style="margin-top: 3%; margin-bottom: 10%;">

    	<section class="padding0">

			<div class="tabs tabs-style-linemove">
				<nav>
					<ul>
						<li><a href="#section-linemove-1" class="f-jobs"><p>Jobs</p></a></li>
						<li><a href="#section-linemove-2" class="f-pro"><p>Employer</p></a></li>
						<li><a href="#section-linemove-3" class="icon f-serv"><p>College</p></a></li>
						<li><a href="#section-linemove-4" class="icon f-arti"><p>Freelancer</p></a></li>
						
					</ul>
				</nav>
				<div class="content-wrap">
                    <%--<section id="section1">
                    	<div name="searchform" id="Div1">
					        <span class="keyblock col-md-3 padding0">
					        	<input type="text" autocomplete="off" id="Text4" placeholder="Full Name" class="form-control">
					        </span>
					        <span class="keyblock col-md-5 padding0">
					        	<input type="text" style="border-left:1px solid #ddd" autocomplete="off" id="Text5" placeholder="Enter Email Address" class="form-control">
					        </span>
							<span class="keyblock col-md-4 padding0">
					        	<button type="button" class="font-s-14 search-btn btn btn-default btn-yellow" id="Button3"><i class="fa fa-bell"></i> Receive Job Alert</button>
					            
					        </span>
					        <div class="clearfix"></div>
  						</div>
                    </section>--%>
					<section id="section-linemove-1">
                    	<div name="searchform" id="searchjobs">
					        <span class="keyblock col-md-5 padding0">
					        	<input type="text" autocomplete="off" id="Keywords" placeholder="Search by designation, skills, etc." class="form-control">
					        </span>
					        <span class="keyblock col-md-5 padding0">
					        	<input type="text" style="border-left:1px solid #ddd" autocomplete="off" id="SearchLocation" placeholder="Search by country, state, or city." class="form-control">
					        </span>
							<span class="keyblock col-md-2 padding0">
					        	<button type="submit" class="font-s-14 search-btn btn btn-default btn-yellow" id="btnSearchJobs"><i class="fa fa-search"></i> Search</button>
					            
					        </span>
					        <div class="clearfix"></div>
  						</div>
                    </section>
					<section id="section-linemove-2">
						<div name="searchform" id="searchprofessionals">
					        <span class="keyblock col-md-10 padding0">
					        	<input type="text" autocomplete="off" id="Text1" placeholder="Search by name, designation, skills, etc." class="form-control">
					        </span>
					        <span class="keyblock col-md-2 padding0">
					        	<button type="submit" class="font-s-14 search-btn btn btn-default btn-yellow" id="submitSearch"><i class="fa fa-search"></i> Search </button>
					        </span>
					        <div class="clearfix"></div>
  						</div>
					</section>
					<section id="section-linemove-3">
						<div name="searchform" id="searchservices">
					        <span class="keyblock col-md-10 padding0">
					        	<input type="text" autocomplete="off" id="Text2" placeholder="Eg. Bhopal" class="form-control">
					        </span>
					        <span class="keyblock col-md-2 padding0">
					        	<button type="submit" class="font-s-14 search-btn btn btn-default btn-yellow" id="Button1"><i class="fa fa-search"></i> Search </button>
					        </span>
					        <div class="clearfix"></div>
  						</div>
					</section>
					<section id="section-linemove-4">
						<div name="searchform" id="searcharticles">
					        <span class="keyblock col-md-10 padding0">
					        	<input type="text" placeholder="Enter City Name to List Top Recruiters" autocomplete="off" id="Text3" class="form-control">
					        </span>
					        <span class="keyblock col-md-2 padding0">
					        	<button type="submit" class="font-s-14 search-btn btn btn-default btn-yellow" id="Button2"><i class="fa fa-search"></i> Search </button>
					        </span>
					        <div class="clearfix"></div>
  						</div>

					</section>
				</div><!-- /content -->
			</div><!-- /tabs -->
           
            <div class="search-links">  
                    <a href="browse-all-jobs">All Jobs</a><a href="jobs-by-category">Jobs by Category</a><a href="jobs-by-designation">Jobs by Designation</a><a href="top-jobs-by-skill">Jobs by Skills</a><a href="jobs-by-location">Jobs by Location</a><a>Jobs by Company</a>
                </div>                			<div class="jobs-links">
				<%--<a href="campus/registration-college.aspx" class="btn-trans font-s-12 center-block">List your College for Campus Drive</a>--%>
				<div class="clearfix"></div>
			</div>		
            		</section>
      <div class="clearfix"></div>
    </div>
							<div class="row ">
								
							</div>
							<div class="row pt-5 pb-10 bg-white">
								<div class="container-boxed max"style="
    box-shadow: rgba(0,0,0,0.15) 0px 1px 5px;    padding: 1%;
">
									<div class="col-md-8">
										<div class="noo-tabs">
											<div class="tab-control tab-control-right clearfix">
												<ul class="nav nav-tabs" role="tablist" id="noo-tabs-2">
													<li><a role="tab" data-toggle="tab" href="#tab-1">Featured Jobs</a></li>
													<%--<li><a role="tab" data-toggle="tab" href="#tab-2">Featured Jobs</a></li>--%>
												</ul>
											</div>
											<div class="tab-content clearfix">
												<div class="tab-pane" id="tab-1">
													<div class="jobs posts-loop">
														<div class="posts-loop-title">
															<h3>
													<span class="text-primary">Top 10 </span> Premium jobs for you
															</h3>
														</div>
														<div class="posts-loop-content">
														
                                                            <asp:Literal ID="LtrRecentJobList" runat="server"></asp:Literal>
														</div>
														<%--<div class="pagination list-center">
															<a href="#" class="prev page-numbers">
																<i class="fa fa-long-arrow-left"></i>
															</a>
															<a href="#" class="next page-numbers">
																<i class="fa fa-long-arrow-right"></i>
															</a>
														</div>--%>
													</div>
												</div>
												
											</div>
										</div>
									</div>                                    
                                    <div class="col-md-4">
                                    <div class="pl-5">
            <h4 class="txt-t text-center">Recent Posted Jobs</h4>
                                        <asp:Repeater ID="rptrRecentJobs" runat="server">
                                        <ItemTemplate>
                                        <div style="border-bottom: 1px solid #d2d0d0;padding: 10px 0;">
                            <a href='jobs/<%# MyUrl(Eval("JobTitleDesignation").ToString()).Trim('-') %>-<%# Eval("JobId") %>' target='_blank' style="font-weight:bold;color:#80BB03;font-size:12px;"><%# Eval("JobTitleDesignation") %></a>
                            <span class='show'><i class='fa fa-suitcase'></i> 1 to 2  years, <i class='fa fa-gears'></i> Asp.net, c#, javascript </span>
                            <span class='show'><i class='fa fa-map-marker'></i> Bhopal , <i class='fa fa-calendar'></i> 12-08-2017</span>
                           
                            </div> </ItemTemplate>
                                        </asp:Repeater>
                             
           
              


        </div>
                                    </div>
                                    
                                   
                                    <div class="col-md-4">
                                    <br /><br />
										<div class="noo-tabs">
											<div class="tab-control tab-control-right clearfix">
												<ul class="nav nav-tabs" role="tablist" id="Ul1">
		<li class="active"><a role="tab" data-toggle="tab" href="#sidebar-tab-1">Active jobs by Skills</a></li>
					<li><a role="tab" data-toggle="tab" href="#sidebar-tab-2">Cities</a></li>
					<li><a role="tab" data-toggle="tab" href="#sidebar-tab-3">Designation</a></li>

												</ul>
											</div>
											<div class="tab-content clearfix">
												<div class="tab-pane active" id="sidebar-tab-1">
													<div class="jobs posts-loop">		
														<div class="posts-loop-content">
															<ul>
                                                    <asp:Literal ID="ltrJobsBySkills" runat="server"></asp:Literal>
                                                            </ul>
														</div>														
													</div>
												</div>
												<div class="tab-pane" id="sidebar-tab-2">
													<div class="jobs posts-loop">		
														<div class="posts-loop-content">
															<ul>
                                                    <asp:Literal ID="ltrJobByCity" runat="server"></asp:Literal>
                                                            </ul>
														</div>														
													</div>
												</div>

                                                <div class="tab-pane" id="sidebar-tab-3">
													<div class="jobs posts-loop">		
														<div class="posts-loop-content">
															<ul>
                                                    <asp:Literal ID="ltrJobByDesignation" runat="server"></asp:Literal>
                                                            </ul>
														</div>														
													</div>
												</div>
											</div>
										</div>
									</div>
                                    
								</div>
							</div>

                               <div class="row bg-white">
								<div class="container-boxed max">
									<div class="col-md-12">
										<div class="noo-text-block">
											<h2 class="text-center">Job by Category</h2>
											<p class="text-center text-italic">
												Browse Jobs by Category Like Functional Area / Department / Industry / Sector
											</p>
										</div>
										<hr class="noo-gap mt-4"/>
										<div class="noo-recent-news posts-loop grid">
											<div class="row">
												<div class="noo-rn-item loop-item col-md-3 col-sm-3">
													<div class="loop-item-wrap">
														<div class="loop-item-featured">
															<a href="#">
																<img width="600" height="450" src="images/blog/job-by-category.jpg" alt="blog_9"/>
															</a>
														</div>
														<div class="loop-item-content">
															<h2 class="loop-item-title">
																<a href="/information-technology-jobs">
																IT-Jobs
																</a>
															</h2>															
														</div>
													</div>
												</div>
												<div class="noo-rn-item loop-item col-md-3 col-sm-3">
													<div class="loop-item-wrap">
														<div class="loop-item-featured">
															<a href="#">
																<img width="600" height="450" src="images/blog/job-by-category.jpg" alt="blog_8"/>
															</a>
														</div>
														<div class="loop-item-content">
															<h2 class="loop-item-title">
																<a href="/government-jobs">
																Government Jobs
																</a>
															</h2>															
														</div>
													</div>
												</div>
                                                <div class="noo-rn-item loop-item col-md-3 col-sm-3">
													<div class="loop-item-wrap">
														<div class="loop-item-featured">
															<a href="#">
																<img width="600" height="450" src="images/blog/job-by-category.jpg" alt="blog_8"/>
															</a>
														</div>
														<div class="loop-item-content">
															<h2 class="loop-item-title">
																<a href="/graphic-designer-jobs">
																Graphic Designer Jobs
																</a>
															</h2>															
														</div>
													</div>
												</div>
                                                <div class="noo-rn-item loop-item col-md-3 col-sm-3">
													<div class="loop-item-wrap">
														<div class="loop-item-featured">
															<a href="#">
																<img width="600" height="450" src="images/blog/job-by-category.jpg" alt="blog_8"/>
															</a>
														</div>
														<div class="loop-item-content">
															<h2 class="loop-item-title">
																<a href="/bank-jobs">
																Bank Jobs
																</a>
															</h2>															
														</div>
													</div>
												</div>

                                                <div class="noo-rn-item loop-item col-md-3 col-sm-3">
													<div class="loop-item-wrap">
														<div class="loop-item-featured">
															<a href="#">
																<img width="600" height="450" src="images/blog/job-by-category.jpg" alt="blog_8"/>
															</a>
														</div>
														<div class="loop-item-content">
															<h2 class="loop-item-title">
																<a href="/engineering-jobs">
																Engineering Jobs
																</a>
															</h2>															
														</div>
													</div>
												</div>
                                                <div class="noo-rn-item loop-item col-md-3 col-sm-3">
													<div class="loop-item-wrap">
														<div class="loop-item-featured">
															<a href="#">
																<img width="600" height="450" src="images/blog/job-by-category.jpg" alt="blog_8"/>
															</a>
														</div>
														<div class="loop-item-content">
															<h2 class="loop-item-title">
																<a href="/teacher-jobs">
																Teacher Jobs
																</a>
															</h2>															
														</div>
													</div>
												</div>
                                                <div class="noo-rn-item loop-item col-md-3 col-sm-3">
													<div class="loop-item-wrap">
														<div class="loop-item-featured">
															<a href="#">
																<img width="600" height="450" src="images/blog/job-by-category.jpg" alt="blog_8"/>
															</a>
														</div>
														<div class="loop-item-content">
															<h2 class="loop-item-title">
																<a href="/accounting-jobs">
																Accounting Jobs
																</a>
															</h2>															
														</div>
													</div>
												</div>
                                                <div class="noo-rn-item loop-item col-md-3 col-sm-3">
													<div class="loop-item-wrap">
														<div class="loop-item-featured">
															<a href="#">
																<img width="600" height="450" src="images/blog/job-by-category.jpg" alt="blog_8"/>
															</a>
														</div>
														<div class="loop-item-content">
															<h2 class="loop-item-title">
																<a href="/marketing-jobs">
																Marketing Jobs
																</a>
															</h2>															
														</div>
													</div>
												</div>
											</div>
											
										</div>
									</div>
								</div>
							</div>

                            <div class="row bg-white">
								<div class="container-boxed max">
									<div class="col-md-12">
										<div class="noo-text-block">
											<h2 class="text-center">Job by Location</h2>
											<p class="text-center text-italic">
	           Browse Jobs by Top Locations in States / Union Territories and Cities across India
											</p>
										</div>
										<hr class="noo-gap mt-4"/>
										<div class="noo-recent-news posts-loop grid">
											<div class="row">
												<div class="noo-rn-item loop-item col-md-3 col-sm-3">
													<div class="loop-item-wrap">
														<div class="loop-item-featured">
															<a href="#">
						<img width="600" height="450" src="images/blog/job-by-location.png" alt="blog_9"/>
															</a>
														</div>
														<div class="loop-item-content">
															<h3 class="loop-item-title">
																<a href="/jobs-in-delhi">
																Delhi
																</a>
															</h3>															
														</div>
													</div>
												</div>
												<div class="noo-rn-item loop-item col-md-3 col-sm-3">
													<div class="loop-item-wrap">
														<div class="loop-item-featured">
															<a href="#">
																<img width="600" height="450" src="images/blog/job-by-location.png" alt="blog_8"/>
															</a>
														</div>
														<div class="loop-item-content">
															<h3 class="loop-item-title">
																<a href="/jobs-in-bangalore">
																Bangalore
																</a>
															</h3>															
														</div>
													</div>
												</div>
                                                <div class="noo-rn-item loop-item col-md-3 col-sm-3">
													<div class="loop-item-wrap">
														<div class="loop-item-featured">
															<a href="#">
																<img width="600" height="450" src="images/blog/job-by-location.png" alt="blog_8"/>
															</a>
														</div>
														<div class="loop-item-content">
															<h3 class="loop-item-title">
																<a href="/jobs-in-mumbai">
															Mumbai
																</a>
															</h3>															
														</div>
													</div>
												</div>
                                                <div class="noo-rn-item loop-item col-md-3 col-sm-3">
													<div class="loop-item-wrap">
														<div class="loop-item-featured">
															<a href="#">
				<img width="600" height="450" src="images/blog/job-by-location.png" alt="blog_8"/>
															</a>
														</div>
														<div class="loop-item-content">
															<h3 class="loop-item-title">
																<a href="/jobs-in-chennai">
															Chennai
																</a>
															</h3>															
														</div>
													</div>
												</div>

                                                <div class="noo-rn-item loop-item col-md-3 col-sm-3">
													<div class="loop-item-wrap">
														<div class="loop-item-featured">
															<a href="#">
					<img width="600" height="450" src="images/blog/job-by-location.png" alt="blog_8"/>
															</a>
														</div>
														<div class="loop-item-content">
															<h3 class="loop-item-title">
																<a href="/jobs-in-hyderabad">
																Hyderabad
																</a>
															</h3>															
														</div>
													</div>
												</div>
                                                <div class="noo-rn-item loop-item col-md-3 col-sm-3">
													<div class="loop-item-wrap">
														<div class="loop-item-featured">
															<a href="#">
								<img width="600" height="450" src="images/blog/job-by-location.png" alt="blog_8"/>
															</a>
														</div>
														<div class="loop-item-content">
															<h3 class="loop-item-title">
																<a href="/jobs-in-kolkata">
																Kolkata
																</a>
															</h3>															
														</div>
													</div>
												</div>
                                                <div class="noo-rn-item loop-item col-md-3 col-sm-3">
													<div class="loop-item-wrap">
														<div class="loop-item-featured">
															<a href="#">
					<img width="600" height="450" src="images/blog/job-by-location.png" alt="blog_8"/>
															</a>
														</div>
														<div class="loop-item-content">
															<h3 class="loop-item-title">
																<a href="/jobs-in-pune">
															Pune
																</a>
															</h3>															
														</div>
													</div>
												</div>
                                                <div class="noo-rn-item loop-item col-md-3 col-sm-3">
													<div class="loop-item-wrap">
														<div class="loop-item-featured">
															<a href="#">
							<img width="600" height="450" src="images/blog/job-by-location.png" alt="blog_8"/>
															</a>
														</div>
														<div class="loop-item-content">
															<h3 class="loop-item-title">
																<a href="/jobs-in-chandigarh">
																Chandigarh
																</a>
															</h3>															
														</div>
													</div>
												</div>

                                                <div class="noo-rn-item loop-item col-md-3 col-sm-3">
													<div class="loop-item-wrap">
														<div class="loop-item-featured">
															<a href="#">
							<img width="600" height="450" src="images/blog/job-by-location.png" alt="blog_8"/>
															</a>
														</div>
														<div class="loop-item-content">
															<h3 class="loop-item-title">
																<a href="/jobs-in-lucknow">
																Lucknow
																</a>
															</h3>															
														</div>
													</div>
												</div>

                                                <div class="noo-rn-item loop-item col-md-3 col-sm-3">
													<div class="loop-item-wrap">
														<div class="loop-item-featured">
															<a href="#">
							<img width="600" height="450" src="images/blog/job-by-location.png" alt="blog_8"/>
															</a>
														</div>
														<div class="loop-item-content">
															<h3 class="loop-item-title">
																<a href="/jobs-in-ahmedabad">
																Ahmedabad
																</a>
															</h3>															
														</div>
													</div>
												</div>

                                                <div class="noo-rn-item loop-item col-md-3 col-sm-3">
													<div class="loop-item-wrap">
														<div class="loop-item-featured">
															<a href="#">
						<img width="600" height="450" src="images/blog/job-by-location.png" alt="blog_8"/>
															</a>
														</div>
														<div class="loop-item-content">
															<h3 class="loop-item-title">
																<a href="/jobs-in-bhopal">
																Bhopal
																</a>
															</h3>															
														</div>
													</div>
												</div>

                                                <div class="noo-rn-item loop-item col-md-3 col-sm-3">
													<div class="loop-item-wrap">
														<div class="loop-item-featured">
															<a href="#">
						<img width="600" height="450" src="images/blog/job-by-location.png" alt="blog_8"/>
															</a>
														</div>
														<div class="loop-item-content">
															<h3 class="loop-item-title">
																<a href="/jobs-in-indore">
																Indore
																</a>
															</h3>															
														</div>
													</div>
												</div>

											</div>
											
										</div>
									</div>
								</div>
							</div>

							<div class="row bg-gray pt-9 pb-0">
								<div class="col-md-12">
									<div class="container-boxed max">
										<div class="row">
											<div class="col-md-6 col-sm-12">
												<div class="noo-text-block">
													<h3>Featured Employers</h3>
													<p>
														Top 6 Employers of OpenCampus.in
													</p>
												</div>
												<hr class="noo-clear" />
												<div class="noo-text-block list-image-employer">
													<p>
														<a href="#">
															<img src="images/logo/logo_1.png" alt="Walk-In For Freshers" title="Walk-In For Freshers " width="210" height="120"/>
														</a>
														<a href="#">
															<img src="images/logo/logo_2.png" alt="java developer jobs" title="java developer jobs" width="210" height="120"/>
														</a>
														<a href="#">
															<img src="images/logo/logo_3.png" alt="php developer jobs" title="php developer jobs" width="210" height="120"/>
														</a>
														<a href="#">
															<img src="images/logo/logo_4.png" alt="seo jobs" title="seo jobs" width="210" height="120"/>
														</a>
														<a href="#">
															<img src="images/logo/logo_5.png" alt="current openings" title="current openings" width="210" height="120"/>
														</a>
														<a href="#">
															<img src="images/logo/logo_6.png" alt="current openings in bhopal" title="current openings in bhopal" width="210" height="120"/>
														</a>
													</p>
												</div>
											</div>
											<div class="col-md-6 col-sm-12">
												<img src="images/home-image.png" alt="home-image" class="noo-image">
											</div>
										</div>
									</div>
								</div>
							</div>

							<div class="row bg-gray-overlay bg-image bg-parallax pt-10 pb-10">
								<div class="col-md-12 parallax-content">
									<div id="noo-slider-3" class="noo-slider bottom-indicators">
										<ul class="sliders">
											<li class="slide-item">
												<div class="slide-content">
													<div class="our-customer">
														<p>
															<a class="customer-avatar" href="#">
																<img src="images/avatar/user3.jpg" alt="customer" width="100" height="100"/>
															</a>
														</p>
														<div class="custom-desc">
															<h5>Shikha Joshi</h5>
															<p>
																Tbi Technologies - www.tbitechnologies.com<br/>
																<i>“opencampus portal is useful and we are able to close some positions for our IT requirements. I really appreciate to opencampus.in for the support to the organisation for their urgent requirement, which they can hire candidates on notice period.”</i>
															</p>
														</div>
													</div>
												</div>
											</li>
											<li class="slide-item">
												<div class="slide-content">
													<div class="our-customer">
														<p>
															<a class="customer-avatar" href="#">
																<img src="images/avatar/user2.jpg" alt="job openings" title="job openings" width="100" height="100"/>
															</a>
														</p>
														<div class="custom-desc">
															<h5>Priyanka Srivastava</h5>
															<p>
																Ask & Relax - www.askandrelax.com<br/>
																<i>“opencampus.in has been one of the most innovative and progressive team that helped us find new avenues in our hiring process. Wishing opencampus team the very best.”</i>
															</p>
														</div>
													</div>
												</div>
											</li>
											<li class="slide-item">
												<div class="slide-content">
													<div class="our-customer">
														<p>
															<a class="customer-avatar" href="#">
																<img src="images/avatar/user1.jpg" alt="latest job openings" title="latest job openings" width="100" height="100"/>
															</a>
														</p>
														<div class="custom-desc">
															<h5>Sneha Raj Sharma</h5>
															<p>
															 Magnet Brains Technology - www.magnetbrains.com<br/>
																<i>“I have been using opencampus database and services for the past one years. I am happy with the kind of outcomes my team could drive through opencampus.in. The portal has been a good channel for talent scouting across levels.”</i>
															</p>
														</div>
													</div>
												</div>
											</li>
										</ul>
										<div class="clearfix"></div>
										<div id="noo-slider-3-pagination" class="slider-indicators"></div>
									</div>
								</div>
								<div class="parallax customer" data-parallax="1" data-parallax_no_mobile="1" data-velocity="0.1"></div>
							</div>
							<div class="row bg-gray pt-10 pb-10">
								<div class="col-md-12">
									<div class="noo-text-block">
										<h3 class="text-center">What OpenCampus Offered</h3>
										<p class="text-center text-italic">
											Premium Services offered by OpenCampus
										</p>
									</div>
									<div class="container-boxed max">
										<div class="row pt-5 pb-2">
											<div class="col-md-4 col-sm-6">
												<div class="noo-text-block">
													<p>
														<span class="text-primary pr-2">
															<span class="noo-icon features-icon icon-circle">
																<i class="fa fa-mobile"></i>
															</span>
														</span>
														<strong>RESUME WRITING</strong><br/>
														Get right Resume for a great impression with a Professionally written resume.
													</p>
												</div>
											</div>
											<div class="col-md-4 col-sm-6">
												<div class="noo-text-block">
													<p>
														<span class="text-primary pr-2">
															<span class="noo-icon features-icon icon-circle">
																<i class="fa fa-cog"></i>
															</span>
														</span>
														<strong>CAMPUS EDUCATION</strong><br/>
													Fast track your way up the career ladder! Expand your learning and skills, take an online certificate course or opt for an e-learning certification to further your career growth.
													</p>
												</div>
											</div>
											<div class="col-md-4 col-sm-6">
												<div class="noo-text-block">
													<p>
														<span class="text-primary pr-2">
															<span class="noo-icon features-icon icon-circle">
																<i class="fa fa-star"></i>
															</span>
														</span>
														<strong>Campus College Hiring</strong><br/>
														OpenCampus Provide Online Hiring Solution for Campus Drive
                                                        by Providing Online Student Registration or Online Examination System.
													</p>
												</div>
											</div>
										</div>
									</div>
									
								</div>
							</div>

                         
							
						</div>  
					</div> 
				</div> 
			</div>
    <script type='text/javascript' src='js/jquery.js'></script>
    <script type='text/javascript' src='js/jquery.carouFredSel-6.2.1-packed.js'></script>
    <script type='text/javascript' src='js/bootstrap.min.js'></script>
    <script>

        jQuery('document').ready(function ($) {
            $('#noo-slider-3 .sliders').carouFredSel({
                infinite: true,
                circular: true,
                responsive: true,
                debug: false,
                items: {
                    start: 0
                },
                scroll: {
                    items: 1,
                    duration: 400,

                    fx: "scroll"
                },
                auto: {
                    timeoutDuration: 3000,

                    play: true
                },

                pagination: {
                    container: "#noo-slider-3-pagination"
                },
                swipe: {
                    onTouch: true,
                    onMouse: true
                }
            });
            $('#noo-tabs-2 a:eq(0)').tab('show');
        });
		</script>
    <script async src="//pagead2.googlesyndication.com/pagead/js/adsbygoogle.js"></script>
<script>
    (adsbygoogle = window.adsbygoogle || []).push({
        google_ad_client: "ca-pub-6041013670397498",
        enable_page_level_ads: true
    });
</script>
</asp:Content>

