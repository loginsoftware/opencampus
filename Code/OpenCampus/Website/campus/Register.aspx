﻿<%@ Page Title="" Language="C#" MasterPageFile="~/campus/College.master" AutoEventWireup="true" CodeFile="Register.aspx.cs" Inherits="campus_Register" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
<style>
.left-side {
position: relative;
     text-align: center; 
    background: url('images/b-register.jpg') no-repeat center center;
    min-height: 125vh;
	}
	
	.right-side {
    position: relative;
    padding: 30px 57px 0 120px;
    background: #fff;
	box-shadow: 1px 1px 64px 0px black;
}
.topie{
    padding-top: 72px;

}
.cd-btn{
box-shadow: 1px -6px 16px 1px black;

}
</style>

</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="cph" Runat="Server">
<div id="main-wrapper">
  <div class="container-fluid padding0">
    <div class="row margin0 topie">
      <div class="col-md-6 left-side">
        <header class="col-md-9 col-md-offset-1 text-center">
        	
        </header>
      </div>
      <div class="col-md-6 right-side">
      	<h2 class="gray font-s-30 wt-sm text-center margin0">Create a free account</h2><hr>
		
		
	  <div role="form" id="certificationportfolio" class="text-l" method="post">
         
        <div class="col-md-12">
          <div class="row">
            <div class="form-group col-md-12">
			
              <label class="clr67 form-text requiredClass">Email Address</label>
                
				<input type="text" class="user_email form-control" name="user_email" required placeholder="Email">
              <div class="clearfix"></div>
            </div>

            <div class="clearfix"></div>
			<div class="form-group col-md-12">
			
 <label class="clr67 form-text requiredClass">Password *</label>
             	<input type="password" class="user_password form-control" required value="" name="user_password" placeholder="Password">
              <div class="clearfix"></div>
            </div>

            <div class="clearfix"></div>
			<div class="form-group col-md-12">
			
 <label class="clr67 form-text">Confirm password *</label>
        		<input type="password" class="cuser_password form-control" required value="" name="cuser_password" placeholder="Repeat password">
		  	  
     
              <div class="clearfix"></div>
            </div>

            <div class="clearfix"></div>
			<div class="form-group col-md-12">
			
 <label class="clr67 form-text">Register As a</label>
          <select class="user_role" name="user_role" required>
                                            <option value="1">I&#039;m a candidate looking for a job</option>
											<option value="2">I&#039;m an employer looking to hire</option>
											
										</select>
              <div class="clearfix"></div>
            </div>

            <div class="clearfix"></div>
			 <div class="col-md-12"> <br>
          <br>
	
          <button type="button" class="btn btn-lg btn-primary" name="btnCreateAccount" value="true" onclick="RegisterCandidate();">Create my account</button>
          <br>
          <br>
        </div>
      </div>
          </div>
        </div>
       
  </div>
</div>
</asp:Content>

