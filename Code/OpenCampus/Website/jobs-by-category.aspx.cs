﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using BusinessLayer;

public partial class jobs_by_category : Utility // System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!Page.IsPostBack)
        {
            LoadJobCategory();
        }
    }

    private void LoadJobCategory()
    {
        FilterMasterBL objF = new FilterMasterBL();
        rptrJobsByFunctionalArea.DataSource = objF.LoadFilterMasterByFilterType("CategoryFunctionalAreaDepartment","");
        rptrJobsByFunctionalArea.DataBind();

        rptrJobsByIndustry.DataSource = objF.LoadFilterMasterByFilterType("CategoryIndustrySector","");
        rptrJobsByIndustry.DataBind();
    }
}