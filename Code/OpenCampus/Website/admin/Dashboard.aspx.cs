﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using BusinessLayer;

public partial class admin_Dashboard : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!Page.IsPostBack)
        {
            LoadCandidateTop10();
            LoadEmployeeTop10();
            LoadCollegeTop10();

            LoadChartData();
        }
    }

    private void LoadCandidateTop10()
    {
        CandidateMasterBL obj = new CandidateMasterBL();
        GVCandidate.DataSource = obj.LoadCandidateMasterTop(0); // candidate id is 0 when fecth new top 10 records
        GVCandidate.DataBind();
    }
    private void LoadEmployeeTop10()
    {
        EmployeeMasterBL obj = new EmployeeMasterBL();
        GVEmployer.DataSource = obj.LoadEmployeeMasterTop(0); // employee id is 0 when fecth new top 10 records
        GVEmployer.DataBind();
    }
    private void LoadCollegeTop10()
    {
        CollegeMasterBL obj = new CollegeMasterBL();
        GVCollege.DataSource = obj.LoadCollegeMasterTop(0); // college id is 0 when fecth new top 10 records
        GVCollege.DataBind();
    }

    private void LoadChartData()
    {
        DashboardBL obj = new DashboardBL();
        List<DataLayer.DashboardTotalState_Result> Dt = obj.LoadAllDashboardTotalState();
        List<DataLayer.DashboardChartState_Result> Dc = obj.LoadDashboardChartState();
        //List<DataLayer.DashboardChartStateWeek_Result> Dw = obj.LoadDashboardChartStateWeek(DateTime.Parse(DateTime.Now.ToString("dd-MMM-yyyy")));
        List<DataLayer.DashboardChartStateWeek_Result> Dw = obj.LoadDashboardChartStateWeek(DateTime.Parse("5-Jul-2017"));


        //----total stat
        lblTotalCandidate.InnerText = Dt[0].TotalCandiate.ToString();
        lblTotalEmployee.InnerText = Dt[0].TotalEmployer.ToString();
        lblTotalCollege.InnerText = Dt[0].TotalCollege.ToString();
        lblTotalJobPost.InnerText = Dt[0].TotalJobPost.ToString();
        lblTotalJobApplied.InnerText = Dt[0].TotalJobApplied.ToString();

        //---- spark stat
        if (Dw.Count > 0)
        {
            string _candidate = "", _emp = "", _college="", _job = "", _jobApp = "";
            for (int i = 0; i < Dw.Count; i++)
            {
                _candidate += Dw[i].Candiate.ToString() + ",";
                _emp += Dw[i].Employer.ToString() + ",";
                _college += Dw[i].College.ToString() + ",";
                _job += Dw[i].JobPost.ToString() + ",";
                _jobApp += Dw[i].JobApplied.ToString() + ",";
            }
            lblSpark_Candidate.Attributes.Add("data-values", _candidate.TrimEnd(','));
            lblSpark_Emp.Attributes.Add("data-values", _emp.TrimEnd(','));
            lblSpark_College.Attributes.Add("data-values", _college.TrimEnd(','));
            lblSpark_jobPost.Attributes.Add("data-values", _job.TrimEnd(','));
            lblSpark_JobApplied.Attributes.Add("data-values", _jobApp.TrimEnd(','));
        }

        //---- line chart result data
        string _months = "", _totalCandidate = "", _totalEmp = "", _totalCollege = "";
        for (int i = Dc.Count; i-- > 0;)
        {
            _months += DateTime.Parse("1-"+Dc[i].DateMonth+ "-2017").ToString("MMM") + "-" + Dc[i].DateYear + ",";
            _totalCandidate += Dc[i].TotalCandidate + ",";
            _totalEmp += Dc[i].TotalEmployer + ",";
            _totalCollege += Dc[i].TotalCollege + ",";
        }
        hfMonthName.Value = _months.TrimEnd(',');
        hfCandidate.Value = _totalCandidate.TrimEnd(',');
        hfEmployer.Value = _totalEmp.TrimEnd(',');
        hfCollege.Value = _totalCollege.TrimEnd(',');
    }
}