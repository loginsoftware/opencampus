﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.master" AutoEventWireup="true" CodeFile="jobs-by-category.aspx.cs" Inherits="jobs_by_category" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
      <title>Job Category List in India | Main Job Categories – OpenCampus.in</title>
<meta name="robots" content="INDEX,FOLLOW" />
<meta  property="og:description" name="description" content="Browse the most popular job categories - Accounting, Content Writing, Engineering, HR, DBA, Network administrator, IT Jobs, Telecom Software, BPO, Marketing, Pharma, Teacher, Graphic Designer Jobs Vacancies in your desired sector."/>

   <meta name="keywords" content=" Accounting Jobs, Content Writing Jobs, Engineering Jobs, HR Jobs, DBA Jobs, Network administrator Jobs, IT Jobs vacancies, Telecom Software Jobs, BPO Jobs, Marketing Jobs, Pharma Jobs, Teacher Jobs, Graphic Designer Jobs "/>


   <meta name="application-name" content="opencampus.in - Your job search starts and ends with us."/>

   <meta name="copyright" content="2017 opencampus.in" />

   <meta name="content-language" content="EN" />

   <meta name="author" content="www.opencampus.in" />

   <meta name="distribution" content="GLOBAL" />


   <meta name="robots" content="ALL" />

   <meta name="pragma" content="no-cache" />

   <meta name="revisit-after" content="1 day" />

   <meta name="classification" content="Jobs by category, jobs category list" />

   <meta property="og:image" content="https://opencampus.in/images/logo-opencampus.png"/>

   <link rel="image_src" href="https://opencampus.in/images/logo-opencampus.png" />

   <link href="https://plus.google.com/+opencampus1" rel="publisher" />


	<meta property="og:type" content="website" />

	<meta property="og:url" content="https://opencampus.in" />  

	<meta property="og:title" content="Open Campus" /> 

	<meta property="og:description" content="Open Campus, is India's leading online career and recruitment resource with its cutting edge technology provides relevant profiles to employers"/> 


	<meta property="og:image" content="https://opencampus.in/images/logo-opencampus.png" /> 

	<meta property="og:site_name" content="opencampus.in"/>

<meta name="twitter:card" content="summary">
<meta name="twitter:url" content="https://www.opencampus.in">
<meta name="twitter:site" content="@opencampusdot">
<meta name="twitter:title" content=" Job Category List in India | Main Job Categories – OpenCampus.in ">
<meta name="twitter:description" content="Browse the most popular job categories - Accounting, Content Writing, Engineering, HR, DBA, Network administrator, IT Jobs, Telecom Software, BPO, Marketing, Pharma, Teacher, Graphic Designer Jobs Vacancies in your desired sector."/>
<meta name="twitter:image" content="https://pbs.twimg.com/profile_images/872485092932833280/umLVkRsm_400x400.jpg" />

</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="cph" Runat="Server">

			<UC:MenuCategory ID="ucMenuCategoryLinks" runat="server" />

			<div class="container-wrap">
				<div class="main-content container-boxed max">
					<div class="row">
                    
						<div class="noo-main col-md-12">
							<div class="member-manage">
								 <h1>Browse Jobs by Functional Area/Department</h1> 
							<div class="member-manage-inner">
                             <ul>
                                <asp:Repeater ID="rptrJobsByFunctionalArea" runat="server">
                                <ItemTemplate>
                    <li><a href="<%# MyUrl(Eval("FilterName").ToString()) %>-jobs" title="<%# Eval("FilterName") %> Jobs"><%# Eval("FilterName") %></a></li>
                                </ItemTemplate>
                                </asp:Repeater>				
                                </ul>						

                                    
                                    </div>
							</div>                           
						</div>  
                        <div class="noo-main col-md-12">
							<div class="member-manage">
								<h1>Browse Jobs by Industry/Sector</h1>
							<div class="member-manage-inner">
                            <ul>
                                <asp:Repeater ID="rptrJobsByIndustry" runat="server">
                                <ItemTemplate>
                                <li><a href="<%# MyUrl(Eval("FilterName").ToString()) %>-jobs" title="<%# Eval("FilterName") %> Jobs"><%# Eval("FilterName") %></a></li>
                                </ItemTemplate>
                                </asp:Repeater>				
                                </ul>
                                    
                                    </div>
							</div>                           
						</div>  


					</div> 
				</div> 
			</div>
</asp:Content>

