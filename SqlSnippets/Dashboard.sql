

------- opencampus load chart state result for last 7 days or week --------------

DECLARE @TodayDate DATE;
SELECT @TodayDate = '5-Jul-2017';

DECLARE @i int = 0

SELECT DATEADD(day,@i,@TodayDate) as DateValue,  count(*) as Candiate,
(select isnull(count(*),0) from LoginDetail where CAST(CreatedOn as DATE) = DATEADD(day,@i,@TodayDate) and UserTypeId =2) as Employer,
(select isnull(count(*),0) from LoginDetail where CAST(CreatedOn as DATE) = DATEADD(day,@i,@TodayDate) and UserTypeId =3) as College,
(select isnull(count(*),0) from JobPosting where CAST(CreatedOn as DATE) = DATEADD(day,@i,@TodayDate)) as JobPost,
(select isnull(count(*),0) from JobApplication where CAST(CreatedOn as DATE) = DATEADD(day,@i,@TodayDate)) as JobApplied

FROM LoginDetail U
WHERE CAST(U.CreatedOn as DATE) = DATEADD(day,@i,@TodayDate) and UserTypeId =1

union all

SELECT DATEADD(day,@i-1,@TodayDate) as DateValue,  count(*) as Candiate,
(select isnull(count(*),0) from LoginDetail where CAST(CreatedOn as DATE) = DATEADD(day,@i -1,@TodayDate) and UserTypeId =2) as Employer,
(select isnull(count(*),0) from LoginDetail where CAST(CreatedOn as DATE) = DATEADD(day,@i-1,@TodayDate) and UserTypeId =3) as College,
(select isnull(count(*),0) from JobPosting where CAST(CreatedOn as DATE) = DATEADD(day,@i-1,@TodayDate)) as JobPost,
(select isnull(count(*),0) from JobApplication where CAST(CreatedOn as DATE) = DATEADD(day,@i-1,@TodayDate)) as JobApplied

FROM LoginDetail U
WHERE CAST(U.CreatedOn as DATE) = DATEADD(day,@i-1,@TodayDate) and UserTypeId =1

union all

SELECT DATEADD(day,@i-2,@TodayDate) as DateValue,  count(*) as Candiate,
(select isnull(count(*),0) from LoginDetail where CAST(CreatedOn as DATE) = DATEADD(day,@i -2,@TodayDate) and UserTypeId =2) as Employer,
(select isnull(count(*),0) from LoginDetail where CAST(CreatedOn as DATE) = DATEADD(day,@i-2,@TodayDate) and UserTypeId =3) as College,
(select isnull(count(*),0) from JobPosting where CAST(CreatedOn as DATE) = DATEADD(day,@i-2,@TodayDate)) as JobPost,
(select isnull(count(*),0) from JobApplication where CAST(CreatedOn as DATE) = DATEADD(day,@i-2,@TodayDate)) as JobApplied

FROM LoginDetail U
WHERE CAST(U.CreatedOn as DATE) = DATEADD(day,@i-2,@TodayDate) and UserTypeId =1

union all

SELECT DATEADD(day,@i-3,@TodayDate) as DateValue,  count(*) as Candiate,
(select isnull(count(*),0) from LoginDetail where CAST(CreatedOn as DATE) = DATEADD(day,@i -3,@TodayDate) and UserTypeId =2) as Employer,
(select isnull(count(*),0) from LoginDetail where CAST(CreatedOn as DATE) = DATEADD(day,@i-3,@TodayDate) and UserTypeId =3) as College,
(select isnull(count(*),0) from JobPosting where CAST(CreatedOn as DATE) = DATEADD(day,@i-3,@TodayDate)) as JobPost,
(select isnull(count(*),0) from JobApplication where CAST(CreatedOn as DATE) = DATEADD(day,@i-3,@TodayDate)) as JobApplied

FROM LoginDetail U
WHERE CAST(U.CreatedOn as DATE) = DATEADD(day,@i-3,@TodayDate) and UserTypeId =1

union all

SELECT DATEADD(day,@i-4,@TodayDate) as DateValue,  count(*) as Candiate,
(select isnull(count(*),0) from LoginDetail where CAST(CreatedOn as DATE) = DATEADD(day,@i -4,@TodayDate) and UserTypeId =2) as Employer,
(select isnull(count(*),0) from LoginDetail where CAST(CreatedOn as DATE) = DATEADD(day,@i-4,@TodayDate) and UserTypeId =3) as College,
(select isnull(count(*),0) from JobPosting where CAST(CreatedOn as DATE) = DATEADD(day,@i-4,@TodayDate)) as JobPost,
(select isnull(count(*),0) from JobApplication where CAST(CreatedOn as DATE) = DATEADD(day,@i-4,@TodayDate)) as JobApplied

FROM LoginDetail U
WHERE CAST(U.CreatedOn as DATE) = DATEADD(day,@i-4,@TodayDate) and UserTypeId =1

union all

SELECT DATEADD(day,@i-5,@TodayDate) as DateValue,  count(*) as Candiate,
(select isnull(count(*),0) from LoginDetail where CAST(CreatedOn as DATE) = DATEADD(day,@i -5,@TodayDate) and UserTypeId =2) as Employer,
(select isnull(count(*),0) from LoginDetail where CAST(CreatedOn as DATE) = DATEADD(day,@i-5,@TodayDate) and UserTypeId =3) as College,
(select isnull(count(*),0) from JobPosting where CAST(CreatedOn as DATE) = DATEADD(day,@i-5,@TodayDate)) as JobPost,
(select isnull(count(*),0) from JobApplication where CAST(CreatedOn as DATE) = DATEADD(day,@i-5,@TodayDate)) as JobApplied

FROM LoginDetail U
WHERE CAST(U.CreatedOn as DATE) = DATEADD(day,@i-5,@TodayDate) and UserTypeId =1

union all

SELECT DATEADD(day,@i-6,@TodayDate) as DateValue,  count(*) as Candiate,
(select isnull(count(*),0) from LoginDetail where CAST(CreatedOn as DATE) = DATEADD(day,@i -6,@TodayDate) and UserTypeId =2) as Employer,
(select isnull(count(*),0) from LoginDetail where CAST(CreatedOn as DATE) = DATEADD(day,@i-6,@TodayDate) and UserTypeId =3) as College,
(select isnull(count(*),0) from JobPosting where CAST(CreatedOn as DATE) = DATEADD(day,@i-6,@TodayDate)) as JobPost,
(select isnull(count(*),0) from JobApplication where CAST(CreatedOn as DATE) = DATEADD(day,@i-6,@TodayDate)) as JobApplied

FROM LoginDetail U
WHERE CAST(U.CreatedOn as DATE) = DATEADD(day,@i-6,@TodayDate) and UserTypeId =1

------- opencampus load chart state result for last 7 days or week --------------




-- opencampus Load dashboard  chart result ------
with cte as(
SELECT top(6) YEAR(CreatedOn) as DateYear,
         MONTH(CreatedOn) as DateMonth,
         Count(LoginId) AS TotalCandidate
		    FROM LoginDetail L where UserTypeId = 1
GROUP BY YEAR(CreatedOn), MONTH(CreatedOn)
ORDER BY YEAR(CreatedOn), MONTH(CreatedOn) desc
)

select *,
(Select isNull(Count(LoginId),0) from LoginDetail where UserTypeId = 2 and Month(CreatedOn) = DateMonth) as TotalEmployer,
(Select isNull(Count(LoginId),0) from LoginDetail where UserTypeId = 3 and Month(CreatedOn) = DateMonth) as TotalCollege
 from cte
 -- opencampus Load dashboard  chart result ------


 ---- opencampus load total state----


 SELECT  isnull(count(LoginId),0) as TotalCandiate,
(select isnull(count(LoginId),0) from LoginDetail where UserTypeId =2) as TotalEmployer,
(select isnull(count(LoginId),0) from LoginDetail where UserTypeId =3) as TotalCollege,
(select isnull(count(JobId),0) from JobPosting) as TotalJobPost,
(select isnull(count(JobApplicationId),0) from JobApplication) as TotalJobApplied
FROM LoginDetail where UserTypeId = 1


  ---- opencampus load total state----









  
	
	

