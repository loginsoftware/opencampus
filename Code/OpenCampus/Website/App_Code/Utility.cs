﻿using System;
using System.Data;
using System.Configuration;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using System.Data.SqlClient;
using System.Management;
using System.IO;
using System.Text.RegularExpressions;
using System.Collections.Generic;
using System.Text;
using System.Security.Cryptography;
using System.Web;
using System.Collections;


public partial class Utility : Page
    {
        public SqlConnection Conn = new SqlConnection();
        public SqlCommand Cmd = new SqlCommand();
        public SqlDataAdapter da = new SqlDataAdapter();
        public ArrayList arlId = new ArrayList();
        public ArrayList arlProperty = new ArrayList();
        public ArrayList arlValue = new ArrayList();
        public DataSet Ds;

        

        public bool CheckFileType(string FileName)
        {
            bool FileValid = false;
            string[] FileExtention = { ".jpg", ".jpeg", ".png", ".gif", ".xlsx" , ".docx", ".doc", ".pptx" , ".xls" };

            for (int i = 0; i < FileExtention.Length; i++)
            {
                if (System.IO.Path.GetExtension(FileName).Contains(FileExtention[i]) == true)
                {
                    FileValid = true;
                }
            }

            return FileValid;

        }

        public string CheckFileNameExist(string FolderNm, string FileNm)
        {
            FileNm = RemoveWhiteSpaceFromString(FileNm);
            string[] NewFileNm;
            int i = 1;

            NewFileNm = FileNm.Split('.');

            while (System.IO.File.Exists(Server.MapPath("~/" + FolderNm + "/" + FileNm)) == true)
            {
                i = i + 1;
                FileNm = NewFileNm[0] + i.ToString() + "." + NewFileNm[1];
            }
            return FileNm;
        }

        public string RemoveWhiteSpaceFromString(string _string)
        {
            _string = Regex.Replace(_string, @"[\s+]", "");
            return _string;
        }

        

        public bool IsNumeric(string Value, int bit)
        {
            try
            {
                if (bit == 0) int.Parse(Value);
                if (bit == 16) Int16.Parse(Value);
                if (bit == 32) Int32.Parse(Value);
                if (bit == 64) Int64.Parse(Value);
            }
            catch { return false; }

            return true;
        }

        public bool IsNumeric(string Value)
        {
            try
            {
              int.Parse(Value);                
            }
            catch { return false; }

            return true;
        }

        public bool IsDecimal(string Value)
        {
            try { decimal.Parse(Value); }
            catch { return false; }

            return true;
        }

        public bool IsDatetime(string Value)
        {
            try { DateTime.Parse(Value); }
            catch { return false; }

            return true;
        }

        public bool IsEmpty(string Value)
        {
            return String.IsNullOrEmpty(Value);
        }

        public bool IsEmail(string value)
        {
            string reg = @"^((([\w]+\.[\w]+)+)|([\w]+))@(([\w]+\.)+)([A-Za-z]{1,3})$";
            if (Regex.IsMatch(value, reg))
            {
                return true;
            }
            return false;
        }

        public bool IsPasswordValid(string password)
        {
            HashSet<char> specialCharacters = new HashSet<char>() { '!','@','#','$','%','^','&','*','(',')','-','+','_','=','<','>','?','/' };
             //(password.Any(char.IsLower) && //Lower case 
                 //password.Any(char.IsUpper) &&
            if(password.Any(char.IsDigit) && password.Any(char.IsLetter) &&
                 password.Any(specialCharacters.Contains) && password.Length >= 6)
            {
                return true;
            }

            return false;
        }

        public void ShowMessage(string msg)
        {
            ScriptManager.RegisterClientScriptBlock(Page, Page.GetType(), Guid.NewGuid().ToString(), "alert('" + msg + "');", true);
        }

        public void ShowMessage(string msg,Page page)
        {
            msg = msg.ToString().Replace("'", " ").Replace("\r\n", " ");

            if (msg.Contains("UK_"))
            {
                msg = "Data already exist !";
            }

            ScriptManager.RegisterClientScriptBlock(page, page.GetType(), Guid.NewGuid().ToString(), "alert('" + msg + "');", true);
        }

        //public void ShowMessageUC(string msg)
        //{
        //    Page.ClientScript.RegisterClientScriptBlock(Page.GetType(), Guid.NewGuid().ToString(), "alert('" + msg + "');", true);
        //}

        public void runjQueryCode(string jsCodetoRun, Page MyPage)
        {
            ScriptManager requestSM = ScriptManager.GetCurrent(MyPage);

            //if (requestSM != null && requestSM.IsInAsyncPostBack)
            //{
            ScriptManager.RegisterClientScriptBlock(MyPage, typeof(Page), Guid.NewGuid().ToString(), getjQueryCode(jsCodetoRun), true);
            // }
            //else
            //{
            //    ClientScript.RegisterClientScriptBlock(typeof(Page), Guid.NewGuid().ToString(), getjQueryCode(jsCodetoRun), true);
            //}
        }

        public string getjQueryCode(string jsCodetoRun)
        {
            StringBuilder sb = new StringBuilder();

            sb.AppendLine("$(document).ready(function() {");

            sb.AppendLine(jsCodetoRun);

            sb.AppendLine(" });");

            return sb.ToString();
        }

        public string CreateSalt(int size)
        {
            var provider = new RNGCryptoServiceProvider();
            byte[] data = new byte[size];
            provider.GetBytes(data);
            return Convert.ToBase64String(data);
        }

        public string CreateHash(string StringToHash, string salt)
        {
            //MD5, SHA1
            string passwordFormat = "SHA1";
            if (String.IsNullOrEmpty(passwordFormat))
                passwordFormat = "SHA1";

            return FormsAuthentication.HashPasswordForStoringInConfigFile(StringToHash + salt, passwordFormat);
        }

        public string CreateHash(string StringToHash)
        {
            //MD5, SHA1
            string passwordFormat = "SHA1";
            if (String.IsNullOrEmpty(passwordFormat))
                passwordFormat = "SHA1";

            return FormsAuthentication.HashPasswordForStoringInConfigFile(StringToHash , passwordFormat);
        }

        public string CreatePasswordHash(string password, string salt)
        {
            //MD5, SHA1
            string passwordFormat = "SHA1";
            if (String.IsNullOrEmpty(passwordFormat))
                passwordFormat = "SHA1";

            return FormsAuthentication.HashPasswordForStoringInConfigFile(password + salt, passwordFormat);
        }

        //public string EncryptString(string StrToEncrypt)
        //{
        //    Random rdm = new Random();
        //    Session["RandomNumberForEncrpt"] = rdm.Next(99999, 9999999);
        //    StrToEncrypt += Session["RandomNumberForEncrpt"];

        //    byte[] b = ASCIIEncoding.ASCII.GetBytes(StrToEncrypt);
        //    string encryptedstring = Convert.ToBase64String(b);
        //    return encryptedstring;
        //}

        //public string DecryptString(string EncryptedString)
        //{
        //    byte[] b = Convert.FromBase64String(EncryptedString);
        //    string decryptedstring = System.Text.ASCIIEncoding.ASCII.GetString(b);
        //    decryptedstring = decryptedstring.Substring(0, decryptedstring.Length - Session["RandomNumberForEncrpt"].ToString().Length);
        //    return decryptedstring;
        //}

        public string EncryptString(string StrToEncrypt)
        {
            Random rdm = new Random();
            string rdmNumber = rdm.Next(10000000, 99999999).ToString();
            StrToEncrypt += rdmNumber;
           
            byte[] b = ASCIIEncoding.ASCII.GetBytes(StrToEncrypt);
            string encryptedstring = Convert.ToBase64String(b);
            return encryptedstring;
        }

        public void DeleteFileFromFolder(string path)
        {
            string Deletefile = Server.MapPath(path);
            FileInfo file = new FileInfo(Deletefile);
            if (file.Exists)
            {
                file.Delete();
            }
        }

        public string DecryptString(string EncryptedString)
        {
            byte[] b = Convert.FromBase64String(EncryptedString);
            string decryptedstring = System.Text.ASCIIEncoding.ASCII.GetString(b);

            if (decryptedstring.Length > 0)
            {
                decryptedstring = decryptedstring.Substring(0, decryptedstring.Length - 8);
            }

            return decryptedstring;
        }

        public string NumbersToWords(int inputNumber)
        {
            int inputNo = inputNumber;

            if (inputNo == 0)
                return "Zero";

            int[] numbers = new int[4];
            int first = 0;
            int u, h, t;
            System.Text.StringBuilder sb = new System.Text.StringBuilder();

            if (inputNo < 0)
            {
                sb.Append("Minus ");
                inputNo = -inputNo;
            }

            string[] words0 = {"" ,"One ", "Two ", "Three ", "Four ",
            "Five " ,"Six ", "Seven ", "Eight ", "Nine "};
            string[] words1 = {"Ten ", "Eleven ", "Twelve ", "Thirteen ", "Fourteen ",
            "Fifteen ","Sixteen ","Seventeen ","Eighteen ", "Nineteen "};
            string[] words2 = {"Twenty ", "Thirty ", "Forty ", "Fifty ", "Sixty ",
            "Seventy ","Eighty ", "Ninety "};
            string[] words3 = { "Thousand ", "Lakh ", "Crore " };

            numbers[0] = inputNo % 1000; // units
            numbers[1] = inputNo / 1000;
            numbers[2] = inputNo / 100000;
            numbers[1] = numbers[1] - 100 * numbers[2]; // thousands
            numbers[3] = inputNo / 10000000; // crores
            numbers[2] = numbers[2] - 100 * numbers[3]; // lakhs

            for (int i = 3; i > 0; i--)
            {
                if (numbers[i] != 0)
                {
                    first = i;
                    break;
                }
            }
            for (int i = first; i >= 0; i--)
            {
                if (numbers[i] == 0) continue;
                u = numbers[i] % 10; // ones
                t = numbers[i] / 10;
                h = numbers[i] / 100; // hundreds
                t = t - 10 * h; // tens
                if (h > 0) sb.Append(words0[h] + "Hundred ");
                if (u > 0 || t > 0)
                {
                    if (h > 0 || i == 0) sb.Append("and ");
                    if (t == 0)
                        sb.Append(words0[u]);
                    else if (t == 1)
                        sb.Append(words1[u]);
                    else
                        sb.Append(words2[t - 2] + words0[u]);
                }
                if (i != 0) sb.Append(words3[i - 1]);
            }
            return sb.ToString().TrimEnd();
        }

        public void ClearControls(ControlCollection controlCollection)
        {
            foreach (Control cntrl in controlCollection)
            {
                ClearControls(cntrl.Controls);

                if (cntrl is TextBox)
                {
                    TextBox c = (TextBox)cntrl;
                    c.Text = string.Empty;
                    c.Enabled = true;

                    for (int i = 0; i < arlId.Count; i++)
                    {
                        if (c.ID == arlId[i].ToString())
                        {
                            if (arlProperty[i].ToString().ToLower() == "text") c.Text = arlValue[i].ToString();
                            if (arlProperty[i].ToString().ToLower() == "enabled") c.Enabled = bool.Parse(arlValue[i].ToString());
                        }
                    }
                }
                else if (cntrl is CheckBox)
                {
                    CheckBox c = (CheckBox)cntrl;
                    c.Checked = false;
                    c.Enabled = true;

                    for (int i = 0; i < arlId.Count; i++)
                    {
                        if (c.ID == arlId[i].ToString())
                        {
                            if (arlProperty[i].ToString().ToLower() == "checked") c.Checked = bool.Parse(arlValue[i].ToString());
                            if (arlProperty[i].ToString().ToLower() == "enabled") c.Enabled = bool.Parse(arlValue[i].ToString());
                            if (arlProperty[i].ToString().ToLower() == "text") c.Text = arlValue[i].ToString();
                        }
                    }
                }
                else if (cntrl is Button)
                {
                    Button c = (Button)cntrl;

                    if (c.ID.ToLower() == "btnsave") c.Text = "Save";

                    for (int i = 0; i < arlId.Count; i++)
                    {
                        if (c.ID == arlId[i].ToString())
                        {
                            if (arlProperty[i].ToString().ToLower() == "enabled") c.Enabled = bool.Parse(arlValue[i].ToString());
                            if (arlProperty[i].ToString().ToLower() == "text") c.Text = arlValue[i].ToString();
                        }
                    }
                }
                else if (cntrl is RadioButtonList)
                {
                    RadioButtonList c = (RadioButtonList)cntrl;
                    c.SelectedIndex = 0;
                    c.Enabled = true;

                    for (int i = 0; i < arlId.Count; i++)
                    {
                        if (c.ID == arlId[i].ToString())
                        {
                            if (arlProperty[i].ToString().ToLower() == "enabled") c.Enabled = bool.Parse(arlValue[i].ToString());
                            if (arlProperty[i].ToString().ToLower() == "selectedIndex") c.SelectedIndex = int.Parse(arlValue[i].ToString());
                        }
                    }
                }
                else if (cntrl is DropDownList)
                {
                    DropDownList c = (DropDownList)cntrl;
                    c.SelectedIndex = 0;
                    c.Enabled = true;

                    for (int i = 0; i < arlId.Count; i++)
                    {
                        if (c.ID == arlId[i].ToString())
                        {
                            if (arlProperty[i].ToString().ToLower() == "enabled") c.Enabled = bool.Parse(arlValue[i].ToString());
                            if (arlProperty[i].ToString().ToLower() == "selectedIndex") c.SelectedIndex = int.Parse(arlValue[i].ToString());
                        }
                    }
                }
                else if (cntrl is HiddenField)
                {
                    HiddenField c = (HiddenField)cntrl;
                    c.Value = "";

                    for (int i = 0; i < arlId.Count; i++)
                    {
                        if (c.ID == arlId[i].ToString())
                        {
                            if (arlProperty[i].ToString().ToLower() == "value") c.Value = arlValue[i].ToString();
                        }
                    }
                }
            }
        }

        public string GenerateRandomPassword()
        {
            string otpa = "";
            string PasswordLength = "6";
            // string NewPassword = "";
            string allowedChars = "";
            allowedChars = "1,2,3,4,5,6,7,8,9,0";
            allowedChars += "A,B,C,D,E,F,G,H,I,J,K,L,M,N,O,P,Q,R,S,T,U,V,W,X,Y,Z,";
            //allowedChars += "a,b,c,d,e,f,g,h,i,j,k,l,m,n,o,p,q,r,s,t,u,v,w,x,y,z,";
            //allowedChars += "~,!,@,#,$,%,^,&,*,+,?";
            //Then working with an array...
            char[] sep = { ',' };
            string[] arr = allowedChars.Split(sep);
            string IDString = "";
            string temp = "";
            //utilize the "random" class
            Random rand = new Random();
            //and lastly - loop through the generation process...
            for (int i = 0; i < Convert.ToInt32(PasswordLength); i++)
            {
                temp = arr[rand.Next(0, arr.Length)];
                IDString += temp;
                otpa = IDString;

                //For Testing purposes, I used a label on the front end to show me the generated password.
                // fullname.Text = IDString;

            }

            return otpa;
        }

        public void SetCookie(Page myPage, string CookieName, string CookieValue)
        {
            HttpCookie CN = new HttpCookie(CookieName);

            CN.Value = CookieValue;

            if (CookieValue == null)
            {
                // myPage.Response.Cookies.Remove(CookieName);
                myPage.Response.Cookies[CookieName].Expires = DateTime.Now.AddDays(-1);
            }
            else
            {
                myPage.Response.Cookies.Add(CN);
            }

        }

        public string GetCookie(Page myPage, string CookieName)
        {
            string cookieValue = "";
            try
            {
                cookieValue = myPage.Request.Cookies[CookieName].Value;
            }
            catch (Exception ex)
            {
                cookieValue = null;
            }
            return cookieValue;
        }

   

        
    }

