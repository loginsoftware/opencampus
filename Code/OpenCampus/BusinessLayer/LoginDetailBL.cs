﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data.Objects;
using DataLayer;

namespace BusinessLayer
{
    public class LoginDetailBL
    {
        public int InsertLoginDetail(string UserNameEmail, string Password, int UserTypeId, DateTime CreatedOn)
        {
            ObjectParameter LoginId = new ObjectParameter("LoginId", typeof(Int32));
            DBEntities Obj = new DBEntities();

            Obj.InsertLoginDetail(LoginId, UserNameEmail, Password, UserTypeId, CreatedOn);
            return int.Parse(LoginId.Value.ToString());
        }

        public int UpdateLoginDetail(int LoginId, string UserNameEmail, string Password, int UserTypeId, DateTime CreatedOn)
        {
            DBEntities Obj = new DBEntities();
            Obj.UpdateLoginDetail(LoginId, UserNameEmail, Password, UserTypeId, CreatedOn);
            return LoginId;
        }

        public int DeleteLoginDetail(int LoginId)
        {
            DBEntities Obj = new DBEntities();
            Obj.DeleteLoginDetail(LoginId);
            return LoginId;
        }

        public List<LoginDetail> LoadLoginDetailByPrimaryKey(int LoginId)
        {
            DBEntities Obj = new DBEntities();
            return Obj.LoadLoginDetailByPrimaryKey(LoginId).ToList();
        }

        public List<LoginDetail> LoadAllLoginDetail()
        {
            DBEntities Obj = new DBEntities();
            return Obj.LoadAllLoginDetail().ToList();
        }

        public List<VLoginDetail> LoadLoginDetailByUserNameEmail(string UserNameEmail)
        {
            DBEntities Obj = new DBEntities();
            return Obj.LoadLoginDetailByUserNameEmail(UserNameEmail).ToList();
        }

        public List<LoginDetail> LoadLoginDetailEmpByUserName(string UserNameEmail)
        {
            DBEntities Obj = new DBEntities();
            return Obj.LoadLoginDetailEmpByUserName(UserNameEmail).ToList();
        }

        public int UpdateLoginDetailPassword(int LoginId, string Password)
        {
            DBEntities Obj = new DBEntities();
            Obj.UpdateLoginDetailPassword(LoginId, Password);
            return LoginId;
        }
    }
}
