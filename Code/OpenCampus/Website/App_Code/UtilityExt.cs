﻿using System;
using System.Collections.Generic;
//using System.Linq;
using System.Web;

using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Data;



using System.Configuration;
using System.Net.Mail;
using DataLayer;
using BusinessLayer;
using System.Reflection;
using System.Net.Mail;
using System.Net;
using System.IO;
using System.Text;
using System.Text.RegularExpressions;
using System.Data.SqlClient;

/// <summary>
/// Summary description for UtilityExt
/// </summary>
public partial class Utility 
{
    string constring = ConfigurationManager.ConnectionStrings["loginsoftware_opencampusConnectionString"].ConnectionString;
    public void SendMail(string RecipientEmail, string subjectmail, string bodymail)
    {
        
        //create the mail message
        MailMessage mail = new MailMessage();
        //set the FROM address
        mail.From = new MailAddress("info@opencampus.in", subjectmail);
        //set the RECIPIENTS
        mail.To.Add(RecipientEmail);
        //enter a SUBJECT
        mail.Subject = subjectmail;
        //Enter the message BODY
        mail.Body = bodymail;
        mail.IsBodyHtml = true;
        //set the mail server (default should be smtp.1and1.com)
        SmtpClient smtp = new SmtpClient("smtp.zoho.com");
        smtp.DeliveryMethod = SmtpDeliveryMethod.Network;
        smtp.EnableSsl = true;
        smtp.Host = "smtp.zoho.com";
        smtp.Port = 587;
        smtp.UseDefaultCredentials = false;
        //Enter your full e-mail address and password
        smtp.Credentials = new NetworkCredential("info@opencampus.in", "info@07592");
        //send the message 
        smtp.Send(mail);
    }

    public int GetLoginId()
    {
        int loginId = 0;

        if (Session["LoginId"] != null as string)
        {
            loginId = int.Parse(Session["LoginId"].ToString());
        }
        else
        {
            Response.Redirect("Login.aspx");
        }

        return loginId;
    }

    public string MyUrl(string String)
    {
       return String = Regex.Replace(String, "[^0-9A-Za-z]+", "-").Replace("--","-").Replace("#","").Replace("++","").ToLower();
    }

    

    public string GetCompanyLogoUrl()
    {
        return "images/logo-opencampus.png";
    }
    
    public void FillCountry(DropDownList ControlName)
    {
        CountryMasterBL obj = new CountryMasterBL();
        ControlName.DataSource = obj.LoadAllCountryMaster();
        ControlName.DataTextField = "Name";
        ControlName.DataValueField = "ID";
        ControlName.DataBind();
    }

    public void FillState(DropDownList ControlName, int CountryId)
    {
        ControlName.Items.Clear();
        ControlName.Items.Add("--Select State--");
        StateMasterBL obj = new StateMasterBL();
        ControlName.DataSource = obj.LoadStateMasterByCountryID(CountryId);
        ControlName.DataTextField = "Name";
        ControlName.DataValueField = "ID";
        ControlName.DataBind();
    }

    public void FillCity(DropDownList ControlName, int StateId)
    {
        ControlName.Items.Clear();
        ControlName.Items.Add("--Select City--");
        CityMasterBL obj = new CityMasterBL();
        ControlName.DataSource = obj.LoadCityMasterByStateID(StateId);
        ControlName.DataTextField = "Name";
        ControlName.DataValueField = "ID";
        ControlName.DataBind();
    }

    public void FillSectorBySectorType(CheckBoxList ControlName, string SectorType)
    {
        SectorBL obj = new SectorBL();
        ControlName.DataSource = obj.LoadSectorBySectorType(SectorType);
        ControlName.DataTextField = "SectorName";
        ControlName.DataValueField = "SectorId";
        ControlName.DataBind();
    }
    public void FillSkill(ListBox ControlName)
    {
        FilterMasterBL obj = new FilterMasterBL();
        List<DataLayer.FilterMaster> F1 = obj.LoadFilterMasterByFilterType("SkillsIT","");
        List<DataLayer.FilterMaster> F2 = obj.LoadFilterMasterByFilterType("SkillsNonIT","");
        F1.AddRange(F2);
        ControlName.DataSource = F1;
        ControlName.DataTextField = "FilterName";
        ControlName.DataValueField = "FilterId";
        ControlName.DataBind();
    }
    public void FillIndustry(DropDownList ControlName)
    {
        SectorBL obj = new SectorBL();
        ControlName.DataSource = obj.LoadSectorBySectorType("Industries");
        ControlName.DataTextField = "SectorName";
        ControlName.DataValueField = "SectorId";
        ControlName.DataBind();
    }
    public void FillFunctionalArea(DropDownList ControlName)
    {
        SectorBL obj = new SectorBL();
        ControlName.DataSource = obj.LoadSectorBySectorType("Functional Areas");
        ControlName.DataTextField = "SectorName";
        ControlName.DataValueField = "SectorId";
        ControlName.DataBind();
    }

    public void FillJobPostCompany(DropDownList ControlName)
    {
        JobPostCompanyBL obj = new JobPostCompanyBL();
        ControlName.DataSource = obj.LoadAllJobPostCompany();
        ControlName.DataTextField = "CompanyName";
        ControlName.DataValueField = "JobPostCompanyId";
        ControlName.DataBind();
    }
       
    public DataTable ToDataTable<T>(List<T> items)
    {

        DataTable dataTable = new DataTable(typeof(T).Name);

        //Get all the properties

        PropertyInfo[] Props = typeof(T).GetProperties(BindingFlags.Public | BindingFlags.Instance);

        foreach (PropertyInfo prop in Props)
        {

            //Setting column names as Property names

            dataTable.Columns.Add(prop.Name);

        }

        foreach (T item in items)
        {

            var values = new object[Props.Length];

            for (int i = 0; i < Props.Length; i++)
            {

                //inserting property values to datatable rows

                values[i] = Props[i].GetValue(item, null);

            }

            dataTable.Rows.Add(values);

        }

        //put a breakpoint here and check datatable

        return dataTable;

    }

}