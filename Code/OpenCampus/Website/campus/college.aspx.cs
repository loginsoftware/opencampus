﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using BusinessLayer;
using System.Text.RegularExpressions;

public partial class campus_college : Utility // System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!Page.IsPostBack)
        {
            if (Request.QueryString["CollegeId"] != null as string)
            {
                int _collegeId = int.Parse( DecryptString(Request.QueryString["CollegeId"].ToString()));
                LoadCollegeMaster(_collegeId);
            }
            else
            {
                Response.Redirect("Login.aspx", false);
            }
            
        }
    }

    private void LoadCollegeMaster(int collegeId)
    {
        CollegeMasterBL objC = new CollegeMasterBL();
        List<DataLayer.VCollege> C = objC.LoadCollegeMasterByPrimaryKey(collegeId);
        this.Page.Title = C[0].CollegeName + " " + C[0].CityName;
        lblCollegeName.InnerText = C[0].CollegeName;
        lblCollegeAddress.Text = C[0].Address + ",<br>" + C[0].CityName + ", " + C[0].StateName + "<br>" + C[0].CountryName + "-" + C[0].Pincode;
        lblPhoneNo.Text = C[0].PhoneNumber;
        lnkWebsite.NavigateUrl = C[0].CollegeWebsite;
        lnkWebsite.Text = C[0].CollegeWebsite;
        if (C[0].UploadLogoImagePath != "") ImgCollegeLogo.ImageUrl = C[0].UploadLogoImagePath;
        lblAboutCollege.Text = C[0].AboutCollege.Replace("<br/>", "\r\n");
        lblAboutCollegeTag.Text = "About " + C[0].CollegeName;
        lblDeanDirectorName.Text = C[0].DeanDirectorPrincipal;
        lblPlacementCoName.Text = C[0].PlacementCoordinator;
        if (C[0].PlacementPhoneNo.Contains("{oc}"))
        {
            string[] phn = Regex.Split(C[0].PlacementPhoneNo, "{oc}");
            lblPlacementPhoneNo.Text = phn[0].ToString() + ", " + phn[1].ToString();
        }
        else
        {
            lblPlacementPhoneNo.Text = C[0].PlacementPhoneNo;
        }
        lblPlacementEmailId.Text = C[0].PlacementEmailId;
        lblAffiliation.Text = C[0].Affiliations;
        lblUniversity.Text = C[0].UniversityName;

        if (C[0].VideoUrl != "")
        {
            string[] vd = Regex.Split(C[0].VideoUrl, "v=");
            string url = "https://www.youtube.com/embed/" + vd[1].ToString();
            lblYoutubeIframe.Text = "<iframe width='530' height='315' src='" + url + "' frameborder='0' allowfullscreen></iframe>";
        }
        lnkCollegePresentation.NavigateUrl = C[0].UploadPresentationPath;
        lnkCollegePlacementStatistic.NavigateUrl = C[0].UploadPlacementStatisticsPath;
        lnkViewCollegeMap.Attributes.Add("onclick", "showLocationMap('"+C[0].CollegeName + ", " + C[0].CityName + "')");
    }
}