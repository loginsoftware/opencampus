﻿<%@ Page Title="" Language="C#" MasterPageFile="~/admin/Admin.master" AutoEventWireup="true" CodeFile="Dashboard.aspx.cs" Inherits="admin_Dashboard" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="cph" Runat="Server">
    	<div class="page-header">
							<h1>
								Dashboard
								<small>
									<i class="ace-icon fa fa-angle-double-right"></i>
									overview &amp; stats
								</small>
							</h1>
						</div><!-- /.page-header -->
    
						<div class="row">
							<div class="col-xs-12">
								<!-- PAGE CONTENT BEGINS -->
							
								<div class="row">
									<div class="space-6"></div>

									<div class="col-sm-12 infobox-container">
									
										<div class="infobox infobox-blue infobox-dark">
											<div class="infobox-chart">
												<span id="lblSpark_Candidate" runat="server" class="sparkline" data-values=""></span>
											</div>

											<div class="infobox-data">
												<div class="infobox-content">Candidates</div>
												<div id="lblTotalCandidate" runat="server" class="infobox-content">0</div>
											</div>
                                            <div class="stat stat-success">8%</div>
										</div>
                                        <div class="infobox infobox-red infobox-dark">
											<div class="infobox-chart">
												<span id="lblSpark_Emp" runat="server" class="sparkline" data-values=""></span>
											</div>

											<div class="infobox-data">
												<div class="infobox-content">Employers</div>
												<div id="lblTotalEmployee" runat="server" class="infobox-content">0</div>
											</div>
                                            <div class="stat stat-important">8%</div>
										</div>
										 <div class="infobox-orange infobox infobox-dark">
											<div class="infobox-chart">
												<span id="lblSpark_College" runat="server" class="sparkline" data-values=""></span>
											</div>

											<div class="infobox-data">
												<div class="infobox-content">Colleges</div>
												<div id="lblTotalCollege" runat="server" class="infobox-content">0</div>
											</div>
                                             <div class="stat stat-important">8%</div>
										</div>
                                         <div class="infobox-green infobox infobox-dark">
											<div class="infobox-chart">
												<span id="lblSpark_jobPost" runat="server" class="sparkline" data-values=""></span>
											</div>

											<div class="infobox-data">
												<div class="infobox-content">Jobs Post</div>
												<div id="lblTotalJobPost" runat="server" class="infobox-content">0</div>
											</div>
                                             <div class="stat stat-important">8%</div>
										</div>

                                         <div class="infobox-purple infobox infobox-dark">
											<div class="infobox-chart">
												<span id="lblSpark_JobApplied" runat="server" class="sparkline" data-values=""></span>
											</div>

											<div class="infobox-data">
												<div class="infobox-content">Jobs Applied</div>
												<div id="lblTotalJobApplied" runat="server" class="infobox-content">0</div>
											</div>
                                             <div class="stat stat-important">8%</div>
										</div>
									</div>
									
								</div><!-- /.row -->
                                <div class="row">
                                    <div class="col-sm-12">
                                         <canvas id="canvasDashboardStat" height="100"></canvas>
                                    </div>
                                </div>

								<div class="hr hr32 hr-dotted"></div>

								<div class="row">
									<div class="col-sm-6">
                                        <div class="widget-box">
											<div class="widget-header">
												<h4 class="widget-title lighter smaller">
													<i class="ace-icon fa fa-users blue"></i>
													Candidate's
												</h4>
                                                <div class="widget-toolbar">
														
														<a href="#" data-action="fullscreen" class="orange2">
															<i class="ace-icon fa fa-expand"></i>
														</a> | 
                                                    <a href="CandidateDetail.aspx" class="green">
															View All
														</a>
													</div>
											</div>
                                            <div style="overflow-x:scroll;">
       <asp:GridView ID="GVCandidate" runat="server" AutoGenerateColumns="false" CssClass="table  table-bordered table-hover">
           <Columns>
               <asp:BoundField DataField="CandidateName" HeaderText="Name" ItemStyle-Wrap="false" />
               <asp:BoundField DataField="MobileNo" HeaderText="Mobile" />
               <asp:BoundField DataField="UserNameEmail" HeaderText="Email"  ItemStyle-Wrap="false"/>
               <asp:TemplateField HeaderText="Date" ItemStyle-Wrap="false">
                   <ItemTemplate>
                       <asp:Label ID="lblRegisteredDate" ToolTip='<%# Eval("CreatedOn") %>' runat="server" Text='<%# Eval("CreatedOn", "{0:dd-MMM-yyyy}") %>'></asp:Label>
                   </ItemTemplate>
               </asp:TemplateField>
               <asp:BoundField DataField="Gender" HeaderText="Gender" />
               <asp:BoundField DataField="DOB" HeaderText="DOB" DataFormatString="{0:dd-MMM-yyyy}" ItemStyle-Wrap="false" />
           </Columns>

       </asp:GridView>
						</div>			

                                           </div>
									</div><!-- /.col -->

									<div class="col-sm-6">
										 <div class="widget-box">
											<div class="widget-header">
												<h4 class="widget-title lighter smaller">
													<i class="ace-icon fa fa-users blue"></i>
													Employer's
												</h4>
                                                <div class="widget-toolbar">
														
														<a href="#" data-action="fullscreen" class="orange2">
															<i class="ace-icon fa fa-expand"></i>
														</a> | 
                                                    <a href="EmployerDetail.aspx" class="green">
															View All
														</a>
													</div>
											</div>
									
                                             <div style="overflow-x:scroll;">
       <asp:GridView ID="GVEmployer" runat="server" AutoGenerateColumns="false" CssClass="table  table-bordered table-hover">
           <Columns>
               <asp:BoundField DataField="CompanyName" HeaderText="Company" ItemStyle-Wrap="false" />
               <asp:BoundField DataField="ContactPersone" HeaderText="ContactPersone" ItemStyle-Wrap="false" />
               <asp:BoundField DataField="UserNameEmail" HeaderText="Email" />
               <asp:BoundField DataField="MobileNo" HeaderText="EmpMobile" />
               <asp:TemplateField HeaderText="Date" ItemStyle-Wrap="false">
                   <ItemTemplate>
                       <asp:Label ID="lblRegisteredDate" ToolTip='<%# Eval("CreatedOn") %>' runat="server" Text='<%# Eval("CreatedOn", "{0:dd-MMM-yyyy}") %>'></asp:Label>
                   </ItemTemplate>
               </asp:TemplateField>
               <asp:BoundField DataField="Name" HeaderText="City"  ItemStyle-Wrap="false"/>
           </Columns>

       </asp:GridView>
						</div>

                                            </div>
									</div><!-- /.col -->
								</div><!-- /.row -->

								<div class="hr hr32 hr-dotted"></div>

								<div class="row">
									<div class="col-sm-12">
										<div class="widget-box">
											<div class="widget-header">
												<h4 class="widget-title lighter smaller">
													<i class="ace-icon fa fa-users blue"></i>
													College's
												</h4>
                                                <div class="widget-toolbar">
														
														<a href="#" data-action="fullscreen" class="orange2">
															<i class="ace-icon fa fa-expand"></i>
														</a> | 
                                                    <a href="CollegeDetail.aspx" class="green">
															View All
														</a>
													</div>
											</div>
									<div style="overflow-x:scroll;">
       <asp:GridView ID="GVCollege" runat="server" AutoGenerateColumns="false" CssClass="table  table-bordered table-hover">
           <Columns>
               <asp:BoundField DataField="CollegeName" HeaderText="College" ItemStyle-Wrap="false" />
               <asp:BoundField DataField="CityName" HeaderText="City" ItemStyle-Wrap="false" />
               <asp:BoundField DataField="UserNameEmail" HeaderText="Email" />
               <asp:BoundField DataField="PhoneNumber" HeaderText="Phone" />
               <asp:BoundField DataField="PlacementCoordinator" HeaderText="PlacementCoordinator" />
               <asp:BoundField DataField="PlacementPhoneNo" HeaderText="PlacementPhoneNo" />
               <asp:TemplateField HeaderText="Date" ItemStyle-Wrap="false">
                   <ItemTemplate>
                       <asp:Label ID="lblRegisteredDate" ToolTip='<%# Eval("CreatedOn") %>' runat="server" Text='<%# Eval("CreatedOn", "{0:dd-MMM-yyyy}") %>'></asp:Label>
                   </ItemTemplate>
               </asp:TemplateField>
               <asp:BoundField DataField="UniversityName" HeaderText="University" />
               <asp:BoundField DataField="CollegeWebsite" HeaderText="Website" />

           </Columns>

       </asp:GridView>
						</div>

                                            </div>
									</div><!-- /.col -->

								</div><!-- /.row -->

								<!-- PAGE CONTENT ENDS -->
							</div><!-- /.col -->
						</div><!-- /.row -->
    <asp:HiddenField ID="hfMonthName" runat="server" />
    <asp:HiddenField ID="hfCandidate" runat="server" />
    <asp:HiddenField ID="hfEmployer" runat="server" />
    <asp:HiddenField ID="hfCollege" runat="server" />

 <script src="assets/chart/Chart.bundle.js"></script>
    <script src="assets/chart/utils.js"></script>
    <script type="text/javascript">
        var _monthname = [];
        var _ttlCandidate = [];
        var _ttlEmp = [];
        var _ttlCollege = [];
        var config = {
            type: 'line',
            data: {
                labels: _monthname,
                datasets: [{
                    label: "Candidate",
                    backgroundColor: window.chartColors.red,
                    borderColor: window.chartColors.red,
                    data: _ttlCandidate,
                    fill: false,
                }, {
                    label: "Employer",
                    fill: false,
                    backgroundColor: window.chartColors.blue,
                    borderColor: window.chartColors.blue,
                    data: _ttlEmp,
                }, {
                    label: "College",
                    fill: false,
                    backgroundColor: window.chartColors.orange,
                    borderColor: window.chartColors.orange,
                    data: _ttlCollege,
                }]
            },
            options: {
                responsive: true,
                title: {
                    display: true,
                    text: 'Candidate-Employee-College Registeration Analytics'
                },
                tooltips: {
                    mode: 'index',
                    intersect: false,
                },
                hover: {
                    mode: 'nearest',
                    intersect: true
                },
                scales: {
                    xAxes: [{
                        display: true,
                        scaleLabel: {
                            display: true,
                            labelString: 'Month'
                        }
                    }],
                    yAxes: [{
                        display: true,
                        scaleLabel: {
                            display: true,
                            labelString: 'Value'
                        }
                    }]
                }
            }
        };

        window.onload = function () {
            var hfMonth = $("#cph_hfMonthName").val().split(',');
            var hfCandidate = $("#cph_hfCandidate").val().split(',');
            var hfEmployee = $("#cph_hfEmployer").val().split(',');
            var hfCollege = $("#cph_hfCollege").val().split(',');
            for (var i = 0; i < hfMonth.length; i++) {
                _monthname.push(hfMonth[i].toString());
            }
            for (var i = 0; i < hfCandidate.length; i++) {
                _ttlCandidate.push(hfCandidate[i].toString());
            }
            for (var i = 0; i < hfEmployee.length; i++) {
                _ttlEmp.push(hfEmployee[i].toString());
            }
            for (var i = 0; i < hfCollege.length; i++) {
                _ttlCollege.push(hfCollege[i].toString());
            }
            setTimeout(function () {
                var ctx = document.getElementById("canvasDashboardStat").getContext("2d");
                window.myLine = new Chart(ctx, config);
            }, 500);
            
        };
    </script>
</asp:Content>

