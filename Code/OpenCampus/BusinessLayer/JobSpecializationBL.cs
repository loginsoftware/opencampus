﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data.Objects;
using DataLayer;

namespace BusinessLayer
{
    public class JobSpecializationBL
    {
        public int InsertJobSpecialization(int JobQualificationId, string JobSpecialization)
        {
            ObjectParameter JobSpecializationId = new ObjectParameter("JobSpecializationId", typeof(Int32));
            DBEntities Obj = new DBEntities();

            Obj.InsertJobSpecialization(JobSpecializationId, JobQualificationId, JobSpecialization);
            return int.Parse(JobSpecializationId.Value.ToString());
        }

        public int UpdateJobSpecialization(int JobSpecializationId, int JobQualificationId, string JobSpecialization)
        {
            DBEntities Obj = new DBEntities();
            Obj.UpdateJobSpecialization(JobSpecializationId, JobQualificationId,  JobSpecialization);
            return JobSpecializationId;
        }

        public int DeleteJobSpecialization(int JobSpecializationId)
        {
            DBEntities Obj = new DBEntities();
            Obj.DeleteJobSpecialization(JobSpecializationId);
            return JobSpecializationId;
        }

        public List<JobSpecialization> LoadJobSpecializationByPrimaryKey(int JobSpecializationId)
        {
            DBEntities Obj = new DBEntities();
            return Obj.LoadJobSpecializationByPrimaryKey(JobSpecializationId).ToList();
        }

        public List<JobSpecialization> LoadAllJobSpecialization()
        {
            DBEntities Obj = new DBEntities();
            return Obj.LoadAllJobSpecialization().ToList();
        }
    }
}
