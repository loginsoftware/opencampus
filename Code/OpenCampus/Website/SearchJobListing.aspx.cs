﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using BusinessLayer;
using System.IO;
using System.Text.RegularExpressions;

public partial class SearchJobListing : Utility // System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!Page.IsPostBack)
        {
            string param1 = "", param2 = "", param3 = "", param4 = "", param5 = "";
            string pageName = Path.GetFileName(Request.Path);
            try
            {
                param1 = this.Page.RouteData.Values["param1"].ToString();
            }
            catch { }
            try
            {
                param2 = this.Page.RouteData.Values["param2"].ToString();
            }
            catch { }
            try
            {
                param3 = this.Page.RouteData.Values["param3"].ToString();
            }
            catch { }
            try
            {
                param4 = this.Page.RouteData.Values["param4"].ToString();
            }
            catch { }
            try
            {
                param5 = this.Page.RouteData.Values["param5"].ToString();
            }
            catch { }

            if (param1 == "jobs" && param2 == "in" && param3 != "")
            {
                if (param4 != "") param3 += " " + param4;
                LoadJobListing(param3.Trim(), "1");
            }
            else if (pageName == "browse-all-jobs")
            {
                LoadJobListing(pageName, "3"); //-- load all jobs                
            }
            else
            {
                string[] param = Regex.Split(pageName, "jobs");
                string _designationSkill = param[0].ToString().Replace("-", " ");
                LoadJobListing(_designationSkill.Trim(), "2");
            }
            
            
            
            
        }
    }

    private void LoadJobListing(string _searchText, string _loadTypeId)
    {
        int _pageIndex = 1;
        int recordCount = 0;
        string pageName = Path.GetFileName(Request.Path);
        if (Request.QueryString["page"] != null as string)
        {
            _pageIndex = int.Parse(Request.QueryString["page"].ToString());
        }
        JobPostingBL obj = new JobPostingBL();
        List<DataLayer.VJobListing> J = new List<DataLayer.VJobListing> ();
        if (_loadTypeId == "1")
        {
            J = obj.LoadJobPostingByJobLocation(out recordCount, _pageIndex, 20, _searchText);
            this.Page.Title = "Jobs in " + _searchText + " " + (recordCount - ((_pageIndex - 1) * 20)) + "  - Opencampus.in";
            this.Page.MetaDescription = "Apply to " + (recordCount - ((_pageIndex -1) * 20)) + " " + _searchText + " on Opencampus.in, India's No.1 Job Portal. Explore " + _searchText + " Openings in your desired locations Now!";
        }
        else if(_loadTypeId == "2")
        {
            J = obj.LoadJobPostingByJobDesignationAndSkill(out recordCount, _pageIndex, 20, _searchText);
            this.Page.Title = "" + _searchText + " " + (recordCount - ((_pageIndex - 1) * 20)) + " Jobs - Opencampus.in";
            this.Page.MetaDescription = "Apply to " + (recordCount - ((_pageIndex -1) * 20)) + " job for " + _searchText + " on Opencampus.in, India's No.1 Job Portal. Explore " + _searchText + " Openings in your desired locations Now!";
        }
        else if (_loadTypeId == "3")
        {
            J = obj.LoadAllJobPostingJobsByPaging(out recordCount, _pageIndex, 20);

            this.Page.Title = "Browse  " + (recordCount - ((_pageIndex - 1) * 20)) + " Jobs by Company, Location, Skills, Designation & Industry - Opencampus.in";
            this.Page.MetaDescription = "Browse  " + (recordCount - ((_pageIndex - 1) * 20)) + " of Jobs in top companies & industries on basis of your skills or designation on Opencampus.in. Register Free to Apply online";
        }
        lstJobLiting.DataSource = J;
        lstJobLiting.DataBind();
        lblJobCount.Text = recordCount.ToString();
        lblJobSearchName.Text = pageName.ToString().Replace("-", " ");
        decimal _divide = decimal.Parse(recordCount.ToString()) / decimal.Parse("20");
        if (_divide > _pageIndex)
        {
            lnkPagingNext.NavigateUrl = pageName + "?page=" + (int.Parse(_pageIndex.ToString()) + 1);
            lnkPagingNext.Visible = true;      
        }
        
        if (_pageIndex > 1)
        {
            if (_pageIndex == 2)
            {
                lnkPagingPrevious.NavigateUrl = pageName;
                lnkPagingPrevious.Visible = true;
            }
            else
            {

                lnkPagingPrevious.NavigateUrl = pageName + "?page=" + (int.Parse(_pageIndex.ToString()) - 1);
                lnkPagingPrevious.Visible = true;
            }
        }
    }

}