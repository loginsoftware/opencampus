﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data.Objects;
using DataLayer;

namespace BusinessLayer
{
    public class CandidateMasterBL
    {
        public int InsertCandidateMaster(string CandidateName, Nullable<DateTime> DOB, string Gender, string MaritalStatus, string MobileNo, string LandlineNo, string PermanentAddress, string HomeTownCity, string PinCode, string CurrentLocation, string PreferredLocation, string TotalExperience, string AnnualSalary, string Currency, string ResumeHeadline, string KeySkill, string ProfileSummary, string ResumeContent, string ProfileImagePath, string ResumeFilePath, bool IsMobileVerified, bool IsEmailVerified, int LoginId)
        {
            ObjectParameter CandidateId = new ObjectParameter("CandidateId", typeof(Int32));
            DBEntities Obj = new DBEntities();

            Obj.InsertCandidateMaster(CandidateId, CandidateName, DOB, Gender, MaritalStatus, MobileNo, LandlineNo, PermanentAddress, HomeTownCity, PinCode, CurrentLocation, PreferredLocation, TotalExperience, AnnualSalary, Currency, ResumeHeadline, KeySkill, ProfileSummary, ResumeContent, ProfileImagePath, ResumeFilePath, IsMobileVerified, IsEmailVerified, LoginId);
            return int.Parse(CandidateId.Value.ToString());
        }

        public int UpdateCandidateMaster(int CandidateId, string CandidateName, Nullable<DateTime> DOB, string Gender, string MaritalStatus, string MobileNo, string LandlineNo, string PermanentAddress, string HomeTownCity, string PinCode, string CurrentLocation, string PreferredLocation, string TotalExperience, string AnnualSalary, string Currency, string ResumeHeadline, string KeySkill, string ProfileSummary, string ResumeContent, string ProfileImagePath, string ResumeFilePath, bool IsMobileVerified, bool IsEmailVerified, int LoginId)
        {
            DBEntities Obj = new DBEntities();
            Obj.UpdateCandidateMaster(CandidateId, CandidateName, DOB, Gender, MaritalStatus, MobileNo, LandlineNo, PermanentAddress, HomeTownCity, PinCode, CurrentLocation, PreferredLocation, TotalExperience, AnnualSalary, Currency, ResumeHeadline, KeySkill, ProfileSummary, ResumeContent, ProfileImagePath, ResumeFilePath, IsMobileVerified, IsEmailVerified, LoginId);
            return CandidateId;
        }

        public int DeleteCandidateMaster(int CandidateId)
        {
            DBEntities Obj = new DBEntities();
            Obj.DeleteCandidateMaster(CandidateId);
            return CandidateId;
        }

        public List<CandidateMaster> LoadCandidateMasterByPrimaryKey(int CandidateId)
        {
            DBEntities Obj = new DBEntities();
            return Obj.LoadCandidateMasterByPrimaryKey(CandidateId).ToList();
        }

        public List<CandidateMaster> LoadAllCandidateMaster()
        {
            DBEntities Obj = new DBEntities();
            return Obj.LoadAllCandidateMaster().ToList();
        }

        public int UpdateCandidateMasterBasicProfile(int CandidateId, string CandidateName, Nullable<DateTime> DOB, string Gender, string MaritalStatus, string MobileNo, string LandlineNo, string PermanentAddress, string HomeTownCity, string PinCode, string PreferredLocation, string TotalExperience, string AnnualSalary, string Currency, string ResumeHeadline, string KeySkill)
        {
            DBEntities Obj = new DBEntities();
            Obj.UpdateCandidateMasterBasicProfile(CandidateId, CandidateName, DOB, Gender, MaritalStatus, MobileNo, LandlineNo, PermanentAddress, HomeTownCity, PinCode, PreferredLocation, TotalExperience, AnnualSalary, Currency, ResumeHeadline, KeySkill);
            return CandidateId;
        }

        public int UpdateCandidateMasterProfileImagePath(int CandidateId, string ProfileImagePath)
        {
            DBEntities Obj = new DBEntities();
            Obj.UpdateCandidateMasterProfileImagePath(CandidateId, ProfileImagePath);
            return CandidateId;
        }

        public int UpdateCandidateMasterResumePath(int CandidateId, string ResumeFilePath)
        {
            DBEntities Obj = new DBEntities();
            Obj.UpdateCandidateMasterResumePath(CandidateId, ResumeFilePath);
            return CandidateId;
        }

        public List<CandidateMaster> LoadCandidateMasterByLoginId(int LoginId)
        {
            DBEntities Obj = new DBEntities();
            return Obj.LoadCandidateMasterByLoginId(LoginId).ToList();
        }

        public List<VLoginDetail> LoadCandidateMasterTop(int CandidateId)
        {
            DBEntities Obj = new DBEntities();
            return Obj.LoadCandidateMasterTop(CandidateId).ToList();
        }

        public List<VLoginDetail> LoadCandidateMasterBetweenDates(DateTime FromDate, DateTime ToDate, string CandidateName)
        {
            DBEntities Obj = new DBEntities();
            return Obj.LoadCandidateMasterBetweenDates(FromDate, ToDate, CandidateName).ToList();
        }
    }
}
