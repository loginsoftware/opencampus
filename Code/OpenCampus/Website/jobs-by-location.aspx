﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.master" AutoEventWireup="true" CodeFile="jobs-by-location.aspx.cs" Inherits="jobs_by_location" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
   <title> Browse Top Jobs by Locations | Browse Jobs in Your State & City - OpenCampus.in</title>
<meta name="robots" content="INDEX,FOLLOW" />
<meta property="og:description" name="description" content="Find Jobs in Major States/Cities in India & Other Countries on OpenCampus.in – Find number of relevant job opportunities in Indore, Bhopal, Bangalore, Allahabad, Noida, New Delhi, Ahmedabad, Vadodara, Pune, Dubai, US, Uk, Kolkata, India." />

   <meta name="keywords" content=" Jobs in india, latest jobs in indore, top jobs in New Delhi, jobs in dubai, it jobs in Bangalore, jobs in ahmedabad, pune jobs, civil jobs in madhyapradesh"/>


   <meta name="application-name" content="opencampus.in - Your job search starts and ends with us."/>

   <meta name="copyright" content="2017 opencampus.in" />

   <meta name="content-language" content="EN" />

   <meta name="author" content="www.opencampus.in" />

   <meta name="distribution" content="GLOBAL" />


   <meta name="robots" content="ALL" />

   <meta name="pragma" content="no-cache" />

   <meta name="revisit-after" content="1 day" />

   <meta name="classification" content="Browse Top Jobs by Locations | Browse Jobs in Your State & City - OpenCampus.in " />

   <meta property="og:image" content="https://opencampus.in/images/logo-opencampus.png"/>

   <link rel="image_src" href="https://opencampus.in/images/logo-opencampus.png" />

   <link href="https://plus.google.com/+opencampus1" rel="publisher" />


	<meta property="og:type" content="website" />

	<meta property="og:url" content="https://opencampus.in" />  

	<meta property="og:title" content="Open Campus" /> 

	<meta property="og:description" content="Open Campus, is India's leading online career and recruitment resource with its cutting edge technology provides relevant profiles to employers"/> 


	<meta property="og:image" content="https://opencampus.in/images/logo-opencampus.png" /> 

	<meta property="og:site_name" content="opencampus.in"/>

<meta name="twitter:card" content="summary" />
<meta name="twitter:url" content="https://www.opencampus.in" />
<meta name="twitter:site" content="@opencampusdot" />
<meta name="twitter:title" content=" Browse Top Jobs by Locations | Browse Jobs in Your State & City - OpenCampus.in ">
<meta name="twitter:description" content=" Find Jobs in Major States/Cities in India & Other Countries on OpenCampus.in – Find number of relevant job opportunities in Indore, Bhopal, Bangalore, Allahabad, Noida, New Delhi, Ahmedabad, Vadodara, Pune, Dubai, US, Uk, Kolkata, India." />
<meta name="twitter:image" content="https://pbs.twimg.com/profile_images/872485092932833280/umLVkRsm_400x400.jpg" />

</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="cph" Runat="Server">
<UC:MenuCategory ID="ucMenuCategoryLinks" runat="server" />

			<div class="container-wrap">
				<div class="main-content container-boxed max">
					<div class="row">
                    
                    <div class="noo-main col-md-12">
                        <div class="col-md-8">
                            <p>Browse Jobs by Location</p>
                        </div>
                        <div class="col-md-4">
                            <input type="search" id="txtSearch" class="form-control" placeholder="Search by Designation"/>
                        </div>
                    </div>
                    <div class="noo-main col-md-12">
                        <div class="search-links-filter">  
                    <a href="jobs-by-location">All</a>
                    <a href="jobs-by-location-a">A</a><a href="jobs-by-location-b">B</a><a href="jobs-by-location-c">C</a><a href="jobs-by-location-d">D</a><a href="jobs-by-location-e">E</a><a href="jobs-by-location-f">F</a><a href="jobs-by-location-g">G</a><a href="jobs-by-location-h">H</a><a href="jobs-by-location-i">I</a><a href="jobs-by-location-j">J</a><a href="jobs-by-location-k">K</a><a href="jobs-by-location-l">L</a><a href="jobs-by-location-m">M</a><a href="jobs-by-location-n">N</a><a href="jobs-by-location-o">O</a><a href="jobs-by-location-p">P</a><a href="jobs-by-location-q">Q</a><a href="jobs-by-location-r">R</a><a href="jobs-by-location-s">S</a><a href="jobs-by-location-t">T</a><a href="jobs-by-location-u">U</a><a href="jobs-by-location-v">V</a><a href="jobs-by-location-w">W</a><a href="jobs-by-location-x">X</a><a href="jobs-by-location-y">Y</a><a href="jobs-by-location-z">Z</a><a href="jobs-by-location-09">0-9</a>
                </div>    
                    </div>
						<div class="noo-main col-md-12">
							<div class="member-manage">
							<div style="padding:0px 100px 0px 100px">
                             
                                <%--<asp:Repeater ID="rptrJobsByDesignation" runat="server">
                                <ItemTemplate>
                                 <li><a href="<%# MyUrl(Eval("FilterName").ToString()) %>-jobs"><%# Eval("FilterName") %></a></li>
                                </ItemTemplate>
                                </asp:Repeater>--%>	
                                <div class="member-manage-span">
                                <ul>
                                
                                <asp:Literal ID="LtrJobLocation" runat="server"></asp:Literal>
                                </ul>
                                </div>
                               
                                 			
                             					

                                    
                                    </div>
							</div>                           
						</div>  
                        
					</div> 
				</div> 
			</div>
   
            
</asp:Content>

